var statesList = [
  {
    "id": "1",
    "text": "Andaman and Nicobar Islands",
    "country_id": "101",
    "value": "Andaman and Nicobar Islands"
  },
  {
    "id": "2",
    "text": "Andhra Pradesh",
    "country_id": "101",
    "value": "Andhra Pradesh"
  },
  {
    "id": "3",
    "text": "Arunachal Pradesh",
    "country_id": "101",
    "value": "Arunachal Pradesh"
  },
  {
    "id": "4",
    "text": "Assam",
    "country_id": "101",
    "value": "Assam"
  },
  {
    "id": "5",
    "text": "Bihar",
    "country_id": "101",
    "value": "Bihar"
  },
  {
    "id": "6",
    "text": "Chandigarh",
    "country_id": "101",
    "value": "Chandigarh"
  },
  {
    "id": "7",
    "text": "Chhattisgarh",
    "country_id": "101",
    "value": "Chhattisgarh"
  },
  {
    "id": "8",
    "text": "Dadra and Nagar Haveli",
    "country_id": "101",
    "value": "Dadra and Nagar Haveli"
  },
  {
    "id": "9",
    "text": "Daman and Diu",
    "country_id": "101",
    "value": "Daman and Diu"
  },
  {
    "id": "10",
    "text": "Delhi",
    "country_id": "101",
    "value": "Delhi"
  },
  {
    "id": "11",
    "text": "Goa",
    "country_id": "101",
    "value": "Goa"
  },
  {
    "id": "12",
    "text": "Gujarat",
    "country_id": "101",
    "value": "Gujarat"
  },
  {
    "id": "13",
    "text": "Haryana",
    "country_id": "101",
    "value": "Haryana"
  },
  {
    "id": "14",
    "text": "Himachal Pradesh",
    "country_id": "101",
    "value": "Himachal Pradesh"
  },
  {
    "id": "15",
    "text": "Jammu and Kashmir",
    "country_id": "101",
    "value": "Jammu and Kashmir"
  },
  {
    "id": "16",
    "text": "Jharkhand",
    "country_id": "101",
    "value": "Jharkhand"
  },
  {
    "id": "17",
    "text": "Karnataka",
    "country_id": "101",
    "value": "Karnataka"
  },
  {
    "id": "18",
    "text": "Kenmore",
    "country_id": "101",
    "value": "Kenmore"
  },
  {
    "id": "19",
    "text": "Kerala",
    "country_id": "101",
    "value": "Kerala"
  },
  {
    "id": "20",
    "text": "Lakshadweep",
    "country_id": "101",
    "value": "Lakshadweep"
  },
  {
    "id": "21",
    "text": "Madhya Pradesh",
    "country_id": "101",
    "value": "Madhya Pradesh"
  },
  {
    "id": "22",
    "text": "Maharashtra",
    "country_id": "101",
    "value": "Maharashtra"
  },
  {
    "id": "23",
    "text": "Manipur",
    "country_id": "101",
    "value": "Manipur"
  },
  {
    "id": "24",
    "text": "Meghalaya",
    "country_id": "101",
    "value": "Meghalaya"
  },
  {
    "id": "25",
    "text": "Mizoram",
    "country_id": "101",
    "value": "Mizoram"
  },
  {
    "id": "26",
    "text": "Nagaland",
    "country_id": "101",
    "value": "Nagaland"
  },
  {
    "id": "27",
    "text": "Narora",
    "country_id": "101",
    "value": "Narora"
  },
  {
    "id": "28",
    "text": "Natwar",
    "country_id": "101",
    "value": "Natwar"
  },
  {
    "id": "29",
    "text": "Odisha",
    "country_id": "101",
    "value": "Odisha"
  },
  {
    "id": "30",
    "text": "Paschim Medinipur",
    "country_id": "101",
    "value": "Paschim Medinipur"
  },
  {
    "id": "31",
    "text": "Pondicherry",
    "country_id": "101",
    "value": "Pondicherry"
  },
  {
    "id": "32",
    "text": "Punjab",
    "country_id": "101",
    "value": "Punjab"
  },
  {
    "id": "33",
    "text": "Rajasthan",
    "country_id": "101",
    "value": "Rajasthan"
  },
  {
    "id": "34",
    "text": "Sikkim",
    "country_id": "101",
    "value": "Sikkim"
  },
  {
    "id": "35",
    "text": "Tamil Nadu",
    "country_id": "101",
    "value": "Tamil Nadu"
  },
  {
    "id": "36",
    "text": "Telangana",
    "country_id": "101",
    "value": "Telangana"
  },
  {
    "id": "37",
    "text": "Tripura",
    "country_id": "101",
    "value": "Tripura"
  },
  {
    "id": "38",
    "text": "Uttar Pradesh",
    "country_id": "101",
    "value": "Uttar Pradesh"
  },
  {
    "id": "39",
    "text": "Uttarakhand",
    "country_id": "101",
    "value": "Uttarakhand"
  },
  {
    "id": "40",
    "text": "Vaishali",
    "country_id": "101",
    "value": "Vaishali"
  },
  {
    "id": "41",
    "text": "West Bengal",
    "country_id": "101",
    "value": "West Bengal"
  },
  {
    "id": "42",
    "text": "Badakhshan",
    "country_id": "1",
    "value": "Badakhshan"
  },
  {
    "id": "43",
    "text": "Badgis",
    "country_id": "1",
    "value": "Badgis"
  },
  {
    "id": "44",
    "text": "Baglan",
    "country_id": "1",
    "value": "Baglan"
  },
  {
    "id": "45",
    "text": "Balkh",
    "country_id": "1",
    "value": "Balkh"
  },
  {
    "id": "46",
    "text": "Bamiyan",
    "country_id": "1",
    "value": "Bamiyan"
  },
  {
    "id": "47",
    "text": "Farah",
    "country_id": "1",
    "value": "Farah"
  },
  {
    "id": "48",
    "text": "Faryab",
    "country_id": "1",
    "value": "Faryab"
  },
  {
    "id": "49",
    "text": "Gawr",
    "country_id": "1",
    "value": "Gawr"
  },
  {
    "id": "50",
    "text": "Gazni",
    "country_id": "1",
    "value": "Gazni"
  },
  {
    "id": "51",
    "text": "Herat",
    "country_id": "1",
    "value": "Herat"
  },
  {
    "id": "52",
    "text": "Hilmand",
    "country_id": "1",
    "value": "Hilmand"
  },
  {
    "id": "53",
    "text": "Jawzjan",
    "country_id": "1",
    "value": "Jawzjan"
  },
  {
    "id": "54",
    "text": "Kabul",
    "country_id": "1",
    "value": "Kabul"
  },
  {
    "id": "55",
    "text": "Kapisa",
    "country_id": "1",
    "value": "Kapisa"
  },
  {
    "id": "56",
    "text": "Khawst",
    "country_id": "1",
    "value": "Khawst"
  },
  {
    "id": "57",
    "text": "Kunar",
    "country_id": "1",
    "value": "Kunar"
  },
  {
    "id": "58",
    "text": "Lagman",
    "country_id": "1",
    "value": "Lagman"
  },
  {
    "id": "59",
    "text": "Lawghar",
    "country_id": "1",
    "value": "Lawghar"
  },
  {
    "id": "60",
    "text": "Nangarhar",
    "country_id": "1",
    "value": "Nangarhar"
  },
  {
    "id": "61",
    "text": "Nimruz",
    "country_id": "1",
    "value": "Nimruz"
  },
  {
    "id": "62",
    "text": "Nuristan",
    "country_id": "1",
    "value": "Nuristan"
  },
  {
    "id": "63",
    "text": "Paktika",
    "country_id": "1",
    "value": "Paktika"
  },
  {
    "id": "64",
    "text": "Paktiya",
    "country_id": "1",
    "value": "Paktiya"
  },
  {
    "id": "65",
    "text": "Parwan",
    "country_id": "1",
    "value": "Parwan"
  },
  {
    "id": "66",
    "text": "Qandahar",
    "country_id": "1",
    "value": "Qandahar"
  },
  {
    "id": "67",
    "text": "Qunduz",
    "country_id": "1",
    "value": "Qunduz"
  },
  {
    "id": "68",
    "text": "Samangan",
    "country_id": "1",
    "value": "Samangan"
  },
  {
    "id": "69",
    "text": "Sar-e Pul",
    "country_id": "1",
    "value": "Sar-e Pul"
  },
  {
    "id": "70",
    "text": "Takhar",
    "country_id": "1",
    "value": "Takhar"
  },
  {
    "id": "71",
    "text": "Uruzgan",
    "country_id": "1",
    "value": "Uruzgan"
  },
  {
    "id": "72",
    "text": "Wardag",
    "country_id": "1",
    "value": "Wardag"
  },
  {
    "id": "73",
    "text": "Zabul",
    "country_id": "1",
    "value": "Zabul"
  },
  {
    "id": "74",
    "text": "Berat",
    "country_id": "2",
    "value": "Berat"
  },
  {
    "id": "75",
    "text": "Bulqize",
    "country_id": "2",
    "value": "Bulqize"
  },
  {
    "id": "76",
    "text": "Delvine",
    "country_id": "2",
    "value": "Delvine"
  },
  {
    "id": "77",
    "text": "Devoll",
    "country_id": "2",
    "value": "Devoll"
  },
  {
    "id": "78",
    "text": "Dibre",
    "country_id": "2",
    "value": "Dibre"
  },
  {
    "id": "79",
    "text": "Durres",
    "country_id": "2",
    "value": "Durres"
  },
  {
    "id": "80",
    "text": "Elbasan",
    "country_id": "2",
    "value": "Elbasan"
  },
  {
    "id": "81",
    "text": "Fier",
    "country_id": "2",
    "value": "Fier"
  },
  {
    "id": "82",
    "text": "Gjirokaster",
    "country_id": "2",
    "value": "Gjirokaster"
  },
  {
    "id": "83",
    "text": "Gramsh",
    "country_id": "2",
    "value": "Gramsh"
  },
  {
    "id": "84",
    "text": "Has",
    "country_id": "2",
    "value": "Has"
  },
  {
    "id": "85",
    "text": "Kavaje",
    "country_id": "2",
    "value": "Kavaje"
  },
  {
    "id": "86",
    "text": "Kolonje",
    "country_id": "2",
    "value": "Kolonje"
  },
  {
    "id": "87",
    "text": "Korce",
    "country_id": "2",
    "value": "Korce"
  },
  {
    "id": "88",
    "text": "Kruje",
    "country_id": "2",
    "value": "Kruje"
  },
  {
    "id": "89",
    "text": "Kucove",
    "country_id": "2",
    "value": "Kucove"
  },
  {
    "id": "90",
    "text": "Kukes",
    "country_id": "2",
    "value": "Kukes"
  },
  {
    "id": "91",
    "text": "Kurbin",
    "country_id": "2",
    "value": "Kurbin"
  },
  {
    "id": "92",
    "text": "Lezhe",
    "country_id": "2",
    "value": "Lezhe"
  },
  {
    "id": "93",
    "text": "Librazhd",
    "country_id": "2",
    "value": "Librazhd"
  },
  {
    "id": "94",
    "text": "Lushnje",
    "country_id": "2",
    "value": "Lushnje"
  },
  {
    "id": "95",
    "text": "Mallakaster",
    "country_id": "2",
    "value": "Mallakaster"
  },
  {
    "id": "96",
    "text": "Malsi e Madhe",
    "country_id": "2",
    "value": "Malsi e Madhe"
  },
  {
    "id": "97",
    "text": "Mat",
    "country_id": "2",
    "value": "Mat"
  },
  {
    "id": "98",
    "text": "Mirdite",
    "country_id": "2",
    "value": "Mirdite"
  },
  {
    "id": "99",
    "text": "Peqin",
    "country_id": "2",
    "value": "Peqin"
  },
  {
    "id": "100",
    "text": "Permet",
    "country_id": "2",
    "value": "Permet"
  },
  {
    "id": "101",
    "text": "Pogradec",
    "country_id": "2",
    "value": "Pogradec"
  },
  {
    "id": "102",
    "text": "Puke",
    "country_id": "2",
    "value": "Puke"
  },
  {
    "id": "103",
    "text": "Sarande",
    "country_id": "2",
    "value": "Sarande"
  },
  {
    "id": "104",
    "text": "Shkoder",
    "country_id": "2",
    "value": "Shkoder"
  },
  {
    "id": "105",
    "text": "Skrapar",
    "country_id": "2",
    "value": "Skrapar"
  },
  {
    "id": "106",
    "text": "Tepelene",
    "country_id": "2",
    "value": "Tepelene"
  },
  {
    "id": "107",
    "text": "Tirane",
    "country_id": "2",
    "value": "Tirane"
  },
  {
    "id": "108",
    "text": "Tropoje",
    "country_id": "2",
    "value": "Tropoje"
  },
  {
    "id": "109",
    "text": "Vlore",
    "country_id": "2",
    "value": "Vlore"
  },
  {
    "id": "110",
    "text": "Ayn Daflah",
    "country_id": "3",
    "value": "Ayn Daflah"
  },
  {
    "id": "111",
    "text": "Ayn Tamushanat",
    "country_id": "3",
    "value": "Ayn Tamushanat"
  },
  {
    "id": "112",
    "text": "Adrar",
    "country_id": "3",
    "value": "Adrar"
  },
  {
    "id": "113",
    "text": "Algiers",
    "country_id": "3",
    "value": "Algiers"
  },
  {
    "id": "114",
    "text": "Annabah",
    "country_id": "3",
    "value": "Annabah"
  },
  {
    "id": "115",
    "text": "Bashshar",
    "country_id": "3",
    "value": "Bashshar"
  },
  {
    "id": "116",
    "text": "Batnah",
    "country_id": "3",
    "value": "Batnah"
  },
  {
    "id": "117",
    "text": "Bijayah",
    "country_id": "3",
    "value": "Bijayah"
  },
  {
    "id": "118",
    "text": "Biskrah",
    "country_id": "3",
    "value": "Biskrah"
  },
  {
    "id": "119",
    "text": "Blidah",
    "country_id": "3",
    "value": "Blidah"
  },
  {
    "id": "120",
    "text": "Buirah",
    "country_id": "3",
    "value": "Buirah"
  },
  {
    "id": "121",
    "text": "Bumardas",
    "country_id": "3",
    "value": "Bumardas"
  },
  {
    "id": "122",
    "text": "Burj Bu Arririj",
    "country_id": "3",
    "value": "Burj Bu Arririj"
  },
  {
    "id": "123",
    "text": "Ghalizan",
    "country_id": "3",
    "value": "Ghalizan"
  },
  {
    "id": "124",
    "text": "Ghardayah",
    "country_id": "3",
    "value": "Ghardayah"
  },
  {
    "id": "125",
    "text": "Ilizi",
    "country_id": "3",
    "value": "Ilizi"
  },
  {
    "id": "126",
    "text": "Jijili",
    "country_id": "3",
    "value": "Jijili"
  },
  {
    "id": "127",
    "text": "Jilfah",
    "country_id": "3",
    "value": "Jilfah"
  },
  {
    "id": "128",
    "text": "Khanshalah",
    "country_id": "3",
    "value": "Khanshalah"
  },
  {
    "id": "129",
    "text": "Masilah",
    "country_id": "3",
    "value": "Masilah"
  },
  {
    "id": "130",
    "text": "Midyah",
    "country_id": "3",
    "value": "Midyah"
  },
  {
    "id": "131",
    "text": "Milah",
    "country_id": "3",
    "value": "Milah"
  },
  {
    "id": "132",
    "text": "Muaskar",
    "country_id": "3",
    "value": "Muaskar"
  },
  {
    "id": "133",
    "text": "Mustaghanam",
    "country_id": "3",
    "value": "Mustaghanam"
  },
  {
    "id": "134",
    "text": "Naama",
    "country_id": "3",
    "value": "Naama"
  },
  {
    "id": "135",
    "text": "Oran",
    "country_id": "3",
    "value": "Oran"
  },
  {
    "id": "136",
    "text": "Ouargla",
    "country_id": "3",
    "value": "Ouargla"
  },
  {
    "id": "137",
    "text": "Qalmah",
    "country_id": "3",
    "value": "Qalmah"
  },
  {
    "id": "138",
    "text": "Qustantinah",
    "country_id": "3",
    "value": "Qustantinah"
  },
  {
    "id": "139",
    "text": "Sakikdah",
    "country_id": "3",
    "value": "Sakikdah"
  },
  {
    "id": "140",
    "text": "Satif",
    "country_id": "3",
    "value": "Satif"
  },
  {
    "id": "141",
    "text": "Sayda",
    "country_id": "3",
    "value": "Sayda"
  },
  {
    "id": "142",
    "text": "Sidi ban-al-''Abbas",
    "country_id": "3",
    "value": "Sidi ban-al-''Abbas"
  },
  {
    "id": "143",
    "text": "Suq Ahras",
    "country_id": "3",
    "value": "Suq Ahras"
  },
  {
    "id": "144",
    "text": "Tamanghasat",
    "country_id": "3",
    "value": "Tamanghasat"
  },
  {
    "id": "145",
    "text": "Tibazah",
    "country_id": "3",
    "value": "Tibazah"
  },
  {
    "id": "146",
    "text": "Tibissah",
    "country_id": "3",
    "value": "Tibissah"
  },
  {
    "id": "147",
    "text": "Tilimsan",
    "country_id": "3",
    "value": "Tilimsan"
  },
  {
    "id": "148",
    "text": "Tinduf",
    "country_id": "3",
    "value": "Tinduf"
  },
  {
    "id": "149",
    "text": "Tisamsilt",
    "country_id": "3",
    "value": "Tisamsilt"
  },
  {
    "id": "150",
    "text": "Tiyarat",
    "country_id": "3",
    "value": "Tiyarat"
  },
  {
    "id": "151",
    "text": "Tizi Wazu",
    "country_id": "3",
    "value": "Tizi Wazu"
  },
  {
    "id": "152",
    "text": "Umm-al-Bawaghi",
    "country_id": "3",
    "value": "Umm-al-Bawaghi"
  },
  {
    "id": "153",
    "text": "Wahran",
    "country_id": "3",
    "value": "Wahran"
  },
  {
    "id": "154",
    "text": "Warqla",
    "country_id": "3",
    "value": "Warqla"
  },
  {
    "id": "155",
    "text": "Wilaya d Alger",
    "country_id": "3",
    "value": "Wilaya d Alger"
  },
  {
    "id": "156",
    "text": "Wilaya de Bejaia",
    "country_id": "3",
    "value": "Wilaya de Bejaia"
  },
  {
    "id": "157",
    "text": "Wilaya de Constantine",
    "country_id": "3",
    "value": "Wilaya de Constantine"
  },
  {
    "id": "158",
    "text": "al-Aghwat",
    "country_id": "3",
    "value": "al-Aghwat"
  },
  {
    "id": "159",
    "text": "al-Bayadh",
    "country_id": "3",
    "value": "al-Bayadh"
  },
  {
    "id": "160",
    "text": "al-Jaza''ir",
    "country_id": "3",
    "value": "al-Jaza''ir"
  },
  {
    "id": "161",
    "text": "al-Wad",
    "country_id": "3",
    "value": "al-Wad"
  },
  {
    "id": "162",
    "text": "ash-Shalif",
    "country_id": "3",
    "value": "ash-Shalif"
  },
  {
    "id": "163",
    "text": "at-Tarif",
    "country_id": "3",
    "value": "at-Tarif"
  },
  {
    "id": "164",
    "text": "Eastern",
    "country_id": "4",
    "value": "Eastern"
  },
  {
    "id": "165",
    "text": "Manu''a",
    "country_id": "4",
    "value": "Manu''a"
  },
  {
    "id": "166",
    "text": "Swains Island",
    "country_id": "4",
    "value": "Swains Island"
  },
  {
    "id": "167",
    "text": "Western",
    "country_id": "4",
    "value": "Western"
  },
  {
    "id": "168",
    "text": "Andorra la Vella",
    "country_id": "5",
    "value": "Andorra la Vella"
  },
  {
    "id": "169",
    "text": "Canillo",
    "country_id": "5",
    "value": "Canillo"
  },
  {
    "id": "170",
    "text": "Encamp",
    "country_id": "5",
    "value": "Encamp"
  },
  {
    "id": "171",
    "text": "La Massana",
    "country_id": "5",
    "value": "La Massana"
  },
  {
    "id": "172",
    "text": "Les Escaldes",
    "country_id": "5",
    "value": "Les Escaldes"
  },
  {
    "id": "173",
    "text": "Ordino",
    "country_id": "5",
    "value": "Ordino"
  },
  {
    "id": "174",
    "text": "Sant Julia de Loria",
    "country_id": "5",
    "value": "Sant Julia de Loria"
  },
  {
    "id": "175",
    "text": "Bengo",
    "country_id": "6",
    "value": "Bengo"
  },
  {
    "id": "176",
    "text": "Benguela",
    "country_id": "6",
    "value": "Benguela"
  },
  {
    "id": "177",
    "text": "Bie",
    "country_id": "6",
    "value": "Bie"
  },
  {
    "id": "178",
    "text": "Cabinda",
    "country_id": "6",
    "value": "Cabinda"
  },
  {
    "id": "179",
    "text": "Cunene",
    "country_id": "6",
    "value": "Cunene"
  },
  {
    "id": "180",
    "text": "Huambo",
    "country_id": "6",
    "value": "Huambo"
  },
  {
    "id": "181",
    "text": "Huila",
    "country_id": "6",
    "value": "Huila"
  },
  {
    "id": "182",
    "text": "Kuando-Kubango",
    "country_id": "6",
    "value": "Kuando-Kubango"
  },
  {
    "id": "183",
    "text": "Kwanza Norte",
    "country_id": "6",
    "value": "Kwanza Norte"
  },
  {
    "id": "184",
    "text": "Kwanza Sul",
    "country_id": "6",
    "value": "Kwanza Sul"
  },
  {
    "id": "185",
    "text": "Luanda",
    "country_id": "6",
    "value": "Luanda"
  },
  {
    "id": "186",
    "text": "Lunda Norte",
    "country_id": "6",
    "value": "Lunda Norte"
  },
  {
    "id": "187",
    "text": "Lunda Sul",
    "country_id": "6",
    "value": "Lunda Sul"
  },
  {
    "id": "188",
    "text": "Malanje",
    "country_id": "6",
    "value": "Malanje"
  },
  {
    "id": "189",
    "text": "Moxico",
    "country_id": "6",
    "value": "Moxico"
  },
  {
    "id": "190",
    "text": "Namibe",
    "country_id": "6",
    "value": "Namibe"
  },
  {
    "id": "191",
    "text": "Uige",
    "country_id": "6",
    "value": "Uige"
  },
  {
    "id": "192",
    "text": "Zaire",
    "country_id": "6",
    "value": "Zaire"
  },
  {
    "id": "193",
    "text": "Other Provinces",
    "country_id": "7",
    "value": "Other Provinces"
  },
  {
    "id": "194",
    "text": "Sector claimed by Argentina\/Ch",
    "country_id": "8",
    "value": "Sector claimed by Argentina\/Ch"
  },
  {
    "id": "195",
    "text": "Sector claimed by Argentina\/UK",
    "country_id": "8",
    "value": "Sector claimed by Argentina\/UK"
  },
  {
    "id": "196",
    "text": "Sector claimed by Australia",
    "country_id": "8",
    "value": "Sector claimed by Australia"
  },
  {
    "id": "197",
    "text": "Sector claimed by France",
    "country_id": "8",
    "value": "Sector claimed by France"
  },
  {
    "id": "198",
    "text": "Sector claimed by New Zealand",
    "country_id": "8",
    "value": "Sector claimed by New Zealand"
  },
  {
    "id": "199",
    "text": "Sector claimed by Norway",
    "country_id": "8",
    "value": "Sector claimed by Norway"
  },
  {
    "id": "200",
    "text": "Unclaimed Sector",
    "country_id": "8",
    "value": "Unclaimed Sector"
  },
  {
    "id": "201",
    "text": "Barbuda",
    "country_id": "9",
    "value": "Barbuda"
  },
  {
    "id": "202",
    "text": "Saint George",
    "country_id": "9",
    "value": "Saint George"
  },
  {
    "id": "203",
    "text": "Saint John",
    "country_id": "9",
    "value": "Saint John"
  },
  {
    "id": "204",
    "text": "Saint Mary",
    "country_id": "9",
    "value": "Saint Mary"
  },
  {
    "id": "205",
    "text": "Saint Paul",
    "country_id": "9",
    "value": "Saint Paul"
  },
  {
    "id": "206",
    "text": "Saint Peter",
    "country_id": "9",
    "value": "Saint Peter"
  },
  {
    "id": "207",
    "text": "Saint Philip",
    "country_id": "9",
    "value": "Saint Philip"
  },
  {
    "id": "208",
    "text": "Buenos Aires",
    "country_id": "10",
    "value": "Buenos Aires"
  },
  {
    "id": "209",
    "text": "Catamarca",
    "country_id": "10",
    "value": "Catamarca"
  },
  {
    "id": "210",
    "text": "Chaco",
    "country_id": "10",
    "value": "Chaco"
  },
  {
    "id": "211",
    "text": "Chubut",
    "country_id": "10",
    "value": "Chubut"
  },
  {
    "id": "212",
    "text": "Cordoba",
    "country_id": "10",
    "value": "Cordoba"
  },
  {
    "id": "213",
    "text": "Corrientes",
    "country_id": "10",
    "value": "Corrientes"
  },
  {
    "id": "214",
    "text": "Distrito Federal",
    "country_id": "10",
    "value": "Distrito Federal"
  },
  {
    "id": "215",
    "text": "Entre Rios",
    "country_id": "10",
    "value": "Entre Rios"
  },
  {
    "id": "216",
    "text": "Formosa",
    "country_id": "10",
    "value": "Formosa"
  },
  {
    "id": "217",
    "text": "Jujuy",
    "country_id": "10",
    "value": "Jujuy"
  },
  {
    "id": "218",
    "text": "La Pampa",
    "country_id": "10",
    "value": "La Pampa"
  },
  {
    "id": "219",
    "text": "La Rioja",
    "country_id": "10",
    "value": "La Rioja"
  },
  {
    "id": "220",
    "text": "Mendoza",
    "country_id": "10",
    "value": "Mendoza"
  },
  {
    "id": "221",
    "text": "Misiones",
    "country_id": "10",
    "value": "Misiones"
  },
  {
    "id": "222",
    "text": "Neuquen",
    "country_id": "10",
    "value": "Neuquen"
  },
  {
    "id": "223",
    "text": "Rio Negro",
    "country_id": "10",
    "value": "Rio Negro"
  },
  {
    "id": "224",
    "text": "Salta",
    "country_id": "10",
    "value": "Salta"
  },
  {
    "id": "225",
    "text": "San Juan",
    "country_id": "10",
    "value": "San Juan"
  },
  {
    "id": "226",
    "text": "San Luis",
    "country_id": "10",
    "value": "San Luis"
  },
  {
    "id": "227",
    "text": "Santa Cruz",
    "country_id": "10",
    "value": "Santa Cruz"
  },
  {
    "id": "228",
    "text": "Santa Fe",
    "country_id": "10",
    "value": "Santa Fe"
  },
  {
    "id": "229",
    "text": "Santiago del Estero",
    "country_id": "10",
    "value": "Santiago del Estero"
  },
  {
    "id": "230",
    "text": "Tierra del Fuego",
    "country_id": "10",
    "value": "Tierra del Fuego"
  },
  {
    "id": "231",
    "text": "Tucuman",
    "country_id": "10",
    "value": "Tucuman"
  },
  {
    "id": "232",
    "text": "Aragatsotn",
    "country_id": "11",
    "value": "Aragatsotn"
  },
  {
    "id": "233",
    "text": "Ararat",
    "country_id": "11",
    "value": "Ararat"
  },
  {
    "id": "234",
    "text": "Armavir",
    "country_id": "11",
    "value": "Armavir"
  },
  {
    "id": "235",
    "text": "Gegharkunik",
    "country_id": "11",
    "value": "Gegharkunik"
  },
  {
    "id": "236",
    "text": "Kotaik",
    "country_id": "11",
    "value": "Kotaik"
  },
  {
    "id": "237",
    "text": "Lori",
    "country_id": "11",
    "value": "Lori"
  },
  {
    "id": "238",
    "text": "Shirak",
    "country_id": "11",
    "value": "Shirak"
  },
  {
    "id": "239",
    "text": "Stepanakert",
    "country_id": "11",
    "value": "Stepanakert"
  },
  {
    "id": "240",
    "text": "Syunik",
    "country_id": "11",
    "value": "Syunik"
  },
  {
    "id": "241",
    "text": "Tavush",
    "country_id": "11",
    "value": "Tavush"
  },
  {
    "id": "242",
    "text": "Vayots Dzor",
    "country_id": "11",
    "value": "Vayots Dzor"
  },
  {
    "id": "243",
    "text": "Yerevan",
    "country_id": "11",
    "value": "Yerevan"
  },
  {
    "id": "244",
    "text": "Aruba",
    "country_id": "12",
    "value": "Aruba"
  },
  {
    "id": "245",
    "text": "Auckland",
    "country_id": "13",
    "value": "Auckland"
  },
  {
    "id": "246",
    "text": "Australian Capital Territory",
    "country_id": "13",
    "value": "Australian Capital Territory"
  },
  {
    "id": "247",
    "text": "Balgowlah",
    "country_id": "13",
    "value": "Balgowlah"
  },
  {
    "id": "248",
    "text": "Balmain",
    "country_id": "13",
    "value": "Balmain"
  },
  {
    "id": "249",
    "text": "Bankstown",
    "country_id": "13",
    "value": "Bankstown"
  },
  {
    "id": "250",
    "text": "Baulkham Hills",
    "country_id": "13",
    "value": "Baulkham Hills"
  },
  {
    "id": "251",
    "text": "Bonnet Bay",
    "country_id": "13",
    "value": "Bonnet Bay"
  },
  {
    "id": "252",
    "text": "Camberwell",
    "country_id": "13",
    "value": "Camberwell"
  },
  {
    "id": "253",
    "text": "Carole Park",
    "country_id": "13",
    "value": "Carole Park"
  },
  {
    "id": "254",
    "text": "Castle Hill",
    "country_id": "13",
    "value": "Castle Hill"
  },
  {
    "id": "255",
    "text": "Caulfield",
    "country_id": "13",
    "value": "Caulfield"
  },
  {
    "id": "256",
    "text": "Chatswood",
    "country_id": "13",
    "value": "Chatswood"
  },
  {
    "id": "257",
    "text": "Cheltenham",
    "country_id": "13",
    "value": "Cheltenham"
  },
  {
    "id": "258",
    "text": "Cherrybrook",
    "country_id": "13",
    "value": "Cherrybrook"
  },
  {
    "id": "259",
    "text": "Clayton",
    "country_id": "13",
    "value": "Clayton"
  },
  {
    "id": "260",
    "text": "Collingwood",
    "country_id": "13",
    "value": "Collingwood"
  },
  {
    "id": "261",
    "text": "Frenchs Forest",
    "country_id": "13",
    "value": "Frenchs Forest"
  },
  {
    "id": "262",
    "text": "Hawthorn",
    "country_id": "13",
    "value": "Hawthorn"
  },
  {
    "id": "263",
    "text": "Jannnali",
    "country_id": "13",
    "value": "Jannnali"
  },
  {
    "id": "264",
    "text": "Knoxfield",
    "country_id": "13",
    "value": "Knoxfield"
  },
  {
    "id": "265",
    "text": "Melbourne",
    "country_id": "13",
    "value": "Melbourne"
  },
  {
    "id": "266",
    "text": "New South Wales",
    "country_id": "13",
    "value": "New South Wales"
  },
  {
    "id": "267",
    "text": "Northern Territory",
    "country_id": "13",
    "value": "Northern Territory"
  },
  {
    "id": "268",
    "text": "Perth",
    "country_id": "13",
    "value": "Perth"
  },
  {
    "id": "269",
    "text": "Queensland",
    "country_id": "13",
    "value": "Queensland"
  },
  {
    "id": "270",
    "text": "South Australia",
    "country_id": "13",
    "value": "South Australia"
  },
  {
    "id": "271",
    "text": "Tasmania",
    "country_id": "13",
    "value": "Tasmania"
  },
  {
    "id": "272",
    "text": "Templestowe",
    "country_id": "13",
    "value": "Templestowe"
  },
  {
    "id": "273",
    "text": "Victoria",
    "country_id": "13",
    "value": "Victoria"
  },
  {
    "id": "274",
    "text": "Werribee south",
    "country_id": "13",
    "value": "Werribee south"
  },
  {
    "id": "275",
    "text": "Western Australia",
    "country_id": "13",
    "value": "Western Australia"
  },
  {
    "id": "276",
    "text": "Wheeler",
    "country_id": "13",
    "value": "Wheeler"
  },
  {
    "id": "277",
    "text": "Bundesland Salzburg",
    "country_id": "14",
    "value": "Bundesland Salzburg"
  },
  {
    "id": "278",
    "text": "Bundesland Steiermark",
    "country_id": "14",
    "value": "Bundesland Steiermark"
  },
  {
    "id": "279",
    "text": "Bundesland Tirol",
    "country_id": "14",
    "value": "Bundesland Tirol"
  },
  {
    "id": "280",
    "text": "Burgenland",
    "country_id": "14",
    "value": "Burgenland"
  },
  {
    "id": "281",
    "text": "Carinthia",
    "country_id": "14",
    "value": "Carinthia"
  },
  {
    "id": "282",
    "text": "Karnten",
    "country_id": "14",
    "value": "Karnten"
  },
  {
    "id": "283",
    "text": "Liezen",
    "country_id": "14",
    "value": "Liezen"
  },
  {
    "id": "284",
    "text": "Lower Austria",
    "country_id": "14",
    "value": "Lower Austria"
  },
  {
    "id": "285",
    "text": "Niederosterreich",
    "country_id": "14",
    "value": "Niederosterreich"
  },
  {
    "id": "286",
    "text": "Oberosterreich",
    "country_id": "14",
    "value": "Oberosterreich"
  },
  {
    "id": "287",
    "text": "Salzburg",
    "country_id": "14",
    "value": "Salzburg"
  },
  {
    "id": "288",
    "text": "Schleswig-Holstein",
    "country_id": "14",
    "value": "Schleswig-Holstein"
  },
  {
    "id": "289",
    "text": "Steiermark",
    "country_id": "14",
    "value": "Steiermark"
  },
  {
    "id": "290",
    "text": "Styria",
    "country_id": "14",
    "value": "Styria"
  },
  {
    "id": "291",
    "text": "Tirol",
    "country_id": "14",
    "value": "Tirol"
  },
  {
    "id": "292",
    "text": "Upper Austria",
    "country_id": "14",
    "value": "Upper Austria"
  },
  {
    "id": "293",
    "text": "Vorarlberg",
    "country_id": "14",
    "value": "Vorarlberg"
  },
  {
    "id": "294",
    "text": "Wien",
    "country_id": "14",
    "value": "Wien"
  },
  {
    "id": "295",
    "text": "Abseron",
    "country_id": "15",
    "value": "Abseron"
  },
  {
    "id": "296",
    "text": "Baki Sahari",
    "country_id": "15",
    "value": "Baki Sahari"
  },
  {
    "id": "297",
    "text": "Ganca",
    "country_id": "15",
    "value": "Ganca"
  },
  {
    "id": "298",
    "text": "Ganja",
    "country_id": "15",
    "value": "Ganja"
  },
  {
    "id": "299",
    "text": "Kalbacar",
    "country_id": "15",
    "value": "Kalbacar"
  },
  {
    "id": "300",
    "text": "Lankaran",
    "country_id": "15",
    "value": "Lankaran"
  },
  {
    "id": "301",
    "text": "Mil-Qarabax",
    "country_id": "15",
    "value": "Mil-Qarabax"
  },
  {
    "id": "302",
    "text": "Mugan-Salyan",
    "country_id": "15",
    "value": "Mugan-Salyan"
  },
  {
    "id": "303",
    "text": "Nagorni-Qarabax",
    "country_id": "15",
    "value": "Nagorni-Qarabax"
  },
  {
    "id": "304",
    "text": "Naxcivan",
    "country_id": "15",
    "value": "Naxcivan"
  },
  {
    "id": "305",
    "text": "Priaraks",
    "country_id": "15",
    "value": "Priaraks"
  },
  {
    "id": "306",
    "text": "Qazax",
    "country_id": "15",
    "value": "Qazax"
  },
  {
    "id": "307",
    "text": "Saki",
    "country_id": "15",
    "value": "Saki"
  },
  {
    "id": "308",
    "text": "Sirvan",
    "country_id": "15",
    "value": "Sirvan"
  },
  {
    "id": "309",
    "text": "Xacmaz",
    "country_id": "15",
    "value": "Xacmaz"
  },
  {
    "id": "310",
    "text": "Abaco",
    "country_id": "16",
    "value": "Abaco"
  },
  {
    "id": "311",
    "text": "Acklins Island",
    "country_id": "16",
    "value": "Acklins Island"
  },
  {
    "id": "312",
    "text": "Andros",
    "country_id": "16",
    "value": "Andros"
  },
  {
    "id": "313",
    "text": "Berry Islands",
    "country_id": "16",
    "value": "Berry Islands"
  },
  {
    "id": "314",
    "text": "Biminis",
    "country_id": "16",
    "value": "Biminis"
  },
  {
    "id": "315",
    "text": "Cat Island",
    "country_id": "16",
    "value": "Cat Island"
  },
  {
    "id": "316",
    "text": "Crooked Island",
    "country_id": "16",
    "value": "Crooked Island"
  },
  {
    "id": "317",
    "text": "Eleuthera",
    "country_id": "16",
    "value": "Eleuthera"
  },
  {
    "id": "318",
    "text": "Exuma and Cays",
    "country_id": "16",
    "value": "Exuma and Cays"
  },
  {
    "id": "319",
    "text": "Grand Bahama",
    "country_id": "16",
    "value": "Grand Bahama"
  },
  {
    "id": "320",
    "text": "Inagua Islands",
    "country_id": "16",
    "value": "Inagua Islands"
  },
  {
    "id": "321",
    "text": "Long Island",
    "country_id": "16",
    "value": "Long Island"
  },
  {
    "id": "322",
    "text": "Mayaguana",
    "country_id": "16",
    "value": "Mayaguana"
  },
  {
    "id": "323",
    "text": "New Providence",
    "country_id": "16",
    "value": "New Providence"
  },
  {
    "id": "324",
    "text": "Ragged Island",
    "country_id": "16",
    "value": "Ragged Island"
  },
  {
    "id": "325",
    "text": "Rum Cay",
    "country_id": "16",
    "value": "Rum Cay"
  },
  {
    "id": "326",
    "text": "San Salvador",
    "country_id": "16",
    "value": "San Salvador"
  },
  {
    "id": "327",
    "text": "Isa",
    "country_id": "17",
    "value": "Isa"
  },
  {
    "id": "328",
    "text": "Badiyah",
    "country_id": "17",
    "value": "Badiyah"
  },
  {
    "id": "329",
    "text": "Hidd",
    "country_id": "17",
    "value": "Hidd"
  },
  {
    "id": "330",
    "text": "Jidd Hafs",
    "country_id": "17",
    "value": "Jidd Hafs"
  },
  {
    "id": "331",
    "text": "Mahama",
    "country_id": "17",
    "value": "Mahama"
  },
  {
    "id": "332",
    "text": "Manama",
    "country_id": "17",
    "value": "Manama"
  },
  {
    "id": "333",
    "text": "Sitrah",
    "country_id": "17",
    "value": "Sitrah"
  },
  {
    "id": "334",
    "text": "al-Manamah",
    "country_id": "17",
    "value": "al-Manamah"
  },
  {
    "id": "335",
    "text": "al-Muharraq",
    "country_id": "17",
    "value": "al-Muharraq"
  },
  {
    "id": "336",
    "text": "ar-Rifa''a",
    "country_id": "17",
    "value": "ar-Rifa''a"
  },
  {
    "id": "337",
    "text": "Bagar Hat",
    "country_id": "18",
    "value": "Bagar Hat"
  },
  {
    "id": "338",
    "text": "Bandarban",
    "country_id": "18",
    "value": "Bandarban"
  },
  {
    "id": "339",
    "text": "Barguna",
    "country_id": "18",
    "value": "Barguna"
  },
  {
    "id": "340",
    "text": "Barisal",
    "country_id": "18",
    "value": "Barisal"
  },
  {
    "id": "341",
    "text": "Bhola",
    "country_id": "18",
    "value": "Bhola"
  },
  {
    "id": "342",
    "text": "Bogora",
    "country_id": "18",
    "value": "Bogora"
  },
  {
    "id": "343",
    "text": "Brahman Bariya",
    "country_id": "18",
    "value": "Brahman Bariya"
  },
  {
    "id": "344",
    "text": "Chandpur",
    "country_id": "18",
    "value": "Chandpur"
  },
  {
    "id": "345",
    "text": "Chattagam",
    "country_id": "18",
    "value": "Chattagam"
  },
  {
    "id": "346",
    "text": "Chittagong Division",
    "country_id": "18",
    "value": "Chittagong Division"
  },
  {
    "id": "347",
    "text": "Chuadanga",
    "country_id": "18",
    "value": "Chuadanga"
  },
  {
    "id": "348",
    "text": "Dhaka",
    "country_id": "18",
    "value": "Dhaka"
  },
  {
    "id": "349",
    "text": "Dinajpur",
    "country_id": "18",
    "value": "Dinajpur"
  },
  {
    "id": "350",
    "text": "Faridpur",
    "country_id": "18",
    "value": "Faridpur"
  },
  {
    "id": "351",
    "text": "Feni",
    "country_id": "18",
    "value": "Feni"
  },
  {
    "id": "352",
    "text": "Gaybanda",
    "country_id": "18",
    "value": "Gaybanda"
  },
  {
    "id": "353",
    "text": "Gazipur",
    "country_id": "18",
    "value": "Gazipur"
  },
  {
    "id": "354",
    "text": "Gopalganj",
    "country_id": "18",
    "value": "Gopalganj"
  },
  {
    "id": "355",
    "text": "Habiganj",
    "country_id": "18",
    "value": "Habiganj"
  },
  {
    "id": "356",
    "text": "Jaipur Hat",
    "country_id": "18",
    "value": "Jaipur Hat"
  },
  {
    "id": "357",
    "text": "Jamalpur",
    "country_id": "18",
    "value": "Jamalpur"
  },
  {
    "id": "358",
    "text": "Jessor",
    "country_id": "18",
    "value": "Jessor"
  },
  {
    "id": "359",
    "text": "Jhalakati",
    "country_id": "18",
    "value": "Jhalakati"
  },
  {
    "id": "360",
    "text": "Jhanaydah",
    "country_id": "18",
    "value": "Jhanaydah"
  },
  {
    "id": "361",
    "text": "Khagrachhari",
    "country_id": "18",
    "value": "Khagrachhari"
  },
  {
    "id": "362",
    "text": "Khulna",
    "country_id": "18",
    "value": "Khulna"
  },
  {
    "id": "363",
    "text": "Kishorganj",
    "country_id": "18",
    "value": "Kishorganj"
  },
  {
    "id": "364",
    "text": "Koks Bazar",
    "country_id": "18",
    "value": "Koks Bazar"
  },
  {
    "id": "365",
    "text": "Komilla",
    "country_id": "18",
    "value": "Komilla"
  },
  {
    "id": "366",
    "text": "Kurigram",
    "country_id": "18",
    "value": "Kurigram"
  },
  {
    "id": "367",
    "text": "Kushtiya",
    "country_id": "18",
    "value": "Kushtiya"
  },
  {
    "id": "368",
    "text": "Lakshmipur",
    "country_id": "18",
    "value": "Lakshmipur"
  },
  {
    "id": "369",
    "text": "Lalmanir Hat",
    "country_id": "18",
    "value": "Lalmanir Hat"
  },
  {
    "id": "370",
    "text": "Madaripur",
    "country_id": "18",
    "value": "Madaripur"
  },
  {
    "id": "371",
    "text": "Magura",
    "country_id": "18",
    "value": "Magura"
  },
  {
    "id": "372",
    "text": "Maimansingh",
    "country_id": "18",
    "value": "Maimansingh"
  },
  {
    "id": "373",
    "text": "Manikganj",
    "country_id": "18",
    "value": "Manikganj"
  },
  {
    "id": "374",
    "text": "Maulvi Bazar",
    "country_id": "18",
    "value": "Maulvi Bazar"
  },
  {
    "id": "375",
    "text": "Meherpur",
    "country_id": "18",
    "value": "Meherpur"
  },
  {
    "id": "376",
    "text": "Munshiganj",
    "country_id": "18",
    "value": "Munshiganj"
  },
  {
    "id": "377",
    "text": "Naral",
    "country_id": "18",
    "value": "Naral"
  },
  {
    "id": "378",
    "text": "Narayanganj",
    "country_id": "18",
    "value": "Narayanganj"
  },
  {
    "id": "379",
    "text": "Narsingdi",
    "country_id": "18",
    "value": "Narsingdi"
  },
  {
    "id": "380",
    "text": "Nator",
    "country_id": "18",
    "value": "Nator"
  },
  {
    "id": "381",
    "text": "Naugaon",
    "country_id": "18",
    "value": "Naugaon"
  },
  {
    "id": "382",
    "text": "Nawabganj",
    "country_id": "18",
    "value": "Nawabganj"
  },
  {
    "id": "383",
    "text": "Netrakona",
    "country_id": "18",
    "value": "Netrakona"
  },
  {
    "id": "384",
    "text": "Nilphamari",
    "country_id": "18",
    "value": "Nilphamari"
  },
  {
    "id": "385",
    "text": "Noakhali",
    "country_id": "18",
    "value": "Noakhali"
  },
  {
    "id": "386",
    "text": "Pabna",
    "country_id": "18",
    "value": "Pabna"
  },
  {
    "id": "387",
    "text": "Panchagarh",
    "country_id": "18",
    "value": "Panchagarh"
  },
  {
    "id": "388",
    "text": "Patuakhali",
    "country_id": "18",
    "value": "Patuakhali"
  },
  {
    "id": "389",
    "text": "Pirojpur",
    "country_id": "18",
    "value": "Pirojpur"
  },
  {
    "id": "390",
    "text": "Rajbari",
    "country_id": "18",
    "value": "Rajbari"
  },
  {
    "id": "391",
    "text": "Rajshahi",
    "country_id": "18",
    "value": "Rajshahi"
  },
  {
    "id": "392",
    "text": "Rangamati",
    "country_id": "18",
    "value": "Rangamati"
  },
  {
    "id": "393",
    "text": "Rangpur",
    "country_id": "18",
    "value": "Rangpur"
  },
  {
    "id": "394",
    "text": "Satkhira",
    "country_id": "18",
    "value": "Satkhira"
  },
  {
    "id": "395",
    "text": "Shariatpur",
    "country_id": "18",
    "value": "Shariatpur"
  },
  {
    "id": "396",
    "text": "Sherpur",
    "country_id": "18",
    "value": "Sherpur"
  },
  {
    "id": "397",
    "text": "Silhat",
    "country_id": "18",
    "value": "Silhat"
  },
  {
    "id": "398",
    "text": "Sirajganj",
    "country_id": "18",
    "value": "Sirajganj"
  },
  {
    "id": "399",
    "text": "Sunamganj",
    "country_id": "18",
    "value": "Sunamganj"
  },
  {
    "id": "400",
    "text": "Tangayal",
    "country_id": "18",
    "value": "Tangayal"
  },
  {
    "id": "401",
    "text": "Thakurgaon",
    "country_id": "18",
    "value": "Thakurgaon"
  },
  {
    "id": "402",
    "text": "Christ Church",
    "country_id": "19",
    "value": "Christ Church"
  },
  {
    "id": "403",
    "text": "Saint Andrew",
    "country_id": "19",
    "value": "Saint Andrew"
  },
  {
    "id": "404",
    "text": "Saint George",
    "country_id": "19",
    "value": "Saint George"
  },
  {
    "id": "405",
    "text": "Saint James",
    "country_id": "19",
    "value": "Saint James"
  },
  {
    "id": "406",
    "text": "Saint John",
    "country_id": "19",
    "value": "Saint John"
  },
  {
    "id": "407",
    "text": "Saint Joseph",
    "country_id": "19",
    "value": "Saint Joseph"
  },
  {
    "id": "408",
    "text": "Saint Lucy",
    "country_id": "19",
    "value": "Saint Lucy"
  },
  {
    "id": "409",
    "text": "Saint Michael",
    "country_id": "19",
    "value": "Saint Michael"
  },
  {
    "id": "410",
    "text": "Saint Peter",
    "country_id": "19",
    "value": "Saint Peter"
  },
  {
    "id": "411",
    "text": "Saint Philip",
    "country_id": "19",
    "value": "Saint Philip"
  },
  {
    "id": "412",
    "text": "Saint Thomas",
    "country_id": "19",
    "value": "Saint Thomas"
  },
  {
    "id": "413",
    "text": "Brest",
    "country_id": "20",
    "value": "Brest"
  },
  {
    "id": "414",
    "text": "Homjel",
    "country_id": "20",
    "value": "Homjel"
  },
  {
    "id": "415",
    "text": "Hrodna",
    "country_id": "20",
    "value": "Hrodna"
  },
  {
    "id": "416",
    "text": "Mahiljow",
    "country_id": "20",
    "value": "Mahiljow"
  },
  {
    "id": "417",
    "text": "Mahilyowskaya Voblasts",
    "country_id": "20",
    "value": "Mahilyowskaya Voblasts"
  },
  {
    "id": "418",
    "text": "Minsk",
    "country_id": "20",
    "value": "Minsk"
  },
  {
    "id": "419",
    "text": "Minskaja Voblasts",
    "country_id": "20",
    "value": "Minskaja Voblasts"
  },
  {
    "id": "420",
    "text": "Petrik",
    "country_id": "20",
    "value": "Petrik"
  },
  {
    "id": "421",
    "text": "Vicebsk",
    "country_id": "20",
    "value": "Vicebsk"
  },
  {
    "id": "422",
    "text": "Antwerpen",
    "country_id": "21",
    "value": "Antwerpen"
  },
  {
    "id": "423",
    "text": "Berchem",
    "country_id": "21",
    "value": "Berchem"
  },
  {
    "id": "424",
    "text": "Brabant",
    "country_id": "21",
    "value": "Brabant"
  },
  {
    "id": "425",
    "text": "Brabant Wallon",
    "country_id": "21",
    "value": "Brabant Wallon"
  },
  {
    "id": "426",
    "text": "Brussel",
    "country_id": "21",
    "value": "Brussel"
  },
  {
    "id": "427",
    "text": "East Flanders",
    "country_id": "21",
    "value": "East Flanders"
  },
  {
    "id": "428",
    "text": "Hainaut",
    "country_id": "21",
    "value": "Hainaut"
  },
  {
    "id": "429",
    "text": "Liege",
    "country_id": "21",
    "value": "Liege"
  },
  {
    "id": "430",
    "text": "Limburg",
    "country_id": "21",
    "value": "Limburg"
  },
  {
    "id": "431",
    "text": "Luxembourg",
    "country_id": "21",
    "value": "Luxembourg"
  },
  {
    "id": "432",
    "text": "Namur",
    "country_id": "21",
    "value": "Namur"
  },
  {
    "id": "433",
    "text": "Ontario",
    "country_id": "21",
    "value": "Ontario"
  },
  {
    "id": "434",
    "text": "Oost-Vlaanderen",
    "country_id": "21",
    "value": "Oost-Vlaanderen"
  },
  {
    "id": "435",
    "text": "Provincie Brabant",
    "country_id": "21",
    "value": "Provincie Brabant"
  },
  {
    "id": "436",
    "text": "Vlaams-Brabant",
    "country_id": "21",
    "value": "Vlaams-Brabant"
  },
  {
    "id": "437",
    "text": "Wallonne",
    "country_id": "21",
    "value": "Wallonne"
  },
  {
    "id": "438",
    "text": "West-Vlaanderen",
    "country_id": "21",
    "value": "West-Vlaanderen"
  },
  {
    "id": "439",
    "text": "Belize",
    "country_id": "22",
    "value": "Belize"
  },
  {
    "id": "440",
    "text": "Cayo",
    "country_id": "22",
    "value": "Cayo"
  },
  {
    "id": "441",
    "text": "Corozal",
    "country_id": "22",
    "value": "Corozal"
  },
  {
    "id": "442",
    "text": "Orange Walk",
    "country_id": "22",
    "value": "Orange Walk"
  },
  {
    "id": "443",
    "text": "Stann Creek",
    "country_id": "22",
    "value": "Stann Creek"
  },
  {
    "id": "444",
    "text": "Toledo",
    "country_id": "22",
    "value": "Toledo"
  },
  {
    "id": "445",
    "text": "Alibori",
    "country_id": "23",
    "value": "Alibori"
  },
  {
    "id": "446",
    "text": "Atacora",
    "country_id": "23",
    "value": "Atacora"
  },
  {
    "id": "447",
    "text": "Atlantique",
    "country_id": "23",
    "value": "Atlantique"
  },
  {
    "id": "448",
    "text": "Borgou",
    "country_id": "23",
    "value": "Borgou"
  },
  {
    "id": "449",
    "text": "Collines",
    "country_id": "23",
    "value": "Collines"
  },
  {
    "id": "450",
    "text": "Couffo",
    "country_id": "23",
    "value": "Couffo"
  },
  {
    "id": "451",
    "text": "Donga",
    "country_id": "23",
    "value": "Donga"
  },
  {
    "id": "452",
    "text": "Littoral",
    "country_id": "23",
    "value": "Littoral"
  },
  {
    "id": "453",
    "text": "Mono",
    "country_id": "23",
    "value": "Mono"
  },
  {
    "id": "454",
    "text": "Oueme",
    "country_id": "23",
    "value": "Oueme"
  },
  {
    "id": "455",
    "text": "Plateau",
    "country_id": "23",
    "value": "Plateau"
  },
  {
    "id": "456",
    "text": "Zou",
    "country_id": "23",
    "value": "Zou"
  },
  {
    "id": "457",
    "text": "Hamilton",
    "country_id": "24",
    "value": "Hamilton"
  },
  {
    "id": "458",
    "text": "Saint George",
    "country_id": "24",
    "value": "Saint George"
  },
  {
    "id": "459",
    "text": "Bumthang",
    "country_id": "25",
    "value": "Bumthang"
  },
  {
    "id": "460",
    "text": "Chhukha",
    "country_id": "25",
    "value": "Chhukha"
  },
  {
    "id": "461",
    "text": "Chirang",
    "country_id": "25",
    "value": "Chirang"
  },
  {
    "id": "462",
    "text": "Daga",
    "country_id": "25",
    "value": "Daga"
  },
  {
    "id": "463",
    "text": "Geylegphug",
    "country_id": "25",
    "value": "Geylegphug"
  },
  {
    "id": "464",
    "text": "Ha",
    "country_id": "25",
    "value": "Ha"
  },
  {
    "id": "465",
    "text": "Lhuntshi",
    "country_id": "25",
    "value": "Lhuntshi"
  },
  {
    "id": "466",
    "text": "Mongar",
    "country_id": "25",
    "value": "Mongar"
  },
  {
    "id": "467",
    "text": "Pemagatsel",
    "country_id": "25",
    "value": "Pemagatsel"
  },
  {
    "id": "468",
    "text": "Punakha",
    "country_id": "25",
    "value": "Punakha"
  },
  {
    "id": "469",
    "text": "Rinpung",
    "country_id": "25",
    "value": "Rinpung"
  },
  {
    "id": "470",
    "text": "Samchi",
    "country_id": "25",
    "value": "Samchi"
  },
  {
    "id": "471",
    "text": "Samdrup Jongkhar",
    "country_id": "25",
    "value": "Samdrup Jongkhar"
  },
  {
    "id": "472",
    "text": "Shemgang",
    "country_id": "25",
    "value": "Shemgang"
  },
  {
    "id": "473",
    "text": "Tashigang",
    "country_id": "25",
    "value": "Tashigang"
  },
  {
    "id": "474",
    "text": "Timphu",
    "country_id": "25",
    "value": "Timphu"
  },
  {
    "id": "475",
    "text": "Tongsa",
    "country_id": "25",
    "value": "Tongsa"
  },
  {
    "id": "476",
    "text": "Wangdiphodrang",
    "country_id": "25",
    "value": "Wangdiphodrang"
  },
  {
    "id": "477",
    "text": "Beni",
    "country_id": "26",
    "value": "Beni"
  },
  {
    "id": "478",
    "text": "Chuquisaca",
    "country_id": "26",
    "value": "Chuquisaca"
  },
  {
    "id": "479",
    "text": "Cochabamba",
    "country_id": "26",
    "value": "Cochabamba"
  },
  {
    "id": "480",
    "text": "La Paz",
    "country_id": "26",
    "value": "La Paz"
  },
  {
    "id": "481",
    "text": "Oruro",
    "country_id": "26",
    "value": "Oruro"
  },
  {
    "id": "482",
    "text": "Pando",
    "country_id": "26",
    "value": "Pando"
  },
  {
    "id": "483",
    "text": "Potosi",
    "country_id": "26",
    "value": "Potosi"
  },
  {
    "id": "484",
    "text": "Santa Cruz",
    "country_id": "26",
    "value": "Santa Cruz"
  },
  {
    "id": "485",
    "text": "Tarija",
    "country_id": "26",
    "value": "Tarija"
  },
  {
    "id": "486",
    "text": "Federacija Bosna i Hercegovina",
    "country_id": "27",
    "value": "Federacija Bosna i Hercegovina"
  },
  {
    "id": "487",
    "text": "Republika Srpska",
    "country_id": "27",
    "value": "Republika Srpska"
  },
  {
    "id": "488",
    "text": "Central Bobonong",
    "country_id": "28",
    "value": "Central Bobonong"
  },
  {
    "id": "489",
    "text": "Central Boteti",
    "country_id": "28",
    "value": "Central Boteti"
  },
  {
    "id": "490",
    "text": "Central Mahalapye",
    "country_id": "28",
    "value": "Central Mahalapye"
  },
  {
    "id": "491",
    "text": "Central Serowe-Palapye",
    "country_id": "28",
    "value": "Central Serowe-Palapye"
  },
  {
    "id": "492",
    "text": "Central Tutume",
    "country_id": "28",
    "value": "Central Tutume"
  },
  {
    "id": "493",
    "text": "Chobe",
    "country_id": "28",
    "value": "Chobe"
  },
  {
    "id": "494",
    "text": "Francistown",
    "country_id": "28",
    "value": "Francistown"
  },
  {
    "id": "495",
    "text": "Gaborone",
    "country_id": "28",
    "value": "Gaborone"
  },
  {
    "id": "496",
    "text": "Ghanzi",
    "country_id": "28",
    "value": "Ghanzi"
  },
  {
    "id": "497",
    "text": "Jwaneng",
    "country_id": "28",
    "value": "Jwaneng"
  },
  {
    "id": "498",
    "text": "Kgalagadi North",
    "country_id": "28",
    "value": "Kgalagadi North"
  },
  {
    "id": "499",
    "text": "Kgalagadi South",
    "country_id": "28",
    "value": "Kgalagadi South"
  },
  {
    "id": "500",
    "text": "Kgatleng",
    "country_id": "28",
    "value": "Kgatleng"
  },
  {
    "id": "501",
    "text": "Kweneng",
    "country_id": "28",
    "value": "Kweneng"
  },
  {
    "id": "502",
    "text": "Lobatse",
    "country_id": "28",
    "value": "Lobatse"
  },
  {
    "id": "503",
    "text": "Ngamiland",
    "country_id": "28",
    "value": "Ngamiland"
  },
  {
    "id": "504",
    "text": "Ngwaketse",
    "country_id": "28",
    "value": "Ngwaketse"
  },
  {
    "id": "505",
    "text": "North East",
    "country_id": "28",
    "value": "North East"
  },
  {
    "id": "506",
    "text": "Okavango",
    "country_id": "28",
    "value": "Okavango"
  },
  {
    "id": "507",
    "text": "Orapa",
    "country_id": "28",
    "value": "Orapa"
  },
  {
    "id": "508",
    "text": "Selibe Phikwe",
    "country_id": "28",
    "value": "Selibe Phikwe"
  },
  {
    "id": "509",
    "text": "South East",
    "country_id": "28",
    "value": "South East"
  },
  {
    "id": "510",
    "text": "Sowa",
    "country_id": "28",
    "value": "Sowa"
  },
  {
    "id": "511",
    "text": "Bouvet Island",
    "country_id": "29",
    "value": "Bouvet Island"
  },
  {
    "id": "512",
    "text": "Acre",
    "country_id": "30",
    "value": "Acre"
  },
  {
    "id": "513",
    "text": "Alagoas",
    "country_id": "30",
    "value": "Alagoas"
  },
  {
    "id": "514",
    "text": "Amapa",
    "country_id": "30",
    "value": "Amapa"
  },
  {
    "id": "515",
    "text": "Amazonas",
    "country_id": "30",
    "value": "Amazonas"
  },
  {
    "id": "516",
    "text": "Bahia",
    "country_id": "30",
    "value": "Bahia"
  },
  {
    "id": "517",
    "text": "Ceara",
    "country_id": "30",
    "value": "Ceara"
  },
  {
    "id": "518",
    "text": "Distrito Federal",
    "country_id": "30",
    "value": "Distrito Federal"
  },
  {
    "id": "519",
    "text": "Espirito Santo",
    "country_id": "30",
    "value": "Espirito Santo"
  },
  {
    "id": "520",
    "text": "Estado de Sao Paulo",
    "country_id": "30",
    "value": "Estado de Sao Paulo"
  },
  {
    "id": "521",
    "text": "Goias",
    "country_id": "30",
    "value": "Goias"
  },
  {
    "id": "522",
    "text": "Maranhao",
    "country_id": "30",
    "value": "Maranhao"
  },
  {
    "id": "523",
    "text": "Mato Grosso",
    "country_id": "30",
    "value": "Mato Grosso"
  },
  {
    "id": "524",
    "text": "Mato Grosso do Sul",
    "country_id": "30",
    "value": "Mato Grosso do Sul"
  },
  {
    "id": "525",
    "text": "Minas Gerais",
    "country_id": "30",
    "value": "Minas Gerais"
  },
  {
    "id": "526",
    "text": "Para",
    "country_id": "30",
    "value": "Para"
  },
  {
    "id": "527",
    "text": "Paraiba",
    "country_id": "30",
    "value": "Paraiba"
  },
  {
    "id": "528",
    "text": "Parana",
    "country_id": "30",
    "value": "Parana"
  },
  {
    "id": "529",
    "text": "Pernambuco",
    "country_id": "30",
    "value": "Pernambuco"
  },
  {
    "id": "530",
    "text": "Piaui",
    "country_id": "30",
    "value": "Piaui"
  },
  {
    "id": "531",
    "text": "Rio Grande do Norte",
    "country_id": "30",
    "value": "Rio Grande do Norte"
  },
  {
    "id": "532",
    "text": "Rio Grande do Sul",
    "country_id": "30",
    "value": "Rio Grande do Sul"
  },
  {
    "id": "533",
    "text": "Rio de Janeiro",
    "country_id": "30",
    "value": "Rio de Janeiro"
  },
  {
    "id": "534",
    "text": "Rondonia",
    "country_id": "30",
    "value": "Rondonia"
  },
  {
    "id": "535",
    "text": "Roraima",
    "country_id": "30",
    "value": "Roraima"
  },
  {
    "id": "536",
    "text": "Santa Catarina",
    "country_id": "30",
    "value": "Santa Catarina"
  },
  {
    "id": "537",
    "text": "Sao Paulo",
    "country_id": "30",
    "value": "Sao Paulo"
  },
  {
    "id": "538",
    "text": "Sergipe",
    "country_id": "30",
    "value": "Sergipe"
  },
  {
    "id": "539",
    "text": "Tocantins",
    "country_id": "30",
    "value": "Tocantins"
  },
  {
    "id": "540",
    "text": "British Indian Ocean Territory",
    "country_id": "31",
    "value": "British Indian Ocean Territory"
  },
  {
    "id": "541",
    "text": "Belait",
    "country_id": "32",
    "value": "Belait"
  },
  {
    "id": "542",
    "text": "Brunei-Muara",
    "country_id": "32",
    "value": "Brunei-Muara"
  },
  {
    "id": "543",
    "text": "Temburong",
    "country_id": "32",
    "value": "Temburong"
  },
  {
    "id": "544",
    "text": "Tutong",
    "country_id": "32",
    "value": "Tutong"
  },
  {
    "id": "545",
    "text": "Blagoevgrad",
    "country_id": "33",
    "value": "Blagoevgrad"
  },
  {
    "id": "546",
    "text": "Burgas",
    "country_id": "33",
    "value": "Burgas"
  },
  {
    "id": "547",
    "text": "Dobrich",
    "country_id": "33",
    "value": "Dobrich"
  },
  {
    "id": "548",
    "text": "Gabrovo",
    "country_id": "33",
    "value": "Gabrovo"
  },
  {
    "id": "549",
    "text": "Haskovo",
    "country_id": "33",
    "value": "Haskovo"
  },
  {
    "id": "550",
    "text": "Jambol",
    "country_id": "33",
    "value": "Jambol"
  },
  {
    "id": "551",
    "text": "Kardzhali",
    "country_id": "33",
    "value": "Kardzhali"
  },
  {
    "id": "552",
    "text": "Kjustendil",
    "country_id": "33",
    "value": "Kjustendil"
  },
  {
    "id": "553",
    "text": "Lovech",
    "country_id": "33",
    "value": "Lovech"
  },
  {
    "id": "554",
    "text": "Montana",
    "country_id": "33",
    "value": "Montana"
  },
  {
    "id": "555",
    "text": "Oblast Sofiya-Grad",
    "country_id": "33",
    "value": "Oblast Sofiya-Grad"
  },
  {
    "id": "556",
    "text": "Pazardzhik",
    "country_id": "33",
    "value": "Pazardzhik"
  },
  {
    "id": "557",
    "text": "Pernik",
    "country_id": "33",
    "value": "Pernik"
  },
  {
    "id": "558",
    "text": "Pleven",
    "country_id": "33",
    "value": "Pleven"
  },
  {
    "id": "559",
    "text": "Plovdiv",
    "country_id": "33",
    "value": "Plovdiv"
  },
  {
    "id": "560",
    "text": "Razgrad",
    "country_id": "33",
    "value": "Razgrad"
  },
  {
    "id": "561",
    "text": "Ruse",
    "country_id": "33",
    "value": "Ruse"
  },
  {
    "id": "562",
    "text": "Shumen",
    "country_id": "33",
    "value": "Shumen"
  },
  {
    "id": "563",
    "text": "Silistra",
    "country_id": "33",
    "value": "Silistra"
  },
  {
    "id": "564",
    "text": "Sliven",
    "country_id": "33",
    "value": "Sliven"
  },
  {
    "id": "565",
    "text": "Smoljan",
    "country_id": "33",
    "value": "Smoljan"
  },
  {
    "id": "566",
    "text": "Sofija grad",
    "country_id": "33",
    "value": "Sofija grad"
  },
  {
    "id": "567",
    "text": "Sofijska oblast",
    "country_id": "33",
    "value": "Sofijska oblast"
  },
  {
    "id": "568",
    "text": "Stara Zagora",
    "country_id": "33",
    "value": "Stara Zagora"
  },
  {
    "id": "569",
    "text": "Targovishte",
    "country_id": "33",
    "value": "Targovishte"
  },
  {
    "id": "570",
    "text": "Varna",
    "country_id": "33",
    "value": "Varna"
  },
  {
    "id": "571",
    "text": "Veliko Tarnovo",
    "country_id": "33",
    "value": "Veliko Tarnovo"
  },
  {
    "id": "572",
    "text": "Vidin",
    "country_id": "33",
    "value": "Vidin"
  },
  {
    "id": "573",
    "text": "Vraca",
    "country_id": "33",
    "value": "Vraca"
  },
  {
    "id": "574",
    "text": "Yablaniza",
    "country_id": "33",
    "value": "Yablaniza"
  },
  {
    "id": "575",
    "text": "Bale",
    "country_id": "34",
    "value": "Bale"
  },
  {
    "id": "576",
    "text": "Bam",
    "country_id": "34",
    "value": "Bam"
  },
  {
    "id": "577",
    "text": "Bazega",
    "country_id": "34",
    "value": "Bazega"
  },
  {
    "id": "578",
    "text": "Bougouriba",
    "country_id": "34",
    "value": "Bougouriba"
  },
  {
    "id": "579",
    "text": "Boulgou",
    "country_id": "34",
    "value": "Boulgou"
  },
  {
    "id": "580",
    "text": "Boulkiemde",
    "country_id": "34",
    "value": "Boulkiemde"
  },
  {
    "id": "581",
    "text": "Comoe",
    "country_id": "34",
    "value": "Comoe"
  },
  {
    "id": "582",
    "text": "Ganzourgou",
    "country_id": "34",
    "value": "Ganzourgou"
  },
  {
    "id": "583",
    "text": "Gnagna",
    "country_id": "34",
    "value": "Gnagna"
  },
  {
    "id": "584",
    "text": "Gourma",
    "country_id": "34",
    "value": "Gourma"
  },
  {
    "id": "585",
    "text": "Houet",
    "country_id": "34",
    "value": "Houet"
  },
  {
    "id": "586",
    "text": "Ioba",
    "country_id": "34",
    "value": "Ioba"
  },
  {
    "id": "587",
    "text": "Kadiogo",
    "country_id": "34",
    "value": "Kadiogo"
  },
  {
    "id": "588",
    "text": "Kenedougou",
    "country_id": "34",
    "value": "Kenedougou"
  },
  {
    "id": "589",
    "text": "Komandjari",
    "country_id": "34",
    "value": "Komandjari"
  },
  {
    "id": "590",
    "text": "Kompienga",
    "country_id": "34",
    "value": "Kompienga"
  },
  {
    "id": "591",
    "text": "Kossi",
    "country_id": "34",
    "value": "Kossi"
  },
  {
    "id": "592",
    "text": "Kouritenga",
    "country_id": "34",
    "value": "Kouritenga"
  },
  {
    "id": "593",
    "text": "Kourweogo",
    "country_id": "34",
    "value": "Kourweogo"
  },
  {
    "id": "594",
    "text": "Leraba",
    "country_id": "34",
    "value": "Leraba"
  },
  {
    "id": "595",
    "text": "Mouhoun",
    "country_id": "34",
    "value": "Mouhoun"
  },
  {
    "id": "596",
    "text": "Nahouri",
    "country_id": "34",
    "value": "Nahouri"
  },
  {
    "id": "597",
    "text": "textntenga",
    "country_id": "34",
    "value": "textntenga"
  },
  {
    "id": "598",
    "text": "Noumbiel",
    "country_id": "34",
    "value": "Noumbiel"
  },
  {
    "id": "599",
    "text": "Oubritenga",
    "country_id": "34",
    "value": "Oubritenga"
  },
  {
    "id": "600",
    "text": "Oudalan",
    "country_id": "34",
    "value": "Oudalan"
  },
  {
    "id": "601",
    "text": "Passore",
    "country_id": "34",
    "value": "Passore"
  },
  {
    "id": "602",
    "text": "Poni",
    "country_id": "34",
    "value": "Poni"
  },
  {
    "id": "603",
    "text": "Sanguie",
    "country_id": "34",
    "value": "Sanguie"
  },
  {
    "id": "604",
    "text": "Sanmatenga",
    "country_id": "34",
    "value": "Sanmatenga"
  },
  {
    "id": "605",
    "text": "Seno",
    "country_id": "34",
    "value": "Seno"
  },
  {
    "id": "606",
    "text": "Sissili",
    "country_id": "34",
    "value": "Sissili"
  },
  {
    "id": "607",
    "text": "Soum",
    "country_id": "34",
    "value": "Soum"
  },
  {
    "id": "608",
    "text": "Sourou",
    "country_id": "34",
    "value": "Sourou"
  },
  {
    "id": "609",
    "text": "Tapoa",
    "country_id": "34",
    "value": "Tapoa"
  },
  {
    "id": "610",
    "text": "Tuy",
    "country_id": "34",
    "value": "Tuy"
  },
  {
    "id": "611",
    "text": "Yatenga",
    "country_id": "34",
    "value": "Yatenga"
  },
  {
    "id": "612",
    "text": "Zondoma",
    "country_id": "34",
    "value": "Zondoma"
  },
  {
    "id": "613",
    "text": "Zoundweogo",
    "country_id": "34",
    "value": "Zoundweogo"
  },
  {
    "id": "614",
    "text": "Bubanza",
    "country_id": "35",
    "value": "Bubanza"
  },
  {
    "id": "615",
    "text": "Bujumbura",
    "country_id": "35",
    "value": "Bujumbura"
  },
  {
    "id": "616",
    "text": "Bururi",
    "country_id": "35",
    "value": "Bururi"
  },
  {
    "id": "617",
    "text": "Cankuzo",
    "country_id": "35",
    "value": "Cankuzo"
  },
  {
    "id": "618",
    "text": "Cibitoke",
    "country_id": "35",
    "value": "Cibitoke"
  },
  {
    "id": "619",
    "text": "Gitega",
    "country_id": "35",
    "value": "Gitega"
  },
  {
    "id": "620",
    "text": "Karuzi",
    "country_id": "35",
    "value": "Karuzi"
  },
  {
    "id": "621",
    "text": "Kayanza",
    "country_id": "35",
    "value": "Kayanza"
  },
  {
    "id": "622",
    "text": "Kirundo",
    "country_id": "35",
    "value": "Kirundo"
  },
  {
    "id": "623",
    "text": "Makamba",
    "country_id": "35",
    "value": "Makamba"
  },
  {
    "id": "624",
    "text": "Muramvya",
    "country_id": "35",
    "value": "Muramvya"
  },
  {
    "id": "625",
    "text": "Muyinga",
    "country_id": "35",
    "value": "Muyinga"
  },
  {
    "id": "626",
    "text": "Ngozi",
    "country_id": "35",
    "value": "Ngozi"
  },
  {
    "id": "627",
    "text": "Rutana",
    "country_id": "35",
    "value": "Rutana"
  },
  {
    "id": "628",
    "text": "Ruyigi",
    "country_id": "35",
    "value": "Ruyigi"
  },
  {
    "id": "629",
    "text": "Banteay Mean Chey",
    "country_id": "36",
    "value": "Banteay Mean Chey"
  },
  {
    "id": "630",
    "text": "Bat Dambang",
    "country_id": "36",
    "value": "Bat Dambang"
  },
  {
    "id": "631",
    "text": "Kampong Cham",
    "country_id": "36",
    "value": "Kampong Cham"
  },
  {
    "id": "632",
    "text": "Kampong Chhnang",
    "country_id": "36",
    "value": "Kampong Chhnang"
  },
  {
    "id": "633",
    "text": "Kampong Spoeu",
    "country_id": "36",
    "value": "Kampong Spoeu"
  },
  {
    "id": "634",
    "text": "Kampong Thum",
    "country_id": "36",
    "value": "Kampong Thum"
  },
  {
    "id": "635",
    "text": "Kampot",
    "country_id": "36",
    "value": "Kampot"
  },
  {
    "id": "636",
    "text": "Kandal",
    "country_id": "36",
    "value": "Kandal"
  },
  {
    "id": "637",
    "text": "Kaoh Kong",
    "country_id": "36",
    "value": "Kaoh Kong"
  },
  {
    "id": "638",
    "text": "Kracheh",
    "country_id": "36",
    "value": "Kracheh"
  },
  {
    "id": "639",
    "text": "Krong Kaeb",
    "country_id": "36",
    "value": "Krong Kaeb"
  },
  {
    "id": "640",
    "text": "Krong Pailin",
    "country_id": "36",
    "value": "Krong Pailin"
  },
  {
    "id": "641",
    "text": "Krong Preah Sihanouk",
    "country_id": "36",
    "value": "Krong Preah Sihanouk"
  },
  {
    "id": "642",
    "text": "Mondol Kiri",
    "country_id": "36",
    "value": "Mondol Kiri"
  },
  {
    "id": "643",
    "text": "Otdar Mean Chey",
    "country_id": "36",
    "value": "Otdar Mean Chey"
  },
  {
    "id": "644",
    "text": "Phnum Penh",
    "country_id": "36",
    "value": "Phnum Penh"
  },
  {
    "id": "645",
    "text": "Pousat",
    "country_id": "36",
    "value": "Pousat"
  },
  {
    "id": "646",
    "text": "Preah Vihear",
    "country_id": "36",
    "value": "Preah Vihear"
  },
  {
    "id": "647",
    "text": "Prey Veaeng",
    "country_id": "36",
    "value": "Prey Veaeng"
  },
  {
    "id": "648",
    "text": "Rotanak Kiri",
    "country_id": "36",
    "value": "Rotanak Kiri"
  },
  {
    "id": "649",
    "text": "Siem Reab",
    "country_id": "36",
    "value": "Siem Reab"
  },
  {
    "id": "650",
    "text": "Stueng Traeng",
    "country_id": "36",
    "value": "Stueng Traeng"
  },
  {
    "id": "651",
    "text": "Svay Rieng",
    "country_id": "36",
    "value": "Svay Rieng"
  },
  {
    "id": "652",
    "text": "Takaev",
    "country_id": "36",
    "value": "Takaev"
  },
  {
    "id": "653",
    "text": "Adamaoua",
    "country_id": "37",
    "value": "Adamaoua"
  },
  {
    "id": "654",
    "text": "Centre",
    "country_id": "37",
    "value": "Centre"
  },
  {
    "id": "655",
    "text": "Est",
    "country_id": "37",
    "value": "Est"
  },
  {
    "id": "656",
    "text": "Littoral",
    "country_id": "37",
    "value": "Littoral"
  },
  {
    "id": "657",
    "text": "Nord",
    "country_id": "37",
    "value": "Nord"
  },
  {
    "id": "658",
    "text": "Nord Extreme",
    "country_id": "37",
    "value": "Nord Extreme"
  },
  {
    "id": "659",
    "text": "Nordouest",
    "country_id": "37",
    "value": "Nordouest"
  },
  {
    "id": "660",
    "text": "Ouest",
    "country_id": "37",
    "value": "Ouest"
  },
  {
    "id": "661",
    "text": "Sud",
    "country_id": "37",
    "value": "Sud"
  },
  {
    "id": "662",
    "text": "Sudouest",
    "country_id": "37",
    "value": "Sudouest"
  },
  {
    "id": "663",
    "text": "Alberta",
    "country_id": "38",
    "value": "Alberta"
  },
  {
    "id": "664",
    "text": "British Columbia",
    "country_id": "38",
    "value": "British Columbia"
  },
  {
    "id": "665",
    "text": "Manitoba",
    "country_id": "38",
    "value": "Manitoba"
  },
  {
    "id": "666",
    "text": "New Brunswick",
    "country_id": "38",
    "value": "New Brunswick"
  },
  {
    "id": "667",
    "text": "Newfoundland and Labrador",
    "country_id": "38",
    "value": "Newfoundland and Labrador"
  },
  {
    "id": "668",
    "text": "Northwest Territories",
    "country_id": "38",
    "value": "Northwest Territories"
  },
  {
    "id": "669",
    "text": "Nova Scotia",
    "country_id": "38",
    "value": "Nova Scotia"
  },
  {
    "id": "670",
    "text": "Nunavut",
    "country_id": "38",
    "value": "Nunavut"
  },
  {
    "id": "671",
    "text": "Ontario",
    "country_id": "38",
    "value": "Ontario"
  },
  {
    "id": "672",
    "text": "Prince Edward Island",
    "country_id": "38",
    "value": "Prince Edward Island"
  },
  {
    "id": "673",
    "text": "Quebec",
    "country_id": "38",
    "value": "Quebec"
  },
  {
    "id": "674",
    "text": "Saskatchewan",
    "country_id": "38",
    "value": "Saskatchewan"
  },
  {
    "id": "675",
    "text": "Yukon",
    "country_id": "38",
    "value": "Yukon"
  },
  {
    "id": "676",
    "text": "Boavista",
    "country_id": "39",
    "value": "Boavista"
  },
  {
    "id": "677",
    "text": "Brava",
    "country_id": "39",
    "value": "Brava"
  },
  {
    "id": "678",
    "text": "Fogo",
    "country_id": "39",
    "value": "Fogo"
  },
  {
    "id": "679",
    "text": "Maio",
    "country_id": "39",
    "value": "Maio"
  },
  {
    "id": "680",
    "text": "Sal",
    "country_id": "39",
    "value": "Sal"
  },
  {
    "id": "681",
    "text": "Santo Antao",
    "country_id": "39",
    "value": "Santo Antao"
  },
  {
    "id": "682",
    "text": "Sao Nicolau",
    "country_id": "39",
    "value": "Sao Nicolau"
  },
  {
    "id": "683",
    "text": "Sao Tiago",
    "country_id": "39",
    "value": "Sao Tiago"
  },
  {
    "id": "684",
    "text": "Sao Vicente",
    "country_id": "39",
    "value": "Sao Vicente"
  },
  {
    "id": "685",
    "text": "Grand Cayman",
    "country_id": "40",
    "value": "Grand Cayman"
  },
  {
    "id": "686",
    "text": "Bamingui-Bangoran",
    "country_id": "41",
    "value": "Bamingui-Bangoran"
  },
  {
    "id": "687",
    "text": "Bangui",
    "country_id": "41",
    "value": "Bangui"
  },
  {
    "id": "688",
    "text": "Basse-Kotto",
    "country_id": "41",
    "value": "Basse-Kotto"
  },
  {
    "id": "689",
    "text": "Haut-Mbomou",
    "country_id": "41",
    "value": "Haut-Mbomou"
  },
  {
    "id": "690",
    "text": "Haute-Kotto",
    "country_id": "41",
    "value": "Haute-Kotto"
  },
  {
    "id": "691",
    "text": "Kemo",
    "country_id": "41",
    "value": "Kemo"
  },
  {
    "id": "692",
    "text": "Lobaye",
    "country_id": "41",
    "value": "Lobaye"
  },
  {
    "id": "693",
    "text": "Mambere-Kadei",
    "country_id": "41",
    "value": "Mambere-Kadei"
  },
  {
    "id": "694",
    "text": "Mbomou",
    "country_id": "41",
    "value": "Mbomou"
  },
  {
    "id": "695",
    "text": "Nana-Gribizi",
    "country_id": "41",
    "value": "Nana-Gribizi"
  },
  {
    "id": "696",
    "text": "Nana-Mambere",
    "country_id": "41",
    "value": "Nana-Mambere"
  },
  {
    "id": "697",
    "text": "Ombella Mpoko",
    "country_id": "41",
    "value": "Ombella Mpoko"
  },
  {
    "id": "698",
    "text": "Ouaka",
    "country_id": "41",
    "value": "Ouaka"
  },
  {
    "id": "699",
    "text": "Ouham",
    "country_id": "41",
    "value": "Ouham"
  },
  {
    "id": "700",
    "text": "Ouham-Pende",
    "country_id": "41",
    "value": "Ouham-Pende"
  },
  {
    "id": "701",
    "text": "Sangha-Mbaere",
    "country_id": "41",
    "value": "Sangha-Mbaere"
  },
  {
    "id": "702",
    "text": "Vakaga",
    "country_id": "41",
    "value": "Vakaga"
  },
  {
    "id": "703",
    "text": "Batha",
    "country_id": "42",
    "value": "Batha"
  },
  {
    "id": "704",
    "text": "Biltine",
    "country_id": "42",
    "value": "Biltine"
  },
  {
    "id": "705",
    "text": "Bourkou-Ennedi-Tibesti",
    "country_id": "42",
    "value": "Bourkou-Ennedi-Tibesti"
  },
  {
    "id": "706",
    "text": "Chari-Baguirmi",
    "country_id": "42",
    "value": "Chari-Baguirmi"
  },
  {
    "id": "707",
    "text": "Guera",
    "country_id": "42",
    "value": "Guera"
  },
  {
    "id": "708",
    "text": "Kanem",
    "country_id": "42",
    "value": "Kanem"
  },
  {
    "id": "709",
    "text": "Lac",
    "country_id": "42",
    "value": "Lac"
  },
  {
    "id": "710",
    "text": "Logone Occidental",
    "country_id": "42",
    "value": "Logone Occidental"
  },
  {
    "id": "711",
    "text": "Logone Oriental",
    "country_id": "42",
    "value": "Logone Oriental"
  },
  {
    "id": "712",
    "text": "Mayo-Kebbi",
    "country_id": "42",
    "value": "Mayo-Kebbi"
  },
  {
    "id": "713",
    "text": "Moyen-Chari",
    "country_id": "42",
    "value": "Moyen-Chari"
  },
  {
    "id": "714",
    "text": "Ouaddai",
    "country_id": "42",
    "value": "Ouaddai"
  },
  {
    "id": "715",
    "text": "Salamat",
    "country_id": "42",
    "value": "Salamat"
  },
  {
    "id": "716",
    "text": "Tandjile",
    "country_id": "42",
    "value": "Tandjile"
  },
  {
    "id": "717",
    "text": "Aisen",
    "country_id": "43",
    "value": "Aisen"
  },
  {
    "id": "718",
    "text": "Antofagasta",
    "country_id": "43",
    "value": "Antofagasta"
  },
  {
    "id": "719",
    "text": "Araucania",
    "country_id": "43",
    "value": "Araucania"
  },
  {
    "id": "720",
    "text": "Atacama",
    "country_id": "43",
    "value": "Atacama"
  },
  {
    "id": "721",
    "text": "Bio Bio",
    "country_id": "43",
    "value": "Bio Bio"
  },
  {
    "id": "722",
    "text": "Coquimbo",
    "country_id": "43",
    "value": "Coquimbo"
  },
  {
    "id": "723",
    "text": "Libertador General Bernardo O",
    "country_id": "43",
    "value": "Libertador General Bernardo O"
  },
  {
    "id": "724",
    "text": "Los Lagos",
    "country_id": "43",
    "value": "Los Lagos"
  },
  {
    "id": "725",
    "text": "Magellanes",
    "country_id": "43",
    "value": "Magellanes"
  },
  {
    "id": "726",
    "text": "Maule",
    "country_id": "43",
    "value": "Maule"
  },
  {
    "id": "727",
    "text": "Metropolitana",
    "country_id": "43",
    "value": "Metropolitana"
  },
  {
    "id": "728",
    "text": "Metropolitana de Santiago",
    "country_id": "43",
    "value": "Metropolitana de Santiago"
  },
  {
    "id": "729",
    "text": "Tarapaca",
    "country_id": "43",
    "value": "Tarapaca"
  },
  {
    "id": "730",
    "text": "Valparaiso",
    "country_id": "43",
    "value": "Valparaiso"
  },
  {
    "id": "731",
    "text": "Anhui",
    "country_id": "44",
    "value": "Anhui"
  },
  {
    "id": "732",
    "text": "Anhui Province",
    "country_id": "44",
    "value": "Anhui Province"
  },
  {
    "id": "733",
    "text": "Anhui Sheng",
    "country_id": "44",
    "value": "Anhui Sheng"
  },
  {
    "id": "734",
    "text": "Aomen",
    "country_id": "44",
    "value": "Aomen"
  },
  {
    "id": "735",
    "text": "Beijing",
    "country_id": "44",
    "value": "Beijing"
  },
  {
    "id": "736",
    "text": "Beijing Shi",
    "country_id": "44",
    "value": "Beijing Shi"
  },
  {
    "id": "737",
    "text": "Chongqing",
    "country_id": "44",
    "value": "Chongqing"
  },
  {
    "id": "738",
    "text": "Fujian",
    "country_id": "44",
    "value": "Fujian"
  },
  {
    "id": "739",
    "text": "Fujian Sheng",
    "country_id": "44",
    "value": "Fujian Sheng"
  },
  {
    "id": "740",
    "text": "Gansu",
    "country_id": "44",
    "value": "Gansu"
  },
  {
    "id": "741",
    "text": "Guangdong",
    "country_id": "44",
    "value": "Guangdong"
  },
  {
    "id": "742",
    "text": "Guangdong Sheng",
    "country_id": "44",
    "value": "Guangdong Sheng"
  },
  {
    "id": "743",
    "text": "Guangxi",
    "country_id": "44",
    "value": "Guangxi"
  },
  {
    "id": "744",
    "text": "Guizhou",
    "country_id": "44",
    "value": "Guizhou"
  },
  {
    "id": "745",
    "text": "Hainan",
    "country_id": "44",
    "value": "Hainan"
  },
  {
    "id": "746",
    "text": "Hebei",
    "country_id": "44",
    "value": "Hebei"
  },
  {
    "id": "747",
    "text": "Heilongjiang",
    "country_id": "44",
    "value": "Heilongjiang"
  },
  {
    "id": "748",
    "text": "Henan",
    "country_id": "44",
    "value": "Henan"
  },
  {
    "id": "749",
    "text": "Hubei",
    "country_id": "44",
    "value": "Hubei"
  },
  {
    "id": "750",
    "text": "Hunan",
    "country_id": "44",
    "value": "Hunan"
  },
  {
    "id": "751",
    "text": "Jiangsu",
    "country_id": "44",
    "value": "Jiangsu"
  },
  {
    "id": "752",
    "text": "Jiangsu Sheng",
    "country_id": "44",
    "value": "Jiangsu Sheng"
  },
  {
    "id": "753",
    "text": "Jiangxi",
    "country_id": "44",
    "value": "Jiangxi"
  },
  {
    "id": "754",
    "text": "Jilin",
    "country_id": "44",
    "value": "Jilin"
  },
  {
    "id": "755",
    "text": "Liaoning",
    "country_id": "44",
    "value": "Liaoning"
  },
  {
    "id": "756",
    "text": "Liaoning Sheng",
    "country_id": "44",
    "value": "Liaoning Sheng"
  },
  {
    "id": "757",
    "text": "Nei Monggol",
    "country_id": "44",
    "value": "Nei Monggol"
  },
  {
    "id": "758",
    "text": "Ningxia Hui",
    "country_id": "44",
    "value": "Ningxia Hui"
  },
  {
    "id": "759",
    "text": "Qinghai",
    "country_id": "44",
    "value": "Qinghai"
  },
  {
    "id": "760",
    "text": "Shaanxi",
    "country_id": "44",
    "value": "Shaanxi"
  },
  {
    "id": "761",
    "text": "Shandong",
    "country_id": "44",
    "value": "Shandong"
  },
  {
    "id": "762",
    "text": "Shandong Sheng",
    "country_id": "44",
    "value": "Shandong Sheng"
  },
  {
    "id": "763",
    "text": "Shanghai",
    "country_id": "44",
    "value": "Shanghai"
  },
  {
    "id": "764",
    "text": "Shanxi",
    "country_id": "44",
    "value": "Shanxi"
  },
  {
    "id": "765",
    "text": "Sichuan",
    "country_id": "44",
    "value": "Sichuan"
  },
  {
    "id": "766",
    "text": "Tianjin",
    "country_id": "44",
    "value": "Tianjin"
  },
  {
    "id": "767",
    "text": "Xianggang",
    "country_id": "44",
    "value": "Xianggang"
  },
  {
    "id": "768",
    "text": "Xinjiang",
    "country_id": "44",
    "value": "Xinjiang"
  },
  {
    "id": "769",
    "text": "Xizang",
    "country_id": "44",
    "value": "Xizang"
  },
  {
    "id": "770",
    "text": "Yunnan",
    "country_id": "44",
    "value": "Yunnan"
  },
  {
    "id": "771",
    "text": "Zhejiang",
    "country_id": "44",
    "value": "Zhejiang"
  },
  {
    "id": "772",
    "text": "Zhejiang Sheng",
    "country_id": "44",
    "value": "Zhejiang Sheng"
  },
  {
    "id": "773",
    "text": "Christmas Island",
    "country_id": "45",
    "value": "Christmas Island"
  },
  {
    "id": "774",
    "text": "Cocos (Keeling) Islands",
    "country_id": "46",
    "value": "Cocos (Keeling) Islands"
  },
  {
    "id": "775",
    "text": "Amazonas",
    "country_id": "47",
    "value": "Amazonas"
  },
  {
    "id": "776",
    "text": "Antioquia",
    "country_id": "47",
    "value": "Antioquia"
  },
  {
    "id": "777",
    "text": "Arauca",
    "country_id": "47",
    "value": "Arauca"
  },
  {
    "id": "778",
    "text": "Atlantico",
    "country_id": "47",
    "value": "Atlantico"
  },
  {
    "id": "779",
    "text": "Bogota",
    "country_id": "47",
    "value": "Bogota"
  },
  {
    "id": "780",
    "text": "Bolivar",
    "country_id": "47",
    "value": "Bolivar"
  },
  {
    "id": "781",
    "text": "Boyaca",
    "country_id": "47",
    "value": "Boyaca"
  },
  {
    "id": "782",
    "text": "Caldas",
    "country_id": "47",
    "value": "Caldas"
  },
  {
    "id": "783",
    "text": "Caqueta",
    "country_id": "47",
    "value": "Caqueta"
  },
  {
    "id": "784",
    "text": "Casanare",
    "country_id": "47",
    "value": "Casanare"
  },
  {
    "id": "785",
    "text": "Cauca",
    "country_id": "47",
    "value": "Cauca"
  },
  {
    "id": "786",
    "text": "Cesar",
    "country_id": "47",
    "value": "Cesar"
  },
  {
    "id": "787",
    "text": "Choco",
    "country_id": "47",
    "value": "Choco"
  },
  {
    "id": "788",
    "text": "Cordoba",
    "country_id": "47",
    "value": "Cordoba"
  },
  {
    "id": "789",
    "text": "Cundinamarca",
    "country_id": "47",
    "value": "Cundinamarca"
  },
  {
    "id": "790",
    "text": "Guainia",
    "country_id": "47",
    "value": "Guainia"
  },
  {
    "id": "791",
    "text": "Guaviare",
    "country_id": "47",
    "value": "Guaviare"
  },
  {
    "id": "792",
    "text": "Huila",
    "country_id": "47",
    "value": "Huila"
  },
  {
    "id": "793",
    "text": "La Guajira",
    "country_id": "47",
    "value": "La Guajira"
  },
  {
    "id": "794",
    "text": "Magdalena",
    "country_id": "47",
    "value": "Magdalena"
  },
  {
    "id": "795",
    "text": "Meta",
    "country_id": "47",
    "value": "Meta"
  },
  {
    "id": "796",
    "text": "Narino",
    "country_id": "47",
    "value": "Narino"
  },
  {
    "id": "797",
    "text": "Norte de Santander",
    "country_id": "47",
    "value": "Norte de Santander"
  },
  {
    "id": "798",
    "text": "Putumayo",
    "country_id": "47",
    "value": "Putumayo"
  },
  {
    "id": "799",
    "text": "Quindio",
    "country_id": "47",
    "value": "Quindio"
  },
  {
    "id": "800",
    "text": "Risaralda",
    "country_id": "47",
    "value": "Risaralda"
  },
  {
    "id": "801",
    "text": "San Andres y Providencia",
    "country_id": "47",
    "value": "San Andres y Providencia"
  },
  {
    "id": "802",
    "text": "Santander",
    "country_id": "47",
    "value": "Santander"
  },
  {
    "id": "803",
    "text": "Sucre",
    "country_id": "47",
    "value": "Sucre"
  },
  {
    "id": "804",
    "text": "Tolima",
    "country_id": "47",
    "value": "Tolima"
  },
  {
    "id": "805",
    "text": "Valle del Cauca",
    "country_id": "47",
    "value": "Valle del Cauca"
  },
  {
    "id": "806",
    "text": "Vaupes",
    "country_id": "47",
    "value": "Vaupes"
  },
  {
    "id": "807",
    "text": "Vichada",
    "country_id": "47",
    "value": "Vichada"
  },
  {
    "id": "808",
    "text": "Mwali",
    "country_id": "48",
    "value": "Mwali"
  },
  {
    "id": "809",
    "text": "Njazidja",
    "country_id": "48",
    "value": "Njazidja"
  },
  {
    "id": "810",
    "text": "Nzwani",
    "country_id": "48",
    "value": "Nzwani"
  },
  {
    "id": "811",
    "text": "Bouenza",
    "country_id": "49",
    "value": "Bouenza"
  },
  {
    "id": "812",
    "text": "Brazzaville",
    "country_id": "49",
    "value": "Brazzaville"
  },
  {
    "id": "813",
    "text": "Cuvette",
    "country_id": "49",
    "value": "Cuvette"
  },
  {
    "id": "814",
    "text": "Kouilou",
    "country_id": "49",
    "value": "Kouilou"
  },
  {
    "id": "815",
    "text": "Lekoumou",
    "country_id": "49",
    "value": "Lekoumou"
  },
  {
    "id": "816",
    "text": "Likouala",
    "country_id": "49",
    "value": "Likouala"
  },
  {
    "id": "817",
    "text": "Niari",
    "country_id": "49",
    "value": "Niari"
  },
  {
    "id": "818",
    "text": "Plateaux",
    "country_id": "49",
    "value": "Plateaux"
  },
  {
    "id": "819",
    "text": "Pool",
    "country_id": "49",
    "value": "Pool"
  },
  {
    "id": "820",
    "text": "Sangha",
    "country_id": "49",
    "value": "Sangha"
  },
  {
    "id": "821",
    "text": "Bandundu",
    "country_id": "50",
    "value": "Bandundu"
  },
  {
    "id": "822",
    "text": "Bas-Congo",
    "country_id": "50",
    "value": "Bas-Congo"
  },
  {
    "id": "823",
    "text": "Equateur",
    "country_id": "50",
    "value": "Equateur"
  },
  {
    "id": "824",
    "text": "Haut-Congo",
    "country_id": "50",
    "value": "Haut-Congo"
  },
  {
    "id": "825",
    "text": "Kasai-Occidental",
    "country_id": "50",
    "value": "Kasai-Occidental"
  },
  {
    "id": "826",
    "text": "Kasai-Oriental",
    "country_id": "50",
    "value": "Kasai-Oriental"
  },
  {
    "id": "827",
    "text": "Katanga",
    "country_id": "50",
    "value": "Katanga"
  },
  {
    "id": "828",
    "text": "Kinshasa",
    "country_id": "50",
    "value": "Kinshasa"
  },
  {
    "id": "829",
    "text": "Maniema",
    "country_id": "50",
    "value": "Maniema"
  },
  {
    "id": "830",
    "text": "Nord-Kivu",
    "country_id": "50",
    "value": "Nord-Kivu"
  },
  {
    "id": "831",
    "text": "Sud-Kivu",
    "country_id": "50",
    "value": "Sud-Kivu"
  },
  {
    "id": "832",
    "text": "Aitutaki",
    "country_id": "51",
    "value": "Aitutaki"
  },
  {
    "id": "833",
    "text": "Atiu",
    "country_id": "51",
    "value": "Atiu"
  },
  {
    "id": "834",
    "text": "Mangaia",
    "country_id": "51",
    "value": "Mangaia"
  },
  {
    "id": "835",
    "text": "Manihiki",
    "country_id": "51",
    "value": "Manihiki"
  },
  {
    "id": "836",
    "text": "Mauke",
    "country_id": "51",
    "value": "Mauke"
  },
  {
    "id": "837",
    "text": "Mitiaro",
    "country_id": "51",
    "value": "Mitiaro"
  },
  {
    "id": "838",
    "text": "Nassau",
    "country_id": "51",
    "value": "Nassau"
  },
  {
    "id": "839",
    "text": "Pukapuka",
    "country_id": "51",
    "value": "Pukapuka"
  },
  {
    "id": "840",
    "text": "Rakahanga",
    "country_id": "51",
    "value": "Rakahanga"
  },
  {
    "id": "841",
    "text": "Rarotonga",
    "country_id": "51",
    "value": "Rarotonga"
  },
  {
    "id": "842",
    "text": "Tongareva",
    "country_id": "51",
    "value": "Tongareva"
  },
  {
    "id": "843",
    "text": "Alajuela",
    "country_id": "52",
    "value": "Alajuela"
  },
  {
    "id": "844",
    "text": "Cartago",
    "country_id": "52",
    "value": "Cartago"
  },
  {
    "id": "845",
    "text": "Guanacaste",
    "country_id": "52",
    "value": "Guanacaste"
  },
  {
    "id": "846",
    "text": "Heredia",
    "country_id": "52",
    "value": "Heredia"
  },
  {
    "id": "847",
    "text": "Limon",
    "country_id": "52",
    "value": "Limon"
  },
  {
    "id": "848",
    "text": "Puntarenas",
    "country_id": "52",
    "value": "Puntarenas"
  },
  {
    "id": "849",
    "text": "San Jose",
    "country_id": "52",
    "value": "San Jose"
  },
  {
    "id": "850",
    "text": "Abidjan",
    "country_id": "53",
    "value": "Abidjan"
  },
  {
    "id": "851",
    "text": "Agneby",
    "country_id": "53",
    "value": "Agneby"
  },
  {
    "id": "852",
    "text": "Bafing",
    "country_id": "53",
    "value": "Bafing"
  },
  {
    "id": "853",
    "text": "Denguele",
    "country_id": "53",
    "value": "Denguele"
  },
  {
    "id": "854",
    "text": "Dix-huit Montagnes",
    "country_id": "53",
    "value": "Dix-huit Montagnes"
  },
  {
    "id": "855",
    "text": "Fromager",
    "country_id": "53",
    "value": "Fromager"
  },
  {
    "id": "856",
    "text": "Haut-Sassandra",
    "country_id": "53",
    "value": "Haut-Sassandra"
  },
  {
    "id": "857",
    "text": "Lacs",
    "country_id": "53",
    "value": "Lacs"
  },
  {
    "id": "858",
    "text": "Lagunes",
    "country_id": "53",
    "value": "Lagunes"
  },
  {
    "id": "859",
    "text": "Marahoue",
    "country_id": "53",
    "value": "Marahoue"
  },
  {
    "id": "860",
    "text": "Moyen-Cavally",
    "country_id": "53",
    "value": "Moyen-Cavally"
  },
  {
    "id": "861",
    "text": "Moyen-Comoe",
    "country_id": "53",
    "value": "Moyen-Comoe"
  },
  {
    "id": "862",
    "text": "N''zi-Comoe",
    "country_id": "53",
    "value": "N''zi-Comoe"
  },
  {
    "id": "863",
    "text": "Sassandra",
    "country_id": "53",
    "value": "Sassandra"
  },
  {
    "id": "864",
    "text": "Savanes",
    "country_id": "53",
    "value": "Savanes"
  },
  {
    "id": "865",
    "text": "Sud-Bandama",
    "country_id": "53",
    "value": "Sud-Bandama"
  },
  {
    "id": "866",
    "text": "Sud-Comoe",
    "country_id": "53",
    "value": "Sud-Comoe"
  },
  {
    "id": "867",
    "text": "Vallee du Bandama",
    "country_id": "53",
    "value": "Vallee du Bandama"
  },
  {
    "id": "868",
    "text": "Worodougou",
    "country_id": "53",
    "value": "Worodougou"
  },
  {
    "id": "869",
    "text": "Zanzan",
    "country_id": "53",
    "value": "Zanzan"
  },
  {
    "id": "870",
    "text": "Bjelovar-Bilogora",
    "country_id": "54",
    "value": "Bjelovar-Bilogora"
  },
  {
    "id": "871",
    "text": "Dubrovnik-Neretva",
    "country_id": "54",
    "value": "Dubrovnik-Neretva"
  },
  {
    "id": "872",
    "text": "Grad Zagreb",
    "country_id": "54",
    "value": "Grad Zagreb"
  },
  {
    "id": "873",
    "text": "Istra",
    "country_id": "54",
    "value": "Istra"
  },
  {
    "id": "874",
    "text": "Karlovac",
    "country_id": "54",
    "value": "Karlovac"
  },
  {
    "id": "875",
    "text": "Koprivnica-Krizhevci",
    "country_id": "54",
    "value": "Koprivnica-Krizhevci"
  },
  {
    "id": "876",
    "text": "Krapina-Zagorje",
    "country_id": "54",
    "value": "Krapina-Zagorje"
  },
  {
    "id": "877",
    "text": "Lika-Senj",
    "country_id": "54",
    "value": "Lika-Senj"
  },
  {
    "id": "878",
    "text": "Medhimurje",
    "country_id": "54",
    "value": "Medhimurje"
  },
  {
    "id": "879",
    "text": "Medimurska Zupanija",
    "country_id": "54",
    "value": "Medimurska Zupanija"
  },
  {
    "id": "880",
    "text": "Osijek-Baranja",
    "country_id": "54",
    "value": "Osijek-Baranja"
  },
  {
    "id": "881",
    "text": "Osjecko-Baranjska Zupanija",
    "country_id": "54",
    "value": "Osjecko-Baranjska Zupanija"
  },
  {
    "id": "882",
    "text": "Pozhega-Slavonija",
    "country_id": "54",
    "value": "Pozhega-Slavonija"
  },
  {
    "id": "883",
    "text": "Primorje-Gorski Kotar",
    "country_id": "54",
    "value": "Primorje-Gorski Kotar"
  },
  {
    "id": "884",
    "text": "Shibenik-Knin",
    "country_id": "54",
    "value": "Shibenik-Knin"
  },
  {
    "id": "885",
    "text": "Sisak-Moslavina",
    "country_id": "54",
    "value": "Sisak-Moslavina"
  },
  {
    "id": "886",
    "text": "Slavonski Brod-Posavina",
    "country_id": "54",
    "value": "Slavonski Brod-Posavina"
  },
  {
    "id": "887",
    "text": "Split-Dalmacija",
    "country_id": "54",
    "value": "Split-Dalmacija"
  },
  {
    "id": "888",
    "text": "Varazhdin",
    "country_id": "54",
    "value": "Varazhdin"
  },
  {
    "id": "889",
    "text": "Virovitica-Podravina",
    "country_id": "54",
    "value": "Virovitica-Podravina"
  },
  {
    "id": "890",
    "text": "Vukovar-Srijem",
    "country_id": "54",
    "value": "Vukovar-Srijem"
  },
  {
    "id": "891",
    "text": "Zadar",
    "country_id": "54",
    "value": "Zadar"
  },
  {
    "id": "892",
    "text": "Zagreb",
    "country_id": "54",
    "value": "Zagreb"
  },
  {
    "id": "893",
    "text": "Camaguey",
    "country_id": "55",
    "value": "Camaguey"
  },
  {
    "id": "894",
    "text": "Ciego de Avila",
    "country_id": "55",
    "value": "Ciego de Avila"
  },
  {
    "id": "895",
    "text": "Cienfuegos",
    "country_id": "55",
    "value": "Cienfuegos"
  },
  {
    "id": "896",
    "text": "Ciudad de la Habana",
    "country_id": "55",
    "value": "Ciudad de la Habana"
  },
  {
    "id": "897",
    "text": "Granma",
    "country_id": "55",
    "value": "Granma"
  },
  {
    "id": "898",
    "text": "Guantanamo",
    "country_id": "55",
    "value": "Guantanamo"
  },
  {
    "id": "899",
    "text": "Habana",
    "country_id": "55",
    "value": "Habana"
  },
  {
    "id": "900",
    "text": "Holguin",
    "country_id": "55",
    "value": "Holguin"
  },
  {
    "id": "901",
    "text": "Isla de la Juventud",
    "country_id": "55",
    "value": "Isla de la Juventud"
  },
  {
    "id": "902",
    "text": "La Habana",
    "country_id": "55",
    "value": "La Habana"
  },
  {
    "id": "903",
    "text": "Las Tunas",
    "country_id": "55",
    "value": "Las Tunas"
  },
  {
    "id": "904",
    "text": "Matanzas",
    "country_id": "55",
    "value": "Matanzas"
  },
  {
    "id": "905",
    "text": "Pinar del Rio",
    "country_id": "55",
    "value": "Pinar del Rio"
  },
  {
    "id": "906",
    "text": "Sancti Spiritus",
    "country_id": "55",
    "value": "Sancti Spiritus"
  },
  {
    "id": "907",
    "text": "Santiago de Cuba",
    "country_id": "55",
    "value": "Santiago de Cuba"
  },
  {
    "id": "908",
    "text": "Villa Clara",
    "country_id": "55",
    "value": "Villa Clara"
  },
  {
    "id": "909",
    "text": "Government controlled area",
    "country_id": "56",
    "value": "Government controlled area"
  },
  {
    "id": "910",
    "text": "Limassol",
    "country_id": "56",
    "value": "Limassol"
  },
  {
    "id": "911",
    "text": "Nicosia District",
    "country_id": "56",
    "value": "Nicosia District"
  },
  {
    "id": "912",
    "text": "Paphos",
    "country_id": "56",
    "value": "Paphos"
  },
  {
    "id": "913",
    "text": "Turkish controlled area",
    "country_id": "56",
    "value": "Turkish controlled area"
  },
  {
    "id": "914",
    "text": "Central Bohemian",
    "country_id": "57",
    "value": "Central Bohemian"
  },
  {
    "id": "915",
    "text": "Frycovice",
    "country_id": "57",
    "value": "Frycovice"
  },
  {
    "id": "916",
    "text": "Jihocesky Kraj",
    "country_id": "57",
    "value": "Jihocesky Kraj"
  },
  {
    "id": "917",
    "text": "Jihochesky",
    "country_id": "57",
    "value": "Jihochesky"
  },
  {
    "id": "918",
    "text": "Jihomoravsky",
    "country_id": "57",
    "value": "Jihomoravsky"
  },
  {
    "id": "919",
    "text": "Karlovarsky",
    "country_id": "57",
    "value": "Karlovarsky"
  },
  {
    "id": "920",
    "text": "Klecany",
    "country_id": "57",
    "value": "Klecany"
  },
  {
    "id": "921",
    "text": "Kralovehradecky",
    "country_id": "57",
    "value": "Kralovehradecky"
  },
  {
    "id": "922",
    "text": "Liberecky",
    "country_id": "57",
    "value": "Liberecky"
  },
  {
    "id": "923",
    "text": "Lipov",
    "country_id": "57",
    "value": "Lipov"
  },
  {
    "id": "924",
    "text": "Moravskoslezsky",
    "country_id": "57",
    "value": "Moravskoslezsky"
  },
  {
    "id": "925",
    "text": "Olomoucky",
    "country_id": "57",
    "value": "Olomoucky"
  },
  {
    "id": "926",
    "text": "Olomoucky Kraj",
    "country_id": "57",
    "value": "Olomoucky Kraj"
  },
  {
    "id": "927",
    "text": "Pardubicky",
    "country_id": "57",
    "value": "Pardubicky"
  },
  {
    "id": "928",
    "text": "Plzensky",
    "country_id": "57",
    "value": "Plzensky"
  },
  {
    "id": "929",
    "text": "Praha",
    "country_id": "57",
    "value": "Praha"
  },
  {
    "id": "930",
    "text": "Rajhrad",
    "country_id": "57",
    "value": "Rajhrad"
  },
  {
    "id": "931",
    "text": "Smirice",
    "country_id": "57",
    "value": "Smirice"
  },
  {
    "id": "932",
    "text": "South Moravian",
    "country_id": "57",
    "value": "South Moravian"
  },
  {
    "id": "933",
    "text": "Straz nad Nisou",
    "country_id": "57",
    "value": "Straz nad Nisou"
  },
  {
    "id": "934",
    "text": "Stredochesky",
    "country_id": "57",
    "value": "Stredochesky"
  },
  {
    "id": "935",
    "text": "Unicov",
    "country_id": "57",
    "value": "Unicov"
  },
  {
    "id": "936",
    "text": "Ustecky",
    "country_id": "57",
    "value": "Ustecky"
  },
  {
    "id": "937",
    "text": "Valletta",
    "country_id": "57",
    "value": "Valletta"
  },
  {
    "id": "938",
    "text": "Velesin",
    "country_id": "57",
    "value": "Velesin"
  },
  {
    "id": "939",
    "text": "Vysochina",
    "country_id": "57",
    "value": "Vysochina"
  },
  {
    "id": "940",
    "text": "Zlinsky",
    "country_id": "57",
    "value": "Zlinsky"
  },
  {
    "id": "941",
    "text": "Arhus",
    "country_id": "58",
    "value": "Arhus"
  },
  {
    "id": "942",
    "text": "Bornholm",
    "country_id": "58",
    "value": "Bornholm"
  },
  {
    "id": "943",
    "text": "Frederiksborg",
    "country_id": "58",
    "value": "Frederiksborg"
  },
  {
    "id": "944",
    "text": "Fyn",
    "country_id": "58",
    "value": "Fyn"
  },
  {
    "id": "945",
    "text": "Hovedstaden",
    "country_id": "58",
    "value": "Hovedstaden"
  },
  {
    "id": "946",
    "text": "Kobenhavn",
    "country_id": "58",
    "value": "Kobenhavn"
  },
  {
    "id": "947",
    "text": "Kobenhavns Amt",
    "country_id": "58",
    "value": "Kobenhavns Amt"
  },
  {
    "id": "948",
    "text": "Kobenhavns Kommune",
    "country_id": "58",
    "value": "Kobenhavns Kommune"
  },
  {
    "id": "949",
    "text": "Nordjylland",
    "country_id": "58",
    "value": "Nordjylland"
  },
  {
    "id": "950",
    "text": "Ribe",
    "country_id": "58",
    "value": "Ribe"
  },
  {
    "id": "951",
    "text": "Ringkobing",
    "country_id": "58",
    "value": "Ringkobing"
  },
  {
    "id": "952",
    "text": "Roervig",
    "country_id": "58",
    "value": "Roervig"
  },
  {
    "id": "953",
    "text": "Roskilde",
    "country_id": "58",
    "value": "Roskilde"
  },
  {
    "id": "954",
    "text": "Roslev",
    "country_id": "58",
    "value": "Roslev"
  },
  {
    "id": "955",
    "text": "Sjaelland",
    "country_id": "58",
    "value": "Sjaelland"
  },
  {
    "id": "956",
    "text": "Soeborg",
    "country_id": "58",
    "value": "Soeborg"
  },
  {
    "id": "957",
    "text": "Sonderjylland",
    "country_id": "58",
    "value": "Sonderjylland"
  },
  {
    "id": "958",
    "text": "Storstrom",
    "country_id": "58",
    "value": "Storstrom"
  },
  {
    "id": "959",
    "text": "Syddanmark",
    "country_id": "58",
    "value": "Syddanmark"
  },
  {
    "id": "960",
    "text": "Toelloese",
    "country_id": "58",
    "value": "Toelloese"
  },
  {
    "id": "961",
    "text": "Vejle",
    "country_id": "58",
    "value": "Vejle"
  },
  {
    "id": "962",
    "text": "Vestsjalland",
    "country_id": "58",
    "value": "Vestsjalland"
  },
  {
    "id": "963",
    "text": "Viborg",
    "country_id": "58",
    "value": "Viborg"
  },
  {
    "id": "964",
    "text": "Ali Sabih",
    "country_id": "59",
    "value": "Ali Sabih"
  },
  {
    "id": "965",
    "text": "Dikhil",
    "country_id": "59",
    "value": "Dikhil"
  },
  {
    "id": "966",
    "text": "Jibuti",
    "country_id": "59",
    "value": "Jibuti"
  },
  {
    "id": "967",
    "text": "Tajurah",
    "country_id": "59",
    "value": "Tajurah"
  },
  {
    "id": "968",
    "text": "Ubuk",
    "country_id": "59",
    "value": "Ubuk"
  },
  {
    "id": "969",
    "text": "Saint Andrew",
    "country_id": "60",
    "value": "Saint Andrew"
  },
  {
    "id": "970",
    "text": "Saint David",
    "country_id": "60",
    "value": "Saint David"
  },
  {
    "id": "971",
    "text": "Saint George",
    "country_id": "60",
    "value": "Saint George"
  },
  {
    "id": "972",
    "text": "Saint John",
    "country_id": "60",
    "value": "Saint John"
  },
  {
    "id": "973",
    "text": "Saint Joseph",
    "country_id": "60",
    "value": "Saint Joseph"
  },
  {
    "id": "974",
    "text": "Saint Luke",
    "country_id": "60",
    "value": "Saint Luke"
  },
  {
    "id": "975",
    "text": "Saint Mark",
    "country_id": "60",
    "value": "Saint Mark"
  },
  {
    "id": "976",
    "text": "Saint Patrick",
    "country_id": "60",
    "value": "Saint Patrick"
  },
  {
    "id": "977",
    "text": "Saint Paul",
    "country_id": "60",
    "value": "Saint Paul"
  },
  {
    "id": "978",
    "text": "Saint Peter",
    "country_id": "60",
    "value": "Saint Peter"
  },
  {
    "id": "979",
    "text": "Azua",
    "country_id": "61",
    "value": "Azua"
  },
  {
    "id": "980",
    "text": "Bahoruco",
    "country_id": "61",
    "value": "Bahoruco"
  },
  {
    "id": "981",
    "text": "Barahona",
    "country_id": "61",
    "value": "Barahona"
  },
  {
    "id": "982",
    "text": "Dajabon",
    "country_id": "61",
    "value": "Dajabon"
  },
  {
    "id": "983",
    "text": "Distrito Nacional",
    "country_id": "61",
    "value": "Distrito Nacional"
  },
  {
    "id": "984",
    "text": "Duarte",
    "country_id": "61",
    "value": "Duarte"
  },
  {
    "id": "985",
    "text": "El Seybo",
    "country_id": "61",
    "value": "El Seybo"
  },
  {
    "id": "986",
    "text": "Elias Pina",
    "country_id": "61",
    "value": "Elias Pina"
  },
  {
    "id": "987",
    "text": "Espaillat",
    "country_id": "61",
    "value": "Espaillat"
  },
  {
    "id": "988",
    "text": "Hato Mayor",
    "country_id": "61",
    "value": "Hato Mayor"
  },
  {
    "id": "989",
    "text": "Independencia",
    "country_id": "61",
    "value": "Independencia"
  },
  {
    "id": "990",
    "text": "La Altagracia",
    "country_id": "61",
    "value": "La Altagracia"
  },
  {
    "id": "991",
    "text": "La Romana",
    "country_id": "61",
    "value": "La Romana"
  },
  {
    "id": "992",
    "text": "La Vega",
    "country_id": "61",
    "value": "La Vega"
  },
  {
    "id": "993",
    "text": "Maria Trinidad Sanchez",
    "country_id": "61",
    "value": "Maria Trinidad Sanchez"
  },
  {
    "id": "994",
    "text": "Monsenor Nouel",
    "country_id": "61",
    "value": "Monsenor Nouel"
  },
  {
    "id": "995",
    "text": "Monte Cristi",
    "country_id": "61",
    "value": "Monte Cristi"
  },
  {
    "id": "996",
    "text": "Monte Plata",
    "country_id": "61",
    "value": "Monte Plata"
  },
  {
    "id": "997",
    "text": "Pedernales",
    "country_id": "61",
    "value": "Pedernales"
  },
  {
    "id": "998",
    "text": "Peravia",
    "country_id": "61",
    "value": "Peravia"
  },
  {
    "id": "999",
    "text": "Puerto Plata",
    "country_id": "61",
    "value": "Puerto Plata"
  },
  {
    "id": "1000",
    "text": "Salcedo",
    "country_id": "61",
    "value": "Salcedo"
  },
  {
    "id": "1001",
    "text": "Samana",
    "country_id": "61",
    "value": "Samana"
  },
  {
    "id": "1002",
    "text": "San Cristobal",
    "country_id": "61",
    "value": "San Cristobal"
  },
  {
    "id": "1003",
    "text": "San Juan",
    "country_id": "61",
    "value": "San Juan"
  },
  {
    "id": "1004",
    "text": "San Pedro de Macoris",
    "country_id": "61",
    "value": "San Pedro de Macoris"
  },
  {
    "id": "1005",
    "text": "Sanchez Ramirez",
    "country_id": "61",
    "value": "Sanchez Ramirez"
  },
  {
    "id": "1006",
    "text": "Santiago",
    "country_id": "61",
    "value": "Santiago"
  },
  {
    "id": "1007",
    "text": "Santiago Rodriguez",
    "country_id": "61",
    "value": "Santiago Rodriguez"
  },
  {
    "id": "1008",
    "text": "Valverde",
    "country_id": "61",
    "value": "Valverde"
  },
  {
    "id": "1009",
    "text": "Aileu",
    "country_id": "62",
    "value": "Aileu"
  },
  {
    "id": "1010",
    "text": "Ainaro",
    "country_id": "62",
    "value": "Ainaro"
  },
  {
    "id": "1011",
    "text": "Ambeno",
    "country_id": "62",
    "value": "Ambeno"
  },
  {
    "id": "1012",
    "text": "Baucau",
    "country_id": "62",
    "value": "Baucau"
  },
  {
    "id": "1013",
    "text": "Bobonaro",
    "country_id": "62",
    "value": "Bobonaro"
  },
  {
    "id": "1014",
    "text": "Cova Lima",
    "country_id": "62",
    "value": "Cova Lima"
  },
  {
    "id": "1015",
    "text": "Dili",
    "country_id": "62",
    "value": "Dili"
  },
  {
    "id": "1016",
    "text": "Ermera",
    "country_id": "62",
    "value": "Ermera"
  },
  {
    "id": "1017",
    "text": "Lautem",
    "country_id": "62",
    "value": "Lautem"
  },
  {
    "id": "1018",
    "text": "Liquica",
    "country_id": "62",
    "value": "Liquica"
  },
  {
    "id": "1019",
    "text": "Manatuto",
    "country_id": "62",
    "value": "Manatuto"
  },
  {
    "id": "1020",
    "text": "Manufahi",
    "country_id": "62",
    "value": "Manufahi"
  },
  {
    "id": "1021",
    "text": "Viqueque",
    "country_id": "62",
    "value": "Viqueque"
  },
  {
    "id": "1022",
    "text": "Azuay",
    "country_id": "63",
    "value": "Azuay"
  },
  {
    "id": "1023",
    "text": "Bolivar",
    "country_id": "63",
    "value": "Bolivar"
  },
  {
    "id": "1024",
    "text": "Canar",
    "country_id": "63",
    "value": "Canar"
  },
  {
    "id": "1025",
    "text": "Carchi",
    "country_id": "63",
    "value": "Carchi"
  },
  {
    "id": "1026",
    "text": "Chimborazo",
    "country_id": "63",
    "value": "Chimborazo"
  },
  {
    "id": "1027",
    "text": "Cotopaxi",
    "country_id": "63",
    "value": "Cotopaxi"
  },
  {
    "id": "1028",
    "text": "El Oro",
    "country_id": "63",
    "value": "El Oro"
  },
  {
    "id": "1029",
    "text": "Esmeraldas",
    "country_id": "63",
    "value": "Esmeraldas"
  },
  {
    "id": "1030",
    "text": "Galapagos",
    "country_id": "63",
    "value": "Galapagos"
  },
  {
    "id": "1031",
    "text": "Guayas",
    "country_id": "63",
    "value": "Guayas"
  },
  {
    "id": "1032",
    "text": "Imbabura",
    "country_id": "63",
    "value": "Imbabura"
  },
  {
    "id": "1033",
    "text": "Loja",
    "country_id": "63",
    "value": "Loja"
  },
  {
    "id": "1034",
    "text": "Los Rios",
    "country_id": "63",
    "value": "Los Rios"
  },
  {
    "id": "1035",
    "text": "Manabi",
    "country_id": "63",
    "value": "Manabi"
  },
  {
    "id": "1036",
    "text": "Morona Santiago",
    "country_id": "63",
    "value": "Morona Santiago"
  },
  {
    "id": "1037",
    "text": "Napo",
    "country_id": "63",
    "value": "Napo"
  },
  {
    "id": "1038",
    "text": "Orellana",
    "country_id": "63",
    "value": "Orellana"
  },
  {
    "id": "1039",
    "text": "Pastaza",
    "country_id": "63",
    "value": "Pastaza"
  },
  {
    "id": "1040",
    "text": "Pichincha",
    "country_id": "63",
    "value": "Pichincha"
  },
  {
    "id": "1041",
    "text": "Sucumbios",
    "country_id": "63",
    "value": "Sucumbios"
  },
  {
    "id": "1042",
    "text": "Tungurahua",
    "country_id": "63",
    "value": "Tungurahua"
  },
  {
    "id": "1043",
    "text": "Zamora Chinchipe",
    "country_id": "63",
    "value": "Zamora Chinchipe"
  },
  {
    "id": "1044",
    "text": "Aswan",
    "country_id": "64",
    "value": "Aswan"
  },
  {
    "id": "1045",
    "text": "Asyut",
    "country_id": "64",
    "value": "Asyut"
  },
  {
    "id": "1046",
    "text": "Bani Suwayf",
    "country_id": "64",
    "value": "Bani Suwayf"
  },
  {
    "id": "1047",
    "text": "Bur Sa''id",
    "country_id": "64",
    "value": "Bur Sa''id"
  },
  {
    "id": "1048",
    "text": "Cairo",
    "country_id": "64",
    "value": "Cairo"
  },
  {
    "id": "1049",
    "text": "Dumyat",
    "country_id": "64",
    "value": "Dumyat"
  },
  {
    "id": "1050",
    "text": "Kafr-ash-Shaykh",
    "country_id": "64",
    "value": "Kafr-ash-Shaykh"
  },
  {
    "id": "1051",
    "text": "Matruh",
    "country_id": "64",
    "value": "Matruh"
  },
  {
    "id": "1052",
    "text": "Muhafazat ad Daqahliyah",
    "country_id": "64",
    "value": "Muhafazat ad Daqahliyah"
  },
  {
    "id": "1053",
    "text": "Muhafazat al Fayyum",
    "country_id": "64",
    "value": "Muhafazat al Fayyum"
  },
  {
    "id": "1054",
    "text": "Muhafazat al Gharbiyah",
    "country_id": "64",
    "value": "Muhafazat al Gharbiyah"
  },
  {
    "id": "1055",
    "text": "Muhafazat al Iskandariyah",
    "country_id": "64",
    "value": "Muhafazat al Iskandariyah"
  },
  {
    "id": "1056",
    "text": "Muhafazat al Qahirah",
    "country_id": "64",
    "value": "Muhafazat al Qahirah"
  },
  {
    "id": "1057",
    "text": "Qina",
    "country_id": "64",
    "value": "Qina"
  },
  {
    "id": "1058",
    "text": "Sawhaj",
    "country_id": "64",
    "value": "Sawhaj"
  },
  {
    "id": "1059",
    "text": "Sina al-Janubiyah",
    "country_id": "64",
    "value": "Sina al-Janubiyah"
  },
  {
    "id": "1060",
    "text": "Sina ash-Shamaliyah",
    "country_id": "64",
    "value": "Sina ash-Shamaliyah"
  },
  {
    "id": "1061",
    "text": "ad-Daqahliyah",
    "country_id": "64",
    "value": "ad-Daqahliyah"
  },
  {
    "id": "1062",
    "text": "al-Bahr-al-Ahmar",
    "country_id": "64",
    "value": "al-Bahr-al-Ahmar"
  },
  {
    "id": "1063",
    "text": "al-Buhayrah",
    "country_id": "64",
    "value": "al-Buhayrah"
  },
  {
    "id": "1064",
    "text": "al-Fayyum",
    "country_id": "64",
    "value": "al-Fayyum"
  },
  {
    "id": "1065",
    "text": "al-Gharbiyah",
    "country_id": "64",
    "value": "al-Gharbiyah"
  },
  {
    "id": "1066",
    "text": "al-Iskandariyah",
    "country_id": "64",
    "value": "al-Iskandariyah"
  },
  {
    "id": "1067",
    "text": "al-Ismailiyah",
    "country_id": "64",
    "value": "al-Ismailiyah"
  },
  {
    "id": "1068",
    "text": "al-Jizah",
    "country_id": "64",
    "value": "al-Jizah"
  },
  {
    "id": "1069",
    "text": "al-Minufiyah",
    "country_id": "64",
    "value": "al-Minufiyah"
  },
  {
    "id": "1070",
    "text": "al-Minya",
    "country_id": "64",
    "value": "al-Minya"
  },
  {
    "id": "1071",
    "text": "al-Qahira",
    "country_id": "64",
    "value": "al-Qahira"
  },
  {
    "id": "1072",
    "text": "al-Qalyubiyah",
    "country_id": "64",
    "value": "al-Qalyubiyah"
  },
  {
    "id": "1073",
    "text": "al-Uqsur",
    "country_id": "64",
    "value": "al-Uqsur"
  },
  {
    "id": "1074",
    "text": "al-Wadi al-Jadid",
    "country_id": "64",
    "value": "al-Wadi al-Jadid"
  },
  {
    "id": "1075",
    "text": "as-Suways",
    "country_id": "64",
    "value": "as-Suways"
  },
  {
    "id": "1076",
    "text": "ash-Sharqiyah",
    "country_id": "64",
    "value": "ash-Sharqiyah"
  },
  {
    "id": "1077",
    "text": "Ahuachapan",
    "country_id": "65",
    "value": "Ahuachapan"
  },
  {
    "id": "1078",
    "text": "Cabanas",
    "country_id": "65",
    "value": "Cabanas"
  },
  {
    "id": "1079",
    "text": "Chalatenango",
    "country_id": "65",
    "value": "Chalatenango"
  },
  {
    "id": "1080",
    "text": "Cuscatlan",
    "country_id": "65",
    "value": "Cuscatlan"
  },
  {
    "id": "1081",
    "text": "La Libertad",
    "country_id": "65",
    "value": "La Libertad"
  },
  {
    "id": "1082",
    "text": "La Paz",
    "country_id": "65",
    "value": "La Paz"
  },
  {
    "id": "1083",
    "text": "La Union",
    "country_id": "65",
    "value": "La Union"
  },
  {
    "id": "1084",
    "text": "Morazan",
    "country_id": "65",
    "value": "Morazan"
  },
  {
    "id": "1085",
    "text": "San Miguel",
    "country_id": "65",
    "value": "San Miguel"
  },
  {
    "id": "1086",
    "text": "San Salvador",
    "country_id": "65",
    "value": "San Salvador"
  },
  {
    "id": "1087",
    "text": "San Vicente",
    "country_id": "65",
    "value": "San Vicente"
  },
  {
    "id": "1088",
    "text": "Santa Ana",
    "country_id": "65",
    "value": "Santa Ana"
  },
  {
    "id": "1089",
    "text": "Sonsonate",
    "country_id": "65",
    "value": "Sonsonate"
  },
  {
    "id": "1090",
    "text": "Usulutan",
    "country_id": "65",
    "value": "Usulutan"
  },
  {
    "id": "1091",
    "text": "Annobon",
    "country_id": "66",
    "value": "Annobon"
  },
  {
    "id": "1092",
    "text": "Bioko Norte",
    "country_id": "66",
    "value": "Bioko Norte"
  },
  {
    "id": "1093",
    "text": "Bioko Sur",
    "country_id": "66",
    "value": "Bioko Sur"
  },
  {
    "id": "1094",
    "text": "Centro Sur",
    "country_id": "66",
    "value": "Centro Sur"
  },
  {
    "id": "1095",
    "text": "Kie-Ntem",
    "country_id": "66",
    "value": "Kie-Ntem"
  },
  {
    "id": "1096",
    "text": "Litoral",
    "country_id": "66",
    "value": "Litoral"
  },
  {
    "id": "1097",
    "text": "Wele-Nzas",
    "country_id": "66",
    "value": "Wele-Nzas"
  },
  {
    "id": "1098",
    "text": "Anseba",
    "country_id": "67",
    "value": "Anseba"
  },
  {
    "id": "1099",
    "text": "Debub",
    "country_id": "67",
    "value": "Debub"
  },
  {
    "id": "1100",
    "text": "Debub-Keih-Bahri",
    "country_id": "67",
    "value": "Debub-Keih-Bahri"
  },
  {
    "id": "1101",
    "text": "Gash-Barka",
    "country_id": "67",
    "value": "Gash-Barka"
  },
  {
    "id": "1102",
    "text": "Maekel",
    "country_id": "67",
    "value": "Maekel"
  },
  {
    "id": "1103",
    "text": "Semien-Keih-Bahri",
    "country_id": "67",
    "value": "Semien-Keih-Bahri"
  },
  {
    "id": "1104",
    "text": "Harju",
    "country_id": "68",
    "value": "Harju"
  },
  {
    "id": "1105",
    "text": "Hiiu",
    "country_id": "68",
    "value": "Hiiu"
  },
  {
    "id": "1106",
    "text": "Ida-Viru",
    "country_id": "68",
    "value": "Ida-Viru"
  },
  {
    "id": "1107",
    "text": "Jarva",
    "country_id": "68",
    "value": "Jarva"
  },
  {
    "id": "1108",
    "text": "Jogeva",
    "country_id": "68",
    "value": "Jogeva"
  },
  {
    "id": "1109",
    "text": "Laane",
    "country_id": "68",
    "value": "Laane"
  },
  {
    "id": "1110",
    "text": "Laane-Viru",
    "country_id": "68",
    "value": "Laane-Viru"
  },
  {
    "id": "1111",
    "text": "Parnu",
    "country_id": "68",
    "value": "Parnu"
  },
  {
    "id": "1112",
    "text": "Polva",
    "country_id": "68",
    "value": "Polva"
  },
  {
    "id": "1113",
    "text": "Rapla",
    "country_id": "68",
    "value": "Rapla"
  },
  {
    "id": "1114",
    "text": "Saare",
    "country_id": "68",
    "value": "Saare"
  },
  {
    "id": "1115",
    "text": "Tartu",
    "country_id": "68",
    "value": "Tartu"
  },
  {
    "id": "1116",
    "text": "Valga",
    "country_id": "68",
    "value": "Valga"
  },
  {
    "id": "1117",
    "text": "Viljandi",
    "country_id": "68",
    "value": "Viljandi"
  },
  {
    "id": "1118",
    "text": "Voru",
    "country_id": "68",
    "value": "Voru"
  },
  {
    "id": "1119",
    "text": "Addis Abeba",
    "country_id": "69",
    "value": "Addis Abeba"
  },
  {
    "id": "1120",
    "text": "Afar",
    "country_id": "69",
    "value": "Afar"
  },
  {
    "id": "1121",
    "text": "Amhara",
    "country_id": "69",
    "value": "Amhara"
  },
  {
    "id": "1122",
    "text": "Benishangul",
    "country_id": "69",
    "value": "Benishangul"
  },
  {
    "id": "1123",
    "text": "Diredawa",
    "country_id": "69",
    "value": "Diredawa"
  },
  {
    "id": "1124",
    "text": "Gambella",
    "country_id": "69",
    "value": "Gambella"
  },
  {
    "id": "1125",
    "text": "Harar",
    "country_id": "69",
    "value": "Harar"
  },
  {
    "id": "1126",
    "text": "Jigjiga",
    "country_id": "69",
    "value": "Jigjiga"
  },
  {
    "id": "1127",
    "text": "Mekele",
    "country_id": "69",
    "value": "Mekele"
  },
  {
    "id": "1128",
    "text": "Oromia",
    "country_id": "69",
    "value": "Oromia"
  },
  {
    "id": "1129",
    "text": "Somali",
    "country_id": "69",
    "value": "Somali"
  },
  {
    "id": "1130",
    "text": "Southern",
    "country_id": "69",
    "value": "Southern"
  },
  {
    "id": "1131",
    "text": "Tigray",
    "country_id": "69",
    "value": "Tigray"
  },
  {
    "id": "1132",
    "text": "Christmas Island",
    "country_id": "70",
    "value": "Christmas Island"
  },
  {
    "id": "1133",
    "text": "Cocos Islands",
    "country_id": "70",
    "value": "Cocos Islands"
  },
  {
    "id": "1134",
    "text": "Coral Sea Islands",
    "country_id": "70",
    "value": "Coral Sea Islands"
  },
  {
    "id": "1135",
    "text": "Falkland Islands",
    "country_id": "71",
    "value": "Falkland Islands"
  },
  {
    "id": "1136",
    "text": "South Georgia",
    "country_id": "71",
    "value": "South Georgia"
  },
  {
    "id": "1137",
    "text": "Klaksvik",
    "country_id": "72",
    "value": "Klaksvik"
  },
  {
    "id": "1138",
    "text": "Nor ara Eysturoy",
    "country_id": "72",
    "value": "Nor ara Eysturoy"
  },
  {
    "id": "1139",
    "text": "Nor oy",
    "country_id": "72",
    "value": "Nor oy"
  },
  {
    "id": "1140",
    "text": "Sandoy",
    "country_id": "72",
    "value": "Sandoy"
  },
  {
    "id": "1141",
    "text": "Streymoy",
    "country_id": "72",
    "value": "Streymoy"
  },
  {
    "id": "1142",
    "text": "Su uroy",
    "country_id": "72",
    "value": "Su uroy"
  },
  {
    "id": "1143",
    "text": "Sy ra Eysturoy",
    "country_id": "72",
    "value": "Sy ra Eysturoy"
  },
  {
    "id": "1144",
    "text": "Torshavn",
    "country_id": "72",
    "value": "Torshavn"
  },
  {
    "id": "1145",
    "text": "Vaga",
    "country_id": "72",
    "value": "Vaga"
  },
  {
    "id": "1146",
    "text": "Central",
    "country_id": "73",
    "value": "Central"
  },
  {
    "id": "1147",
    "text": "Eastern",
    "country_id": "73",
    "value": "Eastern"
  },
  {
    "id": "1148",
    "text": "Northern",
    "country_id": "73",
    "value": "Northern"
  },
  {
    "id": "1149",
    "text": "South Pacific",
    "country_id": "73",
    "value": "South Pacific"
  },
  {
    "id": "1150",
    "text": "Western",
    "country_id": "73",
    "value": "Western"
  },
  {
    "id": "1151",
    "text": "Ahvenanmaa",
    "country_id": "74",
    "value": "Ahvenanmaa"
  },
  {
    "id": "1152",
    "text": "Etela-Karjala",
    "country_id": "74",
    "value": "Etela-Karjala"
  },
  {
    "id": "1153",
    "text": "Etela-Pohjanmaa",
    "country_id": "74",
    "value": "Etela-Pohjanmaa"
  },
  {
    "id": "1154",
    "text": "Etela-Savo",
    "country_id": "74",
    "value": "Etela-Savo"
  },
  {
    "id": "1155",
    "text": "Etela-Suomen Laani",
    "country_id": "74",
    "value": "Etela-Suomen Laani"
  },
  {
    "id": "1156",
    "text": "Ita-Suomen Laani",
    "country_id": "74",
    "value": "Ita-Suomen Laani"
  },
  {
    "id": "1157",
    "text": "Ita-Uusimaa",
    "country_id": "74",
    "value": "Ita-Uusimaa"
  },
  {
    "id": "1158",
    "text": "Kainuu",
    "country_id": "74",
    "value": "Kainuu"
  },
  {
    "id": "1159",
    "text": "Kanta-Hame",
    "country_id": "74",
    "value": "Kanta-Hame"
  },
  {
    "id": "1160",
    "text": "Keski-Pohjanmaa",
    "country_id": "74",
    "value": "Keski-Pohjanmaa"
  },
  {
    "id": "1161",
    "text": "Keski-Suomi",
    "country_id": "74",
    "value": "Keski-Suomi"
  },
  {
    "id": "1162",
    "text": "Kymenlaakso",
    "country_id": "74",
    "value": "Kymenlaakso"
  },
  {
    "id": "1163",
    "text": "Lansi-Suomen Laani",
    "country_id": "74",
    "value": "Lansi-Suomen Laani"
  },
  {
    "id": "1164",
    "text": "Lappi",
    "country_id": "74",
    "value": "Lappi"
  },
  {
    "id": "1165",
    "text": "Northern Savonia",
    "country_id": "74",
    "value": "Northern Savonia"
  },
  {
    "id": "1166",
    "text": "Ostrobothnia",
    "country_id": "74",
    "value": "Ostrobothnia"
  },
  {
    "id": "1167",
    "text": "Oulun Laani",
    "country_id": "74",
    "value": "Oulun Laani"
  },
  {
    "id": "1168",
    "text": "Paijat-Hame",
    "country_id": "74",
    "value": "Paijat-Hame"
  },
  {
    "id": "1169",
    "text": "Pirkanmaa",
    "country_id": "74",
    "value": "Pirkanmaa"
  },
  {
    "id": "1170",
    "text": "Pohjanmaa",
    "country_id": "74",
    "value": "Pohjanmaa"
  },
  {
    "id": "1171",
    "text": "Pohjois-Karjala",
    "country_id": "74",
    "value": "Pohjois-Karjala"
  },
  {
    "id": "1172",
    "text": "Pohjois-Pohjanmaa",
    "country_id": "74",
    "value": "Pohjois-Pohjanmaa"
  },
  {
    "id": "1173",
    "text": "Pohjois-Savo",
    "country_id": "74",
    "value": "Pohjois-Savo"
  },
  {
    "id": "1174",
    "text": "Saarijarvi",
    "country_id": "74",
    "value": "Saarijarvi"
  },
  {
    "id": "1175",
    "text": "Satakunta",
    "country_id": "74",
    "value": "Satakunta"
  },
  {
    "id": "1176",
    "text": "Southern Savonia",
    "country_id": "74",
    "value": "Southern Savonia"
  },
  {
    "id": "1177",
    "text": "Tavastia Proper",
    "country_id": "74",
    "value": "Tavastia Proper"
  },
  {
    "id": "1178",
    "text": "Uleaborgs Lan",
    "country_id": "74",
    "value": "Uleaborgs Lan"
  },
  {
    "id": "1179",
    "text": "Uusimaa",
    "country_id": "74",
    "value": "Uusimaa"
  },
  {
    "id": "1180",
    "text": "Varsinais-Suomi",
    "country_id": "74",
    "value": "Varsinais-Suomi"
  },
  {
    "id": "1181",
    "text": "Ain",
    "country_id": "75",
    "value": "Ain"
  },
  {
    "id": "1182",
    "text": "Aisne",
    "country_id": "75",
    "value": "Aisne"
  },
  {
    "id": "1183",
    "text": "Albi Le Sequestre",
    "country_id": "75",
    "value": "Albi Le Sequestre"
  },
  {
    "id": "1184",
    "text": "Allier",
    "country_id": "75",
    "value": "Allier"
  },
  {
    "id": "1185",
    "text": "Alpes-Cote dAzur",
    "country_id": "75",
    "value": "Alpes-Cote dAzur"
  },
  {
    "id": "1186",
    "text": "Alpes-Maritimes",
    "country_id": "75",
    "value": "Alpes-Maritimes"
  },
  {
    "id": "1187",
    "text": "Alpes-de-Haute-Provence",
    "country_id": "75",
    "value": "Alpes-de-Haute-Provence"
  },
  {
    "id": "1188",
    "text": "Alsace",
    "country_id": "75",
    "value": "Alsace"
  },
  {
    "id": "1189",
    "text": "Aquitaine",
    "country_id": "75",
    "value": "Aquitaine"
  },
  {
    "id": "1190",
    "text": "Ardeche",
    "country_id": "75",
    "value": "Ardeche"
  },
  {
    "id": "1191",
    "text": "Ardennes",
    "country_id": "75",
    "value": "Ardennes"
  },
  {
    "id": "1192",
    "text": "Ariege",
    "country_id": "75",
    "value": "Ariege"
  },
  {
    "id": "1193",
    "text": "Aube",
    "country_id": "75",
    "value": "Aube"
  },
  {
    "id": "1194",
    "text": "Aude",
    "country_id": "75",
    "value": "Aude"
  },
  {
    "id": "1195",
    "text": "Auvergne",
    "country_id": "75",
    "value": "Auvergne"
  },
  {
    "id": "1196",
    "text": "Aveyron",
    "country_id": "75",
    "value": "Aveyron"
  },
  {
    "id": "1197",
    "text": "Bas-Rhin",
    "country_id": "75",
    "value": "Bas-Rhin"
  },
  {
    "id": "1198",
    "text": "Basse-Normandie",
    "country_id": "75",
    "value": "Basse-Normandie"
  },
  {
    "id": "1199",
    "text": "Bouches-du-Rhone",
    "country_id": "75",
    "value": "Bouches-du-Rhone"
  },
  {
    "id": "1200",
    "text": "Bourgogne",
    "country_id": "75",
    "value": "Bourgogne"
  },
  {
    "id": "1201",
    "text": "Bretagne",
    "country_id": "75",
    "value": "Bretagne"
  },
  {
    "id": "1202",
    "text": "Brittany",
    "country_id": "75",
    "value": "Brittany"
  },
  {
    "id": "1203",
    "text": "Burgundy",
    "country_id": "75",
    "value": "Burgundy"
  },
  {
    "id": "1204",
    "text": "Calvados",
    "country_id": "75",
    "value": "Calvados"
  },
  {
    "id": "1205",
    "text": "Cantal",
    "country_id": "75",
    "value": "Cantal"
  },
  {
    "id": "1206",
    "text": "Cedex",
    "country_id": "75",
    "value": "Cedex"
  },
  {
    "id": "1207",
    "text": "Centre",
    "country_id": "75",
    "value": "Centre"
  },
  {
    "id": "1208",
    "text": "Charente",
    "country_id": "75",
    "value": "Charente"
  },
  {
    "id": "1209",
    "text": "Charente-Maritime",
    "country_id": "75",
    "value": "Charente-Maritime"
  },
  {
    "id": "1210",
    "text": "Cher",
    "country_id": "75",
    "value": "Cher"
  },
  {
    "id": "1211",
    "text": "Correze",
    "country_id": "75",
    "value": "Correze"
  },
  {
    "id": "1212",
    "text": "Corse-du-Sud",
    "country_id": "75",
    "value": "Corse-du-Sud"
  },
  {
    "id": "1213",
    "text": "Cote-d''Or",
    "country_id": "75",
    "value": "Cote-d''Or"
  },
  {
    "id": "1214",
    "text": "Cotes-d''Armor",
    "country_id": "75",
    "value": "Cotes-d''Armor"
  },
  {
    "id": "1215",
    "text": "Creuse",
    "country_id": "75",
    "value": "Creuse"
  },
  {
    "id": "1216",
    "text": "Crolles",
    "country_id": "75",
    "value": "Crolles"
  },
  {
    "id": "1217",
    "text": "Deux-Sevres",
    "country_id": "75",
    "value": "Deux-Sevres"
  },
  {
    "id": "1218",
    "text": "Dordogne",
    "country_id": "75",
    "value": "Dordogne"
  },
  {
    "id": "1219",
    "text": "Doubs",
    "country_id": "75",
    "value": "Doubs"
  },
  {
    "id": "1220",
    "text": "Drome",
    "country_id": "75",
    "value": "Drome"
  },
  {
    "id": "1221",
    "text": "Essonne",
    "country_id": "75",
    "value": "Essonne"
  },
  {
    "id": "1222",
    "text": "Eure",
    "country_id": "75",
    "value": "Eure"
  },
  {
    "id": "1223",
    "text": "Eure-et-Loir",
    "country_id": "75",
    "value": "Eure-et-Loir"
  },
  {
    "id": "1224",
    "text": "Feucherolles",
    "country_id": "75",
    "value": "Feucherolles"
  },
  {
    "id": "1225",
    "text": "Finistere",
    "country_id": "75",
    "value": "Finistere"
  },
  {
    "id": "1226",
    "text": "Franche-Comte",
    "country_id": "75",
    "value": "Franche-Comte"
  },
  {
    "id": "1227",
    "text": "Gard",
    "country_id": "75",
    "value": "Gard"
  },
  {
    "id": "1228",
    "text": "Gers",
    "country_id": "75",
    "value": "Gers"
  },
  {
    "id": "1229",
    "text": "Gironde",
    "country_id": "75",
    "value": "Gironde"
  },
  {
    "id": "1230",
    "text": "Haut-Rhin",
    "country_id": "75",
    "value": "Haut-Rhin"
  },
  {
    "id": "1231",
    "text": "Haute-Corse",
    "country_id": "75",
    "value": "Haute-Corse"
  },
  {
    "id": "1232",
    "text": "Haute-Garonne",
    "country_id": "75",
    "value": "Haute-Garonne"
  },
  {
    "id": "1233",
    "text": "Haute-Loire",
    "country_id": "75",
    "value": "Haute-Loire"
  },
  {
    "id": "1234",
    "text": "Haute-Marne",
    "country_id": "75",
    "value": "Haute-Marne"
  },
  {
    "id": "1235",
    "text": "Haute-Saone",
    "country_id": "75",
    "value": "Haute-Saone"
  },
  {
    "id": "1236",
    "text": "Haute-Savoie",
    "country_id": "75",
    "value": "Haute-Savoie"
  },
  {
    "id": "1237",
    "text": "Haute-Vienne",
    "country_id": "75",
    "value": "Haute-Vienne"
  },
  {
    "id": "1238",
    "text": "Hautes-Alpes",
    "country_id": "75",
    "value": "Hautes-Alpes"
  },
  {
    "id": "1239",
    "text": "Hautes-Pyrenees",
    "country_id": "75",
    "value": "Hautes-Pyrenees"
  },
  {
    "id": "1240",
    "text": "Hauts-de-Seine",
    "country_id": "75",
    "value": "Hauts-de-Seine"
  },
  {
    "id": "1241",
    "text": "Herault",
    "country_id": "75",
    "value": "Herault"
  },
  {
    "id": "1242",
    "text": "Ile-de-France",
    "country_id": "75",
    "value": "Ile-de-France"
  },
  {
    "id": "1243",
    "text": "Ille-et-Vilaine",
    "country_id": "75",
    "value": "Ille-et-Vilaine"
  },
  {
    "id": "1244",
    "text": "Indre",
    "country_id": "75",
    "value": "Indre"
  },
  {
    "id": "1245",
    "text": "Indre-et-Loire",
    "country_id": "75",
    "value": "Indre-et-Loire"
  },
  {
    "id": "1246",
    "text": "Isere",
    "country_id": "75",
    "value": "Isere"
  },
  {
    "id": "1247",
    "text": "Jura",
    "country_id": "75",
    "value": "Jura"
  },
  {
    "id": "1248",
    "text": "Klagenfurt",
    "country_id": "75",
    "value": "Klagenfurt"
  },
  {
    "id": "1249",
    "text": "Landes",
    "country_id": "75",
    "value": "Landes"
  },
  {
    "id": "1250",
    "text": "Languedoc-Roussillon",
    "country_id": "75",
    "value": "Languedoc-Roussillon"
  },
  {
    "id": "1251",
    "text": "Larcay",
    "country_id": "75",
    "value": "Larcay"
  },
  {
    "id": "1252",
    "text": "Le Castellet",
    "country_id": "75",
    "value": "Le Castellet"
  },
  {
    "id": "1253",
    "text": "Le Creusot",
    "country_id": "75",
    "value": "Le Creusot"
  },
  {
    "id": "1254",
    "text": "Limousin",
    "country_id": "75",
    "value": "Limousin"
  },
  {
    "id": "1255",
    "text": "Loir-et-Cher",
    "country_id": "75",
    "value": "Loir-et-Cher"
  },
  {
    "id": "1256",
    "text": "Loire",
    "country_id": "75",
    "value": "Loire"
  },
  {
    "id": "1257",
    "text": "Loire-Atlantique",
    "country_id": "75",
    "value": "Loire-Atlantique"
  },
  {
    "id": "1258",
    "text": "Loiret",
    "country_id": "75",
    "value": "Loiret"
  },
  {
    "id": "1259",
    "text": "Lorraine",
    "country_id": "75",
    "value": "Lorraine"
  },
  {
    "id": "1260",
    "text": "Lot",
    "country_id": "75",
    "value": "Lot"
  },
  {
    "id": "1261",
    "text": "Lot-et-Garonne",
    "country_id": "75",
    "value": "Lot-et-Garonne"
  },
  {
    "id": "1262",
    "text": "Lower Normandy",
    "country_id": "75",
    "value": "Lower Normandy"
  },
  {
    "id": "1263",
    "text": "Lozere",
    "country_id": "75",
    "value": "Lozere"
  },
  {
    "id": "1264",
    "text": "Maine-et-Loire",
    "country_id": "75",
    "value": "Maine-et-Loire"
  },
  {
    "id": "1265",
    "text": "Manche",
    "country_id": "75",
    "value": "Manche"
  },
  {
    "id": "1266",
    "text": "Marne",
    "country_id": "75",
    "value": "Marne"
  },
  {
    "id": "1267",
    "text": "Mayenne",
    "country_id": "75",
    "value": "Mayenne"
  },
  {
    "id": "1268",
    "text": "Meurthe-et-Moselle",
    "country_id": "75",
    "value": "Meurthe-et-Moselle"
  },
  {
    "id": "1269",
    "text": "Meuse",
    "country_id": "75",
    "value": "Meuse"
  },
  {
    "id": "1270",
    "text": "Midi-Pyrenees",
    "country_id": "75",
    "value": "Midi-Pyrenees"
  },
  {
    "id": "1271",
    "text": "Morbihan",
    "country_id": "75",
    "value": "Morbihan"
  },
  {
    "id": "1272",
    "text": "Moselle",
    "country_id": "75",
    "value": "Moselle"
  },
  {
    "id": "1273",
    "text": "Nievre",
    "country_id": "75",
    "value": "Nievre"
  },
  {
    "id": "1274",
    "text": "Nord",
    "country_id": "75",
    "value": "Nord"
  },
  {
    "id": "1275",
    "text": "Nord-Pas-de-Calais",
    "country_id": "75",
    "value": "Nord-Pas-de-Calais"
  },
  {
    "id": "1276",
    "text": "Oise",
    "country_id": "75",
    "value": "Oise"
  },
  {
    "id": "1277",
    "text": "Orne",
    "country_id": "75",
    "value": "Orne"
  },
  {
    "id": "1278",
    "text": "Paris",
    "country_id": "75",
    "value": "Paris"
  },
  {
    "id": "1279",
    "text": "Pas-de-Calais",
    "country_id": "75",
    "value": "Pas-de-Calais"
  },
  {
    "id": "1280",
    "text": "Pays de la Loire",
    "country_id": "75",
    "value": "Pays de la Loire"
  },
  {
    "id": "1281",
    "text": "Pays-de-la-Loire",
    "country_id": "75",
    "value": "Pays-de-la-Loire"
  },
  {
    "id": "1282",
    "text": "Picardy",
    "country_id": "75",
    "value": "Picardy"
  },
  {
    "id": "1283",
    "text": "Puy-de-Dome",
    "country_id": "75",
    "value": "Puy-de-Dome"
  },
  {
    "id": "1284",
    "text": "Pyrenees-Atlantiques",
    "country_id": "75",
    "value": "Pyrenees-Atlantiques"
  },
  {
    "id": "1285",
    "text": "Pyrenees-Orientales",
    "country_id": "75",
    "value": "Pyrenees-Orientales"
  },
  {
    "id": "1286",
    "text": "Quelmes",
    "country_id": "75",
    "value": "Quelmes"
  },
  {
    "id": "1287",
    "text": "Rhone",
    "country_id": "75",
    "value": "Rhone"
  },
  {
    "id": "1288",
    "text": "Rhone-Alpes",
    "country_id": "75",
    "value": "Rhone-Alpes"
  },
  {
    "id": "1289",
    "text": "Saint Ouen",
    "country_id": "75",
    "value": "Saint Ouen"
  },
  {
    "id": "1290",
    "text": "Saint Viatre",
    "country_id": "75",
    "value": "Saint Viatre"
  },
  {
    "id": "1291",
    "text": "Saone-et-Loire",
    "country_id": "75",
    "value": "Saone-et-Loire"
  },
  {
    "id": "1292",
    "text": "Sarthe",
    "country_id": "75",
    "value": "Sarthe"
  },
  {
    "id": "1293",
    "text": "Savoie",
    "country_id": "75",
    "value": "Savoie"
  },
  {
    "id": "1294",
    "text": "Seine-Maritime",
    "country_id": "75",
    "value": "Seine-Maritime"
  },
  {
    "id": "1295",
    "text": "Seine-Saint-Denis",
    "country_id": "75",
    "value": "Seine-Saint-Denis"
  },
  {
    "id": "1296",
    "text": "Seine-et-Marne",
    "country_id": "75",
    "value": "Seine-et-Marne"
  },
  {
    "id": "1297",
    "text": "Somme",
    "country_id": "75",
    "value": "Somme"
  },
  {
    "id": "1298",
    "text": "Sophia Antipolis",
    "country_id": "75",
    "value": "Sophia Antipolis"
  },
  {
    "id": "1299",
    "text": "Souvans",
    "country_id": "75",
    "value": "Souvans"
  },
  {
    "id": "1300",
    "text": "Tarn",
    "country_id": "75",
    "value": "Tarn"
  },
  {
    "id": "1301",
    "text": "Tarn-et-Garonne",
    "country_id": "75",
    "value": "Tarn-et-Garonne"
  },
  {
    "id": "1302",
    "text": "Territoire de Belfort",
    "country_id": "75",
    "value": "Territoire de Belfort"
  },
  {
    "id": "1303",
    "text": "Treignac",
    "country_id": "75",
    "value": "Treignac"
  },
  {
    "id": "1304",
    "text": "Upper Normandy",
    "country_id": "75",
    "value": "Upper Normandy"
  },
  {
    "id": "1305",
    "text": "Val-d''Oise",
    "country_id": "75",
    "value": "Val-d''Oise"
  },
  {
    "id": "1306",
    "text": "Val-de-Marne",
    "country_id": "75",
    "value": "Val-de-Marne"
  },
  {
    "id": "1307",
    "text": "Var",
    "country_id": "75",
    "value": "Var"
  },
  {
    "id": "1308",
    "text": "Vaucluse",
    "country_id": "75",
    "value": "Vaucluse"
  },
  {
    "id": "1309",
    "text": "Vellise",
    "country_id": "75",
    "value": "Vellise"
  },
  {
    "id": "1310",
    "text": "Vendee",
    "country_id": "75",
    "value": "Vendee"
  },
  {
    "id": "1311",
    "text": "Vienne",
    "country_id": "75",
    "value": "Vienne"
  },
  {
    "id": "1312",
    "text": "Vosges",
    "country_id": "75",
    "value": "Vosges"
  },
  {
    "id": "1313",
    "text": "Yonne",
    "country_id": "75",
    "value": "Yonne"
  },
  {
    "id": "1314",
    "text": "Yvelines",
    "country_id": "75",
    "value": "Yvelines"
  },
  {
    "id": "1315",
    "text": "Cayenne",
    "country_id": "76",
    "value": "Cayenne"
  },
  {
    "id": "1316",
    "text": "Saint-Laurent-du-Maroni",
    "country_id": "76",
    "value": "Saint-Laurent-du-Maroni"
  },
  {
    "id": "1317",
    "text": "Iles du Vent",
    "country_id": "77",
    "value": "Iles du Vent"
  },
  {
    "id": "1318",
    "text": "Iles sous le Vent",
    "country_id": "77",
    "value": "Iles sous le Vent"
  },
  {
    "id": "1319",
    "text": "Marquesas",
    "country_id": "77",
    "value": "Marquesas"
  },
  {
    "id": "1320",
    "text": "Tuamotu",
    "country_id": "77",
    "value": "Tuamotu"
  },
  {
    "id": "1321",
    "text": "Tubuai",
    "country_id": "77",
    "value": "Tubuai"
  },
  {
    "id": "1322",
    "text": "Amsterdam",
    "country_id": "78",
    "value": "Amsterdam"
  },
  {
    "id": "1323",
    "text": "Crozet Islands",
    "country_id": "78",
    "value": "Crozet Islands"
  },
  {
    "id": "1324",
    "text": "Kerguelen",
    "country_id": "78",
    "value": "Kerguelen"
  },
  {
    "id": "1325",
    "text": "Estuaire",
    "country_id": "79",
    "value": "Estuaire"
  },
  {
    "id": "1326",
    "text": "Haut-Ogooue",
    "country_id": "79",
    "value": "Haut-Ogooue"
  },
  {
    "id": "1327",
    "text": "Moyen-Ogooue",
    "country_id": "79",
    "value": "Moyen-Ogooue"
  },
  {
    "id": "1328",
    "text": "Ngounie",
    "country_id": "79",
    "value": "Ngounie"
  },
  {
    "id": "1329",
    "text": "Nyanga",
    "country_id": "79",
    "value": "Nyanga"
  },
  {
    "id": "1330",
    "text": "Ogooue-Ivindo",
    "country_id": "79",
    "value": "Ogooue-Ivindo"
  },
  {
    "id": "1331",
    "text": "Ogooue-Lolo",
    "country_id": "79",
    "value": "Ogooue-Lolo"
  },
  {
    "id": "1332",
    "text": "Ogooue-Maritime",
    "country_id": "79",
    "value": "Ogooue-Maritime"
  },
  {
    "id": "1333",
    "text": "Woleu-Ntem",
    "country_id": "79",
    "value": "Woleu-Ntem"
  },
  {
    "id": "1334",
    "text": "Banjul",
    "country_id": "80",
    "value": "Banjul"
  },
  {
    "id": "1335",
    "text": "Basse",
    "country_id": "80",
    "value": "Basse"
  },
  {
    "id": "1336",
    "text": "Brikama",
    "country_id": "80",
    "value": "Brikama"
  },
  {
    "id": "1337",
    "text": "Janjanbureh",
    "country_id": "80",
    "value": "Janjanbureh"
  },
  {
    "id": "1338",
    "text": "Kanifing",
    "country_id": "80",
    "value": "Kanifing"
  },
  {
    "id": "1339",
    "text": "Kerewan",
    "country_id": "80",
    "value": "Kerewan"
  },
  {
    "id": "1340",
    "text": "Kuntaur",
    "country_id": "80",
    "value": "Kuntaur"
  },
  {
    "id": "1341",
    "text": "Mansakonko",
    "country_id": "80",
    "value": "Mansakonko"
  },
  {
    "id": "1342",
    "text": "Abhasia",
    "country_id": "81",
    "value": "Abhasia"
  },
  {
    "id": "1343",
    "text": "Ajaria",
    "country_id": "81",
    "value": "Ajaria"
  },
  {
    "id": "1344",
    "text": "Guria",
    "country_id": "81",
    "value": "Guria"
  },
  {
    "id": "1345",
    "text": "Imereti",
    "country_id": "81",
    "value": "Imereti"
  },
  {
    "id": "1346",
    "text": "Kaheti",
    "country_id": "81",
    "value": "Kaheti"
  },
  {
    "id": "1347",
    "text": "Kvemo Kartli",
    "country_id": "81",
    "value": "Kvemo Kartli"
  },
  {
    "id": "1348",
    "text": "Mcheta-Mtianeti",
    "country_id": "81",
    "value": "Mcheta-Mtianeti"
  },
  {
    "id": "1349",
    "text": "Racha",
    "country_id": "81",
    "value": "Racha"
  },
  {
    "id": "1350",
    "text": "Samagrelo-Zemo Svaneti",
    "country_id": "81",
    "value": "Samagrelo-Zemo Svaneti"
  },
  {
    "id": "1351",
    "text": "Samche-Zhavaheti",
    "country_id": "81",
    "value": "Samche-Zhavaheti"
  },
  {
    "id": "1352",
    "text": "Shida Kartli",
    "country_id": "81",
    "value": "Shida Kartli"
  },
  {
    "id": "1353",
    "text": "Tbilisi",
    "country_id": "81",
    "value": "Tbilisi"
  },
  {
    "id": "1354",
    "text": "Auvergne",
    "country_id": "82",
    "value": "Auvergne"
  },
  {
    "id": "1355",
    "text": "Baden-Wurttemberg",
    "country_id": "82",
    "value": "Baden-Wurttemberg"
  },
  {
    "id": "1356",
    "text": "Bavaria",
    "country_id": "82",
    "value": "Bavaria"
  },
  {
    "id": "1357",
    "text": "Bayern",
    "country_id": "82",
    "value": "Bayern"
  },
  {
    "id": "1358",
    "text": "Beilstein Wurtt",
    "country_id": "82",
    "value": "Beilstein Wurtt"
  },
  {
    "id": "1359",
    "text": "Berlin",
    "country_id": "82",
    "value": "Berlin"
  },
  {
    "id": "1360",
    "text": "Brandenburg",
    "country_id": "82",
    "value": "Brandenburg"
  },
  {
    "id": "1361",
    "text": "Bremen",
    "country_id": "82",
    "value": "Bremen"
  },
  {
    "id": "1362",
    "text": "Dreisbach",
    "country_id": "82",
    "value": "Dreisbach"
  },
  {
    "id": "1363",
    "text": "Freistaat Bayern",
    "country_id": "82",
    "value": "Freistaat Bayern"
  },
  {
    "id": "1364",
    "text": "Hamburg",
    "country_id": "82",
    "value": "Hamburg"
  },
  {
    "id": "1365",
    "text": "Hannover",
    "country_id": "82",
    "value": "Hannover"
  },
  {
    "id": "1366",
    "text": "Heroldstatt",
    "country_id": "82",
    "value": "Heroldstatt"
  },
  {
    "id": "1367",
    "text": "Hessen",
    "country_id": "82",
    "value": "Hessen"
  },
  {
    "id": "1368",
    "text": "Kortenberg",
    "country_id": "82",
    "value": "Kortenberg"
  },
  {
    "id": "1369",
    "text": "Laasdorf",
    "country_id": "82",
    "value": "Laasdorf"
  },
  {
    "id": "1370",
    "text": "Land Baden-Wurttemberg",
    "country_id": "82",
    "value": "Land Baden-Wurttemberg"
  },
  {
    "id": "1371",
    "text": "Land Bayern",
    "country_id": "82",
    "value": "Land Bayern"
  },
  {
    "id": "1372",
    "text": "Land Brandenburg",
    "country_id": "82",
    "value": "Land Brandenburg"
  },
  {
    "id": "1373",
    "text": "Land Hessen",
    "country_id": "82",
    "value": "Land Hessen"
  },
  {
    "id": "1374",
    "text": "Land Mecklenburg-Vorpommern",
    "country_id": "82",
    "value": "Land Mecklenburg-Vorpommern"
  },
  {
    "id": "1375",
    "text": "Land Nordrhein-Westfalen",
    "country_id": "82",
    "value": "Land Nordrhein-Westfalen"
  },
  {
    "id": "1376",
    "text": "Land Rheinland-Pfalz",
    "country_id": "82",
    "value": "Land Rheinland-Pfalz"
  },
  {
    "id": "1377",
    "text": "Land Sachsen",
    "country_id": "82",
    "value": "Land Sachsen"
  },
  {
    "id": "1378",
    "text": "Land Sachsen-Anhalt",
    "country_id": "82",
    "value": "Land Sachsen-Anhalt"
  },
  {
    "id": "1379",
    "text": "Land Thuringen",
    "country_id": "82",
    "value": "Land Thuringen"
  },
  {
    "id": "1380",
    "text": "Lower Saxony",
    "country_id": "82",
    "value": "Lower Saxony"
  },
  {
    "id": "1381",
    "text": "Mecklenburg-Vorpommern",
    "country_id": "82",
    "value": "Mecklenburg-Vorpommern"
  },
  {
    "id": "1382",
    "text": "Mulfingen",
    "country_id": "82",
    "value": "Mulfingen"
  },
  {
    "id": "1383",
    "text": "Munich",
    "country_id": "82",
    "value": "Munich"
  },
  {
    "id": "1384",
    "text": "Neubeuern",
    "country_id": "82",
    "value": "Neubeuern"
  },
  {
    "id": "1385",
    "text": "Niedersachsen",
    "country_id": "82",
    "value": "Niedersachsen"
  },
  {
    "id": "1386",
    "text": "Noord-Holland",
    "country_id": "82",
    "value": "Noord-Holland"
  },
  {
    "id": "1387",
    "text": "Nordrhein-Westfalen",
    "country_id": "82",
    "value": "Nordrhein-Westfalen"
  },
  {
    "id": "1388",
    "text": "North Rhine-Westphalia",
    "country_id": "82",
    "value": "North Rhine-Westphalia"
  },
  {
    "id": "1389",
    "text": "Osterode",
    "country_id": "82",
    "value": "Osterode"
  },
  {
    "id": "1390",
    "text": "Rheinland-Pfalz",
    "country_id": "82",
    "value": "Rheinland-Pfalz"
  },
  {
    "id": "1391",
    "text": "Rhineland-Palatinate",
    "country_id": "82",
    "value": "Rhineland-Palatinate"
  },
  {
    "id": "1392",
    "text": "Saarland",
    "country_id": "82",
    "value": "Saarland"
  },
  {
    "id": "1393",
    "text": "Sachsen",
    "country_id": "82",
    "value": "Sachsen"
  },
  {
    "id": "1394",
    "text": "Sachsen-Anhalt",
    "country_id": "82",
    "value": "Sachsen-Anhalt"
  },
  {
    "id": "1395",
    "text": "Saxony",
    "country_id": "82",
    "value": "Saxony"
  },
  {
    "id": "1396",
    "text": "Schleswig-Holstein",
    "country_id": "82",
    "value": "Schleswig-Holstein"
  },
  {
    "id": "1397",
    "text": "Thuringia",
    "country_id": "82",
    "value": "Thuringia"
  },
  {
    "id": "1398",
    "text": "Webling",
    "country_id": "82",
    "value": "Webling"
  },
  {
    "id": "1399",
    "text": "Weinstrabe",
    "country_id": "82",
    "value": "Weinstrabe"
  },
  {
    "id": "1400",
    "text": "schlobborn",
    "country_id": "82",
    "value": "schlobborn"
  },
  {
    "id": "1401",
    "text": "Ashanti",
    "country_id": "83",
    "value": "Ashanti"
  },
  {
    "id": "1402",
    "text": "Brong-Ahafo",
    "country_id": "83",
    "value": "Brong-Ahafo"
  },
  {
    "id": "1403",
    "text": "Central",
    "country_id": "83",
    "value": "Central"
  },
  {
    "id": "1404",
    "text": "Eastern",
    "country_id": "83",
    "value": "Eastern"
  },
  {
    "id": "1405",
    "text": "Greater Accra",
    "country_id": "83",
    "value": "Greater Accra"
  },
  {
    "id": "1406",
    "text": "Northern",
    "country_id": "83",
    "value": "Northern"
  },
  {
    "id": "1407",
    "text": "Upper East",
    "country_id": "83",
    "value": "Upper East"
  },
  {
    "id": "1408",
    "text": "Upper West",
    "country_id": "83",
    "value": "Upper West"
  },
  {
    "id": "1409",
    "text": "Volta",
    "country_id": "83",
    "value": "Volta"
  },
  {
    "id": "1410",
    "text": "Western",
    "country_id": "83",
    "value": "Western"
  },
  {
    "id": "1411",
    "text": "Gibraltar",
    "country_id": "84",
    "value": "Gibraltar"
  },
  {
    "id": "1412",
    "text": "Acharnes",
    "country_id": "85",
    "value": "Acharnes"
  },
  {
    "id": "1413",
    "text": "Ahaia",
    "country_id": "85",
    "value": "Ahaia"
  },
  {
    "id": "1414",
    "text": "Aitolia kai Akarnania",
    "country_id": "85",
    "value": "Aitolia kai Akarnania"
  },
  {
    "id": "1415",
    "text": "Argolis",
    "country_id": "85",
    "value": "Argolis"
  },
  {
    "id": "1416",
    "text": "Arkadia",
    "country_id": "85",
    "value": "Arkadia"
  },
  {
    "id": "1417",
    "text": "Arta",
    "country_id": "85",
    "value": "Arta"
  },
  {
    "id": "1418",
    "text": "Attica",
    "country_id": "85",
    "value": "Attica"
  },
  {
    "id": "1419",
    "text": "Attiki",
    "country_id": "85",
    "value": "Attiki"
  },
  {
    "id": "1420",
    "text": "Ayion Oros",
    "country_id": "85",
    "value": "Ayion Oros"
  },
  {
    "id": "1421",
    "text": "Crete",
    "country_id": "85",
    "value": "Crete"
  },
  {
    "id": "1422",
    "text": "Dodekanisos",
    "country_id": "85",
    "value": "Dodekanisos"
  },
  {
    "id": "1423",
    "text": "Drama",
    "country_id": "85",
    "value": "Drama"
  },
  {
    "id": "1424",
    "text": "Evia",
    "country_id": "85",
    "value": "Evia"
  },
  {
    "id": "1425",
    "text": "Evritania",
    "country_id": "85",
    "value": "Evritania"
  },
  {
    "id": "1426",
    "text": "Evros",
    "country_id": "85",
    "value": "Evros"
  },
  {
    "id": "1427",
    "text": "Evvoia",
    "country_id": "85",
    "value": "Evvoia"
  },
  {
    "id": "1428",
    "text": "Florina",
    "country_id": "85",
    "value": "Florina"
  },
  {
    "id": "1429",
    "text": "Fokis",
    "country_id": "85",
    "value": "Fokis"
  },
  {
    "id": "1430",
    "text": "Fthiotis",
    "country_id": "85",
    "value": "Fthiotis"
  },
  {
    "id": "1431",
    "text": "Grevena",
    "country_id": "85",
    "value": "Grevena"
  },
  {
    "id": "1432",
    "text": "Halandri",
    "country_id": "85",
    "value": "Halandri"
  },
  {
    "id": "1433",
    "text": "Halkidiki",
    "country_id": "85",
    "value": "Halkidiki"
  },
  {
    "id": "1434",
    "text": "Hania",
    "country_id": "85",
    "value": "Hania"
  },
  {
    "id": "1435",
    "text": "Heraklion",
    "country_id": "85",
    "value": "Heraklion"
  },
  {
    "id": "1436",
    "text": "Hios",
    "country_id": "85",
    "value": "Hios"
  },
  {
    "id": "1437",
    "text": "Ilia",
    "country_id": "85",
    "value": "Ilia"
  },
  {
    "id": "1438",
    "text": "Imathia",
    "country_id": "85",
    "value": "Imathia"
  },
  {
    "id": "1439",
    "text": "Ioannina",
    "country_id": "85",
    "value": "Ioannina"
  },
  {
    "id": "1440",
    "text": "Iraklion",
    "country_id": "85",
    "value": "Iraklion"
  },
  {
    "id": "1441",
    "text": "Karditsa",
    "country_id": "85",
    "value": "Karditsa"
  },
  {
    "id": "1442",
    "text": "Kastoria",
    "country_id": "85",
    "value": "Kastoria"
  },
  {
    "id": "1443",
    "text": "Kavala",
    "country_id": "85",
    "value": "Kavala"
  },
  {
    "id": "1444",
    "text": "Kefallinia",
    "country_id": "85",
    "value": "Kefallinia"
  },
  {
    "id": "1445",
    "text": "Kerkira",
    "country_id": "85",
    "value": "Kerkira"
  },
  {
    "id": "1446",
    "text": "Kiklades",
    "country_id": "85",
    "value": "Kiklades"
  },
  {
    "id": "1447",
    "text": "Kilkis",
    "country_id": "85",
    "value": "Kilkis"
  },
  {
    "id": "1448",
    "text": "Korinthia",
    "country_id": "85",
    "value": "Korinthia"
  },
  {
    "id": "1449",
    "text": "Kozani",
    "country_id": "85",
    "value": "Kozani"
  },
  {
    "id": "1450",
    "text": "Lakonia",
    "country_id": "85",
    "value": "Lakonia"
  },
  {
    "id": "1451",
    "text": "Larisa",
    "country_id": "85",
    "value": "Larisa"
  },
  {
    "id": "1452",
    "text": "Lasithi",
    "country_id": "85",
    "value": "Lasithi"
  },
  {
    "id": "1453",
    "text": "Lesvos",
    "country_id": "85",
    "value": "Lesvos"
  },
  {
    "id": "1454",
    "text": "Levkas",
    "country_id": "85",
    "value": "Levkas"
  },
  {
    "id": "1455",
    "text": "Magnisia",
    "country_id": "85",
    "value": "Magnisia"
  },
  {
    "id": "1456",
    "text": "Messinia",
    "country_id": "85",
    "value": "Messinia"
  },
  {
    "id": "1457",
    "text": "Nomos Attikis",
    "country_id": "85",
    "value": "Nomos Attikis"
  },
  {
    "id": "1458",
    "text": "Nomos Zakynthou",
    "country_id": "85",
    "value": "Nomos Zakynthou"
  },
  {
    "id": "1459",
    "text": "Pella",
    "country_id": "85",
    "value": "Pella"
  },
  {
    "id": "1460",
    "text": "Pieria",
    "country_id": "85",
    "value": "Pieria"
  },
  {
    "id": "1461",
    "text": "Piraios",
    "country_id": "85",
    "value": "Piraios"
  },
  {
    "id": "1462",
    "text": "Preveza",
    "country_id": "85",
    "value": "Preveza"
  },
  {
    "id": "1463",
    "text": "Rethimni",
    "country_id": "85",
    "value": "Rethimni"
  },
  {
    "id": "1464",
    "text": "Rodopi",
    "country_id": "85",
    "value": "Rodopi"
  },
  {
    "id": "1465",
    "text": "Samos",
    "country_id": "85",
    "value": "Samos"
  },
  {
    "id": "1466",
    "text": "Serrai",
    "country_id": "85",
    "value": "Serrai"
  },
  {
    "id": "1467",
    "text": "Thesprotia",
    "country_id": "85",
    "value": "Thesprotia"
  },
  {
    "id": "1468",
    "text": "Thessaloniki",
    "country_id": "85",
    "value": "Thessaloniki"
  },
  {
    "id": "1469",
    "text": "Trikala",
    "country_id": "85",
    "value": "Trikala"
  },
  {
    "id": "1470",
    "text": "Voiotia",
    "country_id": "85",
    "value": "Voiotia"
  },
  {
    "id": "1471",
    "text": "West Greece",
    "country_id": "85",
    "value": "West Greece"
  },
  {
    "id": "1472",
    "text": "Xanthi",
    "country_id": "85",
    "value": "Xanthi"
  },
  {
    "id": "1473",
    "text": "Zakinthos",
    "country_id": "85",
    "value": "Zakinthos"
  },
  {
    "id": "1474",
    "text": "Aasiaat",
    "country_id": "86",
    "value": "Aasiaat"
  },
  {
    "id": "1475",
    "text": "Ammassalik",
    "country_id": "86",
    "value": "Ammassalik"
  },
  {
    "id": "1476",
    "text": "Illoqqortoormiut",
    "country_id": "86",
    "value": "Illoqqortoormiut"
  },
  {
    "id": "1477",
    "text": "Ilulissat",
    "country_id": "86",
    "value": "Ilulissat"
  },
  {
    "id": "1478",
    "text": "Ivittuut",
    "country_id": "86",
    "value": "Ivittuut"
  },
  {
    "id": "1479",
    "text": "Kangaatsiaq",
    "country_id": "86",
    "value": "Kangaatsiaq"
  },
  {
    "id": "1480",
    "text": "Maniitsoq",
    "country_id": "86",
    "value": "Maniitsoq"
  },
  {
    "id": "1481",
    "text": "Nanortalik",
    "country_id": "86",
    "value": "Nanortalik"
  },
  {
    "id": "1482",
    "text": "Narsaq",
    "country_id": "86",
    "value": "Narsaq"
  },
  {
    "id": "1483",
    "text": "Nuuk",
    "country_id": "86",
    "value": "Nuuk"
  },
  {
    "id": "1484",
    "text": "Paamiut",
    "country_id": "86",
    "value": "Paamiut"
  },
  {
    "id": "1485",
    "text": "Qaanaaq",
    "country_id": "86",
    "value": "Qaanaaq"
  },
  {
    "id": "1486",
    "text": "Qaqortoq",
    "country_id": "86",
    "value": "Qaqortoq"
  },
  {
    "id": "1487",
    "text": "Qasigiannguit",
    "country_id": "86",
    "value": "Qasigiannguit"
  },
  {
    "id": "1488",
    "text": "Qeqertarsuaq",
    "country_id": "86",
    "value": "Qeqertarsuaq"
  },
  {
    "id": "1489",
    "text": "Sisimiut",
    "country_id": "86",
    "value": "Sisimiut"
  },
  {
    "id": "1490",
    "text": "Udenfor kommunal inddeling",
    "country_id": "86",
    "value": "Udenfor kommunal inddeling"
  },
  {
    "id": "1491",
    "text": "Upernavik",
    "country_id": "86",
    "value": "Upernavik"
  },
  {
    "id": "1492",
    "text": "Uummannaq",
    "country_id": "86",
    "value": "Uummannaq"
  },
  {
    "id": "1493",
    "text": "Carriacou-Petite Martinique",
    "country_id": "87",
    "value": "Carriacou-Petite Martinique"
  },
  {
    "id": "1494",
    "text": "Saint Andrew",
    "country_id": "87",
    "value": "Saint Andrew"
  },
  {
    "id": "1495",
    "text": "Saint Davids",
    "country_id": "87",
    "value": "Saint Davids"
  },
  {
    "id": "1496",
    "text": "Saint George''s",
    "country_id": "87",
    "value": "Saint George''s"
  },
  {
    "id": "1497",
    "text": "Saint John",
    "country_id": "87",
    "value": "Saint John"
  },
  {
    "id": "1498",
    "text": "Saint Mark",
    "country_id": "87",
    "value": "Saint Mark"
  },
  {
    "id": "1499",
    "text": "Saint Patrick",
    "country_id": "87",
    "value": "Saint Patrick"
  },
  {
    "id": "1500",
    "text": "Basse-Terre",
    "country_id": "88",
    "value": "Basse-Terre"
  },
  {
    "id": "1501",
    "text": "Grande-Terre",
    "country_id": "88",
    "value": "Grande-Terre"
  },
  {
    "id": "1502",
    "text": "Iles des Saintes",
    "country_id": "88",
    "value": "Iles des Saintes"
  },
  {
    "id": "1503",
    "text": "La Desirade",
    "country_id": "88",
    "value": "La Desirade"
  },
  {
    "id": "1504",
    "text": "Marie-Galante",
    "country_id": "88",
    "value": "Marie-Galante"
  },
  {
    "id": "1505",
    "text": "Saint Barthelemy",
    "country_id": "88",
    "value": "Saint Barthelemy"
  },
  {
    "id": "1506",
    "text": "Saint Martin",
    "country_id": "88",
    "value": "Saint Martin"
  },
  {
    "id": "1507",
    "text": "Agana Heights",
    "country_id": "89",
    "value": "Agana Heights"
  },
  {
    "id": "1508",
    "text": "Agat",
    "country_id": "89",
    "value": "Agat"
  },
  {
    "id": "1509",
    "text": "Barrigada",
    "country_id": "89",
    "value": "Barrigada"
  },
  {
    "id": "1510",
    "text": "Chalan-Pago-Ordot",
    "country_id": "89",
    "value": "Chalan-Pago-Ordot"
  },
  {
    "id": "1511",
    "text": "Dededo",
    "country_id": "89",
    "value": "Dededo"
  },
  {
    "id": "1512",
    "text": "Hagatna",
    "country_id": "89",
    "value": "Hagatna"
  },
  {
    "id": "1513",
    "text": "Inarajan",
    "country_id": "89",
    "value": "Inarajan"
  },
  {
    "id": "1514",
    "text": "Mangilao",
    "country_id": "89",
    "value": "Mangilao"
  },
  {
    "id": "1515",
    "text": "Merizo",
    "country_id": "89",
    "value": "Merizo"
  },
  {
    "id": "1516",
    "text": "Mongmong-Toto-Maite",
    "country_id": "89",
    "value": "Mongmong-Toto-Maite"
  },
  {
    "id": "1517",
    "text": "Santa Rita",
    "country_id": "89",
    "value": "Santa Rita"
  },
  {
    "id": "1518",
    "text": "Sinajana",
    "country_id": "89",
    "value": "Sinajana"
  },
  {
    "id": "1519",
    "text": "Talofofo",
    "country_id": "89",
    "value": "Talofofo"
  },
  {
    "id": "1520",
    "text": "Tamuning",
    "country_id": "89",
    "value": "Tamuning"
  },
  {
    "id": "1521",
    "text": "Yigo",
    "country_id": "89",
    "value": "Yigo"
  },
  {
    "id": "1522",
    "text": "Yona",
    "country_id": "89",
    "value": "Yona"
  },
  {
    "id": "1523",
    "text": "Alta Verapaz",
    "country_id": "90",
    "value": "Alta Verapaz"
  },
  {
    "id": "1524",
    "text": "Baja Verapaz",
    "country_id": "90",
    "value": "Baja Verapaz"
  },
  {
    "id": "1525",
    "text": "Chimaltenango",
    "country_id": "90",
    "value": "Chimaltenango"
  },
  {
    "id": "1526",
    "text": "Chiquimula",
    "country_id": "90",
    "value": "Chiquimula"
  },
  {
    "id": "1527",
    "text": "El Progreso",
    "country_id": "90",
    "value": "El Progreso"
  },
  {
    "id": "1528",
    "text": "Escuintla",
    "country_id": "90",
    "value": "Escuintla"
  },
  {
    "id": "1529",
    "text": "Guatemala",
    "country_id": "90",
    "value": "Guatemala"
  },
  {
    "id": "1530",
    "text": "Huehuetenango",
    "country_id": "90",
    "value": "Huehuetenango"
  },
  {
    "id": "1531",
    "text": "Izabal",
    "country_id": "90",
    "value": "Izabal"
  },
  {
    "id": "1532",
    "text": "Jalapa",
    "country_id": "90",
    "value": "Jalapa"
  },
  {
    "id": "1533",
    "text": "Jutiapa",
    "country_id": "90",
    "value": "Jutiapa"
  },
  {
    "id": "1534",
    "text": "Peten",
    "country_id": "90",
    "value": "Peten"
  },
  {
    "id": "1535",
    "text": "Quezaltenango",
    "country_id": "90",
    "value": "Quezaltenango"
  },
  {
    "id": "1536",
    "text": "Quiche",
    "country_id": "90",
    "value": "Quiche"
  },
  {
    "id": "1537",
    "text": "Retalhuleu",
    "country_id": "90",
    "value": "Retalhuleu"
  },
  {
    "id": "1538",
    "text": "Sacatepequez",
    "country_id": "90",
    "value": "Sacatepequez"
  },
  {
    "id": "1539",
    "text": "San Marcos",
    "country_id": "90",
    "value": "San Marcos"
  },
  {
    "id": "1540",
    "text": "Santa Rosa",
    "country_id": "90",
    "value": "Santa Rosa"
  },
  {
    "id": "1541",
    "text": "Solola",
    "country_id": "90",
    "value": "Solola"
  },
  {
    "id": "1542",
    "text": "Suchitepequez",
    "country_id": "90",
    "value": "Suchitepequez"
  },
  {
    "id": "1543",
    "text": "Totonicapan",
    "country_id": "90",
    "value": "Totonicapan"
  },
  {
    "id": "1544",
    "text": "Zacapa",
    "country_id": "90",
    "value": "Zacapa"
  },
  {
    "id": "1545",
    "text": "Alderney",
    "country_id": "91",
    "value": "Alderney"
  },
  {
    "id": "1546",
    "text": "Castel",
    "country_id": "91",
    "value": "Castel"
  },
  {
    "id": "1547",
    "text": "Forest",
    "country_id": "91",
    "value": "Forest"
  },
  {
    "id": "1548",
    "text": "Saint Andrew",
    "country_id": "91",
    "value": "Saint Andrew"
  },
  {
    "id": "1549",
    "text": "Saint Martin",
    "country_id": "91",
    "value": "Saint Martin"
  },
  {
    "id": "1550",
    "text": "Saint Peter Port",
    "country_id": "91",
    "value": "Saint Peter Port"
  },
  {
    "id": "1551",
    "text": "Saint Pierre du Bois",
    "country_id": "91",
    "value": "Saint Pierre du Bois"
  },
  {
    "id": "1552",
    "text": "Saint Sampson",
    "country_id": "91",
    "value": "Saint Sampson"
  },
  {
    "id": "1553",
    "text": "Saint Saviour",
    "country_id": "91",
    "value": "Saint Saviour"
  },
  {
    "id": "1554",
    "text": "Sark",
    "country_id": "91",
    "value": "Sark"
  },
  {
    "id": "1555",
    "text": "Torteval",
    "country_id": "91",
    "value": "Torteval"
  },
  {
    "id": "1556",
    "text": "Vale",
    "country_id": "91",
    "value": "Vale"
  },
  {
    "id": "1557",
    "text": "Beyla",
    "country_id": "92",
    "value": "Beyla"
  },
  {
    "id": "1558",
    "text": "Boffa",
    "country_id": "92",
    "value": "Boffa"
  },
  {
    "id": "1559",
    "text": "Boke",
    "country_id": "92",
    "value": "Boke"
  },
  {
    "id": "1560",
    "text": "Conakry",
    "country_id": "92",
    "value": "Conakry"
  },
  {
    "id": "1561",
    "text": "Coyah",
    "country_id": "92",
    "value": "Coyah"
  },
  {
    "id": "1562",
    "text": "Dabola",
    "country_id": "92",
    "value": "Dabola"
  },
  {
    "id": "1563",
    "text": "Dalaba",
    "country_id": "92",
    "value": "Dalaba"
  },
  {
    "id": "1564",
    "text": "Dinguiraye",
    "country_id": "92",
    "value": "Dinguiraye"
  },
  {
    "id": "1565",
    "text": "Faranah",
    "country_id": "92",
    "value": "Faranah"
  },
  {
    "id": "1566",
    "text": "Forecariah",
    "country_id": "92",
    "value": "Forecariah"
  },
  {
    "id": "1567",
    "text": "Fria",
    "country_id": "92",
    "value": "Fria"
  },
  {
    "id": "1568",
    "text": "Gaoual",
    "country_id": "92",
    "value": "Gaoual"
  },
  {
    "id": "1569",
    "text": "Gueckedou",
    "country_id": "92",
    "value": "Gueckedou"
  },
  {
    "id": "1570",
    "text": "Kankan",
    "country_id": "92",
    "value": "Kankan"
  },
  {
    "id": "1571",
    "text": "Kerouane",
    "country_id": "92",
    "value": "Kerouane"
  },
  {
    "id": "1572",
    "text": "Kindia",
    "country_id": "92",
    "value": "Kindia"
  },
  {
    "id": "1573",
    "text": "Kissidougou",
    "country_id": "92",
    "value": "Kissidougou"
  },
  {
    "id": "1574",
    "text": "Koubia",
    "country_id": "92",
    "value": "Koubia"
  },
  {
    "id": "1575",
    "text": "Koundara",
    "country_id": "92",
    "value": "Koundara"
  },
  {
    "id": "1576",
    "text": "Kouroussa",
    "country_id": "92",
    "value": "Kouroussa"
  },
  {
    "id": "1577",
    "text": "Labe",
    "country_id": "92",
    "value": "Labe"
  },
  {
    "id": "1578",
    "text": "Lola",
    "country_id": "92",
    "value": "Lola"
  },
  {
    "id": "1579",
    "text": "Macenta",
    "country_id": "92",
    "value": "Macenta"
  },
  {
    "id": "1580",
    "text": "Mali",
    "country_id": "92",
    "value": "Mali"
  },
  {
    "id": "1581",
    "text": "Mamou",
    "country_id": "92",
    "value": "Mamou"
  },
  {
    "id": "1582",
    "text": "Mandiana",
    "country_id": "92",
    "value": "Mandiana"
  },
  {
    "id": "1583",
    "text": "Nzerekore",
    "country_id": "92",
    "value": "Nzerekore"
  },
  {
    "id": "1584",
    "text": "Pita",
    "country_id": "92",
    "value": "Pita"
  },
  {
    "id": "1585",
    "text": "Siguiri",
    "country_id": "92",
    "value": "Siguiri"
  },
  {
    "id": "1586",
    "text": "Telimele",
    "country_id": "92",
    "value": "Telimele"
  },
  {
    "id": "1587",
    "text": "Tougue",
    "country_id": "92",
    "value": "Tougue"
  },
  {
    "id": "1588",
    "text": "Yomou",
    "country_id": "92",
    "value": "Yomou"
  },
  {
    "id": "1589",
    "text": "Bafata",
    "country_id": "93",
    "value": "Bafata"
  },
  {
    "id": "1590",
    "text": "Bissau",
    "country_id": "93",
    "value": "Bissau"
  },
  {
    "id": "1591",
    "text": "Bolama",
    "country_id": "93",
    "value": "Bolama"
  },
  {
    "id": "1592",
    "text": "Cacheu",
    "country_id": "93",
    "value": "Cacheu"
  },
  {
    "id": "1593",
    "text": "Gabu",
    "country_id": "93",
    "value": "Gabu"
  },
  {
    "id": "1594",
    "text": "Oio",
    "country_id": "93",
    "value": "Oio"
  },
  {
    "id": "1595",
    "text": "Quinara",
    "country_id": "93",
    "value": "Quinara"
  },
  {
    "id": "1596",
    "text": "Tombali",
    "country_id": "93",
    "value": "Tombali"
  },
  {
    "id": "1597",
    "text": "Barima-Waini",
    "country_id": "94",
    "value": "Barima-Waini"
  },
  {
    "id": "1598",
    "text": "Cuyuni-Mazaruni",
    "country_id": "94",
    "value": "Cuyuni-Mazaruni"
  },
  {
    "id": "1599",
    "text": "Demerara-Mahaica",
    "country_id": "94",
    "value": "Demerara-Mahaica"
  },
  {
    "id": "1600",
    "text": "East Berbice-Corentyne",
    "country_id": "94",
    "value": "East Berbice-Corentyne"
  },
  {
    "id": "1601",
    "text": "Essequibo Islands-West Demerar",
    "country_id": "94",
    "value": "Essequibo Islands-West Demerar"
  },
  {
    "id": "1602",
    "text": "Mahaica-Berbice",
    "country_id": "94",
    "value": "Mahaica-Berbice"
  },
  {
    "id": "1603",
    "text": "Pomeroon-Supenaam",
    "country_id": "94",
    "value": "Pomeroon-Supenaam"
  },
  {
    "id": "1604",
    "text": "Potaro-Siparuni",
    "country_id": "94",
    "value": "Potaro-Siparuni"
  },
  {
    "id": "1605",
    "text": "Upper Demerara-Berbice",
    "country_id": "94",
    "value": "Upper Demerara-Berbice"
  },
  {
    "id": "1606",
    "text": "Upper Takutu-Upper Essequibo",
    "country_id": "94",
    "value": "Upper Takutu-Upper Essequibo"
  },
  {
    "id": "1607",
    "text": "Artibonite",
    "country_id": "95",
    "value": "Artibonite"
  },
  {
    "id": "1608",
    "text": "Centre",
    "country_id": "95",
    "value": "Centre"
  },
  {
    "id": "1609",
    "text": "Grand''Anse",
    "country_id": "95",
    "value": "Grand''Anse"
  },
  {
    "id": "1610",
    "text": "Nord",
    "country_id": "95",
    "value": "Nord"
  },
  {
    "id": "1611",
    "text": "Nord-Est",
    "country_id": "95",
    "value": "Nord-Est"
  },
  {
    "id": "1612",
    "text": "Nord-Ouest",
    "country_id": "95",
    "value": "Nord-Ouest"
  },
  {
    "id": "1613",
    "text": "Ouest",
    "country_id": "95",
    "value": "Ouest"
  },
  {
    "id": "1614",
    "text": "Sud",
    "country_id": "95",
    "value": "Sud"
  },
  {
    "id": "1615",
    "text": "Sud-Est",
    "country_id": "95",
    "value": "Sud-Est"
  },
  {
    "id": "1616",
    "text": "Heard and McDonald Islands",
    "country_id": "96",
    "value": "Heard and McDonald Islands"
  },
  {
    "id": "1617",
    "text": "Atlantida",
    "country_id": "97",
    "value": "Atlantida"
  },
  {
    "id": "1618",
    "text": "Choluteca",
    "country_id": "97",
    "value": "Choluteca"
  },
  {
    "id": "1619",
    "text": "Colon",
    "country_id": "97",
    "value": "Colon"
  },
  {
    "id": "1620",
    "text": "Comayagua",
    "country_id": "97",
    "value": "Comayagua"
  },
  {
    "id": "1621",
    "text": "Copan",
    "country_id": "97",
    "value": "Copan"
  },
  {
    "id": "1622",
    "text": "Cortes",
    "country_id": "97",
    "value": "Cortes"
  },
  {
    "id": "1623",
    "text": "Distrito Central",
    "country_id": "97",
    "value": "Distrito Central"
  },
  {
    "id": "1624",
    "text": "El Paraiso",
    "country_id": "97",
    "value": "El Paraiso"
  },
  {
    "id": "1625",
    "text": "Francisco Morazan",
    "country_id": "97",
    "value": "Francisco Morazan"
  },
  {
    "id": "1626",
    "text": "Gracias a Dios",
    "country_id": "97",
    "value": "Gracias a Dios"
  },
  {
    "id": "1627",
    "text": "Intibuca",
    "country_id": "97",
    "value": "Intibuca"
  },
  {
    "id": "1628",
    "text": "Islas de la Bahia",
    "country_id": "97",
    "value": "Islas de la Bahia"
  },
  {
    "id": "1629",
    "text": "La Paz",
    "country_id": "97",
    "value": "La Paz"
  },
  {
    "id": "1630",
    "text": "Lempira",
    "country_id": "97",
    "value": "Lempira"
  },
  {
    "id": "1631",
    "text": "Ocotepeque",
    "country_id": "97",
    "value": "Ocotepeque"
  },
  {
    "id": "1632",
    "text": "Olancho",
    "country_id": "97",
    "value": "Olancho"
  },
  {
    "id": "1633",
    "text": "Santa Barbara",
    "country_id": "97",
    "value": "Santa Barbara"
  },
  {
    "id": "1634",
    "text": "Valle",
    "country_id": "97",
    "value": "Valle"
  },
  {
    "id": "1635",
    "text": "Yoro",
    "country_id": "97",
    "value": "Yoro"
  },
  {
    "id": "1636",
    "text": "Hong Kong",
    "country_id": "98",
    "value": "Hong Kong"
  },
  {
    "id": "1637",
    "text": "Bacs-Kiskun",
    "country_id": "99",
    "value": "Bacs-Kiskun"
  },
  {
    "id": "1638",
    "text": "Baranya",
    "country_id": "99",
    "value": "Baranya"
  },
  {
    "id": "1639",
    "text": "Bekes",
    "country_id": "99",
    "value": "Bekes"
  },
  {
    "id": "1640",
    "text": "Borsod-Abauj-Zemplen",
    "country_id": "99",
    "value": "Borsod-Abauj-Zemplen"
  },
  {
    "id": "1641",
    "text": "Budapest",
    "country_id": "99",
    "value": "Budapest"
  },
  {
    "id": "1642",
    "text": "Csongrad",
    "country_id": "99",
    "value": "Csongrad"
  },
  {
    "id": "1643",
    "text": "Fejer",
    "country_id": "99",
    "value": "Fejer"
  },
  {
    "id": "1644",
    "text": "Gyor-Moson-Sopron",
    "country_id": "99",
    "value": "Gyor-Moson-Sopron"
  },
  {
    "id": "1645",
    "text": "Hajdu-Bihar",
    "country_id": "99",
    "value": "Hajdu-Bihar"
  },
  {
    "id": "1646",
    "text": "Heves",
    "country_id": "99",
    "value": "Heves"
  },
  {
    "id": "1647",
    "text": "Jasz-Nagykun-Szolnok",
    "country_id": "99",
    "value": "Jasz-Nagykun-Szolnok"
  },
  {
    "id": "1648",
    "text": "Komarom-Esztergom",
    "country_id": "99",
    "value": "Komarom-Esztergom"
  },
  {
    "id": "1649",
    "text": "Nograd",
    "country_id": "99",
    "value": "Nograd"
  },
  {
    "id": "1650",
    "text": "Pest",
    "country_id": "99",
    "value": "Pest"
  },
  {
    "id": "1651",
    "text": "Somogy",
    "country_id": "99",
    "value": "Somogy"
  },
  {
    "id": "1652",
    "text": "Szabolcs-Szatmar-Bereg",
    "country_id": "99",
    "value": "Szabolcs-Szatmar-Bereg"
  },
  {
    "id": "1653",
    "text": "Tolna",
    "country_id": "99",
    "value": "Tolna"
  },
  {
    "id": "1654",
    "text": "Vas",
    "country_id": "99",
    "value": "Vas"
  },
  {
    "id": "1655",
    "text": "Veszprem",
    "country_id": "99",
    "value": "Veszprem"
  },
  {
    "id": "1656",
    "text": "Zala",
    "country_id": "99",
    "value": "Zala"
  },
  {
    "id": "1657",
    "text": "Austurland",
    "country_id": "100",
    "value": "Austurland"
  },
  {
    "id": "1658",
    "text": "Gullbringusysla",
    "country_id": "100",
    "value": "Gullbringusysla"
  },
  {
    "id": "1659",
    "text": "Hofu borgarsva i",
    "country_id": "100",
    "value": "Hofu borgarsva i"
  },
  {
    "id": "1660",
    "text": "Nor urland eystra",
    "country_id": "100",
    "value": "Nor urland eystra"
  },
  {
    "id": "1661",
    "text": "Nor urland vestra",
    "country_id": "100",
    "value": "Nor urland vestra"
  },
  {
    "id": "1662",
    "text": "Su urland",
    "country_id": "100",
    "value": "Su urland"
  },
  {
    "id": "1663",
    "text": "Su urnes",
    "country_id": "100",
    "value": "Su urnes"
  },
  {
    "id": "1664",
    "text": "Vestfir ir",
    "country_id": "100",
    "value": "Vestfir ir"
  },
  {
    "id": "1665",
    "text": "Vesturland",
    "country_id": "100",
    "value": "Vesturland"
  },
  {
    "id": "1666",
    "text": "Aceh",
    "country_id": "102",
    "value": "Aceh"
  },
  {
    "id": "1667",
    "text": "Bali",
    "country_id": "102",
    "value": "Bali"
  },
  {
    "id": "1668",
    "text": "Bangka-Belitung",
    "country_id": "102",
    "value": "Bangka-Belitung"
  },
  {
    "id": "1669",
    "text": "Banten",
    "country_id": "102",
    "value": "Banten"
  },
  {
    "id": "1670",
    "text": "Bengkulu",
    "country_id": "102",
    "value": "Bengkulu"
  },
  {
    "id": "1671",
    "text": "Gandaria",
    "country_id": "102",
    "value": "Gandaria"
  },
  {
    "id": "1672",
    "text": "Gorontalo",
    "country_id": "102",
    "value": "Gorontalo"
  },
  {
    "id": "1673",
    "text": "Jakarta",
    "country_id": "102",
    "value": "Jakarta"
  },
  {
    "id": "1674",
    "text": "Jambi",
    "country_id": "102",
    "value": "Jambi"
  },
  {
    "id": "1675",
    "text": "Jawa Barat",
    "country_id": "102",
    "value": "Jawa Barat"
  },
  {
    "id": "1676",
    "text": "Jawa Tengah",
    "country_id": "102",
    "value": "Jawa Tengah"
  },
  {
    "id": "1677",
    "text": "Jawa Timur",
    "country_id": "102",
    "value": "Jawa Timur"
  },
  {
    "id": "1678",
    "text": "Kalimantan Barat",
    "country_id": "102",
    "value": "Kalimantan Barat"
  },
  {
    "id": "1679",
    "text": "Kalimantan Selatan",
    "country_id": "102",
    "value": "Kalimantan Selatan"
  },
  {
    "id": "1680",
    "text": "Kalimantan Tengah",
    "country_id": "102",
    "value": "Kalimantan Tengah"
  },
  {
    "id": "1681",
    "text": "Kalimantan Timur",
    "country_id": "102",
    "value": "Kalimantan Timur"
  },
  {
    "id": "1682",
    "text": "Kendal",
    "country_id": "102",
    "value": "Kendal"
  },
  {
    "id": "1683",
    "text": "Lampung",
    "country_id": "102",
    "value": "Lampung"
  },
  {
    "id": "1684",
    "text": "Maluku",
    "country_id": "102",
    "value": "Maluku"
  },
  {
    "id": "1685",
    "text": "Maluku Utara",
    "country_id": "102",
    "value": "Maluku Utara"
  },
  {
    "id": "1686",
    "text": "Nusa Tenggara Barat",
    "country_id": "102",
    "value": "Nusa Tenggara Barat"
  },
  {
    "id": "1687",
    "text": "Nusa Tenggara Timur",
    "country_id": "102",
    "value": "Nusa Tenggara Timur"
  },
  {
    "id": "1688",
    "text": "Papua",
    "country_id": "102",
    "value": "Papua"
  },
  {
    "id": "1689",
    "text": "Riau",
    "country_id": "102",
    "value": "Riau"
  },
  {
    "id": "1690",
    "text": "Riau Kepulauan",
    "country_id": "102",
    "value": "Riau Kepulauan"
  },
  {
    "id": "1691",
    "text": "Solo",
    "country_id": "102",
    "value": "Solo"
  },
  {
    "id": "1692",
    "text": "Sulawesi Selatan",
    "country_id": "102",
    "value": "Sulawesi Selatan"
  },
  {
    "id": "1693",
    "text": "Sulawesi Tengah",
    "country_id": "102",
    "value": "Sulawesi Tengah"
  },
  {
    "id": "1694",
    "text": "Sulawesi Tenggara",
    "country_id": "102",
    "value": "Sulawesi Tenggara"
  },
  {
    "id": "1695",
    "text": "Sulawesi Utara",
    "country_id": "102",
    "value": "Sulawesi Utara"
  },
  {
    "id": "1696",
    "text": "Sumatera Barat",
    "country_id": "102",
    "value": "Sumatera Barat"
  },
  {
    "id": "1697",
    "text": "Sumatera Selatan",
    "country_id": "102",
    "value": "Sumatera Selatan"
  },
  {
    "id": "1698",
    "text": "Sumatera Utara",
    "country_id": "102",
    "value": "Sumatera Utara"
  },
  {
    "id": "1699",
    "text": "Yogyakarta",
    "country_id": "102",
    "value": "Yogyakarta"
  },
  {
    "id": "1700",
    "text": "Ardabil",
    "country_id": "103",
    "value": "Ardabil"
  },
  {
    "id": "1701",
    "text": "Azarbayjan-e Bakhtari",
    "country_id": "103",
    "value": "Azarbayjan-e Bakhtari"
  },
  {
    "id": "1702",
    "text": "Azarbayjan-e Khavari",
    "country_id": "103",
    "value": "Azarbayjan-e Khavari"
  },
  {
    "id": "1703",
    "text": "Bushehr",
    "country_id": "103",
    "value": "Bushehr"
  },
  {
    "id": "1704",
    "text": "Chahar Mahal-e Bakhtiari",
    "country_id": "103",
    "value": "Chahar Mahal-e Bakhtiari"
  },
  {
    "id": "1705",
    "text": "Esfahan",
    "country_id": "103",
    "value": "Esfahan"
  },
  {
    "id": "1706",
    "text": "Fars",
    "country_id": "103",
    "value": "Fars"
  },
  {
    "id": "1707",
    "text": "Gilan",
    "country_id": "103",
    "value": "Gilan"
  },
  {
    "id": "1708",
    "text": "Golestan",
    "country_id": "103",
    "value": "Golestan"
  },
  {
    "id": "1709",
    "text": "Hamadan",
    "country_id": "103",
    "value": "Hamadan"
  },
  {
    "id": "1710",
    "text": "Hormozgan",
    "country_id": "103",
    "value": "Hormozgan"
  },
  {
    "id": "1711",
    "text": "Ilam",
    "country_id": "103",
    "value": "Ilam"
  },
  {
    "id": "1712",
    "text": "Kerman",
    "country_id": "103",
    "value": "Kerman"
  },
  {
    "id": "1713",
    "text": "Kermanshah",
    "country_id": "103",
    "value": "Kermanshah"
  },
  {
    "id": "1714",
    "text": "Khorasan",
    "country_id": "103",
    "value": "Khorasan"
  },
  {
    "id": "1715",
    "text": "Khuzestan",
    "country_id": "103",
    "value": "Khuzestan"
  },
  {
    "id": "1716",
    "text": "Kohgiluyeh-e Boyerahmad",
    "country_id": "103",
    "value": "Kohgiluyeh-e Boyerahmad"
  },
  {
    "id": "1717",
    "text": "Kordestan",
    "country_id": "103",
    "value": "Kordestan"
  },
  {
    "id": "1718",
    "text": "Lorestan",
    "country_id": "103",
    "value": "Lorestan"
  },
  {
    "id": "1719",
    "text": "Markazi",
    "country_id": "103",
    "value": "Markazi"
  },
  {
    "id": "1720",
    "text": "Mazandaran",
    "country_id": "103",
    "value": "Mazandaran"
  },
  {
    "id": "1721",
    "text": "Ostan-e Esfahan",
    "country_id": "103",
    "value": "Ostan-e Esfahan"
  },
  {
    "id": "1722",
    "text": "Qazvin",
    "country_id": "103",
    "value": "Qazvin"
  },
  {
    "id": "1723",
    "text": "Qom",
    "country_id": "103",
    "value": "Qom"
  },
  {
    "id": "1724",
    "text": "Semnan",
    "country_id": "103",
    "value": "Semnan"
  },
  {
    "id": "1725",
    "text": "Sistan-e Baluchestan",
    "country_id": "103",
    "value": "Sistan-e Baluchestan"
  },
  {
    "id": "1726",
    "text": "Tehran",
    "country_id": "103",
    "value": "Tehran"
  },
  {
    "id": "1727",
    "text": "Yazd",
    "country_id": "103",
    "value": "Yazd"
  },
  {
    "id": "1728",
    "text": "Zanjan",
    "country_id": "103",
    "value": "Zanjan"
  },
  {
    "id": "1729",
    "text": "Babil",
    "country_id": "104",
    "value": "Babil"
  },
  {
    "id": "1730",
    "text": "Baghdad",
    "country_id": "104",
    "value": "Baghdad"
  },
  {
    "id": "1731",
    "text": "Dahuk",
    "country_id": "104",
    "value": "Dahuk"
  },
  {
    "id": "1732",
    "text": "Dhi Qar",
    "country_id": "104",
    "value": "Dhi Qar"
  },
  {
    "id": "1733",
    "text": "Diyala",
    "country_id": "104",
    "value": "Diyala"
  },
  {
    "id": "1734",
    "text": "Erbil",
    "country_id": "104",
    "value": "Erbil"
  },
  {
    "id": "1735",
    "text": "Irbil",
    "country_id": "104",
    "value": "Irbil"
  },
  {
    "id": "1736",
    "text": "Karbala",
    "country_id": "104",
    "value": "Karbala"
  },
  {
    "id": "1737",
    "text": "Kurdistan",
    "country_id": "104",
    "value": "Kurdistan"
  },
  {
    "id": "1738",
    "text": "Maysan",
    "country_id": "104",
    "value": "Maysan"
  },
  {
    "id": "1739",
    "text": "Ninawa",
    "country_id": "104",
    "value": "Ninawa"
  },
  {
    "id": "1740",
    "text": "Salah-ad-Din",
    "country_id": "104",
    "value": "Salah-ad-Din"
  },
  {
    "id": "1741",
    "text": "Wasit",
    "country_id": "104",
    "value": "Wasit"
  },
  {
    "id": "1742",
    "text": "al-Anbar",
    "country_id": "104",
    "value": "al-Anbar"
  },
  {
    "id": "1743",
    "text": "al-Basrah",
    "country_id": "104",
    "value": "al-Basrah"
  },
  {
    "id": "1744",
    "text": "al-Muthanna",
    "country_id": "104",
    "value": "al-Muthanna"
  },
  {
    "id": "1745",
    "text": "al-Qadisiyah",
    "country_id": "104",
    "value": "al-Qadisiyah"
  },
  {
    "id": "1746",
    "text": "an-Najaf",
    "country_id": "104",
    "value": "an-Najaf"
  },
  {
    "id": "1747",
    "text": "as-Sulaymaniyah",
    "country_id": "104",
    "value": "as-Sulaymaniyah"
  },
  {
    "id": "1748",
    "text": "at-Ta''mim",
    "country_id": "104",
    "value": "at-Ta''mim"
  },
  {
    "id": "1749",
    "text": "Armagh",
    "country_id": "105",
    "value": "Armagh"
  },
  {
    "id": "1750",
    "text": "Carlow",
    "country_id": "105",
    "value": "Carlow"
  },
  {
    "id": "1751",
    "text": "Cavan",
    "country_id": "105",
    "value": "Cavan"
  },
  {
    "id": "1752",
    "text": "Clare",
    "country_id": "105",
    "value": "Clare"
  },
  {
    "id": "1753",
    "text": "Cork",
    "country_id": "105",
    "value": "Cork"
  },
  {
    "id": "1754",
    "text": "Donegal",
    "country_id": "105",
    "value": "Donegal"
  },
  {
    "id": "1755",
    "text": "Dublin",
    "country_id": "105",
    "value": "Dublin"
  },
  {
    "id": "1756",
    "text": "Galway",
    "country_id": "105",
    "value": "Galway"
  },
  {
    "id": "1757",
    "text": "Kerry",
    "country_id": "105",
    "value": "Kerry"
  },
  {
    "id": "1758",
    "text": "Kildare",
    "country_id": "105",
    "value": "Kildare"
  },
  {
    "id": "1759",
    "text": "Kilkenny",
    "country_id": "105",
    "value": "Kilkenny"
  },
  {
    "id": "1760",
    "text": "Laois",
    "country_id": "105",
    "value": "Laois"
  },
  {
    "id": "1761",
    "text": "Leinster",
    "country_id": "105",
    "value": "Leinster"
  },
  {
    "id": "1762",
    "text": "Leitrim",
    "country_id": "105",
    "value": "Leitrim"
  },
  {
    "id": "1763",
    "text": "Limerick",
    "country_id": "105",
    "value": "Limerick"
  },
  {
    "id": "1764",
    "text": "Loch Garman",
    "country_id": "105",
    "value": "Loch Garman"
  },
  {
    "id": "1765",
    "text": "Longford",
    "country_id": "105",
    "value": "Longford"
  },
  {
    "id": "1766",
    "text": "Louth",
    "country_id": "105",
    "value": "Louth"
  },
  {
    "id": "1767",
    "text": "Mayo",
    "country_id": "105",
    "value": "Mayo"
  },
  {
    "id": "1768",
    "text": "Meath",
    "country_id": "105",
    "value": "Meath"
  },
  {
    "id": "1769",
    "text": "Monaghan",
    "country_id": "105",
    "value": "Monaghan"
  },
  {
    "id": "1770",
    "text": "Offaly",
    "country_id": "105",
    "value": "Offaly"
  },
  {
    "id": "1771",
    "text": "Roscommon",
    "country_id": "105",
    "value": "Roscommon"
  },
  {
    "id": "1772",
    "text": "Sligo",
    "country_id": "105",
    "value": "Sligo"
  },
  {
    "id": "1773",
    "text": "Tipperary North Riding",
    "country_id": "105",
    "value": "Tipperary North Riding"
  },
  {
    "id": "1774",
    "text": "Tipperary South Riding",
    "country_id": "105",
    "value": "Tipperary South Riding"
  },
  {
    "id": "1775",
    "text": "Ulster",
    "country_id": "105",
    "value": "Ulster"
  },
  {
    "id": "1776",
    "text": "Waterford",
    "country_id": "105",
    "value": "Waterford"
  },
  {
    "id": "1777",
    "text": "Westmeath",
    "country_id": "105",
    "value": "Westmeath"
  },
  {
    "id": "1778",
    "text": "Wexford",
    "country_id": "105",
    "value": "Wexford"
  },
  {
    "id": "1779",
    "text": "Wicklow",
    "country_id": "105",
    "value": "Wicklow"
  },
  {
    "id": "1780",
    "text": "Beit Hanania",
    "country_id": "106",
    "value": "Beit Hanania"
  },
  {
    "id": "1781",
    "text": "Ben Gurion Airport",
    "country_id": "106",
    "value": "Ben Gurion Airport"
  },
  {
    "id": "1782",
    "text": "Bethlehem",
    "country_id": "106",
    "value": "Bethlehem"
  },
  {
    "id": "1783",
    "text": "Caesarea",
    "country_id": "106",
    "value": "Caesarea"
  },
  {
    "id": "1784",
    "text": "Centre",
    "country_id": "106",
    "value": "Centre"
  },
  {
    "id": "1785",
    "text": "Gaza",
    "country_id": "106",
    "value": "Gaza"
  },
  {
    "id": "1786",
    "text": "Hadaron",
    "country_id": "106",
    "value": "Hadaron"
  },
  {
    "id": "1787",
    "text": "Haifa District",
    "country_id": "106",
    "value": "Haifa District"
  },
  {
    "id": "1788",
    "text": "Hamerkaz",
    "country_id": "106",
    "value": "Hamerkaz"
  },
  {
    "id": "1789",
    "text": "Hazafon",
    "country_id": "106",
    "value": "Hazafon"
  },
  {
    "id": "1790",
    "text": "Hebron",
    "country_id": "106",
    "value": "Hebron"
  },
  {
    "id": "1791",
    "text": "Jaffa",
    "country_id": "106",
    "value": "Jaffa"
  },
  {
    "id": "1792",
    "text": "Jerusalem",
    "country_id": "106",
    "value": "Jerusalem"
  },
  {
    "id": "1793",
    "text": "Khefa",
    "country_id": "106",
    "value": "Khefa"
  },
  {
    "id": "1794",
    "text": "Kiryat Yam",
    "country_id": "106",
    "value": "Kiryat Yam"
  },
  {
    "id": "1795",
    "text": "Lower Galilee",
    "country_id": "106",
    "value": "Lower Galilee"
  },
  {
    "id": "1796",
    "text": "Qalqilya",
    "country_id": "106",
    "value": "Qalqilya"
  },
  {
    "id": "1797",
    "text": "Talme Elazar",
    "country_id": "106",
    "value": "Talme Elazar"
  },
  {
    "id": "1798",
    "text": "Tel Aviv",
    "country_id": "106",
    "value": "Tel Aviv"
  },
  {
    "id": "1799",
    "text": "Tsafon",
    "country_id": "106",
    "value": "Tsafon"
  },
  {
    "id": "1800",
    "text": "Umm El Fahem",
    "country_id": "106",
    "value": "Umm El Fahem"
  },
  {
    "id": "1801",
    "text": "Yerushalayim",
    "country_id": "106",
    "value": "Yerushalayim"
  },
  {
    "id": "1802",
    "text": "Abruzzi",
    "country_id": "107",
    "value": "Abruzzi"
  },
  {
    "id": "1803",
    "text": "Abruzzo",
    "country_id": "107",
    "value": "Abruzzo"
  },
  {
    "id": "1804",
    "text": "Agrigento",
    "country_id": "107",
    "value": "Agrigento"
  },
  {
    "id": "1805",
    "text": "Alessandria",
    "country_id": "107",
    "value": "Alessandria"
  },
  {
    "id": "1806",
    "text": "Ancona",
    "country_id": "107",
    "value": "Ancona"
  },
  {
    "id": "1807",
    "text": "Arezzo",
    "country_id": "107",
    "value": "Arezzo"
  },
  {
    "id": "1808",
    "text": "Ascoli Piceno",
    "country_id": "107",
    "value": "Ascoli Piceno"
  },
  {
    "id": "1809",
    "text": "Asti",
    "country_id": "107",
    "value": "Asti"
  },
  {
    "id": "1810",
    "text": "Avellino",
    "country_id": "107",
    "value": "Avellino"
  },
  {
    "id": "1811",
    "text": "Bari",
    "country_id": "107",
    "value": "Bari"
  },
  {
    "id": "1812",
    "text": "Basilicata",
    "country_id": "107",
    "value": "Basilicata"
  },
  {
    "id": "1813",
    "text": "Belluno",
    "country_id": "107",
    "value": "Belluno"
  },
  {
    "id": "1814",
    "text": "Benevento",
    "country_id": "107",
    "value": "Benevento"
  },
  {
    "id": "1815",
    "text": "Bergamo",
    "country_id": "107",
    "value": "Bergamo"
  },
  {
    "id": "1816",
    "text": "Biella",
    "country_id": "107",
    "value": "Biella"
  },
  {
    "id": "1817",
    "text": "Bologna",
    "country_id": "107",
    "value": "Bologna"
  },
  {
    "id": "1818",
    "text": "Bolzano",
    "country_id": "107",
    "value": "Bolzano"
  },
  {
    "id": "1819",
    "text": "Brescia",
    "country_id": "107",
    "value": "Brescia"
  },
  {
    "id": "1820",
    "text": "Brindisi",
    "country_id": "107",
    "value": "Brindisi"
  },
  {
    "id": "1821",
    "text": "Calabria",
    "country_id": "107",
    "value": "Calabria"
  },
  {
    "id": "1822",
    "text": "Campania",
    "country_id": "107",
    "value": "Campania"
  },
  {
    "id": "1823",
    "text": "Cartoceto",
    "country_id": "107",
    "value": "Cartoceto"
  },
  {
    "id": "1824",
    "text": "Caserta",
    "country_id": "107",
    "value": "Caserta"
  },
  {
    "id": "1825",
    "text": "Catania",
    "country_id": "107",
    "value": "Catania"
  },
  {
    "id": "1826",
    "text": "Chieti",
    "country_id": "107",
    "value": "Chieti"
  },
  {
    "id": "1827",
    "text": "Como",
    "country_id": "107",
    "value": "Como"
  },
  {
    "id": "1828",
    "text": "Cosenza",
    "country_id": "107",
    "value": "Cosenza"
  },
  {
    "id": "1829",
    "text": "Cremona",
    "country_id": "107",
    "value": "Cremona"
  },
  {
    "id": "1830",
    "text": "Cuneo",
    "country_id": "107",
    "value": "Cuneo"
  },
  {
    "id": "1831",
    "text": "Emilia-Romagna",
    "country_id": "107",
    "value": "Emilia-Romagna"
  },
  {
    "id": "1832",
    "text": "Ferrara",
    "country_id": "107",
    "value": "Ferrara"
  },
  {
    "id": "1833",
    "text": "Firenze",
    "country_id": "107",
    "value": "Firenze"
  },
  {
    "id": "1834",
    "text": "Florence",
    "country_id": "107",
    "value": "Florence"
  },
  {
    "id": "1835",
    "text": "Forli-Cesena",
    "country_id": "107",
    "value": "Forli-Cesena"
  },
  {
    "id": "1836",
    "text": "Friuli-Venezia Giulia",
    "country_id": "107",
    "value": "Friuli-Venezia Giulia"
  },
  {
    "id": "1837",
    "text": "Frosinone",
    "country_id": "107",
    "value": "Frosinone"
  },
  {
    "id": "1838",
    "text": "Genoa",
    "country_id": "107",
    "value": "Genoa"
  },
  {
    "id": "1839",
    "text": "Gorizia",
    "country_id": "107",
    "value": "Gorizia"
  },
  {
    "id": "1840",
    "text": "L''Aquila",
    "country_id": "107",
    "value": "L''Aquila"
  },
  {
    "id": "1841",
    "text": "Lazio",
    "country_id": "107",
    "value": "Lazio"
  },
  {
    "id": "1842",
    "text": "Lecce",
    "country_id": "107",
    "value": "Lecce"
  },
  {
    "id": "1843",
    "text": "Lecco",
    "country_id": "107",
    "value": "Lecco"
  },
  {
    "id": "1844",
    "text": "Lecco Province",
    "country_id": "107",
    "value": "Lecco Province"
  },
  {
    "id": "1845",
    "text": "Liguria",
    "country_id": "107",
    "value": "Liguria"
  },
  {
    "id": "1846",
    "text": "Lodi",
    "country_id": "107",
    "value": "Lodi"
  },
  {
    "id": "1847",
    "text": "Lombardia",
    "country_id": "107",
    "value": "Lombardia"
  },
  {
    "id": "1848",
    "text": "Lombardy",
    "country_id": "107",
    "value": "Lombardy"
  },
  {
    "id": "1849",
    "text": "Macerata",
    "country_id": "107",
    "value": "Macerata"
  },
  {
    "id": "1850",
    "text": "Mantova",
    "country_id": "107",
    "value": "Mantova"
  },
  {
    "id": "1851",
    "text": "Marche",
    "country_id": "107",
    "value": "Marche"
  },
  {
    "id": "1852",
    "text": "Messina",
    "country_id": "107",
    "value": "Messina"
  },
  {
    "id": "1853",
    "text": "Milan",
    "country_id": "107",
    "value": "Milan"
  },
  {
    "id": "1854",
    "text": "Modena",
    "country_id": "107",
    "value": "Modena"
  },
  {
    "id": "1855",
    "text": "Molise",
    "country_id": "107",
    "value": "Molise"
  },
  {
    "id": "1856",
    "text": "Molteno",
    "country_id": "107",
    "value": "Molteno"
  },
  {
    "id": "1857",
    "text": "Montenegro",
    "country_id": "107",
    "value": "Montenegro"
  },
  {
    "id": "1858",
    "text": "Monza and Brianza",
    "country_id": "107",
    "value": "Monza and Brianza"
  },
  {
    "id": "1859",
    "text": "Naples",
    "country_id": "107",
    "value": "Naples"
  },
  {
    "id": "1860",
    "text": "Novara",
    "country_id": "107",
    "value": "Novara"
  },
  {
    "id": "1861",
    "text": "Padova",
    "country_id": "107",
    "value": "Padova"
  },
  {
    "id": "1862",
    "text": "Parma",
    "country_id": "107",
    "value": "Parma"
  },
  {
    "id": "1863",
    "text": "Pavia",
    "country_id": "107",
    "value": "Pavia"
  },
  {
    "id": "1864",
    "text": "Perugia",
    "country_id": "107",
    "value": "Perugia"
  },
  {
    "id": "1865",
    "text": "Pesaro-Urbino",
    "country_id": "107",
    "value": "Pesaro-Urbino"
  },
  {
    "id": "1866",
    "text": "Piacenza",
    "country_id": "107",
    "value": "Piacenza"
  },
  {
    "id": "1867",
    "text": "Piedmont",
    "country_id": "107",
    "value": "Piedmont"
  },
  {
    "id": "1868",
    "text": "Piemonte",
    "country_id": "107",
    "value": "Piemonte"
  },
  {
    "id": "1869",
    "text": "Pisa",
    "country_id": "107",
    "value": "Pisa"
  },
  {
    "id": "1870",
    "text": "Pordenone",
    "country_id": "107",
    "value": "Pordenone"
  },
  {
    "id": "1871",
    "text": "Potenza",
    "country_id": "107",
    "value": "Potenza"
  },
  {
    "id": "1872",
    "text": "Puglia",
    "country_id": "107",
    "value": "Puglia"
  },
  {
    "id": "1873",
    "text": "Reggio Emilia",
    "country_id": "107",
    "value": "Reggio Emilia"
  },
  {
    "id": "1874",
    "text": "Rimini",
    "country_id": "107",
    "value": "Rimini"
  },
  {
    "id": "1875",
    "text": "Roma",
    "country_id": "107",
    "value": "Roma"
  },
  {
    "id": "1876",
    "text": "Salerno",
    "country_id": "107",
    "value": "Salerno"
  },
  {
    "id": "1877",
    "text": "Sardegna",
    "country_id": "107",
    "value": "Sardegna"
  },
  {
    "id": "1878",
    "text": "Sassari",
    "country_id": "107",
    "value": "Sassari"
  },
  {
    "id": "1879",
    "text": "Savona",
    "country_id": "107",
    "value": "Savona"
  },
  {
    "id": "1880",
    "text": "Sicilia",
    "country_id": "107",
    "value": "Sicilia"
  },
  {
    "id": "1881",
    "text": "Siena",
    "country_id": "107",
    "value": "Siena"
  },
  {
    "id": "1882",
    "text": "Sondrio",
    "country_id": "107",
    "value": "Sondrio"
  },
  {
    "id": "1883",
    "text": "South Tyrol",
    "country_id": "107",
    "value": "South Tyrol"
  },
  {
    "id": "1884",
    "text": "Taranto",
    "country_id": "107",
    "value": "Taranto"
  },
  {
    "id": "1885",
    "text": "Teramo",
    "country_id": "107",
    "value": "Teramo"
  },
  {
    "id": "1886",
    "text": "Torino",
    "country_id": "107",
    "value": "Torino"
  },
  {
    "id": "1887",
    "text": "Toscana",
    "country_id": "107",
    "value": "Toscana"
  },
  {
    "id": "1888",
    "text": "Trapani",
    "country_id": "107",
    "value": "Trapani"
  },
  {
    "id": "1889",
    "text": "Trentino-Alto Adige",
    "country_id": "107",
    "value": "Trentino-Alto Adige"
  },
  {
    "id": "1890",
    "text": "Trento",
    "country_id": "107",
    "value": "Trento"
  },
  {
    "id": "1891",
    "text": "Treviso",
    "country_id": "107",
    "value": "Treviso"
  },
  {
    "id": "1892",
    "text": "Udine",
    "country_id": "107",
    "value": "Udine"
  },
  {
    "id": "1893",
    "text": "Umbria",
    "country_id": "107",
    "value": "Umbria"
  },
  {
    "id": "1894",
    "text": "Valle d''Aosta",
    "country_id": "107",
    "value": "Valle d''Aosta"
  },
  {
    "id": "1895",
    "text": "Varese",
    "country_id": "107",
    "value": "Varese"
  },
  {
    "id": "1896",
    "text": "Veneto",
    "country_id": "107",
    "value": "Veneto"
  },
  {
    "id": "1897",
    "text": "Venezia",
    "country_id": "107",
    "value": "Venezia"
  },
  {
    "id": "1898",
    "text": "Verbano-Cusio-Ossola",
    "country_id": "107",
    "value": "Verbano-Cusio-Ossola"
  },
  {
    "id": "1899",
    "text": "Vercelli",
    "country_id": "107",
    "value": "Vercelli"
  },
  {
    "id": "1900",
    "text": "Verona",
    "country_id": "107",
    "value": "Verona"
  },
  {
    "id": "1901",
    "text": "Vicenza",
    "country_id": "107",
    "value": "Vicenza"
  },
  {
    "id": "1902",
    "text": "Viterbo",
    "country_id": "107",
    "value": "Viterbo"
  },
  {
    "id": "1903",
    "text": "Buxoro Viloyati",
    "country_id": "108",
    "value": "Buxoro Viloyati"
  },
  {
    "id": "1904",
    "text": "Clarendon",
    "country_id": "108",
    "value": "Clarendon"
  },
  {
    "id": "1905",
    "text": "Hanover",
    "country_id": "108",
    "value": "Hanover"
  },
  {
    "id": "1906",
    "text": "Kingston",
    "country_id": "108",
    "value": "Kingston"
  },
  {
    "id": "1907",
    "text": "Manchester",
    "country_id": "108",
    "value": "Manchester"
  },
  {
    "id": "1908",
    "text": "Portland",
    "country_id": "108",
    "value": "Portland"
  },
  {
    "id": "1909",
    "text": "Saint Andrews",
    "country_id": "108",
    "value": "Saint Andrews"
  },
  {
    "id": "1910",
    "text": "Saint Ann",
    "country_id": "108",
    "value": "Saint Ann"
  },
  {
    "id": "1911",
    "text": "Saint Catherine",
    "country_id": "108",
    "value": "Saint Catherine"
  },
  {
    "id": "1912",
    "text": "Saint Elizabeth",
    "country_id": "108",
    "value": "Saint Elizabeth"
  },
  {
    "id": "1913",
    "text": "Saint James",
    "country_id": "108",
    "value": "Saint James"
  },
  {
    "id": "1914",
    "text": "Saint Mary",
    "country_id": "108",
    "value": "Saint Mary"
  },
  {
    "id": "1915",
    "text": "Saint Thomas",
    "country_id": "108",
    "value": "Saint Thomas"
  },
  {
    "id": "1916",
    "text": "Trelawney",
    "country_id": "108",
    "value": "Trelawney"
  },
  {
    "id": "1917",
    "text": "Westmoreland",
    "country_id": "108",
    "value": "Westmoreland"
  },
  {
    "id": "1918",
    "text": "Aichi",
    "country_id": "109",
    "value": "Aichi"
  },
  {
    "id": "1919",
    "text": "Akita",
    "country_id": "109",
    "value": "Akita"
  },
  {
    "id": "1920",
    "text": "Aomori",
    "country_id": "109",
    "value": "Aomori"
  },
  {
    "id": "1921",
    "text": "Chiba",
    "country_id": "109",
    "value": "Chiba"
  },
  {
    "id": "1922",
    "text": "Ehime",
    "country_id": "109",
    "value": "Ehime"
  },
  {
    "id": "1923",
    "text": "Fukui",
    "country_id": "109",
    "value": "Fukui"
  },
  {
    "id": "1924",
    "text": "Fukuoka",
    "country_id": "109",
    "value": "Fukuoka"
  },
  {
    "id": "1925",
    "text": "Fukushima",
    "country_id": "109",
    "value": "Fukushima"
  },
  {
    "id": "1926",
    "text": "Gifu",
    "country_id": "109",
    "value": "Gifu"
  },
  {
    "id": "1927",
    "text": "Gumma",
    "country_id": "109",
    "value": "Gumma"
  },
  {
    "id": "1928",
    "text": "Hiroshima",
    "country_id": "109",
    "value": "Hiroshima"
  },
  {
    "id": "1929",
    "text": "Hokkaido",
    "country_id": "109",
    "value": "Hokkaido"
  },
  {
    "id": "1930",
    "text": "Hyogo",
    "country_id": "109",
    "value": "Hyogo"
  },
  {
    "id": "1931",
    "text": "Ibaraki",
    "country_id": "109",
    "value": "Ibaraki"
  },
  {
    "id": "1932",
    "text": "Ishikawa",
    "country_id": "109",
    "value": "Ishikawa"
  },
  {
    "id": "1933",
    "text": "Iwate",
    "country_id": "109",
    "value": "Iwate"
  },
  {
    "id": "1934",
    "text": "Kagawa",
    "country_id": "109",
    "value": "Kagawa"
  },
  {
    "id": "1935",
    "text": "Kagoshima",
    "country_id": "109",
    "value": "Kagoshima"
  },
  {
    "id": "1936",
    "text": "Kanagawa",
    "country_id": "109",
    "value": "Kanagawa"
  },
  {
    "id": "1937",
    "text": "Kanto",
    "country_id": "109",
    "value": "Kanto"
  },
  {
    "id": "1938",
    "text": "Kochi",
    "country_id": "109",
    "value": "Kochi"
  },
  {
    "id": "1939",
    "text": "Kumamoto",
    "country_id": "109",
    "value": "Kumamoto"
  },
  {
    "id": "1940",
    "text": "Kyoto",
    "country_id": "109",
    "value": "Kyoto"
  },
  {
    "id": "1941",
    "text": "Mie",
    "country_id": "109",
    "value": "Mie"
  },
  {
    "id": "1942",
    "text": "Miyagi",
    "country_id": "109",
    "value": "Miyagi"
  },
  {
    "id": "1943",
    "text": "Miyazaki",
    "country_id": "109",
    "value": "Miyazaki"
  },
  {
    "id": "1944",
    "text": "Nagano",
    "country_id": "109",
    "value": "Nagano"
  },
  {
    "id": "1945",
    "text": "Nagasaki",
    "country_id": "109",
    "value": "Nagasaki"
  },
  {
    "id": "1946",
    "text": "Nara",
    "country_id": "109",
    "value": "Nara"
  },
  {
    "id": "1947",
    "text": "Niigata",
    "country_id": "109",
    "value": "Niigata"
  },
  {
    "id": "1948",
    "text": "Oita",
    "country_id": "109",
    "value": "Oita"
  },
  {
    "id": "1949",
    "text": "Okayama",
    "country_id": "109",
    "value": "Okayama"
  },
  {
    "id": "1950",
    "text": "Okinawa",
    "country_id": "109",
    "value": "Okinawa"
  },
  {
    "id": "1951",
    "text": "Osaka",
    "country_id": "109",
    "value": "Osaka"
  },
  {
    "id": "1952",
    "text": "Saga",
    "country_id": "109",
    "value": "Saga"
  },
  {
    "id": "1953",
    "text": "Saitama",
    "country_id": "109",
    "value": "Saitama"
  },
  {
    "id": "1954",
    "text": "Shiga",
    "country_id": "109",
    "value": "Shiga"
  },
  {
    "id": "1955",
    "text": "Shimane",
    "country_id": "109",
    "value": "Shimane"
  },
  {
    "id": "1956",
    "text": "Shizuoka",
    "country_id": "109",
    "value": "Shizuoka"
  },
  {
    "id": "1957",
    "text": "Tochigi",
    "country_id": "109",
    "value": "Tochigi"
  },
  {
    "id": "1958",
    "text": "Tokushima",
    "country_id": "109",
    "value": "Tokushima"
  },
  {
    "id": "1959",
    "text": "Tokyo",
    "country_id": "109",
    "value": "Tokyo"
  },
  {
    "id": "1960",
    "text": "Tottori",
    "country_id": "109",
    "value": "Tottori"
  },
  {
    "id": "1961",
    "text": "Toyama",
    "country_id": "109",
    "value": "Toyama"
  },
  {
    "id": "1962",
    "text": "Wakayama",
    "country_id": "109",
    "value": "Wakayama"
  },
  {
    "id": "1963",
    "text": "Yamagata",
    "country_id": "109",
    "value": "Yamagata"
  },
  {
    "id": "1964",
    "text": "Yamaguchi",
    "country_id": "109",
    "value": "Yamaguchi"
  },
  {
    "id": "1965",
    "text": "Yamanashi",
    "country_id": "109",
    "value": "Yamanashi"
  },
  {
    "id": "1966",
    "text": "Grouville",
    "country_id": "110",
    "value": "Grouville"
  },
  {
    "id": "1967",
    "text": "Saint Brelade",
    "country_id": "110",
    "value": "Saint Brelade"
  },
  {
    "id": "1968",
    "text": "Saint Clement",
    "country_id": "110",
    "value": "Saint Clement"
  },
  {
    "id": "1969",
    "text": "Saint Helier",
    "country_id": "110",
    "value": "Saint Helier"
  },
  {
    "id": "1970",
    "text": "Saint John",
    "country_id": "110",
    "value": "Saint John"
  },
  {
    "id": "1971",
    "text": "Saint Lawrence",
    "country_id": "110",
    "value": "Saint Lawrence"
  },
  {
    "id": "1972",
    "text": "Saint Martin",
    "country_id": "110",
    "value": "Saint Martin"
  },
  {
    "id": "1973",
    "text": "Saint Mary",
    "country_id": "110",
    "value": "Saint Mary"
  },
  {
    "id": "1974",
    "text": "Saint Peter",
    "country_id": "110",
    "value": "Saint Peter"
  },
  {
    "id": "1975",
    "text": "Saint Saviour",
    "country_id": "110",
    "value": "Saint Saviour"
  },
  {
    "id": "1976",
    "text": "Trinity",
    "country_id": "110",
    "value": "Trinity"
  },
  {
    "id": "1977",
    "text": "Ajlun",
    "country_id": "111",
    "value": "Ajlun"
  },
  {
    "id": "1978",
    "text": "Amman",
    "country_id": "111",
    "value": "Amman"
  },
  {
    "id": "1979",
    "text": "Irbid",
    "country_id": "111",
    "value": "Irbid"
  },
  {
    "id": "1980",
    "text": "Jarash",
    "country_id": "111",
    "value": "Jarash"
  },
  {
    "id": "1981",
    "text": "Ma''an",
    "country_id": "111",
    "value": "Ma''an"
  },
  {
    "id": "1982",
    "text": "Madaba",
    "country_id": "111",
    "value": "Madaba"
  },
  {
    "id": "1983",
    "text": "al-''Aqabah",
    "country_id": "111",
    "value": "al-''Aqabah"
  },
  {
    "id": "1984",
    "text": "al-Balqa",
    "country_id": "111",
    "value": "al-Balqa"
  },
  {
    "id": "1985",
    "text": "al-Karak",
    "country_id": "111",
    "value": "al-Karak"
  },
  {
    "id": "1986",
    "text": "al-Mafraq",
    "country_id": "111",
    "value": "al-Mafraq"
  },
  {
    "id": "1987",
    "text": "at-Tafilah",
    "country_id": "111",
    "value": "at-Tafilah"
  },
  {
    "id": "1988",
    "text": "az-Zarqa",
    "country_id": "111",
    "value": "az-Zarqa"
  },
  {
    "id": "1989",
    "text": "Akmecet",
    "country_id": "112",
    "value": "Akmecet"
  },
  {
    "id": "1990",
    "text": "Akmola",
    "country_id": "112",
    "value": "Akmola"
  },
  {
    "id": "1991",
    "text": "Aktobe",
    "country_id": "112",
    "value": "Aktobe"
  },
  {
    "id": "1992",
    "text": "Almati",
    "country_id": "112",
    "value": "Almati"
  },
  {
    "id": "1993",
    "text": "Atirau",
    "country_id": "112",
    "value": "Atirau"
  },
  {
    "id": "1994",
    "text": "Batis Kazakstan",
    "country_id": "112",
    "value": "Batis Kazakstan"
  },
  {
    "id": "1995",
    "text": "Burlinsky Region",
    "country_id": "112",
    "value": "Burlinsky Region"
  },
  {
    "id": "1996",
    "text": "Karagandi",
    "country_id": "112",
    "value": "Karagandi"
  },
  {
    "id": "1997",
    "text": "Kostanay",
    "country_id": "112",
    "value": "Kostanay"
  },
  {
    "id": "1998",
    "text": "Mankistau",
    "country_id": "112",
    "value": "Mankistau"
  },
  {
    "id": "1999",
    "text": "Ontustik Kazakstan",
    "country_id": "112",
    "value": "Ontustik Kazakstan"
  },
  {
    "id": "2000",
    "text": "Pavlodar",
    "country_id": "112",
    "value": "Pavlodar"
  },
  {
    "id": "2001",
    "text": "Sigis Kazakstan",
    "country_id": "112",
    "value": "Sigis Kazakstan"
  },
  {
    "id": "2002",
    "text": "Soltustik Kazakstan",
    "country_id": "112",
    "value": "Soltustik Kazakstan"
  },
  {
    "id": "2003",
    "text": "Taraz",
    "country_id": "112",
    "value": "Taraz"
  },
  {
    "id": "2004",
    "text": "Central",
    "country_id": "113",
    "value": "Central"
  },
  {
    "id": "2005",
    "text": "Coast",
    "country_id": "113",
    "value": "Coast"
  },
  {
    "id": "2006",
    "text": "Eastern",
    "country_id": "113",
    "value": "Eastern"
  },
  {
    "id": "2007",
    "text": "Nairobi",
    "country_id": "113",
    "value": "Nairobi"
  },
  {
    "id": "2008",
    "text": "North Eastern",
    "country_id": "113",
    "value": "North Eastern"
  },
  {
    "id": "2009",
    "text": "Nyanza",
    "country_id": "113",
    "value": "Nyanza"
  },
  {
    "id": "2010",
    "text": "Rift Valley",
    "country_id": "113",
    "value": "Rift Valley"
  },
  {
    "id": "2011",
    "text": "Western",
    "country_id": "113",
    "value": "Western"
  },
  {
    "id": "2012",
    "text": "Abaiang",
    "country_id": "114",
    "value": "Abaiang"
  },
  {
    "id": "2013",
    "text": "Abemana",
    "country_id": "114",
    "value": "Abemana"
  },
  {
    "id": "2014",
    "text": "Aranuka",
    "country_id": "114",
    "value": "Aranuka"
  },
  {
    "id": "2015",
    "text": "Arorae",
    "country_id": "114",
    "value": "Arorae"
  },
  {
    "id": "2016",
    "text": "Banaba",
    "country_id": "114",
    "value": "Banaba"
  },
  {
    "id": "2017",
    "text": "Beru",
    "country_id": "114",
    "value": "Beru"
  },
  {
    "id": "2018",
    "text": "Butaritari",
    "country_id": "114",
    "value": "Butaritari"
  },
  {
    "id": "2019",
    "text": "Kiritimati",
    "country_id": "114",
    "value": "Kiritimati"
  },
  {
    "id": "2020",
    "text": "Kuria",
    "country_id": "114",
    "value": "Kuria"
  },
  {
    "id": "2021",
    "text": "Maiana",
    "country_id": "114",
    "value": "Maiana"
  },
  {
    "id": "2022",
    "text": "Makin",
    "country_id": "114",
    "value": "Makin"
  },
  {
    "id": "2023",
    "text": "Marakei",
    "country_id": "114",
    "value": "Marakei"
  },
  {
    "id": "2024",
    "text": "Nikunau",
    "country_id": "114",
    "value": "Nikunau"
  },
  {
    "id": "2025",
    "text": "Nonouti",
    "country_id": "114",
    "value": "Nonouti"
  },
  {
    "id": "2026",
    "text": "Onotoa",
    "country_id": "114",
    "value": "Onotoa"
  },
  {
    "id": "2027",
    "text": "Phoenix Islands",
    "country_id": "114",
    "value": "Phoenix Islands"
  },
  {
    "id": "2028",
    "text": "Tabiteuea North",
    "country_id": "114",
    "value": "Tabiteuea North"
  },
  {
    "id": "2029",
    "text": "Tabiteuea South",
    "country_id": "114",
    "value": "Tabiteuea South"
  },
  {
    "id": "2030",
    "text": "Tabuaeran",
    "country_id": "114",
    "value": "Tabuaeran"
  },
  {
    "id": "2031",
    "text": "Tamana",
    "country_id": "114",
    "value": "Tamana"
  },
  {
    "id": "2032",
    "text": "Tarawa North",
    "country_id": "114",
    "value": "Tarawa North"
  },
  {
    "id": "2033",
    "text": "Tarawa South",
    "country_id": "114",
    "value": "Tarawa South"
  },
  {
    "id": "2034",
    "text": "Teraina",
    "country_id": "114",
    "value": "Teraina"
  },
  {
    "id": "2035",
    "text": "Chagangdo",
    "country_id": "115",
    "value": "Chagangdo"
  },
  {
    "id": "2036",
    "text": "Hamgyeongbukto",
    "country_id": "115",
    "value": "Hamgyeongbukto"
  },
  {
    "id": "2037",
    "text": "Hamgyeongnamdo",
    "country_id": "115",
    "value": "Hamgyeongnamdo"
  },
  {
    "id": "2038",
    "text": "Hwanghaebukto",
    "country_id": "115",
    "value": "Hwanghaebukto"
  },
  {
    "id": "2039",
    "text": "Hwanghaenamdo",
    "country_id": "115",
    "value": "Hwanghaenamdo"
  },
  {
    "id": "2040",
    "text": "Kaeseong",
    "country_id": "115",
    "value": "Kaeseong"
  },
  {
    "id": "2041",
    "text": "Kangweon",
    "country_id": "115",
    "value": "Kangweon"
  },
  {
    "id": "2042",
    "text": "Nampo",
    "country_id": "115",
    "value": "Nampo"
  },
  {
    "id": "2043",
    "text": "Pyeonganbukto",
    "country_id": "115",
    "value": "Pyeonganbukto"
  },
  {
    "id": "2044",
    "text": "Pyeongannamdo",
    "country_id": "115",
    "value": "Pyeongannamdo"
  },
  {
    "id": "2045",
    "text": "Pyeongyang",
    "country_id": "115",
    "value": "Pyeongyang"
  },
  {
    "id": "2046",
    "text": "Yanggang",
    "country_id": "115",
    "value": "Yanggang"
  },
  {
    "id": "2047",
    "text": "Busan",
    "country_id": "116",
    "value": "Busan"
  },
  {
    "id": "2048",
    "text": "Cheju",
    "country_id": "116",
    "value": "Cheju"
  },
  {
    "id": "2049",
    "text": "Chollabuk",
    "country_id": "116",
    "value": "Chollabuk"
  },
  {
    "id": "2050",
    "text": "Chollanam",
    "country_id": "116",
    "value": "Chollanam"
  },
  {
    "id": "2051",
    "text": "Chungbuk",
    "country_id": "116",
    "value": "Chungbuk"
  },
  {
    "id": "2052",
    "text": "Chungcheongbuk",
    "country_id": "116",
    "value": "Chungcheongbuk"
  },
  {
    "id": "2053",
    "text": "Chungcheongnam",
    "country_id": "116",
    "value": "Chungcheongnam"
  },
  {
    "id": "2054",
    "text": "Chungnam",
    "country_id": "116",
    "value": "Chungnam"
  },
  {
    "id": "2055",
    "text": "Daegu",
    "country_id": "116",
    "value": "Daegu"
  },
  {
    "id": "2056",
    "text": "Gangwon-do",
    "country_id": "116",
    "value": "Gangwon-do"
  },
  {
    "id": "2057",
    "text": "Goyang-si",
    "country_id": "116",
    "value": "Goyang-si"
  },
  {
    "id": "2058",
    "text": "Gyeonggi-do",
    "country_id": "116",
    "value": "Gyeonggi-do"
  },
  {
    "id": "2059",
    "text": "Gyeongsang",
    "country_id": "116",
    "value": "Gyeongsang"
  },
  {
    "id": "2060",
    "text": "Gyeongsangnam-do",
    "country_id": "116",
    "value": "Gyeongsangnam-do"
  },
  {
    "id": "2061",
    "text": "Incheon",
    "country_id": "116",
    "value": "Incheon"
  },
  {
    "id": "2062",
    "text": "Jeju-Si",
    "country_id": "116",
    "value": "Jeju-Si"
  },
  {
    "id": "2063",
    "text": "Jeonbuk",
    "country_id": "116",
    "value": "Jeonbuk"
  },
  {
    "id": "2064",
    "text": "Kangweon",
    "country_id": "116",
    "value": "Kangweon"
  },
  {
    "id": "2065",
    "text": "Kwangju",
    "country_id": "116",
    "value": "Kwangju"
  },
  {
    "id": "2066",
    "text": "Kyeonggi",
    "country_id": "116",
    "value": "Kyeonggi"
  },
  {
    "id": "2067",
    "text": "Kyeongsangbuk",
    "country_id": "116",
    "value": "Kyeongsangbuk"
  },
  {
    "id": "2068",
    "text": "Kyeongsangnam",
    "country_id": "116",
    "value": "Kyeongsangnam"
  },
  {
    "id": "2069",
    "text": "Kyonggi-do",
    "country_id": "116",
    "value": "Kyonggi-do"
  },
  {
    "id": "2070",
    "text": "Kyungbuk-Do",
    "country_id": "116",
    "value": "Kyungbuk-Do"
  },
  {
    "id": "2071",
    "text": "Kyunggi-Do",
    "country_id": "116",
    "value": "Kyunggi-Do"
  },
  {
    "id": "2072",
    "text": "Kyunggi-do",
    "country_id": "116",
    "value": "Kyunggi-do"
  },
  {
    "id": "2073",
    "text": "Pusan",
    "country_id": "116",
    "value": "Pusan"
  },
  {
    "id": "2074",
    "text": "Seoul",
    "country_id": "116",
    "value": "Seoul"
  },
  {
    "id": "2075",
    "text": "Sudogwon",
    "country_id": "116",
    "value": "Sudogwon"
  },
  {
    "id": "2076",
    "text": "Taegu",
    "country_id": "116",
    "value": "Taegu"
  },
  {
    "id": "2077",
    "text": "Taejeon",
    "country_id": "116",
    "value": "Taejeon"
  },
  {
    "id": "2078",
    "text": "Taejon-gwangyoksi",
    "country_id": "116",
    "value": "Taejon-gwangyoksi"
  },
  {
    "id": "2079",
    "text": "Ulsan",
    "country_id": "116",
    "value": "Ulsan"
  },
  {
    "id": "2080",
    "text": "Wonju",
    "country_id": "116",
    "value": "Wonju"
  },
  {
    "id": "2081",
    "text": "gwangyoksi",
    "country_id": "116",
    "value": "gwangyoksi"
  },
  {
    "id": "2082",
    "text": "Al Asimah",
    "country_id": "117",
    "value": "Al Asimah"
  },
  {
    "id": "2083",
    "text": "Hawalli",
    "country_id": "117",
    "value": "Hawalli"
  },
  {
    "id": "2084",
    "text": "Mishref",
    "country_id": "117",
    "value": "Mishref"
  },
  {
    "id": "2085",
    "text": "Qadesiya",
    "country_id": "117",
    "value": "Qadesiya"
  },
  {
    "id": "2086",
    "text": "Safat",
    "country_id": "117",
    "value": "Safat"
  },
  {
    "id": "2087",
    "text": "Salmiya",
    "country_id": "117",
    "value": "Salmiya"
  },
  {
    "id": "2088",
    "text": "al-Ahmadi",
    "country_id": "117",
    "value": "al-Ahmadi"
  },
  {
    "id": "2089",
    "text": "al-Farwaniyah",
    "country_id": "117",
    "value": "al-Farwaniyah"
  },
  {
    "id": "2090",
    "text": "al-Jahra",
    "country_id": "117",
    "value": "al-Jahra"
  },
  {
    "id": "2091",
    "text": "al-Kuwayt",
    "country_id": "117",
    "value": "al-Kuwayt"
  },
  {
    "id": "2092",
    "text": "Batken",
    "country_id": "118",
    "value": "Batken"
  },
  {
    "id": "2093",
    "text": "Bishkek",
    "country_id": "118",
    "value": "Bishkek"
  },
  {
    "id": "2094",
    "text": "Chui",
    "country_id": "118",
    "value": "Chui"
  },
  {
    "id": "2095",
    "text": "Issyk-Kul",
    "country_id": "118",
    "value": "Issyk-Kul"
  },
  {
    "id": "2096",
    "text": "Jalal-Abad",
    "country_id": "118",
    "value": "Jalal-Abad"
  },
  {
    "id": "2097",
    "text": "Naryn",
    "country_id": "118",
    "value": "Naryn"
  },
  {
    "id": "2098",
    "text": "Osh",
    "country_id": "118",
    "value": "Osh"
  },
  {
    "id": "2099",
    "text": "Talas",
    "country_id": "118",
    "value": "Talas"
  },
  {
    "id": "2100",
    "text": "Attopu",
    "country_id": "119",
    "value": "Attopu"
  },
  {
    "id": "2101",
    "text": "Bokeo",
    "country_id": "119",
    "value": "Bokeo"
  },
  {
    "id": "2102",
    "text": "Bolikhamsay",
    "country_id": "119",
    "value": "Bolikhamsay"
  },
  {
    "id": "2103",
    "text": "Champasak",
    "country_id": "119",
    "value": "Champasak"
  },
  {
    "id": "2104",
    "text": "Houaphanh",
    "country_id": "119",
    "value": "Houaphanh"
  },
  {
    "id": "2105",
    "text": "Khammouane",
    "country_id": "119",
    "value": "Khammouane"
  },
  {
    "id": "2106",
    "text": "Luang Nam Tha",
    "country_id": "119",
    "value": "Luang Nam Tha"
  },
  {
    "id": "2107",
    "text": "Luang Prabang",
    "country_id": "119",
    "value": "Luang Prabang"
  },
  {
    "id": "2108",
    "text": "Oudomxay",
    "country_id": "119",
    "value": "Oudomxay"
  },
  {
    "id": "2109",
    "text": "Phongsaly",
    "country_id": "119",
    "value": "Phongsaly"
  },
  {
    "id": "2110",
    "text": "Saravan",
    "country_id": "119",
    "value": "Saravan"
  },
  {
    "id": "2111",
    "text": "Savannakhet",
    "country_id": "119",
    "value": "Savannakhet"
  },
  {
    "id": "2112",
    "text": "Sekong",
    "country_id": "119",
    "value": "Sekong"
  },
  {
    "id": "2113",
    "text": "Viangchan Prefecture",
    "country_id": "119",
    "value": "Viangchan Prefecture"
  },
  {
    "id": "2114",
    "text": "Viangchan Province",
    "country_id": "119",
    "value": "Viangchan Province"
  },
  {
    "id": "2115",
    "text": "Xaignabury",
    "country_id": "119",
    "value": "Xaignabury"
  },
  {
    "id": "2116",
    "text": "Xiang Khuang",
    "country_id": "119",
    "value": "Xiang Khuang"
  },
  {
    "id": "2117",
    "text": "Aizkraukles",
    "country_id": "120",
    "value": "Aizkraukles"
  },
  {
    "id": "2118",
    "text": "Aluksnes",
    "country_id": "120",
    "value": "Aluksnes"
  },
  {
    "id": "2119",
    "text": "Balvu",
    "country_id": "120",
    "value": "Balvu"
  },
  {
    "id": "2120",
    "text": "Bauskas",
    "country_id": "120",
    "value": "Bauskas"
  },
  {
    "id": "2121",
    "text": "Cesu",
    "country_id": "120",
    "value": "Cesu"
  },
  {
    "id": "2122",
    "text": "Daugavpils",
    "country_id": "120",
    "value": "Daugavpils"
  },
  {
    "id": "2123",
    "text": "Daugavpils City",
    "country_id": "120",
    "value": "Daugavpils City"
  },
  {
    "id": "2124",
    "text": "Dobeles",
    "country_id": "120",
    "value": "Dobeles"
  },
  {
    "id": "2125",
    "text": "Gulbenes",
    "country_id": "120",
    "value": "Gulbenes"
  },
  {
    "id": "2126",
    "text": "Jekabspils",
    "country_id": "120",
    "value": "Jekabspils"
  },
  {
    "id": "2127",
    "text": "Jelgava",
    "country_id": "120",
    "value": "Jelgava"
  },
  {
    "id": "2128",
    "text": "Jelgavas",
    "country_id": "120",
    "value": "Jelgavas"
  },
  {
    "id": "2129",
    "text": "Jurmala City",
    "country_id": "120",
    "value": "Jurmala City"
  },
  {
    "id": "2130",
    "text": "Kraslavas",
    "country_id": "120",
    "value": "Kraslavas"
  },
  {
    "id": "2131",
    "text": "Kuldigas",
    "country_id": "120",
    "value": "Kuldigas"
  },
  {
    "id": "2132",
    "text": "Liepaja",
    "country_id": "120",
    "value": "Liepaja"
  },
  {
    "id": "2133",
    "text": "Liepajas",
    "country_id": "120",
    "value": "Liepajas"
  },
  {
    "id": "2134",
    "text": "Limbazhu",
    "country_id": "120",
    "value": "Limbazhu"
  },
  {
    "id": "2135",
    "text": "Ludzas",
    "country_id": "120",
    "value": "Ludzas"
  },
  {
    "id": "2136",
    "text": "Madonas",
    "country_id": "120",
    "value": "Madonas"
  },
  {
    "id": "2137",
    "text": "Ogres",
    "country_id": "120",
    "value": "Ogres"
  },
  {
    "id": "2138",
    "text": "Preilu",
    "country_id": "120",
    "value": "Preilu"
  },
  {
    "id": "2139",
    "text": "Rezekne",
    "country_id": "120",
    "value": "Rezekne"
  },
  {
    "id": "2140",
    "text": "Rezeknes",
    "country_id": "120",
    "value": "Rezeknes"
  },
  {
    "id": "2141",
    "text": "Riga",
    "country_id": "120",
    "value": "Riga"
  },
  {
    "id": "2142",
    "text": "Rigas",
    "country_id": "120",
    "value": "Rigas"
  },
  {
    "id": "2143",
    "text": "Saldus",
    "country_id": "120",
    "value": "Saldus"
  },
  {
    "id": "2144",
    "text": "Talsu",
    "country_id": "120",
    "value": "Talsu"
  },
  {
    "id": "2145",
    "text": "Tukuma",
    "country_id": "120",
    "value": "Tukuma"
  },
  {
    "id": "2146",
    "text": "Valkas",
    "country_id": "120",
    "value": "Valkas"
  },
  {
    "id": "2147",
    "text": "Valmieras",
    "country_id": "120",
    "value": "Valmieras"
  },
  {
    "id": "2148",
    "text": "Ventspils",
    "country_id": "120",
    "value": "Ventspils"
  },
  {
    "id": "2149",
    "text": "Ventspils City",
    "country_id": "120",
    "value": "Ventspils City"
  },
  {
    "id": "2150",
    "text": "Beirut",
    "country_id": "121",
    "value": "Beirut"
  },
  {
    "id": "2151",
    "text": "Jabal Lubnan",
    "country_id": "121",
    "value": "Jabal Lubnan"
  },
  {
    "id": "2152",
    "text": "Mohafazat Liban-Nord",
    "country_id": "121",
    "value": "Mohafazat Liban-Nord"
  },
  {
    "id": "2153",
    "text": "Mohafazat Mont-Liban",
    "country_id": "121",
    "value": "Mohafazat Mont-Liban"
  },
  {
    "id": "2154",
    "text": "Sidon",
    "country_id": "121",
    "value": "Sidon"
  },
  {
    "id": "2155",
    "text": "al-Biqa",
    "country_id": "121",
    "value": "al-Biqa"
  },
  {
    "id": "2156",
    "text": "al-Janub",
    "country_id": "121",
    "value": "al-Janub"
  },
  {
    "id": "2157",
    "text": "an-Nabatiyah",
    "country_id": "121",
    "value": "an-Nabatiyah"
  },
  {
    "id": "2158",
    "text": "ash-Shamal",
    "country_id": "121",
    "value": "ash-Shamal"
  },
  {
    "id": "2159",
    "text": "Berea",
    "country_id": "122",
    "value": "Berea"
  },
  {
    "id": "2160",
    "text": "Butha-Buthe",
    "country_id": "122",
    "value": "Butha-Buthe"
  },
  {
    "id": "2161",
    "text": "Leribe",
    "country_id": "122",
    "value": "Leribe"
  },
  {
    "id": "2162",
    "text": "Mafeteng",
    "country_id": "122",
    "value": "Mafeteng"
  },
  {
    "id": "2163",
    "text": "Maseru",
    "country_id": "122",
    "value": "Maseru"
  },
  {
    "id": "2164",
    "text": "Mohale''s Hoek",
    "country_id": "122",
    "value": "Mohale''s Hoek"
  },
  {
    "id": "2165",
    "text": "Mokhotlong",
    "country_id": "122",
    "value": "Mokhotlong"
  },
  {
    "id": "2166",
    "text": "Qacha''s Nek",
    "country_id": "122",
    "value": "Qacha''s Nek"
  },
  {
    "id": "2167",
    "text": "Quthing",
    "country_id": "122",
    "value": "Quthing"
  },
  {
    "id": "2168",
    "text": "Thaba-Tseka",
    "country_id": "122",
    "value": "Thaba-Tseka"
  },
  {
    "id": "2169",
    "text": "Bomi",
    "country_id": "123",
    "value": "Bomi"
  },
  {
    "id": "2170",
    "text": "Bong",
    "country_id": "123",
    "value": "Bong"
  },
  {
    "id": "2171",
    "text": "Grand Bassa",
    "country_id": "123",
    "value": "Grand Bassa"
  },
  {
    "id": "2172",
    "text": "Grand Cape Mount",
    "country_id": "123",
    "value": "Grand Cape Mount"
  },
  {
    "id": "2173",
    "text": "Grand Gedeh",
    "country_id": "123",
    "value": "Grand Gedeh"
  },
  {
    "id": "2174",
    "text": "Loffa",
    "country_id": "123",
    "value": "Loffa"
  },
  {
    "id": "2175",
    "text": "Margibi",
    "country_id": "123",
    "value": "Margibi"
  },
  {
    "id": "2176",
    "text": "Maryland and Grand Kru",
    "country_id": "123",
    "value": "Maryland and Grand Kru"
  },
  {
    "id": "2177",
    "text": "Montserrado",
    "country_id": "123",
    "value": "Montserrado"
  },
  {
    "id": "2178",
    "text": "Nimba",
    "country_id": "123",
    "value": "Nimba"
  },
  {
    "id": "2179",
    "text": "Rivercess",
    "country_id": "123",
    "value": "Rivercess"
  },
  {
    "id": "2180",
    "text": "Sinoe",
    "country_id": "123",
    "value": "Sinoe"
  },
  {
    "id": "2181",
    "text": "Ajdabiya",
    "country_id": "124",
    "value": "Ajdabiya"
  },
  {
    "id": "2182",
    "text": "Fezzan",
    "country_id": "124",
    "value": "Fezzan"
  },
  {
    "id": "2183",
    "text": "Banghazi",
    "country_id": "124",
    "value": "Banghazi"
  },
  {
    "id": "2184",
    "text": "Darnah",
    "country_id": "124",
    "value": "Darnah"
  },
  {
    "id": "2185",
    "text": "Ghadamis",
    "country_id": "124",
    "value": "Ghadamis"
  },
  {
    "id": "2186",
    "text": "Gharyan",
    "country_id": "124",
    "value": "Gharyan"
  },
  {
    "id": "2187",
    "text": "Misratah",
    "country_id": "124",
    "value": "Misratah"
  },
  {
    "id": "2188",
    "text": "Murzuq",
    "country_id": "124",
    "value": "Murzuq"
  },
  {
    "id": "2189",
    "text": "Sabha",
    "country_id": "124",
    "value": "Sabha"
  },
  {
    "id": "2190",
    "text": "Sawfajjin",
    "country_id": "124",
    "value": "Sawfajjin"
  },
  {
    "id": "2191",
    "text": "Surt",
    "country_id": "124",
    "value": "Surt"
  },
  {
    "id": "2192",
    "text": "Tarabulus",
    "country_id": "124",
    "value": "Tarabulus"
  },
  {
    "id": "2193",
    "text": "Tarhunah",
    "country_id": "124",
    "value": "Tarhunah"
  },
  {
    "id": "2194",
    "text": "Tripolitania",
    "country_id": "124",
    "value": "Tripolitania"
  },
  {
    "id": "2195",
    "text": "Tubruq",
    "country_id": "124",
    "value": "Tubruq"
  },
  {
    "id": "2196",
    "text": "Yafran",
    "country_id": "124",
    "value": "Yafran"
  },
  {
    "id": "2197",
    "text": "Zlitan",
    "country_id": "124",
    "value": "Zlitan"
  },
  {
    "id": "2198",
    "text": "al-''Aziziyah",
    "country_id": "124",
    "value": "al-''Aziziyah"
  },
  {
    "id": "2199",
    "text": "al-Fatih",
    "country_id": "124",
    "value": "al-Fatih"
  },
  {
    "id": "2200",
    "text": "al-Jabal al Akhdar",
    "country_id": "124",
    "value": "al-Jabal al Akhdar"
  },
  {
    "id": "2201",
    "text": "al-Jufrah",
    "country_id": "124",
    "value": "al-Jufrah"
  },
  {
    "id": "2202",
    "text": "al-Khums",
    "country_id": "124",
    "value": "al-Khums"
  },
  {
    "id": "2203",
    "text": "al-Kufrah",
    "country_id": "124",
    "value": "al-Kufrah"
  },
  {
    "id": "2204",
    "text": "an-Nuqat al-Khams",
    "country_id": "124",
    "value": "an-Nuqat al-Khams"
  },
  {
    "id": "2205",
    "text": "ash-Shati",
    "country_id": "124",
    "value": "ash-Shati"
  },
  {
    "id": "2206",
    "text": "az-Zawiyah",
    "country_id": "124",
    "value": "az-Zawiyah"
  },
  {
    "id": "2207",
    "text": "Balzers",
    "country_id": "125",
    "value": "Balzers"
  },
  {
    "id": "2208",
    "text": "Eschen",
    "country_id": "125",
    "value": "Eschen"
  },
  {
    "id": "2209",
    "text": "Gamprin",
    "country_id": "125",
    "value": "Gamprin"
  },
  {
    "id": "2210",
    "text": "Mauren",
    "country_id": "125",
    "value": "Mauren"
  },
  {
    "id": "2211",
    "text": "Planken",
    "country_id": "125",
    "value": "Planken"
  },
  {
    "id": "2212",
    "text": "Ruggell",
    "country_id": "125",
    "value": "Ruggell"
  },
  {
    "id": "2213",
    "text": "Schaan",
    "country_id": "125",
    "value": "Schaan"
  },
  {
    "id": "2214",
    "text": "Schellenberg",
    "country_id": "125",
    "value": "Schellenberg"
  },
  {
    "id": "2215",
    "text": "Triesen",
    "country_id": "125",
    "value": "Triesen"
  },
  {
    "id": "2216",
    "text": "Triesenberg",
    "country_id": "125",
    "value": "Triesenberg"
  },
  {
    "id": "2217",
    "text": "Vaduz",
    "country_id": "125",
    "value": "Vaduz"
  },
  {
    "id": "2218",
    "text": "Alytaus",
    "country_id": "126",
    "value": "Alytaus"
  },
  {
    "id": "2219",
    "text": "Anyksciai",
    "country_id": "126",
    "value": "Anyksciai"
  },
  {
    "id": "2220",
    "text": "Kauno",
    "country_id": "126",
    "value": "Kauno"
  },
  {
    "id": "2221",
    "text": "Klaipedos",
    "country_id": "126",
    "value": "Klaipedos"
  },
  {
    "id": "2222",
    "text": "Marijampoles",
    "country_id": "126",
    "value": "Marijampoles"
  },
  {
    "id": "2223",
    "text": "Panevezhio",
    "country_id": "126",
    "value": "Panevezhio"
  },
  {
    "id": "2224",
    "text": "Panevezys",
    "country_id": "126",
    "value": "Panevezys"
  },
  {
    "id": "2225",
    "text": "Shiauliu",
    "country_id": "126",
    "value": "Shiauliu"
  },
  {
    "id": "2226",
    "text": "Taurages",
    "country_id": "126",
    "value": "Taurages"
  },
  {
    "id": "2227",
    "text": "Telshiu",
    "country_id": "126",
    "value": "Telshiu"
  },
  {
    "id": "2228",
    "text": "Telsiai",
    "country_id": "126",
    "value": "Telsiai"
  },
  {
    "id": "2229",
    "text": "Utenos",
    "country_id": "126",
    "value": "Utenos"
  },
  {
    "id": "2230",
    "text": "Vilniaus",
    "country_id": "126",
    "value": "Vilniaus"
  },
  {
    "id": "2231",
    "text": "Capellen",
    "country_id": "127",
    "value": "Capellen"
  },
  {
    "id": "2232",
    "text": "Clervaux",
    "country_id": "127",
    "value": "Clervaux"
  },
  {
    "id": "2233",
    "text": "Diekirch",
    "country_id": "127",
    "value": "Diekirch"
  },
  {
    "id": "2234",
    "text": "Echternach",
    "country_id": "127",
    "value": "Echternach"
  },
  {
    "id": "2235",
    "text": "Esch-sur-Alzette",
    "country_id": "127",
    "value": "Esch-sur-Alzette"
  },
  {
    "id": "2236",
    "text": "Grevenmacher",
    "country_id": "127",
    "value": "Grevenmacher"
  },
  {
    "id": "2237",
    "text": "Luxembourg",
    "country_id": "127",
    "value": "Luxembourg"
  },
  {
    "id": "2238",
    "text": "Mersch",
    "country_id": "127",
    "value": "Mersch"
  },
  {
    "id": "2239",
    "text": "Redange",
    "country_id": "127",
    "value": "Redange"
  },
  {
    "id": "2240",
    "text": "Remich",
    "country_id": "127",
    "value": "Remich"
  },
  {
    "id": "2241",
    "text": "Vianden",
    "country_id": "127",
    "value": "Vianden"
  },
  {
    "id": "2242",
    "text": "Wiltz",
    "country_id": "127",
    "value": "Wiltz"
  },
  {
    "id": "2243",
    "text": "Macau",
    "country_id": "128",
    "value": "Macau"
  },
  {
    "id": "2244",
    "text": "Berovo",
    "country_id": "129",
    "value": "Berovo"
  },
  {
    "id": "2245",
    "text": "Bitola",
    "country_id": "129",
    "value": "Bitola"
  },
  {
    "id": "2246",
    "text": "Brod",
    "country_id": "129",
    "value": "Brod"
  },
  {
    "id": "2247",
    "text": "Debar",
    "country_id": "129",
    "value": "Debar"
  },
  {
    "id": "2248",
    "text": "Delchevo",
    "country_id": "129",
    "value": "Delchevo"
  },
  {
    "id": "2249",
    "text": "Demir Hisar",
    "country_id": "129",
    "value": "Demir Hisar"
  },
  {
    "id": "2250",
    "text": "Gevgelija",
    "country_id": "129",
    "value": "Gevgelija"
  },
  {
    "id": "2251",
    "text": "Gostivar",
    "country_id": "129",
    "value": "Gostivar"
  },
  {
    "id": "2252",
    "text": "Kavadarci",
    "country_id": "129",
    "value": "Kavadarci"
  },
  {
    "id": "2253",
    "text": "Kichevo",
    "country_id": "129",
    "value": "Kichevo"
  },
  {
    "id": "2254",
    "text": "Kochani",
    "country_id": "129",
    "value": "Kochani"
  },
  {
    "id": "2255",
    "text": "Kratovo",
    "country_id": "129",
    "value": "Kratovo"
  },
  {
    "id": "2256",
    "text": "Kriva Palanka",
    "country_id": "129",
    "value": "Kriva Palanka"
  },
  {
    "id": "2257",
    "text": "Krushevo",
    "country_id": "129",
    "value": "Krushevo"
  },
  {
    "id": "2258",
    "text": "Kumanovo",
    "country_id": "129",
    "value": "Kumanovo"
  },
  {
    "id": "2259",
    "text": "Negotino",
    "country_id": "129",
    "value": "Negotino"
  },
  {
    "id": "2260",
    "text": "Ohrid",
    "country_id": "129",
    "value": "Ohrid"
  },
  {
    "id": "2261",
    "text": "Prilep",
    "country_id": "129",
    "value": "Prilep"
  },
  {
    "id": "2262",
    "text": "Probishtip",
    "country_id": "129",
    "value": "Probishtip"
  },
  {
    "id": "2263",
    "text": "Radovish",
    "country_id": "129",
    "value": "Radovish"
  },
  {
    "id": "2264",
    "text": "Resen",
    "country_id": "129",
    "value": "Resen"
  },
  {
    "id": "2265",
    "text": "Shtip",
    "country_id": "129",
    "value": "Shtip"
  },
  {
    "id": "2266",
    "text": "Skopje",
    "country_id": "129",
    "value": "Skopje"
  },
  {
    "id": "2267",
    "text": "Struga",
    "country_id": "129",
    "value": "Struga"
  },
  {
    "id": "2268",
    "text": "Strumica",
    "country_id": "129",
    "value": "Strumica"
  },
  {
    "id": "2269",
    "text": "Sveti Nikole",
    "country_id": "129",
    "value": "Sveti Nikole"
  },
  {
    "id": "2270",
    "text": "Tetovo",
    "country_id": "129",
    "value": "Tetovo"
  },
  {
    "id": "2271",
    "text": "Valandovo",
    "country_id": "129",
    "value": "Valandovo"
  },
  {
    "id": "2272",
    "text": "Veles",
    "country_id": "129",
    "value": "Veles"
  },
  {
    "id": "2273",
    "text": "Vinica",
    "country_id": "129",
    "value": "Vinica"
  },
  {
    "id": "2274",
    "text": "Antananarivo",
    "country_id": "130",
    "value": "Antananarivo"
  },
  {
    "id": "2275",
    "text": "Antsiranana",
    "country_id": "130",
    "value": "Antsiranana"
  },
  {
    "id": "2276",
    "text": "Fianarantsoa",
    "country_id": "130",
    "value": "Fianarantsoa"
  },
  {
    "id": "2277",
    "text": "Mahajanga",
    "country_id": "130",
    "value": "Mahajanga"
  },
  {
    "id": "2278",
    "text": "Toamasina",
    "country_id": "130",
    "value": "Toamasina"
  },
  {
    "id": "2279",
    "text": "Toliary",
    "country_id": "130",
    "value": "Toliary"
  },
  {
    "id": "2280",
    "text": "Balaka",
    "country_id": "131",
    "value": "Balaka"
  },
  {
    "id": "2281",
    "text": "Blantyre City",
    "country_id": "131",
    "value": "Blantyre City"
  },
  {
    "id": "2282",
    "text": "Chikwawa",
    "country_id": "131",
    "value": "Chikwawa"
  },
  {
    "id": "2283",
    "text": "Chiradzulu",
    "country_id": "131",
    "value": "Chiradzulu"
  },
  {
    "id": "2284",
    "text": "Chitipa",
    "country_id": "131",
    "value": "Chitipa"
  },
  {
    "id": "2285",
    "text": "Dedza",
    "country_id": "131",
    "value": "Dedza"
  },
  {
    "id": "2286",
    "text": "Dowa",
    "country_id": "131",
    "value": "Dowa"
  },
  {
    "id": "2287",
    "text": "Karonga",
    "country_id": "131",
    "value": "Karonga"
  },
  {
    "id": "2288",
    "text": "Kasungu",
    "country_id": "131",
    "value": "Kasungu"
  },
  {
    "id": "2289",
    "text": "Lilongwe City",
    "country_id": "131",
    "value": "Lilongwe City"
  },
  {
    "id": "2290",
    "text": "Machinga",
    "country_id": "131",
    "value": "Machinga"
  },
  {
    "id": "2291",
    "text": "Mangochi",
    "country_id": "131",
    "value": "Mangochi"
  },
  {
    "id": "2292",
    "text": "Mchinji",
    "country_id": "131",
    "value": "Mchinji"
  },
  {
    "id": "2293",
    "text": "Mulanje",
    "country_id": "131",
    "value": "Mulanje"
  },
  {
    "id": "2294",
    "text": "Mwanza",
    "country_id": "131",
    "value": "Mwanza"
  },
  {
    "id": "2295",
    "text": "Mzimba",
    "country_id": "131",
    "value": "Mzimba"
  },
  {
    "id": "2296",
    "text": "Mzuzu City",
    "country_id": "131",
    "value": "Mzuzu City"
  },
  {
    "id": "2297",
    "text": "Nkhata Bay",
    "country_id": "131",
    "value": "Nkhata Bay"
  },
  {
    "id": "2298",
    "text": "Nkhotakota",
    "country_id": "131",
    "value": "Nkhotakota"
  },
  {
    "id": "2299",
    "text": "Nsanje",
    "country_id": "131",
    "value": "Nsanje"
  },
  {
    "id": "2300",
    "text": "Ntcheu",
    "country_id": "131",
    "value": "Ntcheu"
  },
  {
    "id": "2301",
    "text": "Ntchisi",
    "country_id": "131",
    "value": "Ntchisi"
  },
  {
    "id": "2302",
    "text": "Phalombe",
    "country_id": "131",
    "value": "Phalombe"
  },
  {
    "id": "2303",
    "text": "Rumphi",
    "country_id": "131",
    "value": "Rumphi"
  },
  {
    "id": "2304",
    "text": "Salima",
    "country_id": "131",
    "value": "Salima"
  },
  {
    "id": "2305",
    "text": "Thyolo",
    "country_id": "131",
    "value": "Thyolo"
  },
  {
    "id": "2306",
    "text": "Zomba Municipality",
    "country_id": "131",
    "value": "Zomba Municipality"
  },
  {
    "id": "2307",
    "text": "Johor",
    "country_id": "132",
    "value": "Johor"
  },
  {
    "id": "2308",
    "text": "Kedah",
    "country_id": "132",
    "value": "Kedah"
  },
  {
    "id": "2309",
    "text": "Kelantan",
    "country_id": "132",
    "value": "Kelantan"
  },
  {
    "id": "2310",
    "text": "Kuala Lumpur",
    "country_id": "132",
    "value": "Kuala Lumpur"
  },
  {
    "id": "2311",
    "text": "Labuan",
    "country_id": "132",
    "value": "Labuan"
  },
  {
    "id": "2312",
    "text": "Melaka",
    "country_id": "132",
    "value": "Melaka"
  },
  {
    "id": "2313",
    "text": "Negeri Johor",
    "country_id": "132",
    "value": "Negeri Johor"
  },
  {
    "id": "2314",
    "text": "Negeri Sembilan",
    "country_id": "132",
    "value": "Negeri Sembilan"
  },
  {
    "id": "2315",
    "text": "Pahang",
    "country_id": "132",
    "value": "Pahang"
  },
  {
    "id": "2316",
    "text": "Penang",
    "country_id": "132",
    "value": "Penang"
  },
  {
    "id": "2317",
    "text": "Perak",
    "country_id": "132",
    "value": "Perak"
  },
  {
    "id": "2318",
    "text": "Perlis",
    "country_id": "132",
    "value": "Perlis"
  },
  {
    "id": "2319",
    "text": "Pulau Pinang",
    "country_id": "132",
    "value": "Pulau Pinang"
  },
  {
    "id": "2320",
    "text": "Sabah",
    "country_id": "132",
    "value": "Sabah"
  },
  {
    "id": "2321",
    "text": "Sarawak",
    "country_id": "132",
    "value": "Sarawak"
  },
  {
    "id": "2322",
    "text": "Selangor",
    "country_id": "132",
    "value": "Selangor"
  },
  {
    "id": "2323",
    "text": "Sembilan",
    "country_id": "132",
    "value": "Sembilan"
  },
  {
    "id": "2324",
    "text": "Terengganu",
    "country_id": "132",
    "value": "Terengganu"
  },
  {
    "id": "2325",
    "text": "Alif Alif",
    "country_id": "133",
    "value": "Alif Alif"
  },
  {
    "id": "2326",
    "text": "Alif Dhaal",
    "country_id": "133",
    "value": "Alif Dhaal"
  },
  {
    "id": "2327",
    "text": "Baa",
    "country_id": "133",
    "value": "Baa"
  },
  {
    "id": "2328",
    "text": "Dhaal",
    "country_id": "133",
    "value": "Dhaal"
  },
  {
    "id": "2329",
    "text": "Faaf",
    "country_id": "133",
    "value": "Faaf"
  },
  {
    "id": "2330",
    "text": "Gaaf Alif",
    "country_id": "133",
    "value": "Gaaf Alif"
  },
  {
    "id": "2331",
    "text": "Gaaf Dhaal",
    "country_id": "133",
    "value": "Gaaf Dhaal"
  },
  {
    "id": "2332",
    "text": "Ghaviyani",
    "country_id": "133",
    "value": "Ghaviyani"
  },
  {
    "id": "2333",
    "text": "Haa Alif",
    "country_id": "133",
    "value": "Haa Alif"
  },
  {
    "id": "2334",
    "text": "Haa Dhaal",
    "country_id": "133",
    "value": "Haa Dhaal"
  },
  {
    "id": "2335",
    "text": "Kaaf",
    "country_id": "133",
    "value": "Kaaf"
  },
  {
    "id": "2336",
    "text": "Laam",
    "country_id": "133",
    "value": "Laam"
  },
  {
    "id": "2337",
    "text": "Lhaviyani",
    "country_id": "133",
    "value": "Lhaviyani"
  },
  {
    "id": "2338",
    "text": "Male",
    "country_id": "133",
    "value": "Male"
  },
  {
    "id": "2339",
    "text": "Miim",
    "country_id": "133",
    "value": "Miim"
  },
  {
    "id": "2340",
    "text": "Nuun",
    "country_id": "133",
    "value": "Nuun"
  },
  {
    "id": "2341",
    "text": "Raa",
    "country_id": "133",
    "value": "Raa"
  },
  {
    "id": "2342",
    "text": "Shaviyani",
    "country_id": "133",
    "value": "Shaviyani"
  },
  {
    "id": "2343",
    "text": "Siin",
    "country_id": "133",
    "value": "Siin"
  },
  {
    "id": "2344",
    "text": "Thaa",
    "country_id": "133",
    "value": "Thaa"
  },
  {
    "id": "2345",
    "text": "Vaav",
    "country_id": "133",
    "value": "Vaav"
  },
  {
    "id": "2346",
    "text": "Bamako",
    "country_id": "134",
    "value": "Bamako"
  },
  {
    "id": "2347",
    "text": "Gao",
    "country_id": "134",
    "value": "Gao"
  },
  {
    "id": "2348",
    "text": "Kayes",
    "country_id": "134",
    "value": "Kayes"
  },
  {
    "id": "2349",
    "text": "Kidal",
    "country_id": "134",
    "value": "Kidal"
  },
  {
    "id": "2350",
    "text": "Koulikoro",
    "country_id": "134",
    "value": "Koulikoro"
  },
  {
    "id": "2351",
    "text": "Mopti",
    "country_id": "134",
    "value": "Mopti"
  },
  {
    "id": "2352",
    "text": "Segou",
    "country_id": "134",
    "value": "Segou"
  },
  {
    "id": "2353",
    "text": "Sikasso",
    "country_id": "134",
    "value": "Sikasso"
  },
  {
    "id": "2354",
    "text": "Tombouctou",
    "country_id": "134",
    "value": "Tombouctou"
  },
  {
    "id": "2355",
    "text": "Gozo and Comino",
    "country_id": "135",
    "value": "Gozo and Comino"
  },
  {
    "id": "2356",
    "text": "Inner Harbour",
    "country_id": "135",
    "value": "Inner Harbour"
  },
  {
    "id": "2357",
    "text": "Northern",
    "country_id": "135",
    "value": "Northern"
  },
  {
    "id": "2358",
    "text": "Outer Harbour",
    "country_id": "135",
    "value": "Outer Harbour"
  },
  {
    "id": "2359",
    "text": "South Eastern",
    "country_id": "135",
    "value": "South Eastern"
  },
  {
    "id": "2360",
    "text": "Valletta",
    "country_id": "135",
    "value": "Valletta"
  },
  {
    "id": "2361",
    "text": "Western",
    "country_id": "135",
    "value": "Western"
  },
  {
    "id": "2362",
    "text": "Castletown",
    "country_id": "136",
    "value": "Castletown"
  },
  {
    "id": "2363",
    "text": "Douglas",
    "country_id": "136",
    "value": "Douglas"
  },
  {
    "id": "2364",
    "text": "Laxey",
    "country_id": "136",
    "value": "Laxey"
  },
  {
    "id": "2365",
    "text": "Onchan",
    "country_id": "136",
    "value": "Onchan"
  },
  {
    "id": "2366",
    "text": "Peel",
    "country_id": "136",
    "value": "Peel"
  },
  {
    "id": "2367",
    "text": "Port Erin",
    "country_id": "136",
    "value": "Port Erin"
  },
  {
    "id": "2368",
    "text": "Port Saint Mary",
    "country_id": "136",
    "value": "Port Saint Mary"
  },
  {
    "id": "2369",
    "text": "Ramsey",
    "country_id": "136",
    "value": "Ramsey"
  },
  {
    "id": "2370",
    "text": "Ailinlaplap",
    "country_id": "137",
    "value": "Ailinlaplap"
  },
  {
    "id": "2371",
    "text": "Ailuk",
    "country_id": "137",
    "value": "Ailuk"
  },
  {
    "id": "2372",
    "text": "Arno",
    "country_id": "137",
    "value": "Arno"
  },
  {
    "id": "2373",
    "text": "Aur",
    "country_id": "137",
    "value": "Aur"
  },
  {
    "id": "2374",
    "text": "Bikini",
    "country_id": "137",
    "value": "Bikini"
  },
  {
    "id": "2375",
    "text": "Ebon",
    "country_id": "137",
    "value": "Ebon"
  },
  {
    "id": "2376",
    "text": "Enewetak",
    "country_id": "137",
    "value": "Enewetak"
  },
  {
    "id": "2377",
    "text": "Jabat",
    "country_id": "137",
    "value": "Jabat"
  },
  {
    "id": "2378",
    "text": "Jaluit",
    "country_id": "137",
    "value": "Jaluit"
  },
  {
    "id": "2379",
    "text": "Kili",
    "country_id": "137",
    "value": "Kili"
  },
  {
    "id": "2380",
    "text": "Kwajalein",
    "country_id": "137",
    "value": "Kwajalein"
  },
  {
    "id": "2381",
    "text": "Lae",
    "country_id": "137",
    "value": "Lae"
  },
  {
    "id": "2382",
    "text": "Lib",
    "country_id": "137",
    "value": "Lib"
  },
  {
    "id": "2383",
    "text": "Likiep",
    "country_id": "137",
    "value": "Likiep"
  },
  {
    "id": "2384",
    "text": "Majuro",
    "country_id": "137",
    "value": "Majuro"
  },
  {
    "id": "2385",
    "text": "Maloelap",
    "country_id": "137",
    "value": "Maloelap"
  },
  {
    "id": "2386",
    "text": "Mejit",
    "country_id": "137",
    "value": "Mejit"
  },
  {
    "id": "2387",
    "text": "Mili",
    "country_id": "137",
    "value": "Mili"
  },
  {
    "id": "2388",
    "text": "Namorik",
    "country_id": "137",
    "value": "Namorik"
  },
  {
    "id": "2389",
    "text": "Namu",
    "country_id": "137",
    "value": "Namu"
  },
  {
    "id": "2390",
    "text": "Rongelap",
    "country_id": "137",
    "value": "Rongelap"
  },
  {
    "id": "2391",
    "text": "Ujae",
    "country_id": "137",
    "value": "Ujae"
  },
  {
    "id": "2392",
    "text": "Utrik",
    "country_id": "137",
    "value": "Utrik"
  },
  {
    "id": "2393",
    "text": "Wotho",
    "country_id": "137",
    "value": "Wotho"
  },
  {
    "id": "2394",
    "text": "Wotje",
    "country_id": "137",
    "value": "Wotje"
  },
  {
    "id": "2395",
    "text": "Fort-de-France",
    "country_id": "138",
    "value": "Fort-de-France"
  },
  {
    "id": "2396",
    "text": "La Trinite",
    "country_id": "138",
    "value": "La Trinite"
  },
  {
    "id": "2397",
    "text": "Le Marin",
    "country_id": "138",
    "value": "Le Marin"
  },
  {
    "id": "2398",
    "text": "Saint-Pierre",
    "country_id": "138",
    "value": "Saint-Pierre"
  },
  {
    "id": "2399",
    "text": "Adrar",
    "country_id": "139",
    "value": "Adrar"
  },
  {
    "id": "2400",
    "text": "Assaba",
    "country_id": "139",
    "value": "Assaba"
  },
  {
    "id": "2401",
    "text": "Brakna",
    "country_id": "139",
    "value": "Brakna"
  },
  {
    "id": "2402",
    "text": "Dhakhlat Nawadibu",
    "country_id": "139",
    "value": "Dhakhlat Nawadibu"
  },
  {
    "id": "2403",
    "text": "Hudh-al-Gharbi",
    "country_id": "139",
    "value": "Hudh-al-Gharbi"
  },
  {
    "id": "2404",
    "text": "Hudh-ash-Sharqi",
    "country_id": "139",
    "value": "Hudh-ash-Sharqi"
  },
  {
    "id": "2405",
    "text": "Inshiri",
    "country_id": "139",
    "value": "Inshiri"
  },
  {
    "id": "2406",
    "text": "Nawakshut",
    "country_id": "139",
    "value": "Nawakshut"
  },
  {
    "id": "2407",
    "text": "Qidimagha",
    "country_id": "139",
    "value": "Qidimagha"
  },
  {
    "id": "2408",
    "text": "Qurqul",
    "country_id": "139",
    "value": "Qurqul"
  },
  {
    "id": "2409",
    "text": "Taqant",
    "country_id": "139",
    "value": "Taqant"
  },
  {
    "id": "2410",
    "text": "Tiris Zammur",
    "country_id": "139",
    "value": "Tiris Zammur"
  },
  {
    "id": "2411",
    "text": "Trarza",
    "country_id": "139",
    "value": "Trarza"
  },
  {
    "id": "2412",
    "text": "Black River",
    "country_id": "140",
    "value": "Black River"
  },
  {
    "id": "2413",
    "text": "Eau Coulee",
    "country_id": "140",
    "value": "Eau Coulee"
  },
  {
    "id": "2414",
    "text": "Flacq",
    "country_id": "140",
    "value": "Flacq"
  },
  {
    "id": "2415",
    "text": "Floreal",
    "country_id": "140",
    "value": "Floreal"
  },
  {
    "id": "2416",
    "text": "Grand Port",
    "country_id": "140",
    "value": "Grand Port"
  },
  {
    "id": "2417",
    "text": "Moka",
    "country_id": "140",
    "value": "Moka"
  },
  {
    "id": "2418",
    "text": "Pamplempousses",
    "country_id": "140",
    "value": "Pamplempousses"
  },
  {
    "id": "2419",
    "text": "Plaines Wilhelm",
    "country_id": "140",
    "value": "Plaines Wilhelm"
  },
  {
    "id": "2420",
    "text": "Port Louis",
    "country_id": "140",
    "value": "Port Louis"
  },
  {
    "id": "2421",
    "text": "Riviere du Rempart",
    "country_id": "140",
    "value": "Riviere du Rempart"
  },
  {
    "id": "2422",
    "text": "Rodrigues",
    "country_id": "140",
    "value": "Rodrigues"
  },
  {
    "id": "2423",
    "text": "Rose Hill",
    "country_id": "140",
    "value": "Rose Hill"
  },
  {
    "id": "2424",
    "text": "Savanne",
    "country_id": "140",
    "value": "Savanne"
  },
  {
    "id": "2425",
    "text": "Mayotte",
    "country_id": "141",
    "value": "Mayotte"
  },
  {
    "id": "2426",
    "text": "Pamanzi",
    "country_id": "141",
    "value": "Pamanzi"
  },
  {
    "id": "2427",
    "text": "Aguascalientes",
    "country_id": "142",
    "value": "Aguascalientes"
  },
  {
    "id": "2428",
    "text": "Baja California",
    "country_id": "142",
    "value": "Baja California"
  },
  {
    "id": "2429",
    "text": "Baja California Sur",
    "country_id": "142",
    "value": "Baja California Sur"
  },
  {
    "id": "2430",
    "text": "Campeche",
    "country_id": "142",
    "value": "Campeche"
  },
  {
    "id": "2431",
    "text": "Chiapas",
    "country_id": "142",
    "value": "Chiapas"
  },
  {
    "id": "2432",
    "text": "Chihuahua",
    "country_id": "142",
    "value": "Chihuahua"
  },
  {
    "id": "2433",
    "text": "Coahuila",
    "country_id": "142",
    "value": "Coahuila"
  },
  {
    "id": "2434",
    "text": "Colima",
    "country_id": "142",
    "value": "Colima"
  },
  {
    "id": "2435",
    "text": "Distrito Federal",
    "country_id": "142",
    "value": "Distrito Federal"
  },
  {
    "id": "2436",
    "text": "Durango",
    "country_id": "142",
    "value": "Durango"
  },
  {
    "id": "2437",
    "text": "Estado de Mexico",
    "country_id": "142",
    "value": "Estado de Mexico"
  },
  {
    "id": "2438",
    "text": "Guanajuato",
    "country_id": "142",
    "value": "Guanajuato"
  },
  {
    "id": "2439",
    "text": "Guerrero",
    "country_id": "142",
    "value": "Guerrero"
  },
  {
    "id": "2440",
    "text": "Hidalgo",
    "country_id": "142",
    "value": "Hidalgo"
  },
  {
    "id": "2441",
    "text": "Jalisco",
    "country_id": "142",
    "value": "Jalisco"
  },
  {
    "id": "2442",
    "text": "Mexico",
    "country_id": "142",
    "value": "Mexico"
  },
  {
    "id": "2443",
    "text": "Michoacan",
    "country_id": "142",
    "value": "Michoacan"
  },
  {
    "id": "2444",
    "text": "Morelos",
    "country_id": "142",
    "value": "Morelos"
  },
  {
    "id": "2445",
    "text": "Nayarit",
    "country_id": "142",
    "value": "Nayarit"
  },
  {
    "id": "2446",
    "text": "Nuevo Leon",
    "country_id": "142",
    "value": "Nuevo Leon"
  },
  {
    "id": "2447",
    "text": "Oaxaca",
    "country_id": "142",
    "value": "Oaxaca"
  },
  {
    "id": "2448",
    "text": "Puebla",
    "country_id": "142",
    "value": "Puebla"
  },
  {
    "id": "2449",
    "text": "Queretaro",
    "country_id": "142",
    "value": "Queretaro"
  },
  {
    "id": "2450",
    "text": "Quintana Roo",
    "country_id": "142",
    "value": "Quintana Roo"
  },
  {
    "id": "2451",
    "text": "San Luis Potosi",
    "country_id": "142",
    "value": "San Luis Potosi"
  },
  {
    "id": "2452",
    "text": "Sinaloa",
    "country_id": "142",
    "value": "Sinaloa"
  },
  {
    "id": "2453",
    "text": "Sonora",
    "country_id": "142",
    "value": "Sonora"
  },
  {
    "id": "2454",
    "text": "Tabasco",
    "country_id": "142",
    "value": "Tabasco"
  },
  {
    "id": "2455",
    "text": "Tamaulipas",
    "country_id": "142",
    "value": "Tamaulipas"
  },
  {
    "id": "2456",
    "text": "Tlaxcala",
    "country_id": "142",
    "value": "Tlaxcala"
  },
  {
    "id": "2457",
    "text": "Veracruz",
    "country_id": "142",
    "value": "Veracruz"
  },
  {
    "id": "2458",
    "text": "Yucatan",
    "country_id": "142",
    "value": "Yucatan"
  },
  {
    "id": "2459",
    "text": "Zacatecas",
    "country_id": "142",
    "value": "Zacatecas"
  },
  {
    "id": "2460",
    "text": "Chuuk",
    "country_id": "143",
    "value": "Chuuk"
  },
  {
    "id": "2461",
    "text": "Kusaie",
    "country_id": "143",
    "value": "Kusaie"
  },
  {
    "id": "2462",
    "text": "Pohnpei",
    "country_id": "143",
    "value": "Pohnpei"
  },
  {
    "id": "2463",
    "text": "Yap",
    "country_id": "143",
    "value": "Yap"
  },
  {
    "id": "2464",
    "text": "Balti",
    "country_id": "144",
    "value": "Balti"
  },
  {
    "id": "2465",
    "text": "Cahul",
    "country_id": "144",
    "value": "Cahul"
  },
  {
    "id": "2466",
    "text": "Chisinau",
    "country_id": "144",
    "value": "Chisinau"
  },
  {
    "id": "2467",
    "text": "Chisinau Oras",
    "country_id": "144",
    "value": "Chisinau Oras"
  },
  {
    "id": "2468",
    "text": "Edinet",
    "country_id": "144",
    "value": "Edinet"
  },
  {
    "id": "2469",
    "text": "Gagauzia",
    "country_id": "144",
    "value": "Gagauzia"
  },
  {
    "id": "2470",
    "text": "Lapusna",
    "country_id": "144",
    "value": "Lapusna"
  },
  {
    "id": "2471",
    "text": "Orhei",
    "country_id": "144",
    "value": "Orhei"
  },
  {
    "id": "2472",
    "text": "Soroca",
    "country_id": "144",
    "value": "Soroca"
  },
  {
    "id": "2473",
    "text": "Taraclia",
    "country_id": "144",
    "value": "Taraclia"
  },
  {
    "id": "2474",
    "text": "Tighina",
    "country_id": "144",
    "value": "Tighina"
  },
  {
    "id": "2475",
    "text": "Transnistria",
    "country_id": "144",
    "value": "Transnistria"
  },
  {
    "id": "2476",
    "text": "Ungheni",
    "country_id": "144",
    "value": "Ungheni"
  },
  {
    "id": "2477",
    "text": "Fontvieille",
    "country_id": "145",
    "value": "Fontvieille"
  },
  {
    "id": "2478",
    "text": "La Condamine",
    "country_id": "145",
    "value": "La Condamine"
  },
  {
    "id": "2479",
    "text": "Monaco-Ville",
    "country_id": "145",
    "value": "Monaco-Ville"
  },
  {
    "id": "2480",
    "text": "Monte Carlo",
    "country_id": "145",
    "value": "Monte Carlo"
  },
  {
    "id": "2481",
    "text": "Arhangaj",
    "country_id": "146",
    "value": "Arhangaj"
  },
  {
    "id": "2482",
    "text": "Bajan-Olgij",
    "country_id": "146",
    "value": "Bajan-Olgij"
  },
  {
    "id": "2483",
    "text": "Bajanhongor",
    "country_id": "146",
    "value": "Bajanhongor"
  },
  {
    "id": "2484",
    "text": "Bulgan",
    "country_id": "146",
    "value": "Bulgan"
  },
  {
    "id": "2485",
    "text": "Darhan-Uul",
    "country_id": "146",
    "value": "Darhan-Uul"
  },
  {
    "id": "2486",
    "text": "Dornod",
    "country_id": "146",
    "value": "Dornod"
  },
  {
    "id": "2487",
    "text": "Dornogovi",
    "country_id": "146",
    "value": "Dornogovi"
  },
  {
    "id": "2488",
    "text": "Dundgovi",
    "country_id": "146",
    "value": "Dundgovi"
  },
  {
    "id": "2489",
    "text": "Govi-Altaj",
    "country_id": "146",
    "value": "Govi-Altaj"
  },
  {
    "id": "2490",
    "text": "Govisumber",
    "country_id": "146",
    "value": "Govisumber"
  },
  {
    "id": "2491",
    "text": "Hentij",
    "country_id": "146",
    "value": "Hentij"
  },
  {
    "id": "2492",
    "text": "Hovd",
    "country_id": "146",
    "value": "Hovd"
  },
  {
    "id": "2493",
    "text": "Hovsgol",
    "country_id": "146",
    "value": "Hovsgol"
  },
  {
    "id": "2494",
    "text": "Omnogovi",
    "country_id": "146",
    "value": "Omnogovi"
  },
  {
    "id": "2495",
    "text": "Orhon",
    "country_id": "146",
    "value": "Orhon"
  },
  {
    "id": "2496",
    "text": "Ovorhangaj",
    "country_id": "146",
    "value": "Ovorhangaj"
  },
  {
    "id": "2497",
    "text": "Selenge",
    "country_id": "146",
    "value": "Selenge"
  },
  {
    "id": "2498",
    "text": "Suhbaatar",
    "country_id": "146",
    "value": "Suhbaatar"
  },
  {
    "id": "2499",
    "text": "Tov",
    "country_id": "146",
    "value": "Tov"
  },
  {
    "id": "2500",
    "text": "Ulaanbaatar",
    "country_id": "146",
    "value": "Ulaanbaatar"
  },
  {
    "id": "2501",
    "text": "Uvs",
    "country_id": "146",
    "value": "Uvs"
  },
  {
    "id": "2502",
    "text": "Zavhan",
    "country_id": "146",
    "value": "Zavhan"
  },
  {
    "id": "2503",
    "text": "Montserrat",
    "country_id": "147",
    "value": "Montserrat"
  },
  {
    "id": "2504",
    "text": "Agadir",
    "country_id": "148",
    "value": "Agadir"
  },
  {
    "id": "2505",
    "text": "Casablanca",
    "country_id": "148",
    "value": "Casablanca"
  },
  {
    "id": "2506",
    "text": "Chaouia-Ouardigha",
    "country_id": "148",
    "value": "Chaouia-Ouardigha"
  },
  {
    "id": "2507",
    "text": "Doukkala-Abda",
    "country_id": "148",
    "value": "Doukkala-Abda"
  },
  {
    "id": "2508",
    "text": "Fes-Boulemane",
    "country_id": "148",
    "value": "Fes-Boulemane"
  },
  {
    "id": "2509",
    "text": "Gharb-Chrarda-Beni Hssen",
    "country_id": "148",
    "value": "Gharb-Chrarda-Beni Hssen"
  },
  {
    "id": "2510",
    "text": "Guelmim",
    "country_id": "148",
    "value": "Guelmim"
  },
  {
    "id": "2511",
    "text": "Kenitra",
    "country_id": "148",
    "value": "Kenitra"
  },
  {
    "id": "2512",
    "text": "Marrakech-Tensift-Al Haouz",
    "country_id": "148",
    "value": "Marrakech-Tensift-Al Haouz"
  },
  {
    "id": "2513",
    "text": "Meknes-Tafilalet",
    "country_id": "148",
    "value": "Meknes-Tafilalet"
  },
  {
    "id": "2514",
    "text": "Oriental",
    "country_id": "148",
    "value": "Oriental"
  },
  {
    "id": "2515",
    "text": "Oujda",
    "country_id": "148",
    "value": "Oujda"
  },
  {
    "id": "2516",
    "text": "Province de Tanger",
    "country_id": "148",
    "value": "Province de Tanger"
  },
  {
    "id": "2517",
    "text": "Rabat-Sale-Zammour-Zaer",
    "country_id": "148",
    "value": "Rabat-Sale-Zammour-Zaer"
  },
  {
    "id": "2518",
    "text": "Sala Al Jadida",
    "country_id": "148",
    "value": "Sala Al Jadida"
  },
  {
    "id": "2519",
    "text": "Settat",
    "country_id": "148",
    "value": "Settat"
  },
  {
    "id": "2520",
    "text": "Souss Massa-Draa",
    "country_id": "148",
    "value": "Souss Massa-Draa"
  },
  {
    "id": "2521",
    "text": "Tadla-Azilal",
    "country_id": "148",
    "value": "Tadla-Azilal"
  },
  {
    "id": "2522",
    "text": "Tangier-Tetouan",
    "country_id": "148",
    "value": "Tangier-Tetouan"
  },
  {
    "id": "2523",
    "text": "Taza-Al Hoceima-Taounate",
    "country_id": "148",
    "value": "Taza-Al Hoceima-Taounate"
  },
  {
    "id": "2524",
    "text": "Wilaya de Casablanca",
    "country_id": "148",
    "value": "Wilaya de Casablanca"
  },
  {
    "id": "2525",
    "text": "Wilaya de Rabat-Sale",
    "country_id": "148",
    "value": "Wilaya de Rabat-Sale"
  },
  {
    "id": "2526",
    "text": "Cabo Delgado",
    "country_id": "149",
    "value": "Cabo Delgado"
  },
  {
    "id": "2527",
    "text": "Gaza",
    "country_id": "149",
    "value": "Gaza"
  },
  {
    "id": "2528",
    "text": "Inhambane",
    "country_id": "149",
    "value": "Inhambane"
  },
  {
    "id": "2529",
    "text": "Manica",
    "country_id": "149",
    "value": "Manica"
  },
  {
    "id": "2530",
    "text": "Maputo",
    "country_id": "149",
    "value": "Maputo"
  },
  {
    "id": "2531",
    "text": "Maputo Provincia",
    "country_id": "149",
    "value": "Maputo Provincia"
  },
  {
    "id": "2532",
    "text": "Nampula",
    "country_id": "149",
    "value": "Nampula"
  },
  {
    "id": "2533",
    "text": "Niassa",
    "country_id": "149",
    "value": "Niassa"
  },
  {
    "id": "2534",
    "text": "Sofala",
    "country_id": "149",
    "value": "Sofala"
  },
  {
    "id": "2535",
    "text": "Tete",
    "country_id": "149",
    "value": "Tete"
  },
  {
    "id": "2536",
    "text": "Zambezia",
    "country_id": "149",
    "value": "Zambezia"
  },
  {
    "id": "2537",
    "text": "Ayeyarwady",
    "country_id": "150",
    "value": "Ayeyarwady"
  },
  {
    "id": "2538",
    "text": "Bago",
    "country_id": "150",
    "value": "Bago"
  },
  {
    "id": "2539",
    "text": "Chin",
    "country_id": "150",
    "value": "Chin"
  },
  {
    "id": "2540",
    "text": "Kachin",
    "country_id": "150",
    "value": "Kachin"
  },
  {
    "id": "2541",
    "text": "Kayah",
    "country_id": "150",
    "value": "Kayah"
  },
  {
    "id": "2542",
    "text": "Kayin",
    "country_id": "150",
    "value": "Kayin"
  },
  {
    "id": "2543",
    "text": "Magway",
    "country_id": "150",
    "value": "Magway"
  },
  {
    "id": "2544",
    "text": "Mandalay",
    "country_id": "150",
    "value": "Mandalay"
  },
  {
    "id": "2545",
    "text": "Mon",
    "country_id": "150",
    "value": "Mon"
  },
  {
    "id": "2546",
    "text": "Nay Pyi Taw",
    "country_id": "150",
    "value": "Nay Pyi Taw"
  },
  {
    "id": "2547",
    "text": "Rakhine",
    "country_id": "150",
    "value": "Rakhine"
  },
  {
    "id": "2548",
    "text": "Sagaing",
    "country_id": "150",
    "value": "Sagaing"
  },
  {
    "id": "2549",
    "text": "Shan",
    "country_id": "150",
    "value": "Shan"
  },
  {
    "id": "2550",
    "text": "Tanintharyi",
    "country_id": "150",
    "value": "Tanintharyi"
  },
  {
    "id": "2551",
    "text": "Yangon",
    "country_id": "150",
    "value": "Yangon"
  },
  {
    "id": "2552",
    "text": "Caprivi",
    "country_id": "151",
    "value": "Caprivi"
  },
  {
    "id": "2553",
    "text": "Erongo",
    "country_id": "151",
    "value": "Erongo"
  },
  {
    "id": "2554",
    "text": "Hardap",
    "country_id": "151",
    "value": "Hardap"
  },
  {
    "id": "2555",
    "text": "Karas",
    "country_id": "151",
    "value": "Karas"
  },
  {
    "id": "2556",
    "text": "Kavango",
    "country_id": "151",
    "value": "Kavango"
  },
  {
    "id": "2557",
    "text": "Khomas",
    "country_id": "151",
    "value": "Khomas"
  },
  {
    "id": "2558",
    "text": "Kunene",
    "country_id": "151",
    "value": "Kunene"
  },
  {
    "id": "2559",
    "text": "Ohangwena",
    "country_id": "151",
    "value": "Ohangwena"
  },
  {
    "id": "2560",
    "text": "Omaheke",
    "country_id": "151",
    "value": "Omaheke"
  },
  {
    "id": "2561",
    "text": "Omusati",
    "country_id": "151",
    "value": "Omusati"
  },
  {
    "id": "2562",
    "text": "Oshana",
    "country_id": "151",
    "value": "Oshana"
  },
  {
    "id": "2563",
    "text": "Oshikoto",
    "country_id": "151",
    "value": "Oshikoto"
  },
  {
    "id": "2564",
    "text": "Otjozondjupa",
    "country_id": "151",
    "value": "Otjozondjupa"
  },
  {
    "id": "2565",
    "text": "Yaren",
    "country_id": "152",
    "value": "Yaren"
  },
  {
    "id": "2566",
    "text": "Bagmati",
    "country_id": "153",
    "value": "Bagmati"
  },
  {
    "id": "2567",
    "text": "Bheri",
    "country_id": "153",
    "value": "Bheri"
  },
  {
    "id": "2568",
    "text": "Dhawalagiri",
    "country_id": "153",
    "value": "Dhawalagiri"
  },
  {
    "id": "2569",
    "text": "Gandaki",
    "country_id": "153",
    "value": "Gandaki"
  },
  {
    "id": "2570",
    "text": "Janakpur",
    "country_id": "153",
    "value": "Janakpur"
  },
  {
    "id": "2571",
    "text": "Karnali",
    "country_id": "153",
    "value": "Karnali"
  },
  {
    "id": "2572",
    "text": "Koshi",
    "country_id": "153",
    "value": "Koshi"
  },
  {
    "id": "2573",
    "text": "Lumbini",
    "country_id": "153",
    "value": "Lumbini"
  },
  {
    "id": "2574",
    "text": "Mahakali",
    "country_id": "153",
    "value": "Mahakali"
  },
  {
    "id": "2575",
    "text": "Mechi",
    "country_id": "153",
    "value": "Mechi"
  },
  {
    "id": "2576",
    "text": "Narayani",
    "country_id": "153",
    "value": "Narayani"
  },
  {
    "id": "2577",
    "text": "Rapti",
    "country_id": "153",
    "value": "Rapti"
  },
  {
    "id": "2578",
    "text": "Sagarmatha",
    "country_id": "153",
    "value": "Sagarmatha"
  },
  {
    "id": "2579",
    "text": "Seti",
    "country_id": "153",
    "value": "Seti"
  },
  {
    "id": "2580",
    "text": "Bonaire",
    "country_id": "154",
    "value": "Bonaire"
  },
  {
    "id": "2581",
    "text": "Curacao",
    "country_id": "154",
    "value": "Curacao"
  },
  {
    "id": "2582",
    "text": "Saba",
    "country_id": "154",
    "value": "Saba"
  },
  {
    "id": "2583",
    "text": "Sint Eustatius",
    "country_id": "154",
    "value": "Sint Eustatius"
  },
  {
    "id": "2584",
    "text": "Sint Maarten",
    "country_id": "154",
    "value": "Sint Maarten"
  },
  {
    "id": "2585",
    "text": "Amsterdam",
    "country_id": "155",
    "value": "Amsterdam"
  },
  {
    "id": "2586",
    "text": "Benelux",
    "country_id": "155",
    "value": "Benelux"
  },
  {
    "id": "2587",
    "text": "Drenthe",
    "country_id": "155",
    "value": "Drenthe"
  },
  {
    "id": "2588",
    "text": "Flevoland",
    "country_id": "155",
    "value": "Flevoland"
  },
  {
    "id": "2589",
    "text": "Friesland",
    "country_id": "155",
    "value": "Friesland"
  },
  {
    "id": "2590",
    "text": "Gelderland",
    "country_id": "155",
    "value": "Gelderland"
  },
  {
    "id": "2591",
    "text": "Groningen",
    "country_id": "155",
    "value": "Groningen"
  },
  {
    "id": "2592",
    "text": "Limburg",
    "country_id": "155",
    "value": "Limburg"
  },
  {
    "id": "2593",
    "text": "Noord-Brabant",
    "country_id": "155",
    "value": "Noord-Brabant"
  },
  {
    "id": "2594",
    "text": "Noord-Holland",
    "country_id": "155",
    "value": "Noord-Holland"
  },
  {
    "id": "2595",
    "text": "Overijssel",
    "country_id": "155",
    "value": "Overijssel"
  },
  {
    "id": "2596",
    "text": "South Holland",
    "country_id": "155",
    "value": "South Holland"
  },
  {
    "id": "2597",
    "text": "Utrecht",
    "country_id": "155",
    "value": "Utrecht"
  },
  {
    "id": "2598",
    "text": "Zeeland",
    "country_id": "155",
    "value": "Zeeland"
  },
  {
    "id": "2599",
    "text": "Zuid-Holland",
    "country_id": "155",
    "value": "Zuid-Holland"
  },
  {
    "id": "2600",
    "text": "Iles",
    "country_id": "156",
    "value": "Iles"
  },
  {
    "id": "2601",
    "text": "Nord",
    "country_id": "156",
    "value": "Nord"
  },
  {
    "id": "2602",
    "text": "Sud",
    "country_id": "156",
    "value": "Sud"
  },
  {
    "id": "2603",
    "text": "Area Outside Region",
    "country_id": "157",
    "value": "Area Outside Region"
  },
  {
    "id": "2604",
    "text": "Auckland",
    "country_id": "157",
    "value": "Auckland"
  },
  {
    "id": "2605",
    "text": "Bay of Plenty",
    "country_id": "157",
    "value": "Bay of Plenty"
  },
  {
    "id": "2606",
    "text": "Canterbury",
    "country_id": "157",
    "value": "Canterbury"
  },
  {
    "id": "2607",
    "text": "Christchurch",
    "country_id": "157",
    "value": "Christchurch"
  },
  {
    "id": "2608",
    "text": "Gisborne",
    "country_id": "157",
    "value": "Gisborne"
  },
  {
    "id": "2609",
    "text": "Hawke''s Bay",
    "country_id": "157",
    "value": "Hawke''s Bay"
  },
  {
    "id": "2610",
    "text": "Manawatu-Wanganui",
    "country_id": "157",
    "value": "Manawatu-Wanganui"
  },
  {
    "id": "2611",
    "text": "Marlborough",
    "country_id": "157",
    "value": "Marlborough"
  },
  {
    "id": "2612",
    "text": "Nelson",
    "country_id": "157",
    "value": "Nelson"
  },
  {
    "id": "2613",
    "text": "Northland",
    "country_id": "157",
    "value": "Northland"
  },
  {
    "id": "2614",
    "text": "Otago",
    "country_id": "157",
    "value": "Otago"
  },
  {
    "id": "2615",
    "text": "Rodney",
    "country_id": "157",
    "value": "Rodney"
  },
  {
    "id": "2616",
    "text": "Southland",
    "country_id": "157",
    "value": "Southland"
  },
  {
    "id": "2617",
    "text": "Taranaki",
    "country_id": "157",
    "value": "Taranaki"
  },
  {
    "id": "2618",
    "text": "Tasman",
    "country_id": "157",
    "value": "Tasman"
  },
  {
    "id": "2619",
    "text": "Waikato",
    "country_id": "157",
    "value": "Waikato"
  },
  {
    "id": "2620",
    "text": "Wellington",
    "country_id": "157",
    "value": "Wellington"
  },
  {
    "id": "2621",
    "text": "West Coast",
    "country_id": "157",
    "value": "West Coast"
  },
  {
    "id": "2622",
    "text": "Atlantico Norte",
    "country_id": "158",
    "value": "Atlantico Norte"
  },
  {
    "id": "2623",
    "text": "Atlantico Sur",
    "country_id": "158",
    "value": "Atlantico Sur"
  },
  {
    "id": "2624",
    "text": "Boaco",
    "country_id": "158",
    "value": "Boaco"
  },
  {
    "id": "2625",
    "text": "Carazo",
    "country_id": "158",
    "value": "Carazo"
  },
  {
    "id": "2626",
    "text": "Chinandega",
    "country_id": "158",
    "value": "Chinandega"
  },
  {
    "id": "2627",
    "text": "Chontales",
    "country_id": "158",
    "value": "Chontales"
  },
  {
    "id": "2628",
    "text": "Esteli",
    "country_id": "158",
    "value": "Esteli"
  },
  {
    "id": "2629",
    "text": "Granada",
    "country_id": "158",
    "value": "Granada"
  },
  {
    "id": "2630",
    "text": "Jinotega",
    "country_id": "158",
    "value": "Jinotega"
  },
  {
    "id": "2631",
    "text": "Leon",
    "country_id": "158",
    "value": "Leon"
  },
  {
    "id": "2632",
    "text": "Madriz",
    "country_id": "158",
    "value": "Madriz"
  },
  {
    "id": "2633",
    "text": "Managua",
    "country_id": "158",
    "value": "Managua"
  },
  {
    "id": "2634",
    "text": "Masaya",
    "country_id": "158",
    "value": "Masaya"
  },
  {
    "id": "2635",
    "text": "Matagalpa",
    "country_id": "158",
    "value": "Matagalpa"
  },
  {
    "id": "2636",
    "text": "Nueva Segovia",
    "country_id": "158",
    "value": "Nueva Segovia"
  },
  {
    "id": "2637",
    "text": "Rio San Juan",
    "country_id": "158",
    "value": "Rio San Juan"
  },
  {
    "id": "2638",
    "text": "Rivas",
    "country_id": "158",
    "value": "Rivas"
  },
  {
    "id": "2639",
    "text": "Agadez",
    "country_id": "159",
    "value": "Agadez"
  },
  {
    "id": "2640",
    "text": "Diffa",
    "country_id": "159",
    "value": "Diffa"
  },
  {
    "id": "2641",
    "text": "Dosso",
    "country_id": "159",
    "value": "Dosso"
  },
  {
    "id": "2642",
    "text": "Maradi",
    "country_id": "159",
    "value": "Maradi"
  },
  {
    "id": "2643",
    "text": "Niamey",
    "country_id": "159",
    "value": "Niamey"
  },
  {
    "id": "2644",
    "text": "Tahoua",
    "country_id": "159",
    "value": "Tahoua"
  },
  {
    "id": "2645",
    "text": "Tillabery",
    "country_id": "159",
    "value": "Tillabery"
  },
  {
    "id": "2646",
    "text": "Zinder",
    "country_id": "159",
    "value": "Zinder"
  },
  {
    "id": "2647",
    "text": "Abia",
    "country_id": "160",
    "value": "Abia"
  },
  {
    "id": "2648",
    "text": "Abuja Federal Capital Territor",
    "country_id": "160",
    "value": "Abuja Federal Capital Territor"
  },
  {
    "id": "2649",
    "text": "Adamawa",
    "country_id": "160",
    "value": "Adamawa"
  },
  {
    "id": "2650",
    "text": "Akwa Ibom",
    "country_id": "160",
    "value": "Akwa Ibom"
  },
  {
    "id": "2651",
    "text": "Anambra",
    "country_id": "160",
    "value": "Anambra"
  },
  {
    "id": "2652",
    "text": "Bauchi",
    "country_id": "160",
    "value": "Bauchi"
  },
  {
    "id": "2653",
    "text": "Bayelsa",
    "country_id": "160",
    "value": "Bayelsa"
  },
  {
    "id": "2654",
    "text": "Benue",
    "country_id": "160",
    "value": "Benue"
  },
  {
    "id": "2655",
    "text": "Borno",
    "country_id": "160",
    "value": "Borno"
  },
  {
    "id": "2656",
    "text": "Cross River",
    "country_id": "160",
    "value": "Cross River"
  },
  {
    "id": "2657",
    "text": "Delta",
    "country_id": "160",
    "value": "Delta"
  },
  {
    "id": "2658",
    "text": "Ebonyi",
    "country_id": "160",
    "value": "Ebonyi"
  },
  {
    "id": "2659",
    "text": "Edo",
    "country_id": "160",
    "value": "Edo"
  },
  {
    "id": "2660",
    "text": "Ekiti",
    "country_id": "160",
    "value": "Ekiti"
  },
  {
    "id": "2661",
    "text": "Enugu",
    "country_id": "160",
    "value": "Enugu"
  },
  {
    "id": "2662",
    "text": "Gombe",
    "country_id": "160",
    "value": "Gombe"
  },
  {
    "id": "2663",
    "text": "Imo",
    "country_id": "160",
    "value": "Imo"
  },
  {
    "id": "2664",
    "text": "Jigawa",
    "country_id": "160",
    "value": "Jigawa"
  },
  {
    "id": "2665",
    "text": "Kaduna",
    "country_id": "160",
    "value": "Kaduna"
  },
  {
    "id": "2666",
    "text": "Kano",
    "country_id": "160",
    "value": "Kano"
  },
  {
    "id": "2667",
    "text": "Katsina",
    "country_id": "160",
    "value": "Katsina"
  },
  {
    "id": "2668",
    "text": "Kebbi",
    "country_id": "160",
    "value": "Kebbi"
  },
  {
    "id": "2669",
    "text": "Kogi",
    "country_id": "160",
    "value": "Kogi"
  },
  {
    "id": "2670",
    "text": "Kwara",
    "country_id": "160",
    "value": "Kwara"
  },
  {
    "id": "2671",
    "text": "Lagos",
    "country_id": "160",
    "value": "Lagos"
  },
  {
    "id": "2672",
    "text": "Nassarawa",
    "country_id": "160",
    "value": "Nassarawa"
  },
  {
    "id": "2673",
    "text": "Niger",
    "country_id": "160",
    "value": "Niger"
  },
  {
    "id": "2674",
    "text": "Ogun",
    "country_id": "160",
    "value": "Ogun"
  },
  {
    "id": "2675",
    "text": "Ondo",
    "country_id": "160",
    "value": "Ondo"
  },
  {
    "id": "2676",
    "text": "Osun",
    "country_id": "160",
    "value": "Osun"
  },
  {
    "id": "2677",
    "text": "Oyo",
    "country_id": "160",
    "value": "Oyo"
  },
  {
    "id": "2678",
    "text": "Plateau",
    "country_id": "160",
    "value": "Plateau"
  },
  {
    "id": "2679",
    "text": "Rivers",
    "country_id": "160",
    "value": "Rivers"
  },
  {
    "id": "2680",
    "text": "Sokoto",
    "country_id": "160",
    "value": "Sokoto"
  },
  {
    "id": "2681",
    "text": "Taraba",
    "country_id": "160",
    "value": "Taraba"
  },
  {
    "id": "2682",
    "text": "Yobe",
    "country_id": "160",
    "value": "Yobe"
  },
  {
    "id": "2683",
    "text": "Zamfara",
    "country_id": "160",
    "value": "Zamfara"
  },
  {
    "id": "2684",
    "text": "Niue",
    "country_id": "161",
    "value": "Niue"
  },
  {
    "id": "2685",
    "text": "Norfolk Island",
    "country_id": "162",
    "value": "Norfolk Island"
  },
  {
    "id": "2686",
    "text": "Northern Islands",
    "country_id": "163",
    "value": "Northern Islands"
  },
  {
    "id": "2687",
    "text": "Rota",
    "country_id": "163",
    "value": "Rota"
  },
  {
    "id": "2688",
    "text": "Saipan",
    "country_id": "163",
    "value": "Saipan"
  },
  {
    "id": "2689",
    "text": "Tinian",
    "country_id": "163",
    "value": "Tinian"
  },
  {
    "id": "2690",
    "text": "Akershus",
    "country_id": "164",
    "value": "Akershus"
  },
  {
    "id": "2691",
    "text": "Aust Agder",
    "country_id": "164",
    "value": "Aust Agder"
  },
  {
    "id": "2692",
    "text": "Bergen",
    "country_id": "164",
    "value": "Bergen"
  },
  {
    "id": "2693",
    "text": "Buskerud",
    "country_id": "164",
    "value": "Buskerud"
  },
  {
    "id": "2694",
    "text": "Finnmark",
    "country_id": "164",
    "value": "Finnmark"
  },
  {
    "id": "2695",
    "text": "Hedmark",
    "country_id": "164",
    "value": "Hedmark"
  },
  {
    "id": "2696",
    "text": "Hordaland",
    "country_id": "164",
    "value": "Hordaland"
  },
  {
    "id": "2697",
    "text": "Moere og Romsdal",
    "country_id": "164",
    "value": "Moere og Romsdal"
  },
  {
    "id": "2698",
    "text": "Nord Trondelag",
    "country_id": "164",
    "value": "Nord Trondelag"
  },
  {
    "id": "2699",
    "text": "Nordland",
    "country_id": "164",
    "value": "Nordland"
  },
  {
    "id": "2700",
    "text": "Oestfold",
    "country_id": "164",
    "value": "Oestfold"
  },
  {
    "id": "2701",
    "text": "Oppland",
    "country_id": "164",
    "value": "Oppland"
  },
  {
    "id": "2702",
    "text": "Oslo",
    "country_id": "164",
    "value": "Oslo"
  },
  {
    "id": "2703",
    "text": "Rogaland",
    "country_id": "164",
    "value": "Rogaland"
  },
  {
    "id": "2704",
    "text": "Soer Troendelag",
    "country_id": "164",
    "value": "Soer Troendelag"
  },
  {
    "id": "2705",
    "text": "Sogn og Fjordane",
    "country_id": "164",
    "value": "Sogn og Fjordane"
  },
  {
    "id": "2706",
    "text": "Stavern",
    "country_id": "164",
    "value": "Stavern"
  },
  {
    "id": "2707",
    "text": "Sykkylven",
    "country_id": "164",
    "value": "Sykkylven"
  },
  {
    "id": "2708",
    "text": "Telemark",
    "country_id": "164",
    "value": "Telemark"
  },
  {
    "id": "2709",
    "text": "Troms",
    "country_id": "164",
    "value": "Troms"
  },
  {
    "id": "2710",
    "text": "Vest Agder",
    "country_id": "164",
    "value": "Vest Agder"
  },
  {
    "id": "2711",
    "text": "Vestfold",
    "country_id": "164",
    "value": "Vestfold"
  },
  {
    "id": "2712",
    "text": "\u00c3\u0192\u00c2\u02dcstfold",
    "country_id": "164",
    "value": "\u00c3\u0192\u00c2\u02dcstfold"
  },
  {
    "id": "2713",
    "text": "Al Buraimi",
    "country_id": "165",
    "value": "Al Buraimi"
  },
  {
    "id": "2714",
    "text": "Dhufar",
    "country_id": "165",
    "value": "Dhufar"
  },
  {
    "id": "2715",
    "text": "Masqat",
    "country_id": "165",
    "value": "Masqat"
  },
  {
    "id": "2716",
    "text": "Musandam",
    "country_id": "165",
    "value": "Musandam"
  },
  {
    "id": "2717",
    "text": "Rusayl",
    "country_id": "165",
    "value": "Rusayl"
  },
  {
    "id": "2718",
    "text": "Wadi Kabir",
    "country_id": "165",
    "value": "Wadi Kabir"
  },
  {
    "id": "2719",
    "text": "ad-Dakhiliyah",
    "country_id": "165",
    "value": "ad-Dakhiliyah"
  },
  {
    "id": "2720",
    "text": "adh-Dhahirah",
    "country_id": "165",
    "value": "adh-Dhahirah"
  },
  {
    "id": "2721",
    "text": "al-Batinah",
    "country_id": "165",
    "value": "al-Batinah"
  },
  {
    "id": "2722",
    "text": "ash-Sharqiyah",
    "country_id": "165",
    "value": "ash-Sharqiyah"
  },
  {
    "id": "2723",
    "text": "Baluchistan",
    "country_id": "166",
    "value": "Baluchistan"
  },
  {
    "id": "2724",
    "text": "Federal Capital Area",
    "country_id": "166",
    "value": "Federal Capital Area"
  },
  {
    "id": "2725",
    "text": "Federally administered Tribal",
    "country_id": "166",
    "value": "Federally administered Tribal"
  },
  {
    "id": "2726",
    "text": "North-West Frontier",
    "country_id": "166",
    "value": "North-West Frontier"
  },
  {
    "id": "2727",
    "text": "Northern Areas",
    "country_id": "166",
    "value": "Northern Areas"
  },
  {
    "id": "2728",
    "text": "Punjab",
    "country_id": "166",
    "value": "Punjab"
  },
  {
    "id": "2729",
    "text": "Sind",
    "country_id": "166",
    "value": "Sind"
  },
  {
    "id": "2730",
    "text": "Aimeliik",
    "country_id": "167",
    "value": "Aimeliik"
  },
  {
    "id": "2731",
    "text": "Airai",
    "country_id": "167",
    "value": "Airai"
  },
  {
    "id": "2732",
    "text": "Angaur",
    "country_id": "167",
    "value": "Angaur"
  },
  {
    "id": "2733",
    "text": "Hatobohei",
    "country_id": "167",
    "value": "Hatobohei"
  },
  {
    "id": "2734",
    "text": "Kayangel",
    "country_id": "167",
    "value": "Kayangel"
  },
  {
    "id": "2735",
    "text": "Koror",
    "country_id": "167",
    "value": "Koror"
  },
  {
    "id": "2736",
    "text": "Melekeok",
    "country_id": "167",
    "value": "Melekeok"
  },
  {
    "id": "2737",
    "text": "Ngaraard",
    "country_id": "167",
    "value": "Ngaraard"
  },
  {
    "id": "2738",
    "text": "Ngardmau",
    "country_id": "167",
    "value": "Ngardmau"
  },
  {
    "id": "2739",
    "text": "Ngaremlengui",
    "country_id": "167",
    "value": "Ngaremlengui"
  },
  {
    "id": "2740",
    "text": "Ngatpang",
    "country_id": "167",
    "value": "Ngatpang"
  },
  {
    "id": "2741",
    "text": "Ngchesar",
    "country_id": "167",
    "value": "Ngchesar"
  },
  {
    "id": "2742",
    "text": "Ngerchelong",
    "country_id": "167",
    "value": "Ngerchelong"
  },
  {
    "id": "2743",
    "text": "Ngiwal",
    "country_id": "167",
    "value": "Ngiwal"
  },
  {
    "id": "2744",
    "text": "Peleliu",
    "country_id": "167",
    "value": "Peleliu"
  },
  {
    "id": "2745",
    "text": "Sonsorol",
    "country_id": "167",
    "value": "Sonsorol"
  },
  {
    "id": "2746",
    "text": "Ariha",
    "country_id": "168",
    "value": "Ariha"
  },
  {
    "id": "2747",
    "text": "Bayt Lahm",
    "country_id": "168",
    "value": "Bayt Lahm"
  },
  {
    "id": "2748",
    "text": "Bethlehem",
    "country_id": "168",
    "value": "Bethlehem"
  },
  {
    "id": "2749",
    "text": "Dayr-al-Balah",
    "country_id": "168",
    "value": "Dayr-al-Balah"
  },
  {
    "id": "2750",
    "text": "Ghazzah",
    "country_id": "168",
    "value": "Ghazzah"
  },
  {
    "id": "2751",
    "text": "Ghazzah ash-Shamaliyah",
    "country_id": "168",
    "value": "Ghazzah ash-Shamaliyah"
  },
  {
    "id": "2752",
    "text": "Janin",
    "country_id": "168",
    "value": "Janin"
  },
  {
    "id": "2753",
    "text": "Khan Yunis",
    "country_id": "168",
    "value": "Khan Yunis"
  },
  {
    "id": "2754",
    "text": "Nabulus",
    "country_id": "168",
    "value": "Nabulus"
  },
  {
    "id": "2755",
    "text": "Qalqilyah",
    "country_id": "168",
    "value": "Qalqilyah"
  },
  {
    "id": "2756",
    "text": "Rafah",
    "country_id": "168",
    "value": "Rafah"
  },
  {
    "id": "2757",
    "text": "Ram Allah wal-Birah",
    "country_id": "168",
    "value": "Ram Allah wal-Birah"
  },
  {
    "id": "2758",
    "text": "Salfit",
    "country_id": "168",
    "value": "Salfit"
  },
  {
    "id": "2759",
    "text": "Tubas",
    "country_id": "168",
    "value": "Tubas"
  },
  {
    "id": "2760",
    "text": "Tulkarm",
    "country_id": "168",
    "value": "Tulkarm"
  },
  {
    "id": "2761",
    "text": "al-Khalil",
    "country_id": "168",
    "value": "al-Khalil"
  },
  {
    "id": "2762",
    "text": "al-Quds",
    "country_id": "168",
    "value": "al-Quds"
  },
  {
    "id": "2763",
    "text": "Bocas del Toro",
    "country_id": "169",
    "value": "Bocas del Toro"
  },
  {
    "id": "2764",
    "text": "Chiriqui",
    "country_id": "169",
    "value": "Chiriqui"
  },
  {
    "id": "2765",
    "text": "Cocle",
    "country_id": "169",
    "value": "Cocle"
  },
  {
    "id": "2766",
    "text": "Colon",
    "country_id": "169",
    "value": "Colon"
  },
  {
    "id": "2767",
    "text": "Darien",
    "country_id": "169",
    "value": "Darien"
  },
  {
    "id": "2768",
    "text": "Embera",
    "country_id": "169",
    "value": "Embera"
  },
  {
    "id": "2769",
    "text": "Herrera",
    "country_id": "169",
    "value": "Herrera"
  },
  {
    "id": "2770",
    "text": "Kuna Yala",
    "country_id": "169",
    "value": "Kuna Yala"
  },
  {
    "id": "2771",
    "text": "Los Santos",
    "country_id": "169",
    "value": "Los Santos"
  },
  {
    "id": "2772",
    "text": "Ngobe Bugle",
    "country_id": "169",
    "value": "Ngobe Bugle"
  },
  {
    "id": "2773",
    "text": "Panama",
    "country_id": "169",
    "value": "Panama"
  },
  {
    "id": "2774",
    "text": "Veraguas",
    "country_id": "169",
    "value": "Veraguas"
  },
  {
    "id": "2775",
    "text": "East New Britain",
    "country_id": "170",
    "value": "East New Britain"
  },
  {
    "id": "2776",
    "text": "East Sepik",
    "country_id": "170",
    "value": "East Sepik"
  },
  {
    "id": "2777",
    "text": "Eastern Highlands",
    "country_id": "170",
    "value": "Eastern Highlands"
  },
  {
    "id": "2778",
    "text": "Enga",
    "country_id": "170",
    "value": "Enga"
  },
  {
    "id": "2779",
    "text": "Fly River",
    "country_id": "170",
    "value": "Fly River"
  },
  {
    "id": "2780",
    "text": "Gulf",
    "country_id": "170",
    "value": "Gulf"
  },
  {
    "id": "2781",
    "text": "Madang",
    "country_id": "170",
    "value": "Madang"
  },
  {
    "id": "2782",
    "text": "Manus",
    "country_id": "170",
    "value": "Manus"
  },
  {
    "id": "2783",
    "text": "Milne Bay",
    "country_id": "170",
    "value": "Milne Bay"
  },
  {
    "id": "2784",
    "text": "Morobe",
    "country_id": "170",
    "value": "Morobe"
  },
  {
    "id": "2785",
    "text": "National Capital District",
    "country_id": "170",
    "value": "National Capital District"
  },
  {
    "id": "2786",
    "text": "New Ireland",
    "country_id": "170",
    "value": "New Ireland"
  },
  {
    "id": "2787",
    "text": "North Solomons",
    "country_id": "170",
    "value": "North Solomons"
  },
  {
    "id": "2788",
    "text": "Oro",
    "country_id": "170",
    "value": "Oro"
  },
  {
    "id": "2789",
    "text": "Sandaun",
    "country_id": "170",
    "value": "Sandaun"
  },
  {
    "id": "2790",
    "text": "Simbu",
    "country_id": "170",
    "value": "Simbu"
  },
  {
    "id": "2791",
    "text": "Southern Highlands",
    "country_id": "170",
    "value": "Southern Highlands"
  },
  {
    "id": "2792",
    "text": "West New Britain",
    "country_id": "170",
    "value": "West New Britain"
  },
  {
    "id": "2793",
    "text": "Western Highlands",
    "country_id": "170",
    "value": "Western Highlands"
  },
  {
    "id": "2794",
    "text": "Alto Paraguay",
    "country_id": "171",
    "value": "Alto Paraguay"
  },
  {
    "id": "2795",
    "text": "Alto Parana",
    "country_id": "171",
    "value": "Alto Parana"
  },
  {
    "id": "2796",
    "text": "Amambay",
    "country_id": "171",
    "value": "Amambay"
  },
  {
    "id": "2797",
    "text": "Asuncion",
    "country_id": "171",
    "value": "Asuncion"
  },
  {
    "id": "2798",
    "text": "Boqueron",
    "country_id": "171",
    "value": "Boqueron"
  },
  {
    "id": "2799",
    "text": "Caaguazu",
    "country_id": "171",
    "value": "Caaguazu"
  },
  {
    "id": "2800",
    "text": "Caazapa",
    "country_id": "171",
    "value": "Caazapa"
  },
  {
    "id": "2801",
    "text": "Canendiyu",
    "country_id": "171",
    "value": "Canendiyu"
  },
  {
    "id": "2802",
    "text": "Central",
    "country_id": "171",
    "value": "Central"
  },
  {
    "id": "2803",
    "text": "Concepcion",
    "country_id": "171",
    "value": "Concepcion"
  },
  {
    "id": "2804",
    "text": "Cordillera",
    "country_id": "171",
    "value": "Cordillera"
  },
  {
    "id": "2805",
    "text": "Guaira",
    "country_id": "171",
    "value": "Guaira"
  },
  {
    "id": "2806",
    "text": "Itapua",
    "country_id": "171",
    "value": "Itapua"
  },
  {
    "id": "2807",
    "text": "Misiones",
    "country_id": "171",
    "value": "Misiones"
  },
  {
    "id": "2808",
    "text": "Neembucu",
    "country_id": "171",
    "value": "Neembucu"
  },
  {
    "id": "2809",
    "text": "Paraguari",
    "country_id": "171",
    "value": "Paraguari"
  },
  {
    "id": "2810",
    "text": "Presidente Hayes",
    "country_id": "171",
    "value": "Presidente Hayes"
  },
  {
    "id": "2811",
    "text": "San Pedro",
    "country_id": "171",
    "value": "San Pedro"
  },
  {
    "id": "2812",
    "text": "Amazonas",
    "country_id": "172",
    "value": "Amazonas"
  },
  {
    "id": "2813",
    "text": "Ancash",
    "country_id": "172",
    "value": "Ancash"
  },
  {
    "id": "2814",
    "text": "Apurimac",
    "country_id": "172",
    "value": "Apurimac"
  },
  {
    "id": "2815",
    "text": "Arequipa",
    "country_id": "172",
    "value": "Arequipa"
  },
  {
    "id": "2816",
    "text": "Ayacucho",
    "country_id": "172",
    "value": "Ayacucho"
  },
  {
    "id": "2817",
    "text": "Cajamarca",
    "country_id": "172",
    "value": "Cajamarca"
  },
  {
    "id": "2818",
    "text": "Cusco",
    "country_id": "172",
    "value": "Cusco"
  },
  {
    "id": "2819",
    "text": "Huancavelica",
    "country_id": "172",
    "value": "Huancavelica"
  },
  {
    "id": "2820",
    "text": "Huanuco",
    "country_id": "172",
    "value": "Huanuco"
  },
  {
    "id": "2821",
    "text": "Ica",
    "country_id": "172",
    "value": "Ica"
  },
  {
    "id": "2822",
    "text": "Junin",
    "country_id": "172",
    "value": "Junin"
  },
  {
    "id": "2823",
    "text": "La Libertad",
    "country_id": "172",
    "value": "La Libertad"
  },
  {
    "id": "2824",
    "text": "Lambayeque",
    "country_id": "172",
    "value": "Lambayeque"
  },
  {
    "id": "2825",
    "text": "Lima y Callao",
    "country_id": "172",
    "value": "Lima y Callao"
  },
  {
    "id": "2826",
    "text": "Loreto",
    "country_id": "172",
    "value": "Loreto"
  },
  {
    "id": "2827",
    "text": "Madre de Dios",
    "country_id": "172",
    "value": "Madre de Dios"
  },
  {
    "id": "2828",
    "text": "Moquegua",
    "country_id": "172",
    "value": "Moquegua"
  },
  {
    "id": "2829",
    "text": "Pasco",
    "country_id": "172",
    "value": "Pasco"
  },
  {
    "id": "2830",
    "text": "Piura",
    "country_id": "172",
    "value": "Piura"
  },
  {
    "id": "2831",
    "text": "Puno",
    "country_id": "172",
    "value": "Puno"
  },
  {
    "id": "2832",
    "text": "San Martin",
    "country_id": "172",
    "value": "San Martin"
  },
  {
    "id": "2833",
    "text": "Tacna",
    "country_id": "172",
    "value": "Tacna"
  },
  {
    "id": "2834",
    "text": "Tumbes",
    "country_id": "172",
    "value": "Tumbes"
  },
  {
    "id": "2835",
    "text": "Ucayali",
    "country_id": "172",
    "value": "Ucayali"
  },
  {
    "id": "2836",
    "text": "Batangas",
    "country_id": "173",
    "value": "Batangas"
  },
  {
    "id": "2837",
    "text": "Bicol",
    "country_id": "173",
    "value": "Bicol"
  },
  {
    "id": "2838",
    "text": "Bulacan",
    "country_id": "173",
    "value": "Bulacan"
  },
  {
    "id": "2839",
    "text": "Cagayan",
    "country_id": "173",
    "value": "Cagayan"
  },
  {
    "id": "2840",
    "text": "Caraga",
    "country_id": "173",
    "value": "Caraga"
  },
  {
    "id": "2841",
    "text": "Central Luzon",
    "country_id": "173",
    "value": "Central Luzon"
  },
  {
    "id": "2842",
    "text": "Central Mindanao",
    "country_id": "173",
    "value": "Central Mindanao"
  },
  {
    "id": "2843",
    "text": "Central Visayas",
    "country_id": "173",
    "value": "Central Visayas"
  },
  {
    "id": "2844",
    "text": "Cordillera",
    "country_id": "173",
    "value": "Cordillera"
  },
  {
    "id": "2845",
    "text": "Davao",
    "country_id": "173",
    "value": "Davao"
  },
  {
    "id": "2846",
    "text": "Eastern Visayas",
    "country_id": "173",
    "value": "Eastern Visayas"
  },
  {
    "id": "2847",
    "text": "Greater Metropolitan Area",
    "country_id": "173",
    "value": "Greater Metropolitan Area"
  },
  {
    "id": "2848",
    "text": "Ilocos",
    "country_id": "173",
    "value": "Ilocos"
  },
  {
    "id": "2849",
    "text": "Laguna",
    "country_id": "173",
    "value": "Laguna"
  },
  {
    "id": "2850",
    "text": "Luzon",
    "country_id": "173",
    "value": "Luzon"
  },
  {
    "id": "2851",
    "text": "Mactan",
    "country_id": "173",
    "value": "Mactan"
  },
  {
    "id": "2852",
    "text": "Metropolitan Manila Area",
    "country_id": "173",
    "value": "Metropolitan Manila Area"
  },
  {
    "id": "2853",
    "text": "Muslim Mindanao",
    "country_id": "173",
    "value": "Muslim Mindanao"
  },
  {
    "id": "2854",
    "text": "Northern Mindanao",
    "country_id": "173",
    "value": "Northern Mindanao"
  },
  {
    "id": "2855",
    "text": "Southern Mindanao",
    "country_id": "173",
    "value": "Southern Mindanao"
  },
  {
    "id": "2856",
    "text": "Southern Tagalog",
    "country_id": "173",
    "value": "Southern Tagalog"
  },
  {
    "id": "2857",
    "text": "Western Mindanao",
    "country_id": "173",
    "value": "Western Mindanao"
  },
  {
    "id": "2858",
    "text": "Western Visayas",
    "country_id": "173",
    "value": "Western Visayas"
  },
  {
    "id": "2859",
    "text": "Pitcairn Island",
    "country_id": "174",
    "value": "Pitcairn Island"
  },
  {
    "id": "2860",
    "text": "Biale Blota",
    "country_id": "175",
    "value": "Biale Blota"
  },
  {
    "id": "2861",
    "text": "Dobroszyce",
    "country_id": "175",
    "value": "Dobroszyce"
  },
  {
    "id": "2862",
    "text": "Dolnoslaskie",
    "country_id": "175",
    "value": "Dolnoslaskie"
  },
  {
    "id": "2863",
    "text": "Dziekanow Lesny",
    "country_id": "175",
    "value": "Dziekanow Lesny"
  },
  {
    "id": "2864",
    "text": "Hopowo",
    "country_id": "175",
    "value": "Hopowo"
  },
  {
    "id": "2865",
    "text": "Kartuzy",
    "country_id": "175",
    "value": "Kartuzy"
  },
  {
    "id": "2866",
    "text": "Koscian",
    "country_id": "175",
    "value": "Koscian"
  },
  {
    "id": "2867",
    "text": "Krakow",
    "country_id": "175",
    "value": "Krakow"
  },
  {
    "id": "2868",
    "text": "Kujawsko-Pomorskie",
    "country_id": "175",
    "value": "Kujawsko-Pomorskie"
  },
  {
    "id": "2869",
    "text": "Lodzkie",
    "country_id": "175",
    "value": "Lodzkie"
  },
  {
    "id": "2870",
    "text": "Lubelskie",
    "country_id": "175",
    "value": "Lubelskie"
  },
  {
    "id": "2871",
    "text": "Lubuskie",
    "country_id": "175",
    "value": "Lubuskie"
  },
  {
    "id": "2872",
    "text": "Malomice",
    "country_id": "175",
    "value": "Malomice"
  },
  {
    "id": "2873",
    "text": "Malopolskie",
    "country_id": "175",
    "value": "Malopolskie"
  },
  {
    "id": "2874",
    "text": "Mazowieckie",
    "country_id": "175",
    "value": "Mazowieckie"
  },
  {
    "id": "2875",
    "text": "Mirkow",
    "country_id": "175",
    "value": "Mirkow"
  },
  {
    "id": "2876",
    "text": "Opolskie",
    "country_id": "175",
    "value": "Opolskie"
  },
  {
    "id": "2877",
    "text": "Ostrowiec",
    "country_id": "175",
    "value": "Ostrowiec"
  },
  {
    "id": "2878",
    "text": "Podkarpackie",
    "country_id": "175",
    "value": "Podkarpackie"
  },
  {
    "id": "2879",
    "text": "Podlaskie",
    "country_id": "175",
    "value": "Podlaskie"
  },
  {
    "id": "2880",
    "text": "Polska",
    "country_id": "175",
    "value": "Polska"
  },
  {
    "id": "2881",
    "text": "Pomorskie",
    "country_id": "175",
    "value": "Pomorskie"
  },
  {
    "id": "2882",
    "text": "Poznan",
    "country_id": "175",
    "value": "Poznan"
  },
  {
    "id": "2883",
    "text": "Pruszkow",
    "country_id": "175",
    "value": "Pruszkow"
  },
  {
    "id": "2884",
    "text": "Rymanowska",
    "country_id": "175",
    "value": "Rymanowska"
  },
  {
    "id": "2885",
    "text": "Rzeszow",
    "country_id": "175",
    "value": "Rzeszow"
  },
  {
    "id": "2886",
    "text": "Slaskie",
    "country_id": "175",
    "value": "Slaskie"
  },
  {
    "id": "2887",
    "text": "Stare Pole",
    "country_id": "175",
    "value": "Stare Pole"
  },
  {
    "id": "2888",
    "text": "Swietokrzyskie",
    "country_id": "175",
    "value": "Swietokrzyskie"
  },
  {
    "id": "2889",
    "text": "Warminsko-Mazurskie",
    "country_id": "175",
    "value": "Warminsko-Mazurskie"
  },
  {
    "id": "2890",
    "text": "Warsaw",
    "country_id": "175",
    "value": "Warsaw"
  },
  {
    "id": "2891",
    "text": "Wejherowo",
    "country_id": "175",
    "value": "Wejherowo"
  },
  {
    "id": "2892",
    "text": "Wielkopolskie",
    "country_id": "175",
    "value": "Wielkopolskie"
  },
  {
    "id": "2893",
    "text": "Wroclaw",
    "country_id": "175",
    "value": "Wroclaw"
  },
  {
    "id": "2894",
    "text": "Zachodnio-Pomorskie",
    "country_id": "175",
    "value": "Zachodnio-Pomorskie"
  },
  {
    "id": "2895",
    "text": "Zukowo",
    "country_id": "175",
    "value": "Zukowo"
  },
  {
    "id": "2896",
    "text": "Abrantes",
    "country_id": "176",
    "value": "Abrantes"
  },
  {
    "id": "2897",
    "text": "Acores",
    "country_id": "176",
    "value": "Acores"
  },
  {
    "id": "2898",
    "text": "Alentejo",
    "country_id": "176",
    "value": "Alentejo"
  },
  {
    "id": "2899",
    "text": "Algarve",
    "country_id": "176",
    "value": "Algarve"
  },
  {
    "id": "2900",
    "text": "Braga",
    "country_id": "176",
    "value": "Braga"
  },
  {
    "id": "2901",
    "text": "Centro",
    "country_id": "176",
    "value": "Centro"
  },
  {
    "id": "2902",
    "text": "Distrito de Leiria",
    "country_id": "176",
    "value": "Distrito de Leiria"
  },
  {
    "id": "2903",
    "text": "Distrito de Viana do Castelo",
    "country_id": "176",
    "value": "Distrito de Viana do Castelo"
  },
  {
    "id": "2904",
    "text": "Distrito de Vila Real",
    "country_id": "176",
    "value": "Distrito de Vila Real"
  },
  {
    "id": "2905",
    "text": "Distrito do Porto",
    "country_id": "176",
    "value": "Distrito do Porto"
  },
  {
    "id": "2906",
    "text": "Lisboa e Vale do Tejo",
    "country_id": "176",
    "value": "Lisboa e Vale do Tejo"
  },
  {
    "id": "2907",
    "text": "Madeira",
    "country_id": "176",
    "value": "Madeira"
  },
  {
    "id": "2908",
    "text": "Norte",
    "country_id": "176",
    "value": "Norte"
  },
  {
    "id": "2909",
    "text": "Paivas",
    "country_id": "176",
    "value": "Paivas"
  },
  {
    "id": "2910",
    "text": "Arecibo",
    "country_id": "177",
    "value": "Arecibo"
  },
  {
    "id": "2911",
    "text": "Bayamon",
    "country_id": "177",
    "value": "Bayamon"
  },
  {
    "id": "2912",
    "text": "Carolina",
    "country_id": "177",
    "value": "Carolina"
  },
  {
    "id": "2913",
    "text": "Florida",
    "country_id": "177",
    "value": "Florida"
  },
  {
    "id": "2914",
    "text": "Guayama",
    "country_id": "177",
    "value": "Guayama"
  },
  {
    "id": "2915",
    "text": "Humacao",
    "country_id": "177",
    "value": "Humacao"
  },
  {
    "id": "2916",
    "text": "Mayaguez-Aguadilla",
    "country_id": "177",
    "value": "Mayaguez-Aguadilla"
  },
  {
    "id": "2917",
    "text": "Ponce",
    "country_id": "177",
    "value": "Ponce"
  },
  {
    "id": "2918",
    "text": "Salinas",
    "country_id": "177",
    "value": "Salinas"
  },
  {
    "id": "2919",
    "text": "San Juan",
    "country_id": "177",
    "value": "San Juan"
  },
  {
    "id": "2920",
    "text": "Doha",
    "country_id": "178",
    "value": "Doha"
  },
  {
    "id": "2921",
    "text": "Jarian-al-Batnah",
    "country_id": "178",
    "value": "Jarian-al-Batnah"
  },
  {
    "id": "2922",
    "text": "Umm Salal",
    "country_id": "178",
    "value": "Umm Salal"
  },
  {
    "id": "2923",
    "text": "ad-Dawhah",
    "country_id": "178",
    "value": "ad-Dawhah"
  },
  {
    "id": "2924",
    "text": "al-Ghuwayriyah",
    "country_id": "178",
    "value": "al-Ghuwayriyah"
  },
  {
    "id": "2925",
    "text": "al-Jumayliyah",
    "country_id": "178",
    "value": "al-Jumayliyah"
  },
  {
    "id": "2926",
    "text": "al-Khawr",
    "country_id": "178",
    "value": "al-Khawr"
  },
  {
    "id": "2927",
    "text": "al-Wakrah",
    "country_id": "178",
    "value": "al-Wakrah"
  },
  {
    "id": "2928",
    "text": "ar-Rayyan",
    "country_id": "178",
    "value": "ar-Rayyan"
  },
  {
    "id": "2929",
    "text": "ash-Shamal",
    "country_id": "178",
    "value": "ash-Shamal"
  },
  {
    "id": "2930",
    "text": "Saint-Benoit",
    "country_id": "179",
    "value": "Saint-Benoit"
  },
  {
    "id": "2931",
    "text": "Saint-Denis",
    "country_id": "179",
    "value": "Saint-Denis"
  },
  {
    "id": "2932",
    "text": "Saint-Paul",
    "country_id": "179",
    "value": "Saint-Paul"
  },
  {
    "id": "2933",
    "text": "Saint-Pierre",
    "country_id": "179",
    "value": "Saint-Pierre"
  },
  {
    "id": "2934",
    "text": "Alba",
    "country_id": "180",
    "value": "Alba"
  },
  {
    "id": "2935",
    "text": "Arad",
    "country_id": "180",
    "value": "Arad"
  },
  {
    "id": "2936",
    "text": "Arges",
    "country_id": "180",
    "value": "Arges"
  },
  {
    "id": "2937",
    "text": "Bacau",
    "country_id": "180",
    "value": "Bacau"
  },
  {
    "id": "2938",
    "text": "Bihor",
    "country_id": "180",
    "value": "Bihor"
  },
  {
    "id": "2939",
    "text": "Bistrita-Nasaud",
    "country_id": "180",
    "value": "Bistrita-Nasaud"
  },
  {
    "id": "2940",
    "text": "Botosani",
    "country_id": "180",
    "value": "Botosani"
  },
  {
    "id": "2941",
    "text": "Braila",
    "country_id": "180",
    "value": "Braila"
  },
  {
    "id": "2942",
    "text": "Brasov",
    "country_id": "180",
    "value": "Brasov"
  },
  {
    "id": "2943",
    "text": "Bucuresti",
    "country_id": "180",
    "value": "Bucuresti"
  },
  {
    "id": "2944",
    "text": "Buzau",
    "country_id": "180",
    "value": "Buzau"
  },
  {
    "id": "2945",
    "text": "Calarasi",
    "country_id": "180",
    "value": "Calarasi"
  },
  {
    "id": "2946",
    "text": "Caras-Severin",
    "country_id": "180",
    "value": "Caras-Severin"
  },
  {
    "id": "2947",
    "text": "Cluj",
    "country_id": "180",
    "value": "Cluj"
  },
  {
    "id": "2948",
    "text": "Constanta",
    "country_id": "180",
    "value": "Constanta"
  },
  {
    "id": "2949",
    "text": "Covasna",
    "country_id": "180",
    "value": "Covasna"
  },
  {
    "id": "2950",
    "text": "Dambovita",
    "country_id": "180",
    "value": "Dambovita"
  },
  {
    "id": "2951",
    "text": "Dolj",
    "country_id": "180",
    "value": "Dolj"
  },
  {
    "id": "2952",
    "text": "Galati",
    "country_id": "180",
    "value": "Galati"
  },
  {
    "id": "2953",
    "text": "Giurgiu",
    "country_id": "180",
    "value": "Giurgiu"
  },
  {
    "id": "2954",
    "text": "Gorj",
    "country_id": "180",
    "value": "Gorj"
  },
  {
    "id": "2955",
    "text": "Harghita",
    "country_id": "180",
    "value": "Harghita"
  },
  {
    "id": "2956",
    "text": "Hunedoara",
    "country_id": "180",
    "value": "Hunedoara"
  },
  {
    "id": "2957",
    "text": "Ialomita",
    "country_id": "180",
    "value": "Ialomita"
  },
  {
    "id": "2958",
    "text": "Iasi",
    "country_id": "180",
    "value": "Iasi"
  },
  {
    "id": "2959",
    "text": "Ilfov",
    "country_id": "180",
    "value": "Ilfov"
  },
  {
    "id": "2960",
    "text": "Maramures",
    "country_id": "180",
    "value": "Maramures"
  },
  {
    "id": "2961",
    "text": "Mehedinti",
    "country_id": "180",
    "value": "Mehedinti"
  },
  {
    "id": "2962",
    "text": "Mures",
    "country_id": "180",
    "value": "Mures"
  },
  {
    "id": "2963",
    "text": "Neamt",
    "country_id": "180",
    "value": "Neamt"
  },
  {
    "id": "2964",
    "text": "Olt",
    "country_id": "180",
    "value": "Olt"
  },
  {
    "id": "2965",
    "text": "Prahova",
    "country_id": "180",
    "value": "Prahova"
  },
  {
    "id": "2966",
    "text": "Salaj",
    "country_id": "180",
    "value": "Salaj"
  },
  {
    "id": "2967",
    "text": "Satu Mare",
    "country_id": "180",
    "value": "Satu Mare"
  },
  {
    "id": "2968",
    "text": "Sibiu",
    "country_id": "180",
    "value": "Sibiu"
  },
  {
    "id": "2969",
    "text": "Sondelor",
    "country_id": "180",
    "value": "Sondelor"
  },
  {
    "id": "2970",
    "text": "Suceava",
    "country_id": "180",
    "value": "Suceava"
  },
  {
    "id": "2971",
    "text": "Teleorman",
    "country_id": "180",
    "value": "Teleorman"
  },
  {
    "id": "2972",
    "text": "Timis",
    "country_id": "180",
    "value": "Timis"
  },
  {
    "id": "2973",
    "text": "Tulcea",
    "country_id": "180",
    "value": "Tulcea"
  },
  {
    "id": "2974",
    "text": "Valcea",
    "country_id": "180",
    "value": "Valcea"
  },
  {
    "id": "2975",
    "text": "Vaslui",
    "country_id": "180",
    "value": "Vaslui"
  },
  {
    "id": "2976",
    "text": "Vrancea",
    "country_id": "180",
    "value": "Vrancea"
  },
  {
    "id": "2977",
    "text": "Adygeja",
    "country_id": "181",
    "value": "Adygeja"
  },
  {
    "id": "2978",
    "text": "Aga",
    "country_id": "181",
    "value": "Aga"
  },
  {
    "id": "2979",
    "text": "Alanija",
    "country_id": "181",
    "value": "Alanija"
  },
  {
    "id": "2980",
    "text": "Altaj",
    "country_id": "181",
    "value": "Altaj"
  },
  {
    "id": "2981",
    "text": "Amur",
    "country_id": "181",
    "value": "Amur"
  },
  {
    "id": "2982",
    "text": "Arhangelsk",
    "country_id": "181",
    "value": "Arhangelsk"
  },
  {
    "id": "2983",
    "text": "Astrahan",
    "country_id": "181",
    "value": "Astrahan"
  },
  {
    "id": "2984",
    "text": "Bashkortostan",
    "country_id": "181",
    "value": "Bashkortostan"
  },
  {
    "id": "2985",
    "text": "Belgorod",
    "country_id": "181",
    "value": "Belgorod"
  },
  {
    "id": "2986",
    "text": "Brjansk",
    "country_id": "181",
    "value": "Brjansk"
  },
  {
    "id": "2987",
    "text": "Burjatija",
    "country_id": "181",
    "value": "Burjatija"
  },
  {
    "id": "2988",
    "text": "Chechenija",
    "country_id": "181",
    "value": "Chechenija"
  },
  {
    "id": "2989",
    "text": "Cheljabinsk",
    "country_id": "181",
    "value": "Cheljabinsk"
  },
  {
    "id": "2990",
    "text": "Chita",
    "country_id": "181",
    "value": "Chita"
  },
  {
    "id": "2991",
    "text": "Chukotka",
    "country_id": "181",
    "value": "Chukotka"
  },
  {
    "id": "2992",
    "text": "Chuvashija",
    "country_id": "181",
    "value": "Chuvashija"
  },
  {
    "id": "2993",
    "text": "Dagestan",
    "country_id": "181",
    "value": "Dagestan"
  },
  {
    "id": "2994",
    "text": "Evenkija",
    "country_id": "181",
    "value": "Evenkija"
  },
  {
    "id": "2995",
    "text": "Gorno-Altaj",
    "country_id": "181",
    "value": "Gorno-Altaj"
  },
  {
    "id": "2996",
    "text": "Habarovsk",
    "country_id": "181",
    "value": "Habarovsk"
  },
  {
    "id": "2997",
    "text": "Hakasija",
    "country_id": "181",
    "value": "Hakasija"
  },
  {
    "id": "2998",
    "text": "Hanty-Mansija",
    "country_id": "181",
    "value": "Hanty-Mansija"
  },
  {
    "id": "2999",
    "text": "Ingusetija",
    "country_id": "181",
    "value": "Ingusetija"
  },
  {
    "id": "3000",
    "text": "Irkutsk",
    "country_id": "181",
    "value": "Irkutsk"
  },
  {
    "id": "3001",
    "text": "Ivanovo",
    "country_id": "181",
    "value": "Ivanovo"
  },
  {
    "id": "3002",
    "text": "Jamalo-Nenets",
    "country_id": "181",
    "value": "Jamalo-Nenets"
  },
  {
    "id": "3003",
    "text": "Jaroslavl",
    "country_id": "181",
    "value": "Jaroslavl"
  },
  {
    "id": "3004",
    "text": "Jevrej",
    "country_id": "181",
    "value": "Jevrej"
  },
  {
    "id": "3005",
    "text": "Kabardino-Balkarija",
    "country_id": "181",
    "value": "Kabardino-Balkarija"
  },
  {
    "id": "3006",
    "text": "Kaliningrad",
    "country_id": "181",
    "value": "Kaliningrad"
  },
  {
    "id": "3007",
    "text": "Kalmykija",
    "country_id": "181",
    "value": "Kalmykija"
  },
  {
    "id": "3008",
    "text": "Kaluga",
    "country_id": "181",
    "value": "Kaluga"
  },
  {
    "id": "3009",
    "text": "Kamchatka",
    "country_id": "181",
    "value": "Kamchatka"
  },
  {
    "id": "3010",
    "text": "Karachaj-Cherkessija",
    "country_id": "181",
    "value": "Karachaj-Cherkessija"
  },
  {
    "id": "3011",
    "text": "Karelija",
    "country_id": "181",
    "value": "Karelija"
  },
  {
    "id": "3012",
    "text": "Kemerovo",
    "country_id": "181",
    "value": "Kemerovo"
  },
  {
    "id": "3013",
    "text": "Khabarovskiy Kray",
    "country_id": "181",
    "value": "Khabarovskiy Kray"
  },
  {
    "id": "3014",
    "text": "Kirov",
    "country_id": "181",
    "value": "Kirov"
  },
  {
    "id": "3015",
    "text": "Komi",
    "country_id": "181",
    "value": "Komi"
  },
  {
    "id": "3016",
    "text": "Komi-Permjakija",
    "country_id": "181",
    "value": "Komi-Permjakija"
  },
  {
    "id": "3017",
    "text": "Korjakija",
    "country_id": "181",
    "value": "Korjakija"
  },
  {
    "id": "3018",
    "text": "Kostroma",
    "country_id": "181",
    "value": "Kostroma"
  },
  {
    "id": "3019",
    "text": "Krasnodar",
    "country_id": "181",
    "value": "Krasnodar"
  },
  {
    "id": "3020",
    "text": "Krasnojarsk",
    "country_id": "181",
    "value": "Krasnojarsk"
  },
  {
    "id": "3021",
    "text": "Krasnoyarskiy Kray",
    "country_id": "181",
    "value": "Krasnoyarskiy Kray"
  },
  {
    "id": "3022",
    "text": "Kurgan",
    "country_id": "181",
    "value": "Kurgan"
  },
  {
    "id": "3023",
    "text": "Kursk",
    "country_id": "181",
    "value": "Kursk"
  },
  {
    "id": "3024",
    "text": "Leningrad",
    "country_id": "181",
    "value": "Leningrad"
  },
  {
    "id": "3025",
    "text": "Lipeck",
    "country_id": "181",
    "value": "Lipeck"
  },
  {
    "id": "3026",
    "text": "Magadan",
    "country_id": "181",
    "value": "Magadan"
  },
  {
    "id": "3027",
    "text": "Marij El",
    "country_id": "181",
    "value": "Marij El"
  },
  {
    "id": "3028",
    "text": "Mordovija",
    "country_id": "181",
    "value": "Mordovija"
  },
  {
    "id": "3029",
    "text": "Moscow",
    "country_id": "181",
    "value": "Moscow"
  },
  {
    "id": "3030",
    "text": "Moskovskaja Oblast",
    "country_id": "181",
    "value": "Moskovskaja Oblast"
  },
  {
    "id": "3031",
    "text": "Moskovskaya Oblast",
    "country_id": "181",
    "value": "Moskovskaya Oblast"
  },
  {
    "id": "3032",
    "text": "Moskva",
    "country_id": "181",
    "value": "Moskva"
  },
  {
    "id": "3033",
    "text": "Murmansk",
    "country_id": "181",
    "value": "Murmansk"
  },
  {
    "id": "3034",
    "text": "Nenets",
    "country_id": "181",
    "value": "Nenets"
  },
  {
    "id": "3035",
    "text": "Nizhnij Novgorod",
    "country_id": "181",
    "value": "Nizhnij Novgorod"
  },
  {
    "id": "3036",
    "text": "Novgorod",
    "country_id": "181",
    "value": "Novgorod"
  },
  {
    "id": "3037",
    "text": "Novokusnezk",
    "country_id": "181",
    "value": "Novokusnezk"
  },
  {
    "id": "3038",
    "text": "Novosibirsk",
    "country_id": "181",
    "value": "Novosibirsk"
  },
  {
    "id": "3039",
    "text": "Omsk",
    "country_id": "181",
    "value": "Omsk"
  },
  {
    "id": "3040",
    "text": "Orenburg",
    "country_id": "181",
    "value": "Orenburg"
  },
  {
    "id": "3041",
    "text": "Orjol",
    "country_id": "181",
    "value": "Orjol"
  },
  {
    "id": "3042",
    "text": "Penza",
    "country_id": "181",
    "value": "Penza"
  },
  {
    "id": "3043",
    "text": "Perm",
    "country_id": "181",
    "value": "Perm"
  },
  {
    "id": "3044",
    "text": "Primorje",
    "country_id": "181",
    "value": "Primorje"
  },
  {
    "id": "3045",
    "text": "Pskov",
    "country_id": "181",
    "value": "Pskov"
  },
  {
    "id": "3046",
    "text": "Pskovskaya Oblast",
    "country_id": "181",
    "value": "Pskovskaya Oblast"
  },
  {
    "id": "3047",
    "text": "Rjazan",
    "country_id": "181",
    "value": "Rjazan"
  },
  {
    "id": "3048",
    "text": "Rostov",
    "country_id": "181",
    "value": "Rostov"
  },
  {
    "id": "3049",
    "text": "Saha",
    "country_id": "181",
    "value": "Saha"
  },
  {
    "id": "3050",
    "text": "Sahalin",
    "country_id": "181",
    "value": "Sahalin"
  },
  {
    "id": "3051",
    "text": "Samara",
    "country_id": "181",
    "value": "Samara"
  },
  {
    "id": "3052",
    "text": "Samarskaya",
    "country_id": "181",
    "value": "Samarskaya"
  },
  {
    "id": "3053",
    "text": "Sankt-Peterburg",
    "country_id": "181",
    "value": "Sankt-Peterburg"
  },
  {
    "id": "3054",
    "text": "Saratov",
    "country_id": "181",
    "value": "Saratov"
  },
  {
    "id": "3055",
    "text": "Smolensk",
    "country_id": "181",
    "value": "Smolensk"
  },
  {
    "id": "3056",
    "text": "Stavropol",
    "country_id": "181",
    "value": "Stavropol"
  },
  {
    "id": "3057",
    "text": "Sverdlovsk",
    "country_id": "181",
    "value": "Sverdlovsk"
  },
  {
    "id": "3058",
    "text": "Tajmyrija",
    "country_id": "181",
    "value": "Tajmyrija"
  },
  {
    "id": "3059",
    "text": "Tambov",
    "country_id": "181",
    "value": "Tambov"
  },
  {
    "id": "3060",
    "text": "Tatarstan",
    "country_id": "181",
    "value": "Tatarstan"
  },
  {
    "id": "3061",
    "text": "Tjumen",
    "country_id": "181",
    "value": "Tjumen"
  },
  {
    "id": "3062",
    "text": "Tomsk",
    "country_id": "181",
    "value": "Tomsk"
  },
  {
    "id": "3063",
    "text": "Tula",
    "country_id": "181",
    "value": "Tula"
  },
  {
    "id": "3064",
    "text": "Tver",
    "country_id": "181",
    "value": "Tver"
  },
  {
    "id": "3065",
    "text": "Tyva",
    "country_id": "181",
    "value": "Tyva"
  },
  {
    "id": "3066",
    "text": "Udmurtija",
    "country_id": "181",
    "value": "Udmurtija"
  },
  {
    "id": "3067",
    "text": "Uljanovsk",
    "country_id": "181",
    "value": "Uljanovsk"
  },
  {
    "id": "3068",
    "text": "Ulyanovskaya Oblast",
    "country_id": "181",
    "value": "Ulyanovskaya Oblast"
  },
  {
    "id": "3069",
    "text": "Ust-Orda",
    "country_id": "181",
    "value": "Ust-Orda"
  },
  {
    "id": "3070",
    "text": "Vladimir",
    "country_id": "181",
    "value": "Vladimir"
  },
  {
    "id": "3071",
    "text": "Volgograd",
    "country_id": "181",
    "value": "Volgograd"
  },
  {
    "id": "3072",
    "text": "Vologda",
    "country_id": "181",
    "value": "Vologda"
  },
  {
    "id": "3073",
    "text": "Voronezh",
    "country_id": "181",
    "value": "Voronezh"
  },
  {
    "id": "3074",
    "text": "Butare",
    "country_id": "182",
    "value": "Butare"
  },
  {
    "id": "3075",
    "text": "Byumba",
    "country_id": "182",
    "value": "Byumba"
  },
  {
    "id": "3076",
    "text": "Cyangugu",
    "country_id": "182",
    "value": "Cyangugu"
  },
  {
    "id": "3077",
    "text": "Gikongoro",
    "country_id": "182",
    "value": "Gikongoro"
  },
  {
    "id": "3078",
    "text": "Gisenyi",
    "country_id": "182",
    "value": "Gisenyi"
  },
  {
    "id": "3079",
    "text": "Gitarama",
    "country_id": "182",
    "value": "Gitarama"
  },
  {
    "id": "3080",
    "text": "Kibungo",
    "country_id": "182",
    "value": "Kibungo"
  },
  {
    "id": "3081",
    "text": "Kibuye",
    "country_id": "182",
    "value": "Kibuye"
  },
  {
    "id": "3082",
    "text": "Kigali-ngali",
    "country_id": "182",
    "value": "Kigali-ngali"
  },
  {
    "id": "3083",
    "text": "Ruhengeri",
    "country_id": "182",
    "value": "Ruhengeri"
  },
  {
    "id": "3084",
    "text": "Ascension",
    "country_id": "183",
    "value": "Ascension"
  },
  {
    "id": "3085",
    "text": "Gough Island",
    "country_id": "183",
    "value": "Gough Island"
  },
  {
    "id": "3086",
    "text": "Saint Helena",
    "country_id": "183",
    "value": "Saint Helena"
  },
  {
    "id": "3087",
    "text": "Tristan da Cunha",
    "country_id": "183",
    "value": "Tristan da Cunha"
  },
  {
    "id": "3088",
    "text": "Christ Church Nichola Town",
    "country_id": "184",
    "value": "Christ Church Nichola Town"
  },
  {
    "id": "3089",
    "text": "Saint Anne Sandy Point",
    "country_id": "184",
    "value": "Saint Anne Sandy Point"
  },
  {
    "id": "3090",
    "text": "Saint George Basseterre",
    "country_id": "184",
    "value": "Saint George Basseterre"
  },
  {
    "id": "3091",
    "text": "Saint George Gingerland",
    "country_id": "184",
    "value": "Saint George Gingerland"
  },
  {
    "id": "3092",
    "text": "Saint James Windward",
    "country_id": "184",
    "value": "Saint James Windward"
  },
  {
    "id": "3093",
    "text": "Saint John Capesterre",
    "country_id": "184",
    "value": "Saint John Capesterre"
  },
  {
    "id": "3094",
    "text": "Saint John Figtree",
    "country_id": "184",
    "value": "Saint John Figtree"
  },
  {
    "id": "3095",
    "text": "Saint Mary Cayon",
    "country_id": "184",
    "value": "Saint Mary Cayon"
  },
  {
    "id": "3096",
    "text": "Saint Paul Capesterre",
    "country_id": "184",
    "value": "Saint Paul Capesterre"
  },
  {
    "id": "3097",
    "text": "Saint Paul Charlestown",
    "country_id": "184",
    "value": "Saint Paul Charlestown"
  },
  {
    "id": "3098",
    "text": "Saint Peter Basseterre",
    "country_id": "184",
    "value": "Saint Peter Basseterre"
  },
  {
    "id": "3099",
    "text": "Saint Thomas Lowland",
    "country_id": "184",
    "value": "Saint Thomas Lowland"
  },
  {
    "id": "3100",
    "text": "Saint Thomas Middle Island",
    "country_id": "184",
    "value": "Saint Thomas Middle Island"
  },
  {
    "id": "3101",
    "text": "Trinity Palmetto Point",
    "country_id": "184",
    "value": "Trinity Palmetto Point"
  },
  {
    "id": "3102",
    "text": "Anse-la-Raye",
    "country_id": "185",
    "value": "Anse-la-Raye"
  },
  {
    "id": "3103",
    "text": "Canaries",
    "country_id": "185",
    "value": "Canaries"
  },
  {
    "id": "3104",
    "text": "Castries",
    "country_id": "185",
    "value": "Castries"
  },
  {
    "id": "3105",
    "text": "Choiseul",
    "country_id": "185",
    "value": "Choiseul"
  },
  {
    "id": "3106",
    "text": "Dennery",
    "country_id": "185",
    "value": "Dennery"
  },
  {
    "id": "3107",
    "text": "Gros Inlet",
    "country_id": "185",
    "value": "Gros Inlet"
  },
  {
    "id": "3108",
    "text": "Laborie",
    "country_id": "185",
    "value": "Laborie"
  },
  {
    "id": "3109",
    "text": "Micoud",
    "country_id": "185",
    "value": "Micoud"
  },
  {
    "id": "3110",
    "text": "Soufriere",
    "country_id": "185",
    "value": "Soufriere"
  },
  {
    "id": "3111",
    "text": "Vieux Fort",
    "country_id": "185",
    "value": "Vieux Fort"
  },
  {
    "id": "3112",
    "text": "Miquelon-Langlade",
    "country_id": "186",
    "value": "Miquelon-Langlade"
  },
  {
    "id": "3113",
    "text": "Saint-Pierre",
    "country_id": "186",
    "value": "Saint-Pierre"
  },
  {
    "id": "3114",
    "text": "Charlotte",
    "country_id": "187",
    "value": "Charlotte"
  },
  {
    "id": "3115",
    "text": "Grenadines",
    "country_id": "187",
    "value": "Grenadines"
  },
  {
    "id": "3116",
    "text": "Saint Andrew",
    "country_id": "187",
    "value": "Saint Andrew"
  },
  {
    "id": "3117",
    "text": "Saint David",
    "country_id": "187",
    "value": "Saint David"
  },
  {
    "id": "3118",
    "text": "Saint George",
    "country_id": "187",
    "value": "Saint George"
  },
  {
    "id": "3119",
    "text": "Saint Patrick",
    "country_id": "187",
    "value": "Saint Patrick"
  },
  {
    "id": "3120",
    "text": "A''ana",
    "country_id": "188",
    "value": "A''ana"
  },
  {
    "id": "3121",
    "text": "Aiga-i-le-Tai",
    "country_id": "188",
    "value": "Aiga-i-le-Tai"
  },
  {
    "id": "3122",
    "text": "Atua",
    "country_id": "188",
    "value": "Atua"
  },
  {
    "id": "3123",
    "text": "Fa''asaleleaga",
    "country_id": "188",
    "value": "Fa''asaleleaga"
  },
  {
    "id": "3124",
    "text": "Gaga''emauga",
    "country_id": "188",
    "value": "Gaga''emauga"
  },
  {
    "id": "3125",
    "text": "Gagaifomauga",
    "country_id": "188",
    "value": "Gagaifomauga"
  },
  {
    "id": "3126",
    "text": "Palauli",
    "country_id": "188",
    "value": "Palauli"
  },
  {
    "id": "3127",
    "text": "Satupa''itea",
    "country_id": "188",
    "value": "Satupa''itea"
  },
  {
    "id": "3128",
    "text": "Tuamasaga",
    "country_id": "188",
    "value": "Tuamasaga"
  },
  {
    "id": "3129",
    "text": "Va''a-o-Fonoti",
    "country_id": "188",
    "value": "Va''a-o-Fonoti"
  },
  {
    "id": "3130",
    "text": "Vaisigano",
    "country_id": "188",
    "value": "Vaisigano"
  },
  {
    "id": "3131",
    "text": "Acquaviva",
    "country_id": "189",
    "value": "Acquaviva"
  },
  {
    "id": "3132",
    "text": "Borgo Maggiore",
    "country_id": "189",
    "value": "Borgo Maggiore"
  },
  {
    "id": "3133",
    "text": "Chiesanuova",
    "country_id": "189",
    "value": "Chiesanuova"
  },
  {
    "id": "3134",
    "text": "Domagnano",
    "country_id": "189",
    "value": "Domagnano"
  },
  {
    "id": "3135",
    "text": "Faetano",
    "country_id": "189",
    "value": "Faetano"
  },
  {
    "id": "3136",
    "text": "Fiorentino",
    "country_id": "189",
    "value": "Fiorentino"
  },
  {
    "id": "3137",
    "text": "Montegiardino",
    "country_id": "189",
    "value": "Montegiardino"
  },
  {
    "id": "3138",
    "text": "San Marino",
    "country_id": "189",
    "value": "San Marino"
  },
  {
    "id": "3139",
    "text": "Serravalle",
    "country_id": "189",
    "value": "Serravalle"
  },
  {
    "id": "3140",
    "text": "Agua Grande",
    "country_id": "190",
    "value": "Agua Grande"
  },
  {
    "id": "3141",
    "text": "Cantagalo",
    "country_id": "190",
    "value": "Cantagalo"
  },
  {
    "id": "3142",
    "text": "Lemba",
    "country_id": "190",
    "value": "Lemba"
  },
  {
    "id": "3143",
    "text": "Lobata",
    "country_id": "190",
    "value": "Lobata"
  },
  {
    "id": "3144",
    "text": "Me-Zochi",
    "country_id": "190",
    "value": "Me-Zochi"
  },
  {
    "id": "3145",
    "text": "Pague",
    "country_id": "190",
    "value": "Pague"
  },
  {
    "id": "3146",
    "text": "Al Khobar",
    "country_id": "191",
    "value": "Al Khobar"
  },
  {
    "id": "3147",
    "text": "Aseer",
    "country_id": "191",
    "value": "Aseer"
  },
  {
    "id": "3148",
    "text": "Ash Sharqiyah",
    "country_id": "191",
    "value": "Ash Sharqiyah"
  },
  {
    "id": "3149",
    "text": "Asir",
    "country_id": "191",
    "value": "Asir"
  },
  {
    "id": "3150",
    "text": "Central Province",
    "country_id": "191",
    "value": "Central Province"
  },
  {
    "id": "3151",
    "text": "Eastern Province",
    "country_id": "191",
    "value": "Eastern Province"
  },
  {
    "id": "3152",
    "text": "Ha''il",
    "country_id": "191",
    "value": "Ha''il"
  },
  {
    "id": "3153",
    "text": "Jawf",
    "country_id": "191",
    "value": "Jawf"
  },
  {
    "id": "3154",
    "text": "Jizan",
    "country_id": "191",
    "value": "Jizan"
  },
  {
    "id": "3155",
    "text": "Makkah",
    "country_id": "191",
    "value": "Makkah"
  },
  {
    "id": "3156",
    "text": "Najran",
    "country_id": "191",
    "value": "Najran"
  },
  {
    "id": "3157",
    "text": "Qasim",
    "country_id": "191",
    "value": "Qasim"
  },
  {
    "id": "3158",
    "text": "Tabuk",
    "country_id": "191",
    "value": "Tabuk"
  },
  {
    "id": "3159",
    "text": "Western Province",
    "country_id": "191",
    "value": "Western Province"
  },
  {
    "id": "3160",
    "text": "al-Bahah",
    "country_id": "191",
    "value": "al-Bahah"
  },
  {
    "id": "3161",
    "text": "al-Hudud-ash-Shamaliyah",
    "country_id": "191",
    "value": "al-Hudud-ash-Shamaliyah"
  },
  {
    "id": "3162",
    "text": "al-Madinah",
    "country_id": "191",
    "value": "al-Madinah"
  },
  {
    "id": "3163",
    "text": "ar-Riyad",
    "country_id": "191",
    "value": "ar-Riyad"
  },
  {
    "id": "3164",
    "text": "Dakar",
    "country_id": "192",
    "value": "Dakar"
  },
  {
    "id": "3165",
    "text": "Diourbel",
    "country_id": "192",
    "value": "Diourbel"
  },
  {
    "id": "3166",
    "text": "Fatick",
    "country_id": "192",
    "value": "Fatick"
  },
  {
    "id": "3167",
    "text": "Kaolack",
    "country_id": "192",
    "value": "Kaolack"
  },
  {
    "id": "3168",
    "text": "Kolda",
    "country_id": "192",
    "value": "Kolda"
  },
  {
    "id": "3169",
    "text": "Louga",
    "country_id": "192",
    "value": "Louga"
  },
  {
    "id": "3170",
    "text": "Saint-Louis",
    "country_id": "192",
    "value": "Saint-Louis"
  },
  {
    "id": "3171",
    "text": "Tambacounda",
    "country_id": "192",
    "value": "Tambacounda"
  },
  {
    "id": "3172",
    "text": "Thies",
    "country_id": "192",
    "value": "Thies"
  },
  {
    "id": "3173",
    "text": "Ziguinchor",
    "country_id": "192",
    "value": "Ziguinchor"
  },
  {
    "id": "3174",
    "text": "Central Serbia",
    "country_id": "193",
    "value": "Central Serbia"
  },
  {
    "id": "3175",
    "text": "Kosovo and Metohija",
    "country_id": "193",
    "value": "Kosovo and Metohija"
  },
  {
    "id": "3176",
    "text": "Vojvodina",
    "country_id": "193",
    "value": "Vojvodina"
  },
  {
    "id": "3177",
    "text": "Anse Boileau",
    "country_id": "194",
    "value": "Anse Boileau"
  },
  {
    "id": "3178",
    "text": "Anse Royale",
    "country_id": "194",
    "value": "Anse Royale"
  },
  {
    "id": "3179",
    "text": "Cascade",
    "country_id": "194",
    "value": "Cascade"
  },
  {
    "id": "3180",
    "text": "Takamaka",
    "country_id": "194",
    "value": "Takamaka"
  },
  {
    "id": "3181",
    "text": "Victoria",
    "country_id": "194",
    "value": "Victoria"
  },
  {
    "id": "3182",
    "text": "Eastern",
    "country_id": "195",
    "value": "Eastern"
  },
  {
    "id": "3183",
    "text": "Northern",
    "country_id": "195",
    "value": "Northern"
  },
  {
    "id": "3184",
    "text": "Southern",
    "country_id": "195",
    "value": "Southern"
  },
  {
    "id": "3185",
    "text": "Western",
    "country_id": "195",
    "value": "Western"
  },
  {
    "id": "3186",
    "text": "Singapore",
    "country_id": "196",
    "value": "Singapore"
  },
  {
    "id": "3187",
    "text": "Banskobystricky",
    "country_id": "197",
    "value": "Banskobystricky"
  },
  {
    "id": "3188",
    "text": "Bratislavsky",
    "country_id": "197",
    "value": "Bratislavsky"
  },
  {
    "id": "3189",
    "text": "Kosicky",
    "country_id": "197",
    "value": "Kosicky"
  },
  {
    "id": "3190",
    "text": "Nitriansky",
    "country_id": "197",
    "value": "Nitriansky"
  },
  {
    "id": "3191",
    "text": "Presovsky",
    "country_id": "197",
    "value": "Presovsky"
  },
  {
    "id": "3192",
    "text": "Trenciansky",
    "country_id": "197",
    "value": "Trenciansky"
  },
  {
    "id": "3193",
    "text": "Trnavsky",
    "country_id": "197",
    "value": "Trnavsky"
  },
  {
    "id": "3194",
    "text": "Zilinsky",
    "country_id": "197",
    "value": "Zilinsky"
  },
  {
    "id": "3195",
    "text": "Benedikt",
    "country_id": "198",
    "value": "Benedikt"
  },
  {
    "id": "3196",
    "text": "Gorenjska",
    "country_id": "198",
    "value": "Gorenjska"
  },
  {
    "id": "3197",
    "text": "Gorishka",
    "country_id": "198",
    "value": "Gorishka"
  },
  {
    "id": "3198",
    "text": "Jugovzhodna Slovenija",
    "country_id": "198",
    "value": "Jugovzhodna Slovenija"
  },
  {
    "id": "3199",
    "text": "Koroshka",
    "country_id": "198",
    "value": "Koroshka"
  },
  {
    "id": "3200",
    "text": "Notranjsko-krashka",
    "country_id": "198",
    "value": "Notranjsko-krashka"
  },
  {
    "id": "3201",
    "text": "Obalno-krashka",
    "country_id": "198",
    "value": "Obalno-krashka"
  },
  {
    "id": "3202",
    "text": "Obcina Domzale",
    "country_id": "198",
    "value": "Obcina Domzale"
  },
  {
    "id": "3203",
    "text": "Obcina Vitanje",
    "country_id": "198",
    "value": "Obcina Vitanje"
  },
  {
    "id": "3204",
    "text": "Osrednjeslovenska",
    "country_id": "198",
    "value": "Osrednjeslovenska"
  },
  {
    "id": "3205",
    "text": "Podravska",
    "country_id": "198",
    "value": "Podravska"
  },
  {
    "id": "3206",
    "text": "Pomurska",
    "country_id": "198",
    "value": "Pomurska"
  },
  {
    "id": "3207",
    "text": "Savinjska",
    "country_id": "198",
    "value": "Savinjska"
  },
  {
    "id": "3208",
    "text": "Slovenian Littoral",
    "country_id": "198",
    "value": "Slovenian Littoral"
  },
  {
    "id": "3209",
    "text": "Spodnjeposavska",
    "country_id": "198",
    "value": "Spodnjeposavska"
  },
  {
    "id": "3210",
    "text": "Zasavska",
    "country_id": "198",
    "value": "Zasavska"
  },
  {
    "id": "3211",
    "text": "Pitcairn",
    "country_id": "199",
    "value": "Pitcairn"
  },
  {
    "id": "3212",
    "text": "Central",
    "country_id": "200",
    "value": "Central"
  },
  {
    "id": "3213",
    "text": "Choiseul",
    "country_id": "200",
    "value": "Choiseul"
  },
  {
    "id": "3214",
    "text": "Guadalcanal",
    "country_id": "200",
    "value": "Guadalcanal"
  },
  {
    "id": "3215",
    "text": "Isabel",
    "country_id": "200",
    "value": "Isabel"
  },
  {
    "id": "3216",
    "text": "Makira and Ulawa",
    "country_id": "200",
    "value": "Makira and Ulawa"
  },
  {
    "id": "3217",
    "text": "Malaita",
    "country_id": "200",
    "value": "Malaita"
  },
  {
    "id": "3218",
    "text": "Rennell and Bellona",
    "country_id": "200",
    "value": "Rennell and Bellona"
  },
  {
    "id": "3219",
    "text": "Temotu",
    "country_id": "200",
    "value": "Temotu"
  },
  {
    "id": "3220",
    "text": "Western",
    "country_id": "200",
    "value": "Western"
  },
  {
    "id": "3221",
    "text": "Awdal",
    "country_id": "201",
    "value": "Awdal"
  },
  {
    "id": "3222",
    "text": "Bakol",
    "country_id": "201",
    "value": "Bakol"
  },
  {
    "id": "3223",
    "text": "Banadir",
    "country_id": "201",
    "value": "Banadir"
  },
  {
    "id": "3224",
    "text": "Bari",
    "country_id": "201",
    "value": "Bari"
  },
  {
    "id": "3225",
    "text": "Bay",
    "country_id": "201",
    "value": "Bay"
  },
  {
    "id": "3226",
    "text": "Galgudug",
    "country_id": "201",
    "value": "Galgudug"
  },
  {
    "id": "3227",
    "text": "Gedo",
    "country_id": "201",
    "value": "Gedo"
  },
  {
    "id": "3228",
    "text": "Hiran",
    "country_id": "201",
    "value": "Hiran"
  },
  {
    "id": "3229",
    "text": "Jubbada Hose",
    "country_id": "201",
    "value": "Jubbada Hose"
  },
  {
    "id": "3230",
    "text": "Jubbadha Dexe",
    "country_id": "201",
    "value": "Jubbadha Dexe"
  },
  {
    "id": "3231",
    "text": "Mudug",
    "country_id": "201",
    "value": "Mudug"
  },
  {
    "id": "3232",
    "text": "Nugal",
    "country_id": "201",
    "value": "Nugal"
  },
  {
    "id": "3233",
    "text": "Sanag",
    "country_id": "201",
    "value": "Sanag"
  },
  {
    "id": "3234",
    "text": "Shabellaha Dhexe",
    "country_id": "201",
    "value": "Shabellaha Dhexe"
  },
  {
    "id": "3235",
    "text": "Shabellaha Hose",
    "country_id": "201",
    "value": "Shabellaha Hose"
  },
  {
    "id": "3236",
    "text": "Togdher",
    "country_id": "201",
    "value": "Togdher"
  },
  {
    "id": "3237",
    "text": "Woqoyi Galbed",
    "country_id": "201",
    "value": "Woqoyi Galbed"
  },
  {
    "id": "3238",
    "text": "Eastern Cape",
    "country_id": "202",
    "value": "Eastern Cape"
  },
  {
    "id": "3239",
    "text": "Free State",
    "country_id": "202",
    "value": "Free State"
  },
  {
    "id": "3240",
    "text": "Gauteng",
    "country_id": "202",
    "value": "Gauteng"
  },
  {
    "id": "3241",
    "text": "Kempton Park",
    "country_id": "202",
    "value": "Kempton Park"
  },
  {
    "id": "3242",
    "text": "Kramerville",
    "country_id": "202",
    "value": "Kramerville"
  },
  {
    "id": "3243",
    "text": "KwaZulu Natal",
    "country_id": "202",
    "value": "KwaZulu Natal"
  },
  {
    "id": "3244",
    "text": "Limpopo",
    "country_id": "202",
    "value": "Limpopo"
  },
  {
    "id": "3245",
    "text": "Mpumalanga",
    "country_id": "202",
    "value": "Mpumalanga"
  },
  {
    "id": "3246",
    "text": "North West",
    "country_id": "202",
    "value": "North West"
  },
  {
    "id": "3247",
    "text": "Northern Cape",
    "country_id": "202",
    "value": "Northern Cape"
  },
  {
    "id": "3248",
    "text": "Parow",
    "country_id": "202",
    "value": "Parow"
  },
  {
    "id": "3249",
    "text": "Table View",
    "country_id": "202",
    "value": "Table View"
  },
  {
    "id": "3250",
    "text": "Umtentweni",
    "country_id": "202",
    "value": "Umtentweni"
  },
  {
    "id": "3251",
    "text": "Western Cape",
    "country_id": "202",
    "value": "Western Cape"
  },
  {
    "id": "3252",
    "text": "South Georgia",
    "country_id": "203",
    "value": "South Georgia"
  },
  {
    "id": "3253",
    "text": "Central Equatoria",
    "country_id": "204",
    "value": "Central Equatoria"
  },
  {
    "id": "3254",
    "text": "A Coruna",
    "country_id": "205",
    "value": "A Coruna"
  },
  {
    "id": "3255",
    "text": "Alacant",
    "country_id": "205",
    "value": "Alacant"
  },
  {
    "id": "3256",
    "text": "Alava",
    "country_id": "205",
    "value": "Alava"
  },
  {
    "id": "3257",
    "text": "Albacete",
    "country_id": "205",
    "value": "Albacete"
  },
  {
    "id": "3258",
    "text": "Almeria",
    "country_id": "205",
    "value": "Almeria"
  },
  {
    "id": "3259",
    "text": "Andalucia",
    "country_id": "205",
    "value": "Andalucia"
  },
  {
    "id": "3260",
    "text": "Asturias",
    "country_id": "205",
    "value": "Asturias"
  },
  {
    "id": "3261",
    "text": "Avila",
    "country_id": "205",
    "value": "Avila"
  },
  {
    "id": "3262",
    "text": "Badajoz",
    "country_id": "205",
    "value": "Badajoz"
  },
  {
    "id": "3263",
    "text": "Balears",
    "country_id": "205",
    "value": "Balears"
  },
  {
    "id": "3264",
    "text": "Barcelona",
    "country_id": "205",
    "value": "Barcelona"
  },
  {
    "id": "3265",
    "text": "Bertamirans",
    "country_id": "205",
    "value": "Bertamirans"
  },
  {
    "id": "3266",
    "text": "Biscay",
    "country_id": "205",
    "value": "Biscay"
  },
  {
    "id": "3267",
    "text": "Burgos",
    "country_id": "205",
    "value": "Burgos"
  },
  {
    "id": "3268",
    "text": "Caceres",
    "country_id": "205",
    "value": "Caceres"
  },
  {
    "id": "3269",
    "text": "Cadiz",
    "country_id": "205",
    "value": "Cadiz"
  },
  {
    "id": "3270",
    "text": "Cantabria",
    "country_id": "205",
    "value": "Cantabria"
  },
  {
    "id": "3271",
    "text": "Castello",
    "country_id": "205",
    "value": "Castello"
  },
  {
    "id": "3272",
    "text": "Catalunya",
    "country_id": "205",
    "value": "Catalunya"
  },
  {
    "id": "3273",
    "text": "Ceuta",
    "country_id": "205",
    "value": "Ceuta"
  },
  {
    "id": "3274",
    "text": "Ciudad Real",
    "country_id": "205",
    "value": "Ciudad Real"
  },
  {
    "id": "3275",
    "text": "Comunidad Autonoma de Canarias",
    "country_id": "205",
    "value": "Comunidad Autonoma de Canarias"
  },
  {
    "id": "3276",
    "text": "Comunidad Autonoma de Cataluna",
    "country_id": "205",
    "value": "Comunidad Autonoma de Cataluna"
  },
  {
    "id": "3277",
    "text": "Comunidad Autonoma de Galicia",
    "country_id": "205",
    "value": "Comunidad Autonoma de Galicia"
  },
  {
    "id": "3278",
    "text": "Comunidad Autonoma de las Isla",
    "country_id": "205",
    "value": "Comunidad Autonoma de las Isla"
  },
  {
    "id": "3279",
    "text": "Comunidad Autonoma del Princip",
    "country_id": "205",
    "value": "Comunidad Autonoma del Princip"
  },
  {
    "id": "3280",
    "text": "Comunidad Valenciana",
    "country_id": "205",
    "value": "Comunidad Valenciana"
  },
  {
    "id": "3281",
    "text": "Cordoba",
    "country_id": "205",
    "value": "Cordoba"
  },
  {
    "id": "3282",
    "text": "Cuenca",
    "country_id": "205",
    "value": "Cuenca"
  },
  {
    "id": "3283",
    "text": "Gipuzkoa",
    "country_id": "205",
    "value": "Gipuzkoa"
  },
  {
    "id": "3284",
    "text": "Girona",
    "country_id": "205",
    "value": "Girona"
  },
  {
    "id": "3285",
    "text": "Granada",
    "country_id": "205",
    "value": "Granada"
  },
  {
    "id": "3286",
    "text": "Guadalajara",
    "country_id": "205",
    "value": "Guadalajara"
  },
  {
    "id": "3287",
    "text": "Guipuzcoa",
    "country_id": "205",
    "value": "Guipuzcoa"
  },
  {
    "id": "3288",
    "text": "Huelva",
    "country_id": "205",
    "value": "Huelva"
  },
  {
    "id": "3289",
    "text": "Huesca",
    "country_id": "205",
    "value": "Huesca"
  },
  {
    "id": "3290",
    "text": "Jaen",
    "country_id": "205",
    "value": "Jaen"
  },
  {
    "id": "3291",
    "text": "La Rioja",
    "country_id": "205",
    "value": "La Rioja"
  },
  {
    "id": "3292",
    "text": "Las Palmas",
    "country_id": "205",
    "value": "Las Palmas"
  },
  {
    "id": "3293",
    "text": "Leon",
    "country_id": "205",
    "value": "Leon"
  },
  {
    "id": "3294",
    "text": "Lerida",
    "country_id": "205",
    "value": "Lerida"
  },
  {
    "id": "3295",
    "text": "Lleida",
    "country_id": "205",
    "value": "Lleida"
  },
  {
    "id": "3296",
    "text": "Lugo",
    "country_id": "205",
    "value": "Lugo"
  },
  {
    "id": "3297",
    "text": "Madrid",
    "country_id": "205",
    "value": "Madrid"
  },
  {
    "id": "3298",
    "text": "Malaga",
    "country_id": "205",
    "value": "Malaga"
  },
  {
    "id": "3299",
    "text": "Melilla",
    "country_id": "205",
    "value": "Melilla"
  },
  {
    "id": "3300",
    "text": "Murcia",
    "country_id": "205",
    "value": "Murcia"
  },
  {
    "id": "3301",
    "text": "Navarra",
    "country_id": "205",
    "value": "Navarra"
  },
  {
    "id": "3302",
    "text": "Ourense",
    "country_id": "205",
    "value": "Ourense"
  },
  {
    "id": "3303",
    "text": "Pais Vasco",
    "country_id": "205",
    "value": "Pais Vasco"
  },
  {
    "id": "3304",
    "text": "Palencia",
    "country_id": "205",
    "value": "Palencia"
  },
  {
    "id": "3305",
    "text": "Pontevedra",
    "country_id": "205",
    "value": "Pontevedra"
  },
  {
    "id": "3306",
    "text": "Salamanca",
    "country_id": "205",
    "value": "Salamanca"
  },
  {
    "id": "3307",
    "text": "Santa Cruz de Tenerife",
    "country_id": "205",
    "value": "Santa Cruz de Tenerife"
  },
  {
    "id": "3308",
    "text": "Segovia",
    "country_id": "205",
    "value": "Segovia"
  },
  {
    "id": "3309",
    "text": "Sevilla",
    "country_id": "205",
    "value": "Sevilla"
  },
  {
    "id": "3310",
    "text": "Soria",
    "country_id": "205",
    "value": "Soria"
  },
  {
    "id": "3311",
    "text": "Tarragona",
    "country_id": "205",
    "value": "Tarragona"
  },
  {
    "id": "3312",
    "text": "Tenerife",
    "country_id": "205",
    "value": "Tenerife"
  },
  {
    "id": "3313",
    "text": "Teruel",
    "country_id": "205",
    "value": "Teruel"
  },
  {
    "id": "3314",
    "text": "Toledo",
    "country_id": "205",
    "value": "Toledo"
  },
  {
    "id": "3315",
    "text": "Valencia",
    "country_id": "205",
    "value": "Valencia"
  },
  {
    "id": "3316",
    "text": "Valladolid",
    "country_id": "205",
    "value": "Valladolid"
  },
  {
    "id": "3317",
    "text": "Vizcaya",
    "country_id": "205",
    "value": "Vizcaya"
  },
  {
    "id": "3318",
    "text": "Zamora",
    "country_id": "205",
    "value": "Zamora"
  },
  {
    "id": "3319",
    "text": "Zaragoza",
    "country_id": "205",
    "value": "Zaragoza"
  },
  {
    "id": "3320",
    "text": "Amparai",
    "country_id": "206",
    "value": "Amparai"
  },
  {
    "id": "3321",
    "text": "Anuradhapuraya",
    "country_id": "206",
    "value": "Anuradhapuraya"
  },
  {
    "id": "3322",
    "text": "Badulla",
    "country_id": "206",
    "value": "Badulla"
  },
  {
    "id": "3323",
    "text": "Boralesgamuwa",
    "country_id": "206",
    "value": "Boralesgamuwa"
  },
  {
    "id": "3324",
    "text": "Colombo",
    "country_id": "206",
    "value": "Colombo"
  },
  {
    "id": "3325",
    "text": "Galla",
    "country_id": "206",
    "value": "Galla"
  },
  {
    "id": "3326",
    "text": "Gampaha",
    "country_id": "206",
    "value": "Gampaha"
  },
  {
    "id": "3327",
    "text": "Hambantota",
    "country_id": "206",
    "value": "Hambantota"
  },
  {
    "id": "3328",
    "text": "Kalatura",
    "country_id": "206",
    "value": "Kalatura"
  },
  {
    "id": "3329",
    "text": "Kegalla",
    "country_id": "206",
    "value": "Kegalla"
  },
  {
    "id": "3330",
    "text": "Kilinochchi",
    "country_id": "206",
    "value": "Kilinochchi"
  },
  {
    "id": "3331",
    "text": "Kurunegala",
    "country_id": "206",
    "value": "Kurunegala"
  },
  {
    "id": "3332",
    "text": "Madakalpuwa",
    "country_id": "206",
    "value": "Madakalpuwa"
  },
  {
    "id": "3333",
    "text": "Maha Nuwara",
    "country_id": "206",
    "value": "Maha Nuwara"
  },
  {
    "id": "3334",
    "text": "Malwana",
    "country_id": "206",
    "value": "Malwana"
  },
  {
    "id": "3335",
    "text": "Mannarama",
    "country_id": "206",
    "value": "Mannarama"
  },
  {
    "id": "3336",
    "text": "Matale",
    "country_id": "206",
    "value": "Matale"
  },
  {
    "id": "3337",
    "text": "Matara",
    "country_id": "206",
    "value": "Matara"
  },
  {
    "id": "3338",
    "text": "Monaragala",
    "country_id": "206",
    "value": "Monaragala"
  },
  {
    "id": "3339",
    "text": "Mullaitivu",
    "country_id": "206",
    "value": "Mullaitivu"
  },
  {
    "id": "3340",
    "text": "North Eastern Province",
    "country_id": "206",
    "value": "North Eastern Province"
  },
  {
    "id": "3341",
    "text": "North Western Province",
    "country_id": "206",
    "value": "North Western Province"
  },
  {
    "id": "3342",
    "text": "Nuwara Eliya",
    "country_id": "206",
    "value": "Nuwara Eliya"
  },
  {
    "id": "3343",
    "text": "Polonnaruwa",
    "country_id": "206",
    "value": "Polonnaruwa"
  },
  {
    "id": "3344",
    "text": "Puttalama",
    "country_id": "206",
    "value": "Puttalama"
  },
  {
    "id": "3345",
    "text": "Ratnapuraya",
    "country_id": "206",
    "value": "Ratnapuraya"
  },
  {
    "id": "3346",
    "text": "Southern Province",
    "country_id": "206",
    "value": "Southern Province"
  },
  {
    "id": "3347",
    "text": "Tirikunamalaya",
    "country_id": "206",
    "value": "Tirikunamalaya"
  },
  {
    "id": "3348",
    "text": "Tuscany",
    "country_id": "206",
    "value": "Tuscany"
  },
  {
    "id": "3349",
    "text": "Vavuniyawa",
    "country_id": "206",
    "value": "Vavuniyawa"
  },
  {
    "id": "3350",
    "text": "Western Province",
    "country_id": "206",
    "value": "Western Province"
  },
  {
    "id": "3351",
    "text": "Yapanaya",
    "country_id": "206",
    "value": "Yapanaya"
  },
  {
    "id": "3352",
    "text": "kadawatha",
    "country_id": "206",
    "value": "kadawatha"
  },
  {
    "id": "3353",
    "text": "A''ali-an-Nil",
    "country_id": "207",
    "value": "A''ali-an-Nil"
  },
  {
    "id": "3354",
    "text": "Bahr-al-Jabal",
    "country_id": "207",
    "value": "Bahr-al-Jabal"
  },
  {
    "id": "3355",
    "text": "Central Equatoria",
    "country_id": "207",
    "value": "Central Equatoria"
  },
  {
    "id": "3356",
    "text": "Gharb Bahr-al-Ghazal",
    "country_id": "207",
    "value": "Gharb Bahr-al-Ghazal"
  },
  {
    "id": "3357",
    "text": "Gharb Darfur",
    "country_id": "207",
    "value": "Gharb Darfur"
  },
  {
    "id": "3358",
    "text": "Gharb Kurdufan",
    "country_id": "207",
    "value": "Gharb Kurdufan"
  },
  {
    "id": "3359",
    "text": "Gharb-al-Istiwa''iyah",
    "country_id": "207",
    "value": "Gharb-al-Istiwa''iyah"
  },
  {
    "id": "3360",
    "text": "Janub Darfur",
    "country_id": "207",
    "value": "Janub Darfur"
  },
  {
    "id": "3361",
    "text": "Janub Kurdufan",
    "country_id": "207",
    "value": "Janub Kurdufan"
  },
  {
    "id": "3362",
    "text": "Junqali",
    "country_id": "207",
    "value": "Junqali"
  },
  {
    "id": "3363",
    "text": "Kassala",
    "country_id": "207",
    "value": "Kassala"
  },
  {
    "id": "3364",
    "text": "Nahr-an-Nil",
    "country_id": "207",
    "value": "Nahr-an-Nil"
  },
  {
    "id": "3365",
    "text": "Shamal Bahr-al-Ghazal",
    "country_id": "207",
    "value": "Shamal Bahr-al-Ghazal"
  },
  {
    "id": "3366",
    "text": "Shamal Darfur",
    "country_id": "207",
    "value": "Shamal Darfur"
  },
  {
    "id": "3367",
    "text": "Shamal Kurdufan",
    "country_id": "207",
    "value": "Shamal Kurdufan"
  },
  {
    "id": "3368",
    "text": "Sharq-al-Istiwa''iyah",
    "country_id": "207",
    "value": "Sharq-al-Istiwa''iyah"
  },
  {
    "id": "3369",
    "text": "Sinnar",
    "country_id": "207",
    "value": "Sinnar"
  },
  {
    "id": "3370",
    "text": "Warab",
    "country_id": "207",
    "value": "Warab"
  },
  {
    "id": "3371",
    "text": "Wilayat al Khartum",
    "country_id": "207",
    "value": "Wilayat al Khartum"
  },
  {
    "id": "3372",
    "text": "al-Bahr-al-Ahmar",
    "country_id": "207",
    "value": "al-Bahr-al-Ahmar"
  },
  {
    "id": "3373",
    "text": "al-Buhayrat",
    "country_id": "207",
    "value": "al-Buhayrat"
  },
  {
    "id": "3374",
    "text": "al-Jazirah",
    "country_id": "207",
    "value": "al-Jazirah"
  },
  {
    "id": "3375",
    "text": "al-Khartum",
    "country_id": "207",
    "value": "al-Khartum"
  },
  {
    "id": "3376",
    "text": "al-Qadarif",
    "country_id": "207",
    "value": "al-Qadarif"
  },
  {
    "id": "3377",
    "text": "al-Wahdah",
    "country_id": "207",
    "value": "al-Wahdah"
  },
  {
    "id": "3378",
    "text": "an-Nil-al-Abyad",
    "country_id": "207",
    "value": "an-Nil-al-Abyad"
  },
  {
    "id": "3379",
    "text": "an-Nil-al-Azraq",
    "country_id": "207",
    "value": "an-Nil-al-Azraq"
  },
  {
    "id": "3380",
    "text": "ash-Shamaliyah",
    "country_id": "207",
    "value": "ash-Shamaliyah"
  },
  {
    "id": "3381",
    "text": "Brokopondo",
    "country_id": "208",
    "value": "Brokopondo"
  },
  {
    "id": "3382",
    "text": "Commewijne",
    "country_id": "208",
    "value": "Commewijne"
  },
  {
    "id": "3383",
    "text": "Coronie",
    "country_id": "208",
    "value": "Coronie"
  },
  {
    "id": "3384",
    "text": "Marowijne",
    "country_id": "208",
    "value": "Marowijne"
  },
  {
    "id": "3385",
    "text": "Nickerie",
    "country_id": "208",
    "value": "Nickerie"
  },
  {
    "id": "3386",
    "text": "Para",
    "country_id": "208",
    "value": "Para"
  },
  {
    "id": "3387",
    "text": "Paramaribo",
    "country_id": "208",
    "value": "Paramaribo"
  },
  {
    "id": "3388",
    "text": "Saramacca",
    "country_id": "208",
    "value": "Saramacca"
  },
  {
    "id": "3389",
    "text": "Wanica",
    "country_id": "208",
    "value": "Wanica"
  },
  {
    "id": "3390",
    "text": "Svalbard",
    "country_id": "209",
    "value": "Svalbard"
  },
  {
    "id": "3391",
    "text": "Hhohho",
    "country_id": "210",
    "value": "Hhohho"
  },
  {
    "id": "3392",
    "text": "Lubombo",
    "country_id": "210",
    "value": "Lubombo"
  },
  {
    "id": "3393",
    "text": "Manzini",
    "country_id": "210",
    "value": "Manzini"
  },
  {
    "id": "3394",
    "text": "Shiselweni",
    "country_id": "210",
    "value": "Shiselweni"
  },
  {
    "id": "3395",
    "text": "Alvsborgs Lan",
    "country_id": "211",
    "value": "Alvsborgs Lan"
  },
  {
    "id": "3396",
    "text": "Angermanland",
    "country_id": "211",
    "value": "Angermanland"
  },
  {
    "id": "3397",
    "text": "Blekinge",
    "country_id": "211",
    "value": "Blekinge"
  },
  {
    "id": "3398",
    "text": "Bohuslan",
    "country_id": "211",
    "value": "Bohuslan"
  },
  {
    "id": "3399",
    "text": "Dalarna",
    "country_id": "211",
    "value": "Dalarna"
  },
  {
    "id": "3400",
    "text": "Gavleborg",
    "country_id": "211",
    "value": "Gavleborg"
  },
  {
    "id": "3401",
    "text": "Gaza",
    "country_id": "211",
    "value": "Gaza"
  },
  {
    "id": "3402",
    "text": "Gotland",
    "country_id": "211",
    "value": "Gotland"
  },
  {
    "id": "3403",
    "text": "Halland",
    "country_id": "211",
    "value": "Halland"
  },
  {
    "id": "3404",
    "text": "Jamtland",
    "country_id": "211",
    "value": "Jamtland"
  },
  {
    "id": "3405",
    "text": "Jonkoping",
    "country_id": "211",
    "value": "Jonkoping"
  },
  {
    "id": "3406",
    "text": "Kalmar",
    "country_id": "211",
    "value": "Kalmar"
  },
  {
    "id": "3407",
    "text": "Kristianstads",
    "country_id": "211",
    "value": "Kristianstads"
  },
  {
    "id": "3408",
    "text": "Kronoberg",
    "country_id": "211",
    "value": "Kronoberg"
  },
  {
    "id": "3409",
    "text": "Norrbotten",
    "country_id": "211",
    "value": "Norrbotten"
  },
  {
    "id": "3410",
    "text": "Orebro",
    "country_id": "211",
    "value": "Orebro"
  },
  {
    "id": "3411",
    "text": "Ostergotland",
    "country_id": "211",
    "value": "Ostergotland"
  },
  {
    "id": "3412",
    "text": "Saltsjo-Boo",
    "country_id": "211",
    "value": "Saltsjo-Boo"
  },
  {
    "id": "3413",
    "text": "Skane",
    "country_id": "211",
    "value": "Skane"
  },
  {
    "id": "3414",
    "text": "Smaland",
    "country_id": "211",
    "value": "Smaland"
  },
  {
    "id": "3415",
    "text": "Sodermanland",
    "country_id": "211",
    "value": "Sodermanland"
  },
  {
    "id": "3416",
    "text": "Stockholm",
    "country_id": "211",
    "value": "Stockholm"
  },
  {
    "id": "3417",
    "text": "Uppsala",
    "country_id": "211",
    "value": "Uppsala"
  },
  {
    "id": "3418",
    "text": "Varmland",
    "country_id": "211",
    "value": "Varmland"
  },
  {
    "id": "3419",
    "text": "Vasterbotten",
    "country_id": "211",
    "value": "Vasterbotten"
  },
  {
    "id": "3420",
    "text": "Vastergotland",
    "country_id": "211",
    "value": "Vastergotland"
  },
  {
    "id": "3421",
    "text": "Vasternorrland",
    "country_id": "211",
    "value": "Vasternorrland"
  },
  {
    "id": "3422",
    "text": "Vastmanland",
    "country_id": "211",
    "value": "Vastmanland"
  },
  {
    "id": "3423",
    "text": "Vastra Gotaland",
    "country_id": "211",
    "value": "Vastra Gotaland"
  },
  {
    "id": "3424",
    "text": "Aargau",
    "country_id": "212",
    "value": "Aargau"
  },
  {
    "id": "3425",
    "text": "Appenzell Inner-Rhoden",
    "country_id": "212",
    "value": "Appenzell Inner-Rhoden"
  },
  {
    "id": "3426",
    "text": "Appenzell-Ausser Rhoden",
    "country_id": "212",
    "value": "Appenzell-Ausser Rhoden"
  },
  {
    "id": "3427",
    "text": "Basel-Landschaft",
    "country_id": "212",
    "value": "Basel-Landschaft"
  },
  {
    "id": "3428",
    "text": "Basel-Stadt",
    "country_id": "212",
    "value": "Basel-Stadt"
  },
  {
    "id": "3429",
    "text": "Bern",
    "country_id": "212",
    "value": "Bern"
  },
  {
    "id": "3430",
    "text": "Canton Ticino",
    "country_id": "212",
    "value": "Canton Ticino"
  },
  {
    "id": "3431",
    "text": "Fribourg",
    "country_id": "212",
    "value": "Fribourg"
  },
  {
    "id": "3432",
    "text": "Geneve",
    "country_id": "212",
    "value": "Geneve"
  },
  {
    "id": "3433",
    "text": "Glarus",
    "country_id": "212",
    "value": "Glarus"
  },
  {
    "id": "3434",
    "text": "Graubunden",
    "country_id": "212",
    "value": "Graubunden"
  },
  {
    "id": "3435",
    "text": "Heerbrugg",
    "country_id": "212",
    "value": "Heerbrugg"
  },
  {
    "id": "3436",
    "text": "Jura",
    "country_id": "212",
    "value": "Jura"
  },
  {
    "id": "3437",
    "text": "Kanton Aargau",
    "country_id": "212",
    "value": "Kanton Aargau"
  },
  {
    "id": "3438",
    "text": "Luzern",
    "country_id": "212",
    "value": "Luzern"
  },
  {
    "id": "3439",
    "text": "Morbio Inferiore",
    "country_id": "212",
    "value": "Morbio Inferiore"
  },
  {
    "id": "3440",
    "text": "Muhen",
    "country_id": "212",
    "value": "Muhen"
  },
  {
    "id": "3441",
    "text": "Neuchatel",
    "country_id": "212",
    "value": "Neuchatel"
  },
  {
    "id": "3442",
    "text": "Nidwalden",
    "country_id": "212",
    "value": "Nidwalden"
  },
  {
    "id": "3443",
    "text": "Obwalden",
    "country_id": "212",
    "value": "Obwalden"
  },
  {
    "id": "3444",
    "text": "Sankt Gallen",
    "country_id": "212",
    "value": "Sankt Gallen"
  },
  {
    "id": "3445",
    "text": "Schaffhausen",
    "country_id": "212",
    "value": "Schaffhausen"
  },
  {
    "id": "3446",
    "text": "Schwyz",
    "country_id": "212",
    "value": "Schwyz"
  },
  {
    "id": "3447",
    "text": "Solothurn",
    "country_id": "212",
    "value": "Solothurn"
  },
  {
    "id": "3448",
    "text": "Thurgau",
    "country_id": "212",
    "value": "Thurgau"
  },
  {
    "id": "3449",
    "text": "Ticino",
    "country_id": "212",
    "value": "Ticino"
  },
  {
    "id": "3450",
    "text": "Uri",
    "country_id": "212",
    "value": "Uri"
  },
  {
    "id": "3451",
    "text": "Valais",
    "country_id": "212",
    "value": "Valais"
  },
  {
    "id": "3452",
    "text": "Vaud",
    "country_id": "212",
    "value": "Vaud"
  },
  {
    "id": "3453",
    "text": "Vauffelin",
    "country_id": "212",
    "value": "Vauffelin"
  },
  {
    "id": "3454",
    "text": "Zug",
    "country_id": "212",
    "value": "Zug"
  },
  {
    "id": "3455",
    "text": "Zurich",
    "country_id": "212",
    "value": "Zurich"
  },
  {
    "id": "3456",
    "text": "Aleppo",
    "country_id": "213",
    "value": "Aleppo"
  },
  {
    "id": "3457",
    "text": "Dar''a",
    "country_id": "213",
    "value": "Dar''a"
  },
  {
    "id": "3458",
    "text": "Dayr-az-Zawr",
    "country_id": "213",
    "value": "Dayr-az-Zawr"
  },
  {
    "id": "3459",
    "text": "Dimashq",
    "country_id": "213",
    "value": "Dimashq"
  },
  {
    "id": "3460",
    "text": "Halab",
    "country_id": "213",
    "value": "Halab"
  },
  {
    "id": "3461",
    "text": "Hamah",
    "country_id": "213",
    "value": "Hamah"
  },
  {
    "id": "3462",
    "text": "Hims",
    "country_id": "213",
    "value": "Hims"
  },
  {
    "id": "3463",
    "text": "Idlib",
    "country_id": "213",
    "value": "Idlib"
  },
  {
    "id": "3464",
    "text": "Madinat Dimashq",
    "country_id": "213",
    "value": "Madinat Dimashq"
  },
  {
    "id": "3465",
    "text": "Tartus",
    "country_id": "213",
    "value": "Tartus"
  },
  {
    "id": "3466",
    "text": "al-Hasakah",
    "country_id": "213",
    "value": "al-Hasakah"
  },
  {
    "id": "3467",
    "text": "al-Ladhiqiyah",
    "country_id": "213",
    "value": "al-Ladhiqiyah"
  },
  {
    "id": "3468",
    "text": "al-Qunaytirah",
    "country_id": "213",
    "value": "al-Qunaytirah"
  },
  {
    "id": "3469",
    "text": "ar-Raqqah",
    "country_id": "213",
    "value": "ar-Raqqah"
  },
  {
    "id": "3470",
    "text": "as-Suwayda",
    "country_id": "213",
    "value": "as-Suwayda"
  },
  {
    "id": "3471",
    "text": "Changhwa",
    "country_id": "214",
    "value": "Changhwa"
  },
  {
    "id": "3472",
    "text": "Chiayi Hsien",
    "country_id": "214",
    "value": "Chiayi Hsien"
  },
  {
    "id": "3473",
    "text": "Chiayi Shih",
    "country_id": "214",
    "value": "Chiayi Shih"
  },
  {
    "id": "3474",
    "text": "Eastern Taipei",
    "country_id": "214",
    "value": "Eastern Taipei"
  },
  {
    "id": "3475",
    "text": "Hsinchu Hsien",
    "country_id": "214",
    "value": "Hsinchu Hsien"
  },
  {
    "id": "3476",
    "text": "Hsinchu Shih",
    "country_id": "214",
    "value": "Hsinchu Shih"
  },
  {
    "id": "3477",
    "text": "Hualien",
    "country_id": "214",
    "value": "Hualien"
  },
  {
    "id": "3478",
    "text": "Ilan",
    "country_id": "214",
    "value": "Ilan"
  },
  {
    "id": "3479",
    "text": "Kaohsiung Hsien",
    "country_id": "214",
    "value": "Kaohsiung Hsien"
  },
  {
    "id": "3480",
    "text": "Kaohsiung Shih",
    "country_id": "214",
    "value": "Kaohsiung Shih"
  },
  {
    "id": "3481",
    "text": "Keelung Shih",
    "country_id": "214",
    "value": "Keelung Shih"
  },
  {
    "id": "3482",
    "text": "Kinmen",
    "country_id": "214",
    "value": "Kinmen"
  },
  {
    "id": "3483",
    "text": "Miaoli",
    "country_id": "214",
    "value": "Miaoli"
  },
  {
    "id": "3484",
    "text": "Nantou",
    "country_id": "214",
    "value": "Nantou"
  },
  {
    "id": "3485",
    "text": "Northern Taiwan",
    "country_id": "214",
    "value": "Northern Taiwan"
  },
  {
    "id": "3486",
    "text": "Penghu",
    "country_id": "214",
    "value": "Penghu"
  },
  {
    "id": "3487",
    "text": "Pingtung",
    "country_id": "214",
    "value": "Pingtung"
  },
  {
    "id": "3488",
    "text": "Taichung",
    "country_id": "214",
    "value": "Taichung"
  },
  {
    "id": "3489",
    "text": "Taichung Hsien",
    "country_id": "214",
    "value": "Taichung Hsien"
  },
  {
    "id": "3490",
    "text": "Taichung Shih",
    "country_id": "214",
    "value": "Taichung Shih"
  },
  {
    "id": "3491",
    "text": "Tainan Hsien",
    "country_id": "214",
    "value": "Tainan Hsien"
  },
  {
    "id": "3492",
    "text": "Tainan Shih",
    "country_id": "214",
    "value": "Tainan Shih"
  },
  {
    "id": "3493",
    "text": "Taipei Hsien",
    "country_id": "214",
    "value": "Taipei Hsien"
  },
  {
    "id": "3494",
    "text": "Taipei Shih \/ Taipei Hsien",
    "country_id": "214",
    "value": "Taipei Shih \/ Taipei Hsien"
  },
  {
    "id": "3495",
    "text": "Taitung",
    "country_id": "214",
    "value": "Taitung"
  },
  {
    "id": "3496",
    "text": "Taoyuan",
    "country_id": "214",
    "value": "Taoyuan"
  },
  {
    "id": "3497",
    "text": "Yilan",
    "country_id": "214",
    "value": "Yilan"
  },
  {
    "id": "3498",
    "text": "Yun-Lin Hsien",
    "country_id": "214",
    "value": "Yun-Lin Hsien"
  },
  {
    "id": "3499",
    "text": "Yunlin",
    "country_id": "214",
    "value": "Yunlin"
  },
  {
    "id": "3500",
    "text": "Dushanbe",
    "country_id": "215",
    "value": "Dushanbe"
  },
  {
    "id": "3501",
    "text": "Gorno-Badakhshan",
    "country_id": "215",
    "value": "Gorno-Badakhshan"
  },
  {
    "id": "3502",
    "text": "Karotegin",
    "country_id": "215",
    "value": "Karotegin"
  },
  {
    "id": "3503",
    "text": "Khatlon",
    "country_id": "215",
    "value": "Khatlon"
  },
  {
    "id": "3504",
    "text": "Sughd",
    "country_id": "215",
    "value": "Sughd"
  },
  {
    "id": "3505",
    "text": "Arusha",
    "country_id": "216",
    "value": "Arusha"
  },
  {
    "id": "3506",
    "text": "Dar es Salaam",
    "country_id": "216",
    "value": "Dar es Salaam"
  },
  {
    "id": "3507",
    "text": "Dodoma",
    "country_id": "216",
    "value": "Dodoma"
  },
  {
    "id": "3508",
    "text": "Iringa",
    "country_id": "216",
    "value": "Iringa"
  },
  {
    "id": "3509",
    "text": "Kagera",
    "country_id": "216",
    "value": "Kagera"
  },
  {
    "id": "3510",
    "text": "Kigoma",
    "country_id": "216",
    "value": "Kigoma"
  },
  {
    "id": "3511",
    "text": "Kilimanjaro",
    "country_id": "216",
    "value": "Kilimanjaro"
  },
  {
    "id": "3512",
    "text": "Lindi",
    "country_id": "216",
    "value": "Lindi"
  },
  {
    "id": "3513",
    "text": "Mara",
    "country_id": "216",
    "value": "Mara"
  },
  {
    "id": "3514",
    "text": "Mbeya",
    "country_id": "216",
    "value": "Mbeya"
  },
  {
    "id": "3515",
    "text": "Morogoro",
    "country_id": "216",
    "value": "Morogoro"
  },
  {
    "id": "3516",
    "text": "Mtwara",
    "country_id": "216",
    "value": "Mtwara"
  },
  {
    "id": "3517",
    "text": "Mwanza",
    "country_id": "216",
    "value": "Mwanza"
  },
  {
    "id": "3518",
    "text": "Pwani",
    "country_id": "216",
    "value": "Pwani"
  },
  {
    "id": "3519",
    "text": "Rukwa",
    "country_id": "216",
    "value": "Rukwa"
  },
  {
    "id": "3520",
    "text": "Ruvuma",
    "country_id": "216",
    "value": "Ruvuma"
  },
  {
    "id": "3521",
    "text": "Shinyanga",
    "country_id": "216",
    "value": "Shinyanga"
  },
  {
    "id": "3522",
    "text": "Singida",
    "country_id": "216",
    "value": "Singida"
  },
  {
    "id": "3523",
    "text": "Tabora",
    "country_id": "216",
    "value": "Tabora"
  },
  {
    "id": "3524",
    "text": "Tanga",
    "country_id": "216",
    "value": "Tanga"
  },
  {
    "id": "3525",
    "text": "Zanzibar and Pemba",
    "country_id": "216",
    "value": "Zanzibar and Pemba"
  },
  {
    "id": "3526",
    "text": "Amnat Charoen",
    "country_id": "217",
    "value": "Amnat Charoen"
  },
  {
    "id": "3527",
    "text": "Ang Thong",
    "country_id": "217",
    "value": "Ang Thong"
  },
  {
    "id": "3528",
    "text": "Bangkok",
    "country_id": "217",
    "value": "Bangkok"
  },
  {
    "id": "3529",
    "text": "Buri Ram",
    "country_id": "217",
    "value": "Buri Ram"
  },
  {
    "id": "3530",
    "text": "Chachoengsao",
    "country_id": "217",
    "value": "Chachoengsao"
  },
  {
    "id": "3531",
    "text": "Chai Nat",
    "country_id": "217",
    "value": "Chai Nat"
  },
  {
    "id": "3532",
    "text": "Chaiyaphum",
    "country_id": "217",
    "value": "Chaiyaphum"
  },
  {
    "id": "3533",
    "text": "Changwat Chaiyaphum",
    "country_id": "217",
    "value": "Changwat Chaiyaphum"
  },
  {
    "id": "3534",
    "text": "Chanthaburi",
    "country_id": "217",
    "value": "Chanthaburi"
  },
  {
    "id": "3535",
    "text": "Chiang Mai",
    "country_id": "217",
    "value": "Chiang Mai"
  },
  {
    "id": "3536",
    "text": "Chiang Rai",
    "country_id": "217",
    "value": "Chiang Rai"
  },
  {
    "id": "3537",
    "text": "Chon Buri",
    "country_id": "217",
    "value": "Chon Buri"
  },
  {
    "id": "3538",
    "text": "Chumphon",
    "country_id": "217",
    "value": "Chumphon"
  },
  {
    "id": "3539",
    "text": "Kalasin",
    "country_id": "217",
    "value": "Kalasin"
  },
  {
    "id": "3540",
    "text": "Kamphaeng Phet",
    "country_id": "217",
    "value": "Kamphaeng Phet"
  },
  {
    "id": "3541",
    "text": "Kanchanaburi",
    "country_id": "217",
    "value": "Kanchanaburi"
  },
  {
    "id": "3542",
    "text": "Khon Kaen",
    "country_id": "217",
    "value": "Khon Kaen"
  },
  {
    "id": "3543",
    "text": "Krabi",
    "country_id": "217",
    "value": "Krabi"
  },
  {
    "id": "3544",
    "text": "Krung Thep",
    "country_id": "217",
    "value": "Krung Thep"
  },
  {
    "id": "3545",
    "text": "Lampang",
    "country_id": "217",
    "value": "Lampang"
  },
  {
    "id": "3546",
    "text": "Lamphun",
    "country_id": "217",
    "value": "Lamphun"
  },
  {
    "id": "3547",
    "text": "Loei",
    "country_id": "217",
    "value": "Loei"
  },
  {
    "id": "3548",
    "text": "Lop Buri",
    "country_id": "217",
    "value": "Lop Buri"
  },
  {
    "id": "3549",
    "text": "Mae Hong Son",
    "country_id": "217",
    "value": "Mae Hong Son"
  },
  {
    "id": "3550",
    "text": "Maha Sarakham",
    "country_id": "217",
    "value": "Maha Sarakham"
  },
  {
    "id": "3551",
    "text": "Mukdahan",
    "country_id": "217",
    "value": "Mukdahan"
  },
  {
    "id": "3552",
    "text": "Nakhon Nayok",
    "country_id": "217",
    "value": "Nakhon Nayok"
  },
  {
    "id": "3553",
    "text": "Nakhon Pathom",
    "country_id": "217",
    "value": "Nakhon Pathom"
  },
  {
    "id": "3554",
    "text": "Nakhon Phanom",
    "country_id": "217",
    "value": "Nakhon Phanom"
  },
  {
    "id": "3555",
    "text": "Nakhon Ratchasima",
    "country_id": "217",
    "value": "Nakhon Ratchasima"
  },
  {
    "id": "3556",
    "text": "Nakhon Sawan",
    "country_id": "217",
    "value": "Nakhon Sawan"
  },
  {
    "id": "3557",
    "text": "Nakhon Si Thammarat",
    "country_id": "217",
    "value": "Nakhon Si Thammarat"
  },
  {
    "id": "3558",
    "text": "Nan",
    "country_id": "217",
    "value": "Nan"
  },
  {
    "id": "3559",
    "text": "Narathiwat",
    "country_id": "217",
    "value": "Narathiwat"
  },
  {
    "id": "3560",
    "text": "Nong Bua Lam Phu",
    "country_id": "217",
    "value": "Nong Bua Lam Phu"
  },
  {
    "id": "3561",
    "text": "Nong Khai",
    "country_id": "217",
    "value": "Nong Khai"
  },
  {
    "id": "3562",
    "text": "Nonthaburi",
    "country_id": "217",
    "value": "Nonthaburi"
  },
  {
    "id": "3563",
    "text": "Pathum Thani",
    "country_id": "217",
    "value": "Pathum Thani"
  },
  {
    "id": "3564",
    "text": "Pattani",
    "country_id": "217",
    "value": "Pattani"
  },
  {
    "id": "3565",
    "text": "Phangnga",
    "country_id": "217",
    "value": "Phangnga"
  },
  {
    "id": "3566",
    "text": "Phatthalung",
    "country_id": "217",
    "value": "Phatthalung"
  },
  {
    "id": "3567",
    "text": "Phayao",
    "country_id": "217",
    "value": "Phayao"
  },
  {
    "id": "3568",
    "text": "Phetchabun",
    "country_id": "217",
    "value": "Phetchabun"
  },
  {
    "id": "3569",
    "text": "Phetchaburi",
    "country_id": "217",
    "value": "Phetchaburi"
  },
  {
    "id": "3570",
    "text": "Phichit",
    "country_id": "217",
    "value": "Phichit"
  },
  {
    "id": "3571",
    "text": "Phitsanulok",
    "country_id": "217",
    "value": "Phitsanulok"
  },
  {
    "id": "3572",
    "text": "Phra Nakhon Si Ayutthaya",
    "country_id": "217",
    "value": "Phra Nakhon Si Ayutthaya"
  },
  {
    "id": "3573",
    "text": "Phrae",
    "country_id": "217",
    "value": "Phrae"
  },
  {
    "id": "3574",
    "text": "Phuket",
    "country_id": "217",
    "value": "Phuket"
  },
  {
    "id": "3575",
    "text": "Prachin Buri",
    "country_id": "217",
    "value": "Prachin Buri"
  },
  {
    "id": "3576",
    "text": "Prachuap Khiri Khan",
    "country_id": "217",
    "value": "Prachuap Khiri Khan"
  },
  {
    "id": "3577",
    "text": "Ranong",
    "country_id": "217",
    "value": "Ranong"
  },
  {
    "id": "3578",
    "text": "Ratchaburi",
    "country_id": "217",
    "value": "Ratchaburi"
  },
  {
    "id": "3579",
    "text": "Rayong",
    "country_id": "217",
    "value": "Rayong"
  },
  {
    "id": "3580",
    "text": "Roi Et",
    "country_id": "217",
    "value": "Roi Et"
  },
  {
    "id": "3581",
    "text": "Sa Kaeo",
    "country_id": "217",
    "value": "Sa Kaeo"
  },
  {
    "id": "3582",
    "text": "Sakon Nakhon",
    "country_id": "217",
    "value": "Sakon Nakhon"
  },
  {
    "id": "3583",
    "text": "Samut Prakan",
    "country_id": "217",
    "value": "Samut Prakan"
  },
  {
    "id": "3584",
    "text": "Samut Sakhon",
    "country_id": "217",
    "value": "Samut Sakhon"
  },
  {
    "id": "3585",
    "text": "Samut Songkhran",
    "country_id": "217",
    "value": "Samut Songkhran"
  },
  {
    "id": "3586",
    "text": "Saraburi",
    "country_id": "217",
    "value": "Saraburi"
  },
  {
    "id": "3587",
    "text": "Satun",
    "country_id": "217",
    "value": "Satun"
  },
  {
    "id": "3588",
    "text": "Si Sa Ket",
    "country_id": "217",
    "value": "Si Sa Ket"
  },
  {
    "id": "3589",
    "text": "Sing Buri",
    "country_id": "217",
    "value": "Sing Buri"
  },
  {
    "id": "3590",
    "text": "Songkhla",
    "country_id": "217",
    "value": "Songkhla"
  },
  {
    "id": "3591",
    "text": "Sukhothai",
    "country_id": "217",
    "value": "Sukhothai"
  },
  {
    "id": "3592",
    "text": "Suphan Buri",
    "country_id": "217",
    "value": "Suphan Buri"
  },
  {
    "id": "3593",
    "text": "Surat Thani",
    "country_id": "217",
    "value": "Surat Thani"
  },
  {
    "id": "3594",
    "text": "Surin",
    "country_id": "217",
    "value": "Surin"
  },
  {
    "id": "3595",
    "text": "Tak",
    "country_id": "217",
    "value": "Tak"
  },
  {
    "id": "3596",
    "text": "Trang",
    "country_id": "217",
    "value": "Trang"
  },
  {
    "id": "3597",
    "text": "Trat",
    "country_id": "217",
    "value": "Trat"
  },
  {
    "id": "3598",
    "text": "Ubon Ratchathani",
    "country_id": "217",
    "value": "Ubon Ratchathani"
  },
  {
    "id": "3599",
    "text": "Udon Thani",
    "country_id": "217",
    "value": "Udon Thani"
  },
  {
    "id": "3600",
    "text": "Uthai Thani",
    "country_id": "217",
    "value": "Uthai Thani"
  },
  {
    "id": "3601",
    "text": "Uttaradit",
    "country_id": "217",
    "value": "Uttaradit"
  },
  {
    "id": "3602",
    "text": "Yala",
    "country_id": "217",
    "value": "Yala"
  },
  {
    "id": "3603",
    "text": "Yasothon",
    "country_id": "217",
    "value": "Yasothon"
  },
  {
    "id": "3604",
    "text": "Centre",
    "country_id": "218",
    "value": "Centre"
  },
  {
    "id": "3605",
    "text": "Kara",
    "country_id": "218",
    "value": "Kara"
  },
  {
    "id": "3606",
    "text": "Maritime",
    "country_id": "218",
    "value": "Maritime"
  },
  {
    "id": "3607",
    "text": "Plateaux",
    "country_id": "218",
    "value": "Plateaux"
  },
  {
    "id": "3608",
    "text": "Savanes",
    "country_id": "218",
    "value": "Savanes"
  },
  {
    "id": "3609",
    "text": "Atafu",
    "country_id": "219",
    "value": "Atafu"
  },
  {
    "id": "3610",
    "text": "Fakaofo",
    "country_id": "219",
    "value": "Fakaofo"
  },
  {
    "id": "3611",
    "text": "Nukunonu",
    "country_id": "219",
    "value": "Nukunonu"
  },
  {
    "id": "3612",
    "text": "Eua",
    "country_id": "220",
    "value": "Eua"
  },
  {
    "id": "3613",
    "text": "Ha''apai",
    "country_id": "220",
    "value": "Ha''apai"
  },
  {
    "id": "3614",
    "text": "Niuas",
    "country_id": "220",
    "value": "Niuas"
  },
  {
    "id": "3615",
    "text": "Tongatapu",
    "country_id": "220",
    "value": "Tongatapu"
  },
  {
    "id": "3616",
    "text": "Vava''u",
    "country_id": "220",
    "value": "Vava''u"
  },
  {
    "id": "3617",
    "text": "Arima-Tunapuna-Piarco",
    "country_id": "221",
    "value": "Arima-Tunapuna-Piarco"
  },
  {
    "id": "3618",
    "text": "Caroni",
    "country_id": "221",
    "value": "Caroni"
  },
  {
    "id": "3619",
    "text": "Chaguanas",
    "country_id": "221",
    "value": "Chaguanas"
  },
  {
    "id": "3620",
    "text": "Couva-Tabaquite-Talparo",
    "country_id": "221",
    "value": "Couva-Tabaquite-Talparo"
  },
  {
    "id": "3621",
    "text": "Diego Martin",
    "country_id": "221",
    "value": "Diego Martin"
  },
  {
    "id": "3622",
    "text": "Glencoe",
    "country_id": "221",
    "value": "Glencoe"
  },
  {
    "id": "3623",
    "text": "Penal Debe",
    "country_id": "221",
    "value": "Penal Debe"
  },
  {
    "id": "3624",
    "text": "Point Fortin",
    "country_id": "221",
    "value": "Point Fortin"
  },
  {
    "id": "3625",
    "text": "Port of Spain",
    "country_id": "221",
    "value": "Port of Spain"
  },
  {
    "id": "3626",
    "text": "Princes Town",
    "country_id": "221",
    "value": "Princes Town"
  },
  {
    "id": "3627",
    "text": "Saint George",
    "country_id": "221",
    "value": "Saint George"
  },
  {
    "id": "3628",
    "text": "San Fernando",
    "country_id": "221",
    "value": "San Fernando"
  },
  {
    "id": "3629",
    "text": "San Juan",
    "country_id": "221",
    "value": "San Juan"
  },
  {
    "id": "3630",
    "text": "Sangre Grande",
    "country_id": "221",
    "value": "Sangre Grande"
  },
  {
    "id": "3631",
    "text": "Siparia",
    "country_id": "221",
    "value": "Siparia"
  },
  {
    "id": "3632",
    "text": "Tobago",
    "country_id": "221",
    "value": "Tobago"
  },
  {
    "id": "3633",
    "text": "Aryanah",
    "country_id": "222",
    "value": "Aryanah"
  },
  {
    "id": "3634",
    "text": "Bajah",
    "country_id": "222",
    "value": "Bajah"
  },
  {
    "id": "3635",
    "text": "Bin ''Arus",
    "country_id": "222",
    "value": "Bin ''Arus"
  },
  {
    "id": "3636",
    "text": "Binzart",
    "country_id": "222",
    "value": "Binzart"
  },
  {
    "id": "3637",
    "text": "Gouvernorat de Ariana",
    "country_id": "222",
    "value": "Gouvernorat de Ariana"
  },
  {
    "id": "3638",
    "text": "Gouvernorat de Nabeul",
    "country_id": "222",
    "value": "Gouvernorat de Nabeul"
  },
  {
    "id": "3639",
    "text": "Gouvernorat de Sousse",
    "country_id": "222",
    "value": "Gouvernorat de Sousse"
  },
  {
    "id": "3640",
    "text": "Hammamet Yasmine",
    "country_id": "222",
    "value": "Hammamet Yasmine"
  },
  {
    "id": "3641",
    "text": "Jundubah",
    "country_id": "222",
    "value": "Jundubah"
  },
  {
    "id": "3642",
    "text": "Madaniyin",
    "country_id": "222",
    "value": "Madaniyin"
  },
  {
    "id": "3643",
    "text": "Manubah",
    "country_id": "222",
    "value": "Manubah"
  },
  {
    "id": "3644",
    "text": "Monastir",
    "country_id": "222",
    "value": "Monastir"
  },
  {
    "id": "3645",
    "text": "Nabul",
    "country_id": "222",
    "value": "Nabul"
  },
  {
    "id": "3646",
    "text": "Qabis",
    "country_id": "222",
    "value": "Qabis"
  },
  {
    "id": "3647",
    "text": "Qafsah",
    "country_id": "222",
    "value": "Qafsah"
  },
  {
    "id": "3648",
    "text": "Qibili",
    "country_id": "222",
    "value": "Qibili"
  },
  {
    "id": "3649",
    "text": "Safaqis",
    "country_id": "222",
    "value": "Safaqis"
  },
  {
    "id": "3650",
    "text": "Sfax",
    "country_id": "222",
    "value": "Sfax"
  },
  {
    "id": "3651",
    "text": "Sidi Bu Zayd",
    "country_id": "222",
    "value": "Sidi Bu Zayd"
  },
  {
    "id": "3652",
    "text": "Silyanah",
    "country_id": "222",
    "value": "Silyanah"
  },
  {
    "id": "3653",
    "text": "Susah",
    "country_id": "222",
    "value": "Susah"
  },
  {
    "id": "3654",
    "text": "Tatawin",
    "country_id": "222",
    "value": "Tatawin"
  },
  {
    "id": "3655",
    "text": "Tawzar",
    "country_id": "222",
    "value": "Tawzar"
  },
  {
    "id": "3656",
    "text": "Tunis",
    "country_id": "222",
    "value": "Tunis"
  },
  {
    "id": "3657",
    "text": "Zaghwan",
    "country_id": "222",
    "value": "Zaghwan"
  },
  {
    "id": "3658",
    "text": "al-Kaf",
    "country_id": "222",
    "value": "al-Kaf"
  },
  {
    "id": "3659",
    "text": "al-Mahdiyah",
    "country_id": "222",
    "value": "al-Mahdiyah"
  },
  {
    "id": "3660",
    "text": "al-Munastir",
    "country_id": "222",
    "value": "al-Munastir"
  },
  {
    "id": "3661",
    "text": "al-Qasrayn",
    "country_id": "222",
    "value": "al-Qasrayn"
  },
  {
    "id": "3662",
    "text": "al-Qayrawan",
    "country_id": "222",
    "value": "al-Qayrawan"
  },
  {
    "id": "3663",
    "text": "Adana",
    "country_id": "223",
    "value": "Adana"
  },
  {
    "id": "3664",
    "text": "Adiyaman",
    "country_id": "223",
    "value": "Adiyaman"
  },
  {
    "id": "3665",
    "text": "Afyon",
    "country_id": "223",
    "value": "Afyon"
  },
  {
    "id": "3666",
    "text": "Agri",
    "country_id": "223",
    "value": "Agri"
  },
  {
    "id": "3667",
    "text": "Aksaray",
    "country_id": "223",
    "value": "Aksaray"
  },
  {
    "id": "3668",
    "text": "Amasya",
    "country_id": "223",
    "value": "Amasya"
  },
  {
    "id": "3669",
    "text": "Ankara",
    "country_id": "223",
    "value": "Ankara"
  },
  {
    "id": "3670",
    "text": "Antalya",
    "country_id": "223",
    "value": "Antalya"
  },
  {
    "id": "3671",
    "text": "Ardahan",
    "country_id": "223",
    "value": "Ardahan"
  },
  {
    "id": "3672",
    "text": "Artvin",
    "country_id": "223",
    "value": "Artvin"
  },
  {
    "id": "3673",
    "text": "Aydin",
    "country_id": "223",
    "value": "Aydin"
  },
  {
    "id": "3674",
    "text": "Balikesir",
    "country_id": "223",
    "value": "Balikesir"
  },
  {
    "id": "3675",
    "text": "Bartin",
    "country_id": "223",
    "value": "Bartin"
  },
  {
    "id": "3676",
    "text": "Batman",
    "country_id": "223",
    "value": "Batman"
  },
  {
    "id": "3677",
    "text": "Bayburt",
    "country_id": "223",
    "value": "Bayburt"
  },
  {
    "id": "3678",
    "text": "Bilecik",
    "country_id": "223",
    "value": "Bilecik"
  },
  {
    "id": "3679",
    "text": "Bingol",
    "country_id": "223",
    "value": "Bingol"
  },
  {
    "id": "3680",
    "text": "Bitlis",
    "country_id": "223",
    "value": "Bitlis"
  },
  {
    "id": "3681",
    "text": "Bolu",
    "country_id": "223",
    "value": "Bolu"
  },
  {
    "id": "3682",
    "text": "Burdur",
    "country_id": "223",
    "value": "Burdur"
  },
  {
    "id": "3683",
    "text": "Bursa",
    "country_id": "223",
    "value": "Bursa"
  },
  {
    "id": "3684",
    "text": "Canakkale",
    "country_id": "223",
    "value": "Canakkale"
  },
  {
    "id": "3685",
    "text": "Cankiri",
    "country_id": "223",
    "value": "Cankiri"
  },
  {
    "id": "3686",
    "text": "Corum",
    "country_id": "223",
    "value": "Corum"
  },
  {
    "id": "3687",
    "text": "Denizli",
    "country_id": "223",
    "value": "Denizli"
  },
  {
    "id": "3688",
    "text": "Diyarbakir",
    "country_id": "223",
    "value": "Diyarbakir"
  },
  {
    "id": "3689",
    "text": "Duzce",
    "country_id": "223",
    "value": "Duzce"
  },
  {
    "id": "3690",
    "text": "Edirne",
    "country_id": "223",
    "value": "Edirne"
  },
  {
    "id": "3691",
    "text": "Elazig",
    "country_id": "223",
    "value": "Elazig"
  },
  {
    "id": "3692",
    "text": "Erzincan",
    "country_id": "223",
    "value": "Erzincan"
  },
  {
    "id": "3693",
    "text": "Erzurum",
    "country_id": "223",
    "value": "Erzurum"
  },
  {
    "id": "3694",
    "text": "Eskisehir",
    "country_id": "223",
    "value": "Eskisehir"
  },
  {
    "id": "3695",
    "text": "Gaziantep",
    "country_id": "223",
    "value": "Gaziantep"
  },
  {
    "id": "3696",
    "text": "Giresun",
    "country_id": "223",
    "value": "Giresun"
  },
  {
    "id": "3697",
    "text": "Gumushane",
    "country_id": "223",
    "value": "Gumushane"
  },
  {
    "id": "3698",
    "text": "Hakkari",
    "country_id": "223",
    "value": "Hakkari"
  },
  {
    "id": "3699",
    "text": "Hatay",
    "country_id": "223",
    "value": "Hatay"
  },
  {
    "id": "3700",
    "text": "Icel",
    "country_id": "223",
    "value": "Icel"
  },
  {
    "id": "3701",
    "text": "Igdir",
    "country_id": "223",
    "value": "Igdir"
  },
  {
    "id": "3702",
    "text": "Isparta",
    "country_id": "223",
    "value": "Isparta"
  },
  {
    "id": "3703",
    "text": "Istanbul",
    "country_id": "223",
    "value": "Istanbul"
  },
  {
    "id": "3704",
    "text": "Izmir",
    "country_id": "223",
    "value": "Izmir"
  },
  {
    "id": "3705",
    "text": "Kahramanmaras",
    "country_id": "223",
    "value": "Kahramanmaras"
  },
  {
    "id": "3706",
    "text": "Karabuk",
    "country_id": "223",
    "value": "Karabuk"
  },
  {
    "id": "3707",
    "text": "Karaman",
    "country_id": "223",
    "value": "Karaman"
  },
  {
    "id": "3708",
    "text": "Kars",
    "country_id": "223",
    "value": "Kars"
  },
  {
    "id": "3709",
    "text": "Karsiyaka",
    "country_id": "223",
    "value": "Karsiyaka"
  },
  {
    "id": "3710",
    "text": "Kastamonu",
    "country_id": "223",
    "value": "Kastamonu"
  },
  {
    "id": "3711",
    "text": "Kayseri",
    "country_id": "223",
    "value": "Kayseri"
  },
  {
    "id": "3712",
    "text": "Kilis",
    "country_id": "223",
    "value": "Kilis"
  },
  {
    "id": "3713",
    "text": "Kirikkale",
    "country_id": "223",
    "value": "Kirikkale"
  },
  {
    "id": "3714",
    "text": "Kirklareli",
    "country_id": "223",
    "value": "Kirklareli"
  },
  {
    "id": "3715",
    "text": "Kirsehir",
    "country_id": "223",
    "value": "Kirsehir"
  },
  {
    "id": "3716",
    "text": "Kocaeli",
    "country_id": "223",
    "value": "Kocaeli"
  },
  {
    "id": "3717",
    "text": "Konya",
    "country_id": "223",
    "value": "Konya"
  },
  {
    "id": "3718",
    "text": "Kutahya",
    "country_id": "223",
    "value": "Kutahya"
  },
  {
    "id": "3719",
    "text": "Lefkosa",
    "country_id": "223",
    "value": "Lefkosa"
  },
  {
    "id": "3720",
    "text": "Malatya",
    "country_id": "223",
    "value": "Malatya"
  },
  {
    "id": "3721",
    "text": "Manisa",
    "country_id": "223",
    "value": "Manisa"
  },
  {
    "id": "3722",
    "text": "Mardin",
    "country_id": "223",
    "value": "Mardin"
  },
  {
    "id": "3723",
    "text": "Mugla",
    "country_id": "223",
    "value": "Mugla"
  },
  {
    "id": "3724",
    "text": "Mus",
    "country_id": "223",
    "value": "Mus"
  },
  {
    "id": "3725",
    "text": "Nevsehir",
    "country_id": "223",
    "value": "Nevsehir"
  },
  {
    "id": "3726",
    "text": "Nigde",
    "country_id": "223",
    "value": "Nigde"
  },
  {
    "id": "3727",
    "text": "Ordu",
    "country_id": "223",
    "value": "Ordu"
  },
  {
    "id": "3728",
    "text": "Osmaniye",
    "country_id": "223",
    "value": "Osmaniye"
  },
  {
    "id": "3729",
    "text": "Rize",
    "country_id": "223",
    "value": "Rize"
  },
  {
    "id": "3730",
    "text": "Sakarya",
    "country_id": "223",
    "value": "Sakarya"
  },
  {
    "id": "3731",
    "text": "Samsun",
    "country_id": "223",
    "value": "Samsun"
  },
  {
    "id": "3732",
    "text": "Sanliurfa",
    "country_id": "223",
    "value": "Sanliurfa"
  },
  {
    "id": "3733",
    "text": "Siirt",
    "country_id": "223",
    "value": "Siirt"
  },
  {
    "id": "3734",
    "text": "Sinop",
    "country_id": "223",
    "value": "Sinop"
  },
  {
    "id": "3735",
    "text": "Sirnak",
    "country_id": "223",
    "value": "Sirnak"
  },
  {
    "id": "3736",
    "text": "Sivas",
    "country_id": "223",
    "value": "Sivas"
  },
  {
    "id": "3737",
    "text": "Tekirdag",
    "country_id": "223",
    "value": "Tekirdag"
  },
  {
    "id": "3738",
    "text": "Tokat",
    "country_id": "223",
    "value": "Tokat"
  },
  {
    "id": "3739",
    "text": "Trabzon",
    "country_id": "223",
    "value": "Trabzon"
  },
  {
    "id": "3740",
    "text": "Tunceli",
    "country_id": "223",
    "value": "Tunceli"
  },
  {
    "id": "3741",
    "text": "Usak",
    "country_id": "223",
    "value": "Usak"
  },
  {
    "id": "3742",
    "text": "Van",
    "country_id": "223",
    "value": "Van"
  },
  {
    "id": "3743",
    "text": "Yalova",
    "country_id": "223",
    "value": "Yalova"
  },
  {
    "id": "3744",
    "text": "Yozgat",
    "country_id": "223",
    "value": "Yozgat"
  },
  {
    "id": "3745",
    "text": "Zonguldak",
    "country_id": "223",
    "value": "Zonguldak"
  },
  {
    "id": "3746",
    "text": "Ahal",
    "country_id": "224",
    "value": "Ahal"
  },
  {
    "id": "3747",
    "text": "Asgabat",
    "country_id": "224",
    "value": "Asgabat"
  },
  {
    "id": "3748",
    "text": "Balkan",
    "country_id": "224",
    "value": "Balkan"
  },
  {
    "id": "3749",
    "text": "Dasoguz",
    "country_id": "224",
    "value": "Dasoguz"
  },
  {
    "id": "3750",
    "text": "Lebap",
    "country_id": "224",
    "value": "Lebap"
  },
  {
    "id": "3751",
    "text": "Mari",
    "country_id": "224",
    "value": "Mari"
  },
  {
    "id": "3752",
    "text": "Grand Turk",
    "country_id": "225",
    "value": "Grand Turk"
  },
  {
    "id": "3753",
    "text": "South Caicos and East Caicos",
    "country_id": "225",
    "value": "South Caicos and East Caicos"
  },
  {
    "id": "3754",
    "text": "Funafuti",
    "country_id": "226",
    "value": "Funafuti"
  },
  {
    "id": "3755",
    "text": "Nanumanga",
    "country_id": "226",
    "value": "Nanumanga"
  },
  {
    "id": "3756",
    "text": "Nanumea",
    "country_id": "226",
    "value": "Nanumea"
  },
  {
    "id": "3757",
    "text": "Niutao",
    "country_id": "226",
    "value": "Niutao"
  },
  {
    "id": "3758",
    "text": "Nui",
    "country_id": "226",
    "value": "Nui"
  },
  {
    "id": "3759",
    "text": "Nukufetau",
    "country_id": "226",
    "value": "Nukufetau"
  },
  {
    "id": "3760",
    "text": "Nukulaelae",
    "country_id": "226",
    "value": "Nukulaelae"
  },
  {
    "id": "3761",
    "text": "Vaitupu",
    "country_id": "226",
    "value": "Vaitupu"
  },
  {
    "id": "3762",
    "text": "Central",
    "country_id": "227",
    "value": "Central"
  },
  {
    "id": "3763",
    "text": "Eastern",
    "country_id": "227",
    "value": "Eastern"
  },
  {
    "id": "3764",
    "text": "Northern",
    "country_id": "227",
    "value": "Northern"
  },
  {
    "id": "3765",
    "text": "Western",
    "country_id": "227",
    "value": "Western"
  },
  {
    "id": "3766",
    "text": "Cherkas''ka",
    "country_id": "228",
    "value": "Cherkas''ka"
  },
  {
    "id": "3767",
    "text": "Chernihivs''ka",
    "country_id": "228",
    "value": "Chernihivs''ka"
  },
  {
    "id": "3768",
    "text": "Chernivets''ka",
    "country_id": "228",
    "value": "Chernivets''ka"
  },
  {
    "id": "3769",
    "text": "Crimea",
    "country_id": "228",
    "value": "Crimea"
  },
  {
    "id": "3770",
    "text": "Dnipropetrovska",
    "country_id": "228",
    "value": "Dnipropetrovska"
  },
  {
    "id": "3771",
    "text": "Donets''ka",
    "country_id": "228",
    "value": "Donets''ka"
  },
  {
    "id": "3772",
    "text": "Ivano-Frankivs''ka",
    "country_id": "228",
    "value": "Ivano-Frankivs''ka"
  },
  {
    "id": "3773",
    "text": "Kharkiv",
    "country_id": "228",
    "value": "Kharkiv"
  },
  {
    "id": "3774",
    "text": "Kharkov",
    "country_id": "228",
    "value": "Kharkov"
  },
  {
    "id": "3775",
    "text": "Khersonska",
    "country_id": "228",
    "value": "Khersonska"
  },
  {
    "id": "3776",
    "text": "Khmel''nyts''ka",
    "country_id": "228",
    "value": "Khmel''nyts''ka"
  },
  {
    "id": "3777",
    "text": "Kirovohrad",
    "country_id": "228",
    "value": "Kirovohrad"
  },
  {
    "id": "3778",
    "text": "Krym",
    "country_id": "228",
    "value": "Krym"
  },
  {
    "id": "3779",
    "text": "Kyyiv",
    "country_id": "228",
    "value": "Kyyiv"
  },
  {
    "id": "3780",
    "text": "Kyyivs''ka",
    "country_id": "228",
    "value": "Kyyivs''ka"
  },
  {
    "id": "3781",
    "text": "L''vivs''ka",
    "country_id": "228",
    "value": "L''vivs''ka"
  },
  {
    "id": "3782",
    "text": "Luhans''ka",
    "country_id": "228",
    "value": "Luhans''ka"
  },
  {
    "id": "3783",
    "text": "Mykolayivs''ka",
    "country_id": "228",
    "value": "Mykolayivs''ka"
  },
  {
    "id": "3784",
    "text": "Odes''ka",
    "country_id": "228",
    "value": "Odes''ka"
  },
  {
    "id": "3785",
    "text": "Odessa",
    "country_id": "228",
    "value": "Odessa"
  },
  {
    "id": "3786",
    "text": "Poltavs''ka",
    "country_id": "228",
    "value": "Poltavs''ka"
  },
  {
    "id": "3787",
    "text": "Rivnens''ka",
    "country_id": "228",
    "value": "Rivnens''ka"
  },
  {
    "id": "3788",
    "text": "Sevastopol",
    "country_id": "228",
    "value": "Sevastopol"
  },
  {
    "id": "3789",
    "text": "Sums''ka",
    "country_id": "228",
    "value": "Sums''ka"
  },
  {
    "id": "3790",
    "text": "Ternopil''s''ka",
    "country_id": "228",
    "value": "Ternopil''s''ka"
  },
  {
    "id": "3791",
    "text": "Volyns''ka",
    "country_id": "228",
    "value": "Volyns''ka"
  },
  {
    "id": "3792",
    "text": "Vynnyts''ka",
    "country_id": "228",
    "value": "Vynnyts''ka"
  },
  {
    "id": "3793",
    "text": "Zakarpats''ka",
    "country_id": "228",
    "value": "Zakarpats''ka"
  },
  {
    "id": "3794",
    "text": "Zaporizhia",
    "country_id": "228",
    "value": "Zaporizhia"
  },
  {
    "id": "3795",
    "text": "Zhytomyrs''ka",
    "country_id": "228",
    "value": "Zhytomyrs''ka"
  },
  {
    "id": "3796",
    "text": "Abu Zabi",
    "country_id": "229",
    "value": "Abu Zabi"
  },
  {
    "id": "3797",
    "text": "Ajman",
    "country_id": "229",
    "value": "Ajman"
  },
  {
    "id": "3798",
    "text": "Dubai",
    "country_id": "229",
    "value": "Dubai"
  },
  {
    "id": "3799",
    "text": "Ras al-Khaymah",
    "country_id": "229",
    "value": "Ras al-Khaymah"
  },
  {
    "id": "3800",
    "text": "Sharjah",
    "country_id": "229",
    "value": "Sharjah"
  },
  {
    "id": "3801",
    "text": "Sharjha",
    "country_id": "229",
    "value": "Sharjha"
  },
  {
    "id": "3802",
    "text": "Umm al Qaywayn",
    "country_id": "229",
    "value": "Umm al Qaywayn"
  },
  {
    "id": "3803",
    "text": "al-Fujayrah",
    "country_id": "229",
    "value": "al-Fujayrah"
  },
  {
    "id": "3804",
    "text": "ash-Shariqah",
    "country_id": "229",
    "value": "ash-Shariqah"
  },
  {
    "id": "3805",
    "text": "Aberdeen",
    "country_id": "230",
    "value": "Aberdeen"
  },
  {
    "id": "3806",
    "text": "Aberdeenshire",
    "country_id": "230",
    "value": "Aberdeenshire"
  },
  {
    "id": "3807",
    "text": "Argyll",
    "country_id": "230",
    "value": "Argyll"
  },
  {
    "id": "3808",
    "text": "Armagh",
    "country_id": "230",
    "value": "Armagh"
  },
  {
    "id": "3809",
    "text": "Bedfordshire",
    "country_id": "230",
    "value": "Bedfordshire"
  },
  {
    "id": "3810",
    "text": "Belfast",
    "country_id": "230",
    "value": "Belfast"
  },
  {
    "id": "3811",
    "text": "Berkshire",
    "country_id": "230",
    "value": "Berkshire"
  },
  {
    "id": "3812",
    "text": "Birmingham",
    "country_id": "230",
    "value": "Birmingham"
  },
  {
    "id": "3813",
    "text": "Brechin",
    "country_id": "230",
    "value": "Brechin"
  },
  {
    "id": "3814",
    "text": "Bridgnorth",
    "country_id": "230",
    "value": "Bridgnorth"
  },
  {
    "id": "3815",
    "text": "Bristol",
    "country_id": "230",
    "value": "Bristol"
  },
  {
    "id": "3816",
    "text": "Buckinghamshire",
    "country_id": "230",
    "value": "Buckinghamshire"
  },
  {
    "id": "3817",
    "text": "Cambridge",
    "country_id": "230",
    "value": "Cambridge"
  },
  {
    "id": "3818",
    "text": "Cambridgeshire",
    "country_id": "230",
    "value": "Cambridgeshire"
  },
  {
    "id": "3819",
    "text": "Channel Islands",
    "country_id": "230",
    "value": "Channel Islands"
  },
  {
    "id": "3820",
    "text": "Cheshire",
    "country_id": "230",
    "value": "Cheshire"
  },
  {
    "id": "3821",
    "text": "Cleveland",
    "country_id": "230",
    "value": "Cleveland"
  },
  {
    "id": "3822",
    "text": "Co Fermanagh",
    "country_id": "230",
    "value": "Co Fermanagh"
  },
  {
    "id": "3823",
    "text": "Conwy",
    "country_id": "230",
    "value": "Conwy"
  },
  {
    "id": "3824",
    "text": "Cornwall",
    "country_id": "230",
    "value": "Cornwall"
  },
  {
    "id": "3825",
    "text": "Coventry",
    "country_id": "230",
    "value": "Coventry"
  },
  {
    "id": "3826",
    "text": "Craven Arms",
    "country_id": "230",
    "value": "Craven Arms"
  },
  {
    "id": "3827",
    "text": "Cumbria",
    "country_id": "230",
    "value": "Cumbria"
  },
  {
    "id": "3828",
    "text": "Denbighshire",
    "country_id": "230",
    "value": "Denbighshire"
  },
  {
    "id": "3829",
    "text": "Derby",
    "country_id": "230",
    "value": "Derby"
  },
  {
    "id": "3830",
    "text": "Derbyshire",
    "country_id": "230",
    "value": "Derbyshire"
  },
  {
    "id": "3831",
    "text": "Devon",
    "country_id": "230",
    "value": "Devon"
  },
  {
    "id": "3832",
    "text": "Dial Code Dungannon",
    "country_id": "230",
    "value": "Dial Code Dungannon"
  },
  {
    "id": "3833",
    "text": "Didcot",
    "country_id": "230",
    "value": "Didcot"
  },
  {
    "id": "3834",
    "text": "Dorset",
    "country_id": "230",
    "value": "Dorset"
  },
  {
    "id": "3835",
    "text": "Dunbartonshire",
    "country_id": "230",
    "value": "Dunbartonshire"
  },
  {
    "id": "3836",
    "text": "Durham",
    "country_id": "230",
    "value": "Durham"
  },
  {
    "id": "3837",
    "text": "East Dunbartonshire",
    "country_id": "230",
    "value": "East Dunbartonshire"
  },
  {
    "id": "3838",
    "text": "East Lothian",
    "country_id": "230",
    "value": "East Lothian"
  },
  {
    "id": "3839",
    "text": "East Midlands",
    "country_id": "230",
    "value": "East Midlands"
  },
  {
    "id": "3840",
    "text": "East Sussex",
    "country_id": "230",
    "value": "East Sussex"
  },
  {
    "id": "3841",
    "text": "East Yorkshire",
    "country_id": "230",
    "value": "East Yorkshire"
  },
  {
    "id": "3842",
    "text": "England",
    "country_id": "230",
    "value": "England"
  },
  {
    "id": "3843",
    "text": "Essex",
    "country_id": "230",
    "value": "Essex"
  },
  {
    "id": "3844",
    "text": "Fermanagh",
    "country_id": "230",
    "value": "Fermanagh"
  },
  {
    "id": "3845",
    "text": "Fife",
    "country_id": "230",
    "value": "Fife"
  },
  {
    "id": "3846",
    "text": "Flintshire",
    "country_id": "230",
    "value": "Flintshire"
  },
  {
    "id": "3847",
    "text": "Fulham",
    "country_id": "230",
    "value": "Fulham"
  },
  {
    "id": "3848",
    "text": "Gainsborough",
    "country_id": "230",
    "value": "Gainsborough"
  },
  {
    "id": "3849",
    "text": "Glocestershire",
    "country_id": "230",
    "value": "Glocestershire"
  },
  {
    "id": "3850",
    "text": "Gwent",
    "country_id": "230",
    "value": "Gwent"
  },
  {
    "id": "3851",
    "text": "Hampshire",
    "country_id": "230",
    "value": "Hampshire"
  },
  {
    "id": "3852",
    "text": "Hants",
    "country_id": "230",
    "value": "Hants"
  },
  {
    "id": "3853",
    "text": "Herefordshire",
    "country_id": "230",
    "value": "Herefordshire"
  },
  {
    "id": "3854",
    "text": "Hertfordshire",
    "country_id": "230",
    "value": "Hertfordshire"
  },
  {
    "id": "3855",
    "text": "Ireland",
    "country_id": "230",
    "value": "Ireland"
  },
  {
    "id": "3856",
    "text": "Isle Of Man",
    "country_id": "230",
    "value": "Isle Of Man"
  },
  {
    "id": "3857",
    "text": "Isle of Wight",
    "country_id": "230",
    "value": "Isle of Wight"
  },
  {
    "id": "3858",
    "text": "Kenford",
    "country_id": "230",
    "value": "Kenford"
  },
  {
    "id": "3859",
    "text": "Kent",
    "country_id": "230",
    "value": "Kent"
  },
  {
    "id": "3860",
    "text": "Kilmarnock",
    "country_id": "230",
    "value": "Kilmarnock"
  },
  {
    "id": "3861",
    "text": "Lanarkshire",
    "country_id": "230",
    "value": "Lanarkshire"
  },
  {
    "id": "3862",
    "text": "Lancashire",
    "country_id": "230",
    "value": "Lancashire"
  },
  {
    "id": "3863",
    "text": "Leicestershire",
    "country_id": "230",
    "value": "Leicestershire"
  },
  {
    "id": "3864",
    "text": "Lincolnshire",
    "country_id": "230",
    "value": "Lincolnshire"
  },
  {
    "id": "3865",
    "text": "Llanymynech",
    "country_id": "230",
    "value": "Llanymynech"
  },
  {
    "id": "3866",
    "text": "London",
    "country_id": "230",
    "value": "London"
  },
  {
    "id": "3867",
    "text": "Ludlow",
    "country_id": "230",
    "value": "Ludlow"
  },
  {
    "id": "3868",
    "text": "Manchester",
    "country_id": "230",
    "value": "Manchester"
  },
  {
    "id": "3869",
    "text": "Mayfair",
    "country_id": "230",
    "value": "Mayfair"
  },
  {
    "id": "3870",
    "text": "Merseyside",
    "country_id": "230",
    "value": "Merseyside"
  },
  {
    "id": "3871",
    "text": "Mid Glamorgan",
    "country_id": "230",
    "value": "Mid Glamorgan"
  },
  {
    "id": "3872",
    "text": "Middlesex",
    "country_id": "230",
    "value": "Middlesex"
  },
  {
    "id": "3873",
    "text": "Mildenhall",
    "country_id": "230",
    "value": "Mildenhall"
  },
  {
    "id": "3874",
    "text": "Monmouthshire",
    "country_id": "230",
    "value": "Monmouthshire"
  },
  {
    "id": "3875",
    "text": "Newton Stewart",
    "country_id": "230",
    "value": "Newton Stewart"
  },
  {
    "id": "3876",
    "text": "Norfolk",
    "country_id": "230",
    "value": "Norfolk"
  },
  {
    "id": "3877",
    "text": "North Humberside",
    "country_id": "230",
    "value": "North Humberside"
  },
  {
    "id": "3878",
    "text": "North Yorkshire",
    "country_id": "230",
    "value": "North Yorkshire"
  },
  {
    "id": "3879",
    "text": "Northamptonshire",
    "country_id": "230",
    "value": "Northamptonshire"
  },
  {
    "id": "3880",
    "text": "Northants",
    "country_id": "230",
    "value": "Northants"
  },
  {
    "id": "3881",
    "text": "Northern Ireland",
    "country_id": "230",
    "value": "Northern Ireland"
  },
  {
    "id": "3882",
    "text": "Northumberland",
    "country_id": "230",
    "value": "Northumberland"
  },
  {
    "id": "3883",
    "text": "Nottinghamshire",
    "country_id": "230",
    "value": "Nottinghamshire"
  },
  {
    "id": "3884",
    "text": "Oxford",
    "country_id": "230",
    "value": "Oxford"
  },
  {
    "id": "3885",
    "text": "Powys",
    "country_id": "230",
    "value": "Powys"
  },
  {
    "id": "3886",
    "text": "Roos-shire",
    "country_id": "230",
    "value": "Roos-shire"
  },
  {
    "id": "3887",
    "text": "SUSSEX",
    "country_id": "230",
    "value": "SUSSEX"
  },
  {
    "id": "3888",
    "text": "Sark",
    "country_id": "230",
    "value": "Sark"
  },
  {
    "id": "3889",
    "text": "Scotland",
    "country_id": "230",
    "value": "Scotland"
  },
  {
    "id": "3890",
    "text": "Scottish Borders",
    "country_id": "230",
    "value": "Scottish Borders"
  },
  {
    "id": "3891",
    "text": "Shropshire",
    "country_id": "230",
    "value": "Shropshire"
  },
  {
    "id": "3892",
    "text": "Somerset",
    "country_id": "230",
    "value": "Somerset"
  },
  {
    "id": "3893",
    "text": "South Glamorgan",
    "country_id": "230",
    "value": "South Glamorgan"
  },
  {
    "id": "3894",
    "text": "South Wales",
    "country_id": "230",
    "value": "South Wales"
  },
  {
    "id": "3895",
    "text": "South Yorkshire",
    "country_id": "230",
    "value": "South Yorkshire"
  },
  {
    "id": "3896",
    "text": "Southwell",
    "country_id": "230",
    "value": "Southwell"
  },
  {
    "id": "3897",
    "text": "Staffordshire",
    "country_id": "230",
    "value": "Staffordshire"
  },
  {
    "id": "3898",
    "text": "Strabane",
    "country_id": "230",
    "value": "Strabane"
  },
  {
    "id": "3899",
    "text": "Suffolk",
    "country_id": "230",
    "value": "Suffolk"
  },
  {
    "id": "3900",
    "text": "Surrey",
    "country_id": "230",
    "value": "Surrey"
  },
  {
    "id": "3901",
    "text": "Sussex",
    "country_id": "230",
    "value": "Sussex"
  },
  {
    "id": "3902",
    "text": "Twickenham",
    "country_id": "230",
    "value": "Twickenham"
  },
  {
    "id": "3903",
    "text": "Tyne and Wear",
    "country_id": "230",
    "value": "Tyne and Wear"
  },
  {
    "id": "3904",
    "text": "Tyrone",
    "country_id": "230",
    "value": "Tyrone"
  },
  {
    "id": "3905",
    "text": "Utah",
    "country_id": "230",
    "value": "Utah"
  },
  {
    "id": "3906",
    "text": "Wales",
    "country_id": "230",
    "value": "Wales"
  },
  {
    "id": "3907",
    "text": "Warwickshire",
    "country_id": "230",
    "value": "Warwickshire"
  },
  {
    "id": "3908",
    "text": "West Lothian",
    "country_id": "230",
    "value": "West Lothian"
  },
  {
    "id": "3909",
    "text": "West Midlands",
    "country_id": "230",
    "value": "West Midlands"
  },
  {
    "id": "3910",
    "text": "West Sussex",
    "country_id": "230",
    "value": "West Sussex"
  },
  {
    "id": "3911",
    "text": "West Yorkshire",
    "country_id": "230",
    "value": "West Yorkshire"
  },
  {
    "id": "3912",
    "text": "Whissendine",
    "country_id": "230",
    "value": "Whissendine"
  },
  {
    "id": "3913",
    "text": "Wiltshire",
    "country_id": "230",
    "value": "Wiltshire"
  },
  {
    "id": "3914",
    "text": "Wokingham",
    "country_id": "230",
    "value": "Wokingham"
  },
  {
    "id": "3915",
    "text": "Worcestershire",
    "country_id": "230",
    "value": "Worcestershire"
  },
  {
    "id": "3916",
    "text": "Wrexham",
    "country_id": "230",
    "value": "Wrexham"
  },
  {
    "id": "3917",
    "text": "Wurttemberg",
    "country_id": "230",
    "value": "Wurttemberg"
  },
  {
    "id": "3918",
    "text": "Yorkshire",
    "country_id": "230",
    "value": "Yorkshire"
  },
  {
    "id": "3919",
    "text": "Alabama",
    "country_id": "231",
    "value": "Alabama"
  },
  {
    "id": "3920",
    "text": "Alaska",
    "country_id": "231",
    "value": "Alaska"
  },
  {
    "id": "3921",
    "text": "Arizona",
    "country_id": "231",
    "value": "Arizona"
  },
  {
    "id": "3922",
    "text": "Arkansas",
    "country_id": "231",
    "value": "Arkansas"
  },
  {
    "id": "3923",
    "text": "Byram",
    "country_id": "231",
    "value": "Byram"
  },
  {
    "id": "3924",
    "text": "California",
    "country_id": "231",
    "value": "California"
  },
  {
    "id": "3925",
    "text": "Cokato",
    "country_id": "231",
    "value": "Cokato"
  },
  {
    "id": "3926",
    "text": "Colorado",
    "country_id": "231",
    "value": "Colorado"
  },
  {
    "id": "3927",
    "text": "Connecticut",
    "country_id": "231",
    "value": "Connecticut"
  },
  {
    "id": "3928",
    "text": "Delaware",
    "country_id": "231",
    "value": "Delaware"
  },
  {
    "id": "3929",
    "text": "District of Columbia",
    "country_id": "231",
    "value": "District of Columbia"
  },
  {
    "id": "3930",
    "text": "Florida",
    "country_id": "231",
    "value": "Florida"
  },
  {
    "id": "3931",
    "text": "Georgia",
    "country_id": "231",
    "value": "Georgia"
  },
  {
    "id": "3932",
    "text": "Hawaii",
    "country_id": "231",
    "value": "Hawaii"
  },
  {
    "id": "3933",
    "text": "Idaho",
    "country_id": "231",
    "value": "Idaho"
  },
  {
    "id": "3934",
    "text": "Illinois",
    "country_id": "231",
    "value": "Illinois"
  },
  {
    "id": "3935",
    "text": "Indiana",
    "country_id": "231",
    "value": "Indiana"
  },
  {
    "id": "3936",
    "text": "Iowa",
    "country_id": "231",
    "value": "Iowa"
  },
  {
    "id": "3937",
    "text": "Kansas",
    "country_id": "231",
    "value": "Kansas"
  },
  {
    "id": "3938",
    "text": "Kentucky",
    "country_id": "231",
    "value": "Kentucky"
  },
  {
    "id": "3939",
    "text": "Louisiana",
    "country_id": "231",
    "value": "Louisiana"
  },
  {
    "id": "3940",
    "text": "Lowa",
    "country_id": "231",
    "value": "Lowa"
  },
  {
    "id": "3941",
    "text": "Maine",
    "country_id": "231",
    "value": "Maine"
  },
  {
    "id": "3942",
    "text": "Maryland",
    "country_id": "231",
    "value": "Maryland"
  },
  {
    "id": "3943",
    "text": "Massachusetts",
    "country_id": "231",
    "value": "Massachusetts"
  },
  {
    "id": "3944",
    "text": "Medfield",
    "country_id": "231",
    "value": "Medfield"
  },
  {
    "id": "3945",
    "text": "Michigan",
    "country_id": "231",
    "value": "Michigan"
  },
  {
    "id": "3946",
    "text": "Minnesota",
    "country_id": "231",
    "value": "Minnesota"
  },
  {
    "id": "3947",
    "text": "Mississippi",
    "country_id": "231",
    "value": "Mississippi"
  },
  {
    "id": "3948",
    "text": "Missouri",
    "country_id": "231",
    "value": "Missouri"
  },
  {
    "id": "3949",
    "text": "Montana",
    "country_id": "231",
    "value": "Montana"
  },
  {
    "id": "3950",
    "text": "Nebraska",
    "country_id": "231",
    "value": "Nebraska"
  },
  {
    "id": "3951",
    "text": "Nevada",
    "country_id": "231",
    "value": "Nevada"
  },
  {
    "id": "3952",
    "text": "New Hampshire",
    "country_id": "231",
    "value": "New Hampshire"
  },
  {
    "id": "3953",
    "text": "New Jersey",
    "country_id": "231",
    "value": "New Jersey"
  },
  {
    "id": "3954",
    "text": "New Jersy",
    "country_id": "231",
    "value": "New Jersy"
  },
  {
    "id": "3955",
    "text": "New Mexico",
    "country_id": "231",
    "value": "New Mexico"
  },
  {
    "id": "3956",
    "text": "New York",
    "country_id": "231",
    "value": "New York"
  },
  {
    "id": "3957",
    "text": "North Carolina",
    "country_id": "231",
    "value": "North Carolina"
  },
  {
    "id": "3958",
    "text": "North Dakota",
    "country_id": "231",
    "value": "North Dakota"
  },
  {
    "id": "3959",
    "text": "Ohio",
    "country_id": "231",
    "value": "Ohio"
  },
  {
    "id": "3960",
    "text": "Oklahoma",
    "country_id": "231",
    "value": "Oklahoma"
  },
  {
    "id": "3961",
    "text": "Ontario",
    "country_id": "231",
    "value": "Ontario"
  },
  {
    "id": "3962",
    "text": "Oregon",
    "country_id": "231",
    "value": "Oregon"
  },
  {
    "id": "3963",
    "text": "Pennsylvania",
    "country_id": "231",
    "value": "Pennsylvania"
  },
  {
    "id": "3964",
    "text": "Ramey",
    "country_id": "231",
    "value": "Ramey"
  },
  {
    "id": "3965",
    "text": "Rhode Island",
    "country_id": "231",
    "value": "Rhode Island"
  },
  {
    "id": "3966",
    "text": "South Carolina",
    "country_id": "231",
    "value": "South Carolina"
  },
  {
    "id": "3967",
    "text": "South Dakota",
    "country_id": "231",
    "value": "South Dakota"
  },
  {
    "id": "3968",
    "text": "Sublimity",
    "country_id": "231",
    "value": "Sublimity"
  },
  {
    "id": "3969",
    "text": "Tennessee",
    "country_id": "231",
    "value": "Tennessee"
  },
  {
    "id": "3970",
    "text": "Texas",
    "country_id": "231",
    "value": "Texas"
  },
  {
    "id": "3971",
    "text": "Trimble",
    "country_id": "231",
    "value": "Trimble"
  },
  {
    "id": "3972",
    "text": "Utah",
    "country_id": "231",
    "value": "Utah"
  },
  {
    "id": "3973",
    "text": "Vermont",
    "country_id": "231",
    "value": "Vermont"
  },
  {
    "id": "3974",
    "text": "Virginia",
    "country_id": "231",
    "value": "Virginia"
  },
  {
    "id": "3975",
    "text": "Washington",
    "country_id": "231",
    "value": "Washington"
  },
  {
    "id": "3976",
    "text": "West Virginia",
    "country_id": "231",
    "value": "West Virginia"
  },
  {
    "id": "3977",
    "text": "Wisconsin",
    "country_id": "231",
    "value": "Wisconsin"
  },
  {
    "id": "3978",
    "text": "Wyoming",
    "country_id": "231",
    "value": "Wyoming"
  },
  {
    "id": "3979",
    "text": "United States Minor Outlying I",
    "country_id": "232",
    "value": "United States Minor Outlying I"
  },
  {
    "id": "3980",
    "text": "Artigas",
    "country_id": "233",
    "value": "Artigas"
  },
  {
    "id": "3981",
    "text": "Canelones",
    "country_id": "233",
    "value": "Canelones"
  },
  {
    "id": "3982",
    "text": "Cerro Largo",
    "country_id": "233",
    "value": "Cerro Largo"
  },
  {
    "id": "3983",
    "text": "Colonia",
    "country_id": "233",
    "value": "Colonia"
  },
  {
    "id": "3984",
    "text": "Durazno",
    "country_id": "233",
    "value": "Durazno"
  },
  {
    "id": "3985",
    "text": "FLorida",
    "country_id": "233",
    "value": "FLorida"
  },
  {
    "id": "3986",
    "text": "Flores",
    "country_id": "233",
    "value": "Flores"
  },
  {
    "id": "3987",
    "text": "Lavalleja",
    "country_id": "233",
    "value": "Lavalleja"
  },
  {
    "id": "3988",
    "text": "Maldonado",
    "country_id": "233",
    "value": "Maldonado"
  },
  {
    "id": "3989",
    "text": "Montevideo",
    "country_id": "233",
    "value": "Montevideo"
  },
  {
    "id": "3990",
    "text": "Paysandu",
    "country_id": "233",
    "value": "Paysandu"
  },
  {
    "id": "3991",
    "text": "Rio Negro",
    "country_id": "233",
    "value": "Rio Negro"
  },
  {
    "id": "3992",
    "text": "Rivera",
    "country_id": "233",
    "value": "Rivera"
  },
  {
    "id": "3993",
    "text": "Rocha",
    "country_id": "233",
    "value": "Rocha"
  },
  {
    "id": "3994",
    "text": "Salto",
    "country_id": "233",
    "value": "Salto"
  },
  {
    "id": "3995",
    "text": "San Jose",
    "country_id": "233",
    "value": "San Jose"
  },
  {
    "id": "3996",
    "text": "Soriano",
    "country_id": "233",
    "value": "Soriano"
  },
  {
    "id": "3997",
    "text": "Tacuarembo",
    "country_id": "233",
    "value": "Tacuarembo"
  },
  {
    "id": "3998",
    "text": "Treinta y Tres",
    "country_id": "233",
    "value": "Treinta y Tres"
  },
  {
    "id": "3999",
    "text": "Andijon",
    "country_id": "234",
    "value": "Andijon"
  },
  {
    "id": "4000",
    "text": "Buhoro",
    "country_id": "234",
    "value": "Buhoro"
  },
  {
    "id": "4001",
    "text": "Buxoro Viloyati",
    "country_id": "234",
    "value": "Buxoro Viloyati"
  },
  {
    "id": "4002",
    "text": "Cizah",
    "country_id": "234",
    "value": "Cizah"
  },
  {
    "id": "4003",
    "text": "Fargona",
    "country_id": "234",
    "value": "Fargona"
  },
  {
    "id": "4004",
    "text": "Horazm",
    "country_id": "234",
    "value": "Horazm"
  },
  {
    "id": "4005",
    "text": "Kaskadar",
    "country_id": "234",
    "value": "Kaskadar"
  },
  {
    "id": "4006",
    "text": "Korakalpogiston",
    "country_id": "234",
    "value": "Korakalpogiston"
  },
  {
    "id": "4007",
    "text": "Namangan",
    "country_id": "234",
    "value": "Namangan"
  },
  {
    "id": "4008",
    "text": "Navoi",
    "country_id": "234",
    "value": "Navoi"
  },
  {
    "id": "4009",
    "text": "Samarkand",
    "country_id": "234",
    "value": "Samarkand"
  },
  {
    "id": "4010",
    "text": "Sirdare",
    "country_id": "234",
    "value": "Sirdare"
  },
  {
    "id": "4011",
    "text": "Surhondar",
    "country_id": "234",
    "value": "Surhondar"
  },
  {
    "id": "4012",
    "text": "Toskent",
    "country_id": "234",
    "value": "Toskent"
  },
  {
    "id": "4013",
    "text": "Malampa",
    "country_id": "235",
    "value": "Malampa"
  },
  {
    "id": "4014",
    "text": "Penama",
    "country_id": "235",
    "value": "Penama"
  },
  {
    "id": "4015",
    "text": "Sanma",
    "country_id": "235",
    "value": "Sanma"
  },
  {
    "id": "4016",
    "text": "Shefa",
    "country_id": "235",
    "value": "Shefa"
  },
  {
    "id": "4017",
    "text": "Tafea",
    "country_id": "235",
    "value": "Tafea"
  },
  {
    "id": "4018",
    "text": "Torba",
    "country_id": "235",
    "value": "Torba"
  },
  {
    "id": "4019",
    "text": "Vatican City State (Holy See)",
    "country_id": "236",
    "value": "Vatican City State (Holy See)"
  },
  {
    "id": "4020",
    "text": "Amazonas",
    "country_id": "237",
    "value": "Amazonas"
  },
  {
    "id": "4021",
    "text": "Anzoategui",
    "country_id": "237",
    "value": "Anzoategui"
  },
  {
    "id": "4022",
    "text": "Apure",
    "country_id": "237",
    "value": "Apure"
  },
  {
    "id": "4023",
    "text": "Aragua",
    "country_id": "237",
    "value": "Aragua"
  },
  {
    "id": "4024",
    "text": "Barinas",
    "country_id": "237",
    "value": "Barinas"
  },
  {
    "id": "4025",
    "text": "Bolivar",
    "country_id": "237",
    "value": "Bolivar"
  },
  {
    "id": "4026",
    "text": "Carabobo",
    "country_id": "237",
    "value": "Carabobo"
  },
  {
    "id": "4027",
    "text": "Cojedes",
    "country_id": "237",
    "value": "Cojedes"
  },
  {
    "id": "4028",
    "text": "Delta Amacuro",
    "country_id": "237",
    "value": "Delta Amacuro"
  },
  {
    "id": "4029",
    "text": "Distrito Federal",
    "country_id": "237",
    "value": "Distrito Federal"
  },
  {
    "id": "4030",
    "text": "Falcon",
    "country_id": "237",
    "value": "Falcon"
  },
  {
    "id": "4031",
    "text": "Guarico",
    "country_id": "237",
    "value": "Guarico"
  },
  {
    "id": "4032",
    "text": "Lara",
    "country_id": "237",
    "value": "Lara"
  },
  {
    "id": "4033",
    "text": "Merida",
    "country_id": "237",
    "value": "Merida"
  },
  {
    "id": "4034",
    "text": "Miranda",
    "country_id": "237",
    "value": "Miranda"
  },
  {
    "id": "4035",
    "text": "Monagas",
    "country_id": "237",
    "value": "Monagas"
  },
  {
    "id": "4036",
    "text": "Nueva Esparta",
    "country_id": "237",
    "value": "Nueva Esparta"
  },
  {
    "id": "4037",
    "text": "Portuguesa",
    "country_id": "237",
    "value": "Portuguesa"
  },
  {
    "id": "4038",
    "text": "Sucre",
    "country_id": "237",
    "value": "Sucre"
  },
  {
    "id": "4039",
    "text": "Tachira",
    "country_id": "237",
    "value": "Tachira"
  },
  {
    "id": "4040",
    "text": "Trujillo",
    "country_id": "237",
    "value": "Trujillo"
  },
  {
    "id": "4041",
    "text": "Vargas",
    "country_id": "237",
    "value": "Vargas"
  },
  {
    "id": "4042",
    "text": "Yaracuy",
    "country_id": "237",
    "value": "Yaracuy"
  },
  {
    "id": "4043",
    "text": "Zulia",
    "country_id": "237",
    "value": "Zulia"
  },
  {
    "id": "4044",
    "text": "Bac Giang",
    "country_id": "238",
    "value": "Bac Giang"
  },
  {
    "id": "4045",
    "text": "Binh Dinh",
    "country_id": "238",
    "value": "Binh Dinh"
  },
  {
    "id": "4046",
    "text": "Binh Duong",
    "country_id": "238",
    "value": "Binh Duong"
  },
  {
    "id": "4047",
    "text": "Da Nang",
    "country_id": "238",
    "value": "Da Nang"
  },
  {
    "id": "4048",
    "text": "Dong Bang Song Cuu Long",
    "country_id": "238",
    "value": "Dong Bang Song Cuu Long"
  },
  {
    "id": "4049",
    "text": "Dong Bang Song Hong",
    "country_id": "238",
    "value": "Dong Bang Song Hong"
  },
  {
    "id": "4050",
    "text": "Dong Nai",
    "country_id": "238",
    "value": "Dong Nai"
  },
  {
    "id": "4051",
    "text": "Dong Nam Bo",
    "country_id": "238",
    "value": "Dong Nam Bo"
  },
  {
    "id": "4052",
    "text": "Duyen Hai Mien Trung",
    "country_id": "238",
    "value": "Duyen Hai Mien Trung"
  },
  {
    "id": "4053",
    "text": "Hanoi",
    "country_id": "238",
    "value": "Hanoi"
  },
  {
    "id": "4054",
    "text": "Hung Yen",
    "country_id": "238",
    "value": "Hung Yen"
  },
  {
    "id": "4055",
    "text": "Khu Bon Cu",
    "country_id": "238",
    "value": "Khu Bon Cu"
  },
  {
    "id": "4056",
    "text": "Long An",
    "country_id": "238",
    "value": "Long An"
  },
  {
    "id": "4057",
    "text": "Mien Nui Va Trung Du",
    "country_id": "238",
    "value": "Mien Nui Va Trung Du"
  },
  {
    "id": "4058",
    "text": "Thai Nguyen",
    "country_id": "238",
    "value": "Thai Nguyen"
  },
  {
    "id": "4059",
    "text": "Thanh Pho Ho Chi Minh",
    "country_id": "238",
    "value": "Thanh Pho Ho Chi Minh"
  },
  {
    "id": "4060",
    "text": "Thu Do Ha Noi",
    "country_id": "238",
    "value": "Thu Do Ha Noi"
  },
  {
    "id": "4061",
    "text": "Tinh Can Tho",
    "country_id": "238",
    "value": "Tinh Can Tho"
  },
  {
    "id": "4062",
    "text": "Tinh Da Nang",
    "country_id": "238",
    "value": "Tinh Da Nang"
  },
  {
    "id": "4063",
    "text": "Tinh Gia Lai",
    "country_id": "238",
    "value": "Tinh Gia Lai"
  },
  {
    "id": "4064",
    "text": "Anegada",
    "country_id": "239",
    "value": "Anegada"
  },
  {
    "id": "4065",
    "text": "Jost van Dyke",
    "country_id": "239",
    "value": "Jost van Dyke"
  },
  {
    "id": "4066",
    "text": "Tortola",
    "country_id": "239",
    "value": "Tortola"
  },
  {
    "id": "4067",
    "text": "Saint Croix",
    "country_id": "240",
    "value": "Saint Croix"
  },
  {
    "id": "4068",
    "text": "Saint John",
    "country_id": "240",
    "value": "Saint John"
  },
  {
    "id": "4069",
    "text": "Saint Thomas",
    "country_id": "240",
    "value": "Saint Thomas"
  },
  {
    "id": "4070",
    "text": "Alo",
    "country_id": "241",
    "value": "Alo"
  },
  {
    "id": "4071",
    "text": "Singave",
    "country_id": "241",
    "value": "Singave"
  },
  {
    "id": "4072",
    "text": "Wallis",
    "country_id": "241",
    "value": "Wallis"
  },
  {
    "id": "4073",
    "text": "Bu Jaydur",
    "country_id": "242",
    "value": "Bu Jaydur"
  },
  {
    "id": "4074",
    "text": "Wad-adh-Dhahab",
    "country_id": "242",
    "value": "Wad-adh-Dhahab"
  },
  {
    "id": "4075",
    "text": "al-''Ayun",
    "country_id": "242",
    "value": "al-''Ayun"
  },
  {
    "id": "4076",
    "text": "as-Samarah",
    "country_id": "242",
    "value": "as-Samarah"
  },
  {
    "id": "4077",
    "text": "Adan",
    "country_id": "243",
    "value": "Adan"
  },
  {
    "id": "4078",
    "text": "Abyan",
    "country_id": "243",
    "value": "Abyan"
  },
  {
    "id": "4079",
    "text": "Dhamar",
    "country_id": "243",
    "value": "Dhamar"
  },
  {
    "id": "4080",
    "text": "Hadramaut",
    "country_id": "243",
    "value": "Hadramaut"
  },
  {
    "id": "4081",
    "text": "Hajjah",
    "country_id": "243",
    "value": "Hajjah"
  },
  {
    "id": "4082",
    "text": "Hudaydah",
    "country_id": "243",
    "value": "Hudaydah"
  },
  {
    "id": "4083",
    "text": "Ibb",
    "country_id": "243",
    "value": "Ibb"
  },
  {
    "id": "4084",
    "text": "Lahij",
    "country_id": "243",
    "value": "Lahij"
  },
  {
    "id": "4085",
    "text": "Ma''rib",
    "country_id": "243",
    "value": "Ma''rib"
  },
  {
    "id": "4086",
    "text": "Madinat San''a",
    "country_id": "243",
    "value": "Madinat San''a"
  },
  {
    "id": "4087",
    "text": "Sa''dah",
    "country_id": "243",
    "value": "Sa''dah"
  },
  {
    "id": "4088",
    "text": "Sana",
    "country_id": "243",
    "value": "Sana"
  },
  {
    "id": "4089",
    "text": "Shabwah",
    "country_id": "243",
    "value": "Shabwah"
  },
  {
    "id": "4090",
    "text": "Ta''izz",
    "country_id": "243",
    "value": "Ta''izz"
  },
  {
    "id": "4091",
    "text": "al-Bayda",
    "country_id": "243",
    "value": "al-Bayda"
  },
  {
    "id": "4092",
    "text": "al-Hudaydah",
    "country_id": "243",
    "value": "al-Hudaydah"
  },
  {
    "id": "4093",
    "text": "al-Jawf",
    "country_id": "243",
    "value": "al-Jawf"
  },
  {
    "id": "4094",
    "text": "al-Mahrah",
    "country_id": "243",
    "value": "al-Mahrah"
  },
  {
    "id": "4095",
    "text": "al-Mahwit",
    "country_id": "243",
    "value": "al-Mahwit"
  },
  {
    "id": "4096",
    "text": "Central Serbia",
    "country_id": "244",
    "value": "Central Serbia"
  },
  {
    "id": "4097",
    "text": "Kosovo and Metohija",
    "country_id": "244",
    "value": "Kosovo and Metohija"
  },
  {
    "id": "4098",
    "text": "Montenegro",
    "country_id": "244",
    "value": "Montenegro"
  },
  {
    "id": "4099",
    "text": "Republic of Serbia",
    "country_id": "244",
    "value": "Republic of Serbia"
  },
  {
    "id": "4100",
    "text": "Serbia",
    "country_id": "244",
    "value": "Serbia"
  },
  {
    "id": "4101",
    "text": "Vojvodina",
    "country_id": "244",
    "value": "Vojvodina"
  },
  {
    "id": "4102",
    "text": "Central",
    "country_id": "245",
    "value": "Central"
  },
  {
    "id": "4103",
    "text": "Copperbelt",
    "country_id": "245",
    "value": "Copperbelt"
  },
  {
    "id": "4104",
    "text": "Eastern",
    "country_id": "245",
    "value": "Eastern"
  },
  {
    "id": "4105",
    "text": "Luapala",
    "country_id": "245",
    "value": "Luapala"
  },
  {
    "id": "4106",
    "text": "Lusaka",
    "country_id": "245",
    "value": "Lusaka"
  },
  {
    "id": "4107",
    "text": "North-Western",
    "country_id": "245",
    "value": "North-Western"
  },
  {
    "id": "4108",
    "text": "Northern",
    "country_id": "245",
    "value": "Northern"
  },
  {
    "id": "4109",
    "text": "Southern",
    "country_id": "245",
    "value": "Southern"
  },
  {
    "id": "4110",
    "text": "Western",
    "country_id": "245",
    "value": "Western"
  },
  {
    "id": "4111",
    "text": "Bulawayo",
    "country_id": "246",
    "value": "Bulawayo"
  },
  {
    "id": "4112",
    "text": "Harare",
    "country_id": "246",
    "value": "Harare"
  },
  {
    "id": "4113",
    "text": "Manicaland",
    "country_id": "246",
    "value": "Manicaland"
  },
  {
    "id": "4114",
    "text": "Mashonaland Central",
    "country_id": "246",
    "value": "Mashonaland Central"
  },
  {
    "id": "4115",
    "text": "Mashonaland East",
    "country_id": "246",
    "value": "Mashonaland East"
  },
  {
    "id": "4116",
    "text": "Mashonaland West",
    "country_id": "246",
    "value": "Mashonaland West"
  },
  {
    "id": "4117",
    "text": "Masvingo",
    "country_id": "246",
    "value": "Masvingo"
  },
  {
    "id": "4118",
    "text": "Matabeleland North",
    "country_id": "246",
    "value": "Matabeleland North"
  },
  {
    "id": "4119",
    "text": "Matabeleland South",
    "country_id": "246",
    "value": "Matabeleland South"
  },
  {
    "id": "4120",
    "text": "Midlands",
    "country_id": "246",
    "value": "Midlands"
  }
];

module.exports.statesList = statesList;