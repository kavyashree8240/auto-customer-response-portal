
const studyNavBar = {
  width: '2880px',
  height: '128px',
  position: 'fixed',
  background: '#FFFFFF',
  boxShadow: 'rgba(0, 0, 0, 0.1) 0px 0px 0px, rgba(0, 0, 0, 0.1) 0px 0px 15px',
  zIndex: 2
}

const styles = {
  studyNavBar,
};

export default styles;