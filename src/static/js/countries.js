var countriesList = [
  {
    "id": "1",
    "value": "1",
    "text": "Afghanistan"
  },
  {
    "id": "2",
    "value": "2",
    "text": "Albania"
  },
  {
    "id": "3",
    "value": "3",
    "text": "Algeria"
  },
  {
    "id": "4",
    "value": "4",
    "text": "American Samoa"
  },
  {
    "id": "5",
    "value": "5",
    "text": "Andorra"
  },
  {
    "id": "6",
    "value": "6",
    "text": "Angola"
  },
  {
    "id": "7",
    "value": "7",
    "text": "Anguilla"
  },
  {
    "id": "8",
    "value": "8",
    "text": "Antarctica"
  },
  {
    "id": "9",
    "value": "9",
    "text": "Antigua And Barbuda"
  },
  {
    "id": "10",
    "value": "10",
    "text": "Argentina"
  },
  {
    "id": "11",
    "value": "11",
    "text": "Armenia"
  },
  {
    "id": "12",
    "value": "12",
    "text": "Aruba"
  },
  {
    "id": "13",
    "value": "13",
    "text": "Australia"
  },
  {
    "id": "14",
    "value": "14",
    "text": "Austria"
  },
  {
    "id": "15",
    "value": "15",
    "text": "Azerbaijan"
  },
  {
    "id": "16",
    "value": "16",
    "text": "Bahamas The"
  },
  {
    "id": "17",
    "value": "17",
    "text": "Bahrain"
  },
  {
    "id": "18",
    "value": "18",
    "text": "Bangladesh"
  },
  {
    "id": "19",
    "value": "19",
    "text": "Barbados"
  },
  {
    "id": "20",
    "value": "20",
    "text": "Belarus"
  },
  {
    "id": "21",
    "value": "21",
    "text": "Belgium"
  },
  {
    "id": "22",
    "value": "22",
    "text": "Belize"
  },
  {
    "id": "23",
    "value": "23",
    "text": "Benin"
  },
  {
    "id": "24",
    "value": "24",
    "text": "Bermuda"
  },
  {
    "id": "25",
    "value": "25",
    "text": "Bhutan"
  },
  {
    "id": "26",
    "value": "26",
    "text": "Bolivia"
  },
  {
    "id": "27",
    "value": "27",
    "text": "Bosnia and Herzegovina"
  },
  {
    "id": "28",
    "value": "28",
    "text": "Botswana"
  },
  {
    "id": "29",
    "value": "29",
    "text": "Bouvet Island"
  },
  {
    "id": "30",
    "value": "30",
    "text": "Brazil"
  },
  {
    "id": "31",
    "value": "31",
    "text": "British Indian Ocean Territory"
  },
  {
    "id": "32",
    "value": "32",
    "text": "Brunei"
  },
  {
    "id": "33",
    "value": "33",
    "text": "Bulgaria"
  },
  {
    "id": "34",
    "value": "34",
    "text": "Burkina Faso"
  },
  {
    "id": "35",
    "value": "35",
    "text": "Burundi"
  },
  {
    "id": "36",
    "value": "36",
    "text": "Cambodia"
  },
  {
    "id": "37",
    "value": "37",
    "text": "Cameroon"
  },
  {
    "id": "38",
    "value": "38",
    "text": "Canada"
  },
  {
    "id": "39",
    "value": "39",
    "text": "Cape Verde"
  },
  {
    "id": "40",
    "value": "40",
    "text": "Cayman Islands"
  },
  {
    "id": "41",
    "value": "41",
    "text": "Central African Republic"
  },
  {
    "id": "42",
    "value": "42",
    "text": "Chad"
  },
  {
    "id": "43",
    "value": "43",
    "text": "Chile"
  },
  {
    "id": "44",
    "value": "44",
    "text": "China"
  },
  {
    "id": "45",
    "value": "45",
    "text": "Christmas Island"
  },
  {
    "id": "46",
    "value": "46",
    "text": "Cocos (Keeling) Islands"
  },
  {
    "id": "47",
    "value": "47",
    "text": "Colombia"
  },
  {
    "id": "48",
    "value": "48",
    "text": "Comoros"
  },
  {
    "id": "49",
    "value": "49",
    "text": "Congo"
  },
  {
    "id": "50",
    "value": "50",
    "text": "Congo The Democratic Republic Of The"
  },
  {
    "id": "51",
    "value": "51",
    "text": "Cook Islands"
  },
  {
    "id": "52",
    "value": "52",
    "text": "Costa Rica"
  },
  {
    "id": "53",
    "value": "53",
    "text": "Cote D''Ivoire (Ivory Coast)"
  },
  {
    "id": "54",
    "value": "54",
    "text": "Croatia (Hrvatska)"
  },
  {
    "id": "55",
    "value": "55",
    "text": "Cuba"
  },
  {
    "id": "56",
    "value": "56",
    "text": "Cyprus"
  },
  {
    "id": "57",
    "value": "57",
    "text": "Czech Republic"
  },
  {
    "id": "58",
    "value": "58",
    "text": "Denmark"
  },
  {
    "id": "59",
    "value": "59",
    "text": "Djibouti"
  },
  {
    "id": "60",
    "value": "60",
    "text": "Dominica"
  },
  {
    "id": "61",
    "value": "61",
    "text": "Dominican Republic"
  },
  {
    "id": "62",
    "value": "62",
    "text": "East Timor"
  },
  {
    "id": "63",
    "value": "63",
    "text": "Ecuador"
  },
  {
    "id": "64",
    "value": "64",
    "text": "Egypt"
  },
  {
    "id": "65",
    "value": "65",
    "text": "El Salvador"
  },
  {
    "id": "66",
    "value": "66",
    "text": "Equatorial Guinea"
  },
  {
    "id": "67",
    "value": "67",
    "text": "Eritrea"
  },
  {
    "id": "68",
    "value": "68",
    "text": "Estonia"
  },
  {
    "id": "69",
    "value": "69",
    "text": "Ethiopia"
  },
  {
    "id": "70",
    "value": "70",
    "text": "External Territories of Australia"
  },
  {
    "id": "71",
    "value": "71",
    "text": "Falkland Islands"
  },
  {
    "id": "72",
    "value": "72",
    "text": "Faroe Islands"
  },
  {
    "id": "73",
    "value": "73",
    "text": "Fiji Islands"
  },
  {
    "id": "74",
    "value": "74",
    "text": "Finland"
  },
  {
    "id": "75",
    "value": "75",
    "text": "France"
  },
  {
    "id": "76",
    "value": "76",
    "text": "French Guiana"
  },
  {
    "id": "77",
    "value": "77",
    "text": "French Polynesia"
  },
  {
    "id": "78",
    "value": "78",
    "text": "French Southern Territories"
  },
  {
    "id": "79",
    "value": "79",
    "text": "Gabon"
  },
  {
    "id": "80",
    "value": "80",
    "text": "Gambia The"
  },
  {
    "id": "81",
    "value": "81",
    "text": "Georgia"
  },
  {
    "id": "82",
    "value": "82",
    "text": "Germany"
  },
  {
    "id": "83",
    "value": "83",
    "text": "Ghana"
  },
  {
    "id": "84",
    "value": "84",
    "text": "Gibraltar"
  },
  {
    "id": "85",
    "value": "85",
    "text": "Greece"
  },
  {
    "id": "86",
    "value": "86",
    "text": "Greenland"
  },
  {
    "id": "87",
    "value": "87",
    "text": "Grenada"
  },
  {
    "id": "88",
    "value": "88",
    "text": "Guadeloupe"
  },
  {
    "id": "89",
    "value": "89",
    "text": "Guam"
  },
  {
    "id": "90",
    "value": "90",
    "text": "Guatemala"
  },
  {
    "id": "91",
    "value": "91",
    "text": "Guernsey and Alderney"
  },
  {
    "id": "92",
    "value": "92",
    "text": "Guinea"
  },
  {
    "id": "93",
    "value": "93",
    "text": "Guinea-Bissau"
  },
  {
    "id": "94",
    "value": "94",
    "text": "Guyana"
  },
  {
    "id": "95",
    "value": "95",
    "text": "Haiti"
  },
  {
    "id": "96",
    "value": "96",
    "text": "Heard and McDonald Islands"
  },
  {
    "id": "97",
    "value": "97",
    "text": "Honduras"
  },
  {
    "id": "98",
    "value": "98",
    "text": "Hong Kong S.A.R."
  },
  {
    "id": "99",
    "value": "99",
    "text": "Hungary"
  },
  {
    "id": "100",
    "value": "100",
    "text": "Iceland"
  },
  {
    "id": "101",
    "value": "101",
    "text": "India"
  },
  {
    "id": "102",
    "value": "102",
    "text": "Indonesia"
  },
  {
    "id": "103",
    "value": "103",
    "text": "Iran"
  },
  {
    "id": "104",
    "value": "104",
    "text": "Iraq"
  },
  {
    "id": "105",
    "value": "105",
    "text": "Ireland"
  },
  {
    "id": "106",
    "value": "106",
    "text": "Israel"
  },
  {
    "id": "107",
    "value": "107",
    "text": "Italy"
  },
  {
    "id": "108",
    "value": "108",
    "text": "Jamaica"
  },
  {
    "id": "109",
    "value": "109",
    "text": "Japan"
  },
  {
    "id": "110",
    "value": "110",
    "text": "Jersey"
  },
  {
    "id": "111",
    "value": "111",
    "text": "Jordan"
  },
  {
    "id": "112",
    "value": "112",
    "text": "Kazakhstan"
  },
  {
    "id": "113",
    "value": "113",
    "text": "Kenya"
  },
  {
    "id": "114",
    "value": "114",
    "text": "Kiribati"
  },
  {
    "id": "115",
    "value": "115",
    "text": "Korea North"
  },
  {
    "id": "116",
    "value": "116",
    "text": "Korea South"
  },
  {
    "id": "117",
    "value": "117",
    "text": "Kuwait"
  },
  {
    "id": "118",
    "value": "118",
    "text": "Kyrgyzstan"
  },
  {
    "id": "119",
    "value": "119",
    "text": "Laos"
  },
  {
    "id": "120",
    "value": "120",
    "text": "Latvia"
  },
  {
    "id": "121",
    "value": "121",
    "text": "Lebanon"
  },
  {
    "id": "122",
    "value": "122",
    "text": "Lesotho"
  },
  {
    "id": "123",
    "value": "123",
    "text": "Liberia"
  },
  {
    "id": "124",
    "value": "124",
    "text": "Libya"
  },
  {
    "id": "125",
    "value": "125",
    "text": "Liechtenstein"
  },
  {
    "id": "126",
    "value": "126",
    "text": "Lithuania"
  },
  {
    "id": "127",
    "value": "127",
    "text": "Luxembourg"
  },
  {
    "id": "128",
    "value": "128",
    "text": "Macau S.A.R."
  },
  {
    "id": "129",
    "value": "129",
    "text": "Macedonia"
  },
  {
    "id": "130",
    "value": "130",
    "text": "Madagascar"
  },
  {
    "id": "131",
    "value": "131",
    "text": "Malawi"
  },
  {
    "id": "132",
    "value": "132",
    "text": "Malaysia"
  },
  {
    "id": "133",
    "value": "133",
    "text": "Maldives"
  },
  {
    "id": "134",
    "value": "134",
    "text": "Mali"
  },
  {
    "id": "135",
    "value": "135",
    "text": "Malta"
  },
  {
    "id": "136",
    "value": "136",
    "text": "Man (Isle of)"
  },
  {
    "id": "137",
    "value": "137",
    "text": "Marshall Islands"
  },
  {
    "id": "138",
    "value": "138",
    "text": "Martinique"
  },
  {
    "id": "139",
    "value": "139",
    "text": "Mauritania"
  },
  {
    "id": "140",
    "value": "140",
    "text": "Mauritius"
  },
  {
    "id": "141",
    "value": "141",
    "text": "Mayotte"
  },
  {
    "id": "142",
    "value": "142",
    "text": "Mexico"
  },
  {
    "id": "143",
    "value": "143",
    "text": "Micronesia"
  },
  {
    "id": "144",
    "value": "144",
    "text": "Moldova"
  },
  {
    "id": "145",
    "value": "145",
    "text": "Monaco"
  },
  {
    "id": "146",
    "value": "146",
    "text": "Mongolia"
  },
  {
    "id": "147",
    "value": "147",
    "text": "Montserrat"
  },
  {
    "id": "148",
    "value": "148",
    "text": "Morocco"
  },
  {
    "id": "149",
    "value": "149",
    "text": "Mozambique"
  },
  {
    "id": "150",
    "value": "150",
    "text": "Myanmar"
  },
  {
    "id": "151",
    "value": "151",
    "text": "Namibia"
  },
  {
    "id": "152",
    "value": "152",
    "text": "Nauru"
  },
  {
    "id": "153",
    "value": "153",
    "text": "Nepal"
  },
  {
    "id": "154",
    "value": "154",
    "text": "Netherlands Antilles"
  },
  {
    "id": "155",
    "value": "155",
    "text": "Netherlands The"
  },
  {
    "id": "156",
    "value": "156",
    "text": "New Caledonia"
  },
  {
    "id": "157",
    "value": "157",
    "text": "New Zealand"
  },
  {
    "id": "158",
    "value": "158",
    "text": "Nicaragua"
  },
  {
    "id": "159",
    "value": "159",
    "text": "Niger"
  },
  {
    "id": "160",
    "value": "160",
    "text": "Nigeria"
  },
  {
    "id": "161",
    "value": "161",
    "text": "Niue"
  },
  {
    "id": "162",
    "value": "162",
    "text": "Norfolk Island"
  },
  {
    "id": "163",
    "value": "163",
    "text": "Northern Mariana Islands"
  },
  {
    "id": "164",
    "value": "164",
    "text": "Norway"
  },
  {
    "id": "165",
    "value": "165",
    "text": "Oman"
  },
  {
    "id": "166",
    "value": "166",
    "text": "Pakistan"
  },
  {
    "id": "167",
    "value": "167",
    "text": "Palau"
  },
  {
    "id": "168",
    "value": "168",
    "text": "Palestinian Territory Occupied"
  },
  {
    "id": "169",
    "value": "169",
    "text": "Panama"
  },
  {
    "id": "170",
    "value": "170",
    "text": "Papua new Guinea"
  },
  {
    "id": "171",
    "value": "171",
    "text": "Paraguay"
  },
  {
    "id": "172",
    "value": "172",
    "text": "Peru"
  },
  {
    "id": "173",
    "value": "173",
    "text": "Philippines"
  },
  {
    "id": "174",
    "value": "174",
    "text": "Pitcairn Island"
  },
  {
    "id": "175",
    "value": "175",
    "text": "Poland"
  },
  {
    "id": "176",
    "value": "176",
    "text": "Portugal"
  },
  {
    "id": "177",
    "value": "177",
    "text": "Puerto Rico"
  },
  {
    "id": "178",
    "value": "178",
    "text": "Qatar"
  },
  {
    "id": "179",
    "value": "179",
    "text": "Reunion"
  },
  {
    "id": "180",
    "value": "180",
    "text": "Romania"
  },
  {
    "id": "181",
    "value": "181",
    "text": "Russia"
  },
  {
    "id": "182",
    "value": "182",
    "text": "Rwanda"
  },
  {
    "id": "183",
    "value": "183",
    "text": "Saint Helena"
  },
  {
    "id": "184",
    "value": "184",
    "text": "Saint Kitts And Nevis"
  },
  {
    "id": "185",
    "value": "185",
    "text": "Saint Lucia"
  },
  {
    "id": "186",
    "value": "186",
    "text": "Saint Pierre and Miquelon"
  },
  {
    "id": "187",
    "value": "187",
    "text": "Saint Vincent And The Grenadines"
  },
  {
    "id": "188",
    "value": "188",
    "text": "Samoa"
  },
  {
    "id": "189",
    "value": "189",
    "text": "San Marino"
  },
  {
    "id": "190",
    "value": "190",
    "text": "Sao Tome and Principe"
  },
  {
    "id": "191",
    "value": "191",
    "text": "Saudi Arabia"
  },
  {
    "id": "192",
    "value": "192",
    "text": "Senegal"
  },
  {
    "id": "193",
    "value": "193",
    "text": "Serbia"
  },
  {
    "id": "194",
    "value": "194",
    "text": "Seychelles"
  },
  {
    "id": "195",
    "value": "195",
    "text": "Sierra Leone"
  },
  {
    "id": "196",
    "value": "196",
    "text": "Singapore"
  },
  {
    "id": "197",
    "value": "197",
    "text": "Slovakia"
  },
  {
    "id": "198",
    "value": "198",
    "text": "Slovenia"
  },
  {
    "id": "199",
    "value": "199",
    "text": "Smaller Territories of the UK"
  },
  {
    "id": "200",
    "value": "200",
    "text": "Solomon Islands"
  },
  {
    "id": "201",
    "value": "201",
    "text": "Somalia"
  },
  {
    "id": "202",
    "value": "202",
    "text": "South Africa"
  },
  {
    "id": "203",
    "value": "203",
    "text": "South Georgia"
  },
  {
    "id": "204",
    "value": "204",
    "text": "South Sudan"
  },
  {
    "id": "205",
    "value": "205",
    "text": "Spain"
  },
  {
    "id": "206",
    "value": "206",
    "text": "Sri Lanka"
  },
  {
    "id": "207",
    "value": "207",
    "text": "Sudan"
  },
  {
    "id": "208",
    "value": "208",
    "text": "Suritext"
  },
  {
    "id": "209",
    "value": "209",
    "text": "Svalbard And Jan Mayen Islands"
  },
  {
    "id": "210",
    "value": "210",
    "text": "Swaziland"
  },
  {
    "id": "211",
    "value": "211",
    "text": "Sweden"
  },
  {
    "id": "212",
    "value": "212",
    "text": "Switzerland"
  },
  {
    "id": "213",
    "value": "213",
    "text": "Syria"
  },
  {
    "id": "214",
    "value": "214",
    "text": "Taiwan"
  },
  {
    "id": "215",
    "value": "215",
    "text": "Tajikistan"
  },
  {
    "id": "216",
    "value": "216",
    "text": "Tanzania"
  },
  {
    "id": "217",
    "value": "217",
    "text": "Thailand"
  },
  {
    "id": "218",
    "value": "218",
    "text": "Togo"
  },
  {
    "id": "219",
    "value": "219",
    "text": "Tokelau"
  },
  {
    "id": "220",
    "value": "220",
    "text": "Tonga"
  },
  {
    "id": "221",
    "value": "221",
    "text": "Trinidad And Tobago"
  },
  {
    "id": "222",
    "value": "222",
    "text": "Tunisia"
  },
  {
    "id": "223",
    "value": "223",
    "text": "Turkey"
  },
  {
    "id": "224",
    "value": "224",
    "text": "Turkmenistan"
  },
  {
    "id": "225",
    "value": "225",
    "text": "Turks And Caicos Islands"
  },
  {
    "id": "226",
    "value": "226",
    "text": "Tuvalu"
  },
  {
    "id": "227",
    "value": "227",
    "text": "Uganda"
  },
  {
    "id": "228",
    "value": "228",
    "text": "Ukraine"
  },
  {
    "id": "229",
    "value": "229",
    "text": "United Arab Emirates"
  },
  {
    "id": "230",
    "value": "230",
    "text": "United Kingdom"
  },
  {
    "id": "231",
    "value": "231",
    "text": "United States"
  },
  {
    "id": "232",
    "value": "232",
    "text": "United States Minor Outlying Islands"
  },
  {
    "id": "233",
    "value": "233",
    "text": "Uruguay"
  },
  {
    "id": "234",
    "value": "234",
    "text": "Uzbekistan"
  },
  {
    "id": "235",
    "value": "235",
    "text": "Vanuatu"
  },
  {
    "id": "236",
    "value": "236",
    "text": "Vatican City State (Holy See)"
  },
  {
    "id": "237",
    "value": "237",
    "text": "Venezuela"
  },
  {
    "id": "238",
    "value": "238",
    "text": "Vietnam"
  },
  {
    "id": "239",
    "value": "239",
    "text": "Virgin Islands (British)"
  },
  {
    "id": "240",
    "value": "240",
    "text": "Virgin Islands (US)"
  },
  {
    "id": "241",
    "value": "241",
    "text": "Wallis And Futuna Islands"
  },
  {
    "id": "242",
    "value": "242",
    "text": "Western Sahara"
  },
  {
    "id": "243",
    "value": "243",
    "text": "Yemen"
  },
  {
    "id": "244",
    "value": "244",
    "text": "Yugoslavia"
  },
  {
    "id": "245",
    "value": "245",
    "text": "Zambia"
  },
  {
    "id": "246",
    "value": "246",
    "text": "Zimbabwe"
  }
];

module.exports.countriesList = countriesList;