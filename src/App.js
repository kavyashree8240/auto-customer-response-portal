  import React, { Component } from 'react';
  import avatar from './static/img/avatar.svg';
  import logo from './static/img/logo.png';
  import logoPlus from './static/img/logo_plus.png';
  import styles from './static/js/styles.js';
  import { Button } from 'semantic-ui-react';
  import { Dropdown } from 'semantic-ui-react';
  import qs from 'query-string';
  import Api from './utils/Api.js';
  import $ from 'jquery';
  import UserStudyQuestions from './user/UserStudyQuestions';
  import {BrowserRouter, Route, Redirect} from 'react-router-dom';


  class App extends Component {
    constructor(props){
      super(props);
      this.state = {
       
      }
    }
    
    componentDidMount() {
      const query = qs.parse(this.props.location.search);
      if(query.email) {
        Api.getUserStudies(query.email,query.auto_customer_study_id, (response) => {
          if(response.success) {
                 var user_study = response['data']['auto_customer_studies']
                  const userStudiesData =  user_study.map((row, key) => {
                    this.setState({
                            data: response['data'],
                            userEmail: response['data'].email,
                            auto_customer_study_order_number: row.auto_customer_study_order_number,
                            auto_customer_study_id: row['_id'].$oid,
                            userStudyQuestion: row['questions'],
                            studyTitle: row.title,
                            studyDescription: row.description
                      })
                  })
              }
              localStorage.setItem('data', JSON.stringify(this.state.data));
        });
      }
       document.body.style.backgroundColor = '#1fbaee';
    }
       

    signin() {
      console.log(this.state.userStudyQuestion);
      console.log("down");
      console.log(this.state.studyTitle);
       localStorage.setItem('data', JSON.stringify(this.state.data));

       window.location = '/user/home/studies'
      UserStudyQuestions.studyQuestions(this.state.userStudyQuestion)
    }
        
    render() {
      const componentStyles = {
        logo: {
          height: '70px',
          width: '312px',
          marginTop: '35px',
          paddingBottom: '23px',
          marginLeft: '439px'

        },
        rectangle: {
         height: '366px',
         width: '1060px',
         backgroundColor: '#FFFFFF',
         boxShadow: '8px',
         marginLeft: '200px',
         marginRight: '2000px',
         marginTop: '100px',  
         marginBottom: '200px'
        },
        logoPlus: {
          width: '36px',
          height: '36px',
          // position: 'absolute',
          cursor: 'pointer',
          marginLeft: '20px'
        },
        studyNavBar: {
          width: '100%',
          height: '65px',
          position: 'relative',
          background: 'white',
          zIndex: 1
        },
        headerDropdown: {
          float: 'right',
          marginRight: '20px',
          fontSize: '17px'
        },
        studyImage: {
          width: '96px',
          height: '96px',
          objectFit: 'cover',
          borderRadius: '50%'
        },
        studyTitle: {
          color: 'rgb(0, 0, 0)',
          fontFamily: 'Roboto',
          fontSize: '38px',
          paddingLeft: '100px',
          marginRight: '500px',
          paddingRight: '50px',
          marginTop: '0px'
        },
        studyDescription: {
         color: 'rgb(78, 74, 74)',
         fontFamily: 'Arial',
          fontSize: '18px',
          paddingLeft: '320px',
          marginRight: '500px',
          paddingRight: '50px',
          marginTop: '80px',
          fontWeight: 'normal'
        },
        headings: {
          color: 'rgb(0, 0, 0)',
          fontFamily: "Open Sans",
          fontSize: '58px',
          paddingLeft: '380px',
          marginRight: '500px',
          paddingRight: '50px'
        },
        studyDetailsContainer: {
          textAlign: 'center',
          // paddingTop: (this.props.appState.isMobile) ? '30px' : '50px',
          paddingRight: '30px',
          paddingLeft: '30px',
          width: '100%',
          maxHeight: '85%'
        },
        previousStudiesListContainer: {
          overflowY: 'auto',
          position: 'absolute',
          top: '65px',
          left: '20px',
          right: '0px',
          bottom: '0',
          paddingBottom: '100px'
        },
        previousStudiesListContainerMobile: {
          position: 'absolute',
          overflowY: 'auto',
          background: '#1C262F',
          width: '100%'
        },
        previousStudyListItem: {
          display: 'flex',
          cursor: 'pointer',
          margin: '30px',
          alignItems: 'center'
        },
        userQuestion: {
           marginTop: '30px',
           color: '000000',
           fontFamily: "Open Sans",
           fontSize: '25px',
           fontWeight: 'normal',
           textAlign: 'left',
           maxHeight: '165px',
           // overflowY: 'auto',
           marginBottom: '20px',
           marginLeft: '0px',
           marginRight: '0px',
           marginTop: '0px',
           paddingRight: '10px'
        },
        getStartedButton: {
          backgroundColor: '#1FBAEE',
          color: 'white',
          borderRadius: '0px',
          paddingLeft: '40px',
          paddingRight: '40px',
          fontFamily: '"Open Sans"',
          fontSize: '17px',
          fontWeight: '600',
          lineHeight: '20px',
          textAlign: 'center',
          height: '50px',
          marginTop: '15px',
          marginLeft: '450px'
        }
      };
      

      return (
       
          <div style={styles.studyNavBar}>
            <img src={logo} className="logo-study center-vertical" alt="Youplus Logo" style={componentStyles.logo}/>
           <h1 style={componentStyles.rectangle}>
           <h1 style={componentStyles.headings}></h1><div style={componentStyles.studyTitle}>{this.state.studyTitle}</div>
        <div style={componentStyles.studyDescription}>{this.state.studyDescription}</div>
        <button class="ui button" style={componentStyles.getStartedButton} onClick={() => this.signin()}>START STUDY</button>
        </h1>
          </div>
      );  
      }
  }
  export default App;