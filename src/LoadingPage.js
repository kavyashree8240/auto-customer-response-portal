import React, { Component } from 'react';
import logo from './static/img/logo_plus.svg';
import { Loader } from 'semantic-ui-react';

class LoadingPage extends Component {
  render() {
    const componentStyles = {
      loadingContainer: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        background: '#20BFF1'
      },
      logo: {
        height: '250px'
      }
    }
    
    return (
      <div style={componentStyles.loadingContainer}>
        <div className="center-div">
          <img src={logo} style={componentStyles.logo} alt="Youplus Logo"/>
          <Loader size='huge' inline='centered' active inverted/>
        </div>
      </div>
    )
  }
}

export default LoadingPage;