import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import UserStudyQuestions from './user/UserStudyQuestions';
import './index.css';
import {BrowserRouter, Route} from 'react-router-dom';

ReactDOM.render((
  <BrowserRouter>
  	<div className="app">
	  <Route exact path="/" component={App}/>
	  <Route path="/user/home/studies" component={UserStudyQuestions}/>
	  </div>
  </BrowserRouter>
  ),
document.getElementById('root')
);
