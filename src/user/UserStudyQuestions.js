import React, { Component } from 'react';
import Webcam from '../Webcam';
import styles from '../static/js/styles.js';
import logo from '../static/img/logo.png';
import { Popup } from 'semantic-ui-react';
import recordButton from '../static/img/button_record.png';
import uploadButton from '../static/img/button_upload.png';
import stopButton from '../static/img/button_stop.png';
import redoButton from '../static/img/button_redo.png';
import { Button } from 'semantic-ui-react';
import RecordRTC from 'recordrtc';
import $ from 'jquery';
import Api from '../utils/Api.js';
import {generateThumbnailBlobFromFileBlob,  captureUserMedia} from '../utils/Utils.js';



  // alert(localStorage.getItem('kavya'));
   
   var storedArray = JSON.parse(localStorage.getItem("data"));
   var user_study = storedArray['auto_customer_studies']

    
   // console.log("hell")



class UserStudyQuestions extends Component {
  constructor(props){
    super(props);
    this.state = {
      user: {},
      userProfile: {},
      userStudiesData: [],
      userData: storedArray,
      userQuestions: storedArray['questions'],
      currentQuestionIndex: 0,
      previousStudiesList: [],
      recentStudiesList: [],
      recentStudyIndex: 0,
      userStudyQuestion: [],
      userStudyQuestionType: [],
      currentQuestion: [],
      completedQuestionCount: 0,
      currentStudyId: null,
      isLoading: false,
      isPreviousStudiesPaneOpen: false,
      isProfileUpdateRequired: false,
      isRecording: false,
      isRecordingPreview: false,
      isCameraStreamActive: false,
      isEmailConfirmed: true    }
   }

   

   onInitializeCamera() {

    
    this.requestUserMedia(this.state.currentQuestionIndex);
  }

   requestUserMedia(index) {
    captureUserMedia((stream) => {
      if(stream) {
        this.setState({
          previewStream: stream,
          src: (window.URL || window.webkitURL).createObjectURL(stream),
          isAnswerQuestionModalOpen: true,
          selectedQuestion: this,
          isCameraInitialized: true
        });
      } else {
        this.setState({webcamFailed: true});
      }
    });
    
  }


  onCallToActionButtonClicked() {
    if(!this.state.isRecording && !this.state.isRecordingPreview) {
      this.startRecording();
    } else if(!this.state.isRecording && this.state.isRecordingPreview) {
      this.uploadStudyVideo();
    } else if(this.state.isRecording) {
      this.stopRecording();
    }
  }

  startRecording() {
    captureUserMedia((stream) => {
      var recordVideo = RecordRTC(stream, { type: 'video' });
      recordVideo.startRecording();
      this.startRecordingTimer();
      this.setState({
        recordingStream: stream,
        recordVideo: recordVideo,
        isRecording: true
      });
    });
    if(this.state.isRecording) {
      this.stopRecording();
    }
  }

  startRecordingTimer() {
    var recordingTimerInterval = setInterval(this.increaseRecordingTimer.bind(this), 1000);
    this.setState({recordingTimerInterval: recordingTimerInterval});
  }

   increaseRecordingTimer() {
    var hours = this.state.recordingTimerHours;
    var minutes = this.state.recordingTimerMinutes;
    var seconds = this.state.recordingTimerSeconds;
    seconds++;
    if (seconds >= 60) {
      seconds = 0;
      minutes++;
      if (minutes >= 60) {
        minutes = 0;
        hours++;
      }
    }
    this.setState({
      recordingTimerHours: hours,
      recordingTimerMinutes: minutes,
      recordingTimerSeconds: seconds
    });
  }


  stopRecording() {
    this.state.recordVideo.stopRecording(() => {
      this.stopRecordingTimer();
      this.setState({
        isRecording: false,
        isRecordingPreview: true
      });
    });
  }

  stopRecordingTimer() {
    var recordingTimerInterval = this.state.recordingTimerInterval;
    clearInterval(recordingTimerInterval);
  }

  releaseWebcam() {
    if(this.state.previewStream !== null) {
      this.state.previewStream.getAudioTracks().forEach(function(track) {
        track.stop();
      });
      this.state.previewStream.getVideoTracks().forEach(function(track) {
        track.stop();
      });
    }
    if(this.state.recordingStream !== null) {
      this.state.recordingStream.getAudioTracks().forEach(function(track) {
        track.stop();
      });
      this.state.recordingStream.getVideoTracks().forEach(function(track) {
        track.stop();
      });
    }
  }

  resetCamera() {
    document.getElementById('studyWebcamVideo').removeAttribute('poster');
    this.setState({
      isRecording: false,
      isRecordingPreview: false,
      videoAnswerFile: null,
      recordingTimerHours: 0,
      recordingTimerMinutes: 0,
      recordingTimerSeconds: 0
    });
    $("#videoAnswerFile").val("");
    if(this.refs.webcam && this.refs.webcam.setIsPlaying !== undefined) {
      this.refs.webcam.setIsPlaying(false);
    }
  }


   uploadStudyVideo() {
    var firstTry = true;
    generateThumbnailBlobFromFileBlob(this.state.recordVideo.blob, function(thumbnailBlob) {
      if(firstTry && thumbnailBlob === null) {
        firstTry = false;
        return; 
      }
      if (this.state.studyQuestions === undefined){
         var studyVideoObject = {
        user_email: this.state.userData.email,
        video: this.state.recordVideo.blob,
        auto_customer_study_id: this.state.autoCustomerStudyId,
        auto_customer_study_order_number: this.state.autoCustomerStudyOrderNumber,
        // questionId: this.state.questionsList[this.state.currentQuestionIndex]._id.$oid,
        // questionPosition: this.state.questionsList[this.state.currentQuestionIndex].question_order_number,
        thumbnail: thumbnailBlob
        // isStudyCompleted: (this.state.completedQuestionCount === this.state.questionsList.length - 1) ? true : false
      }

      }else {
        
      
      

       // console.log(this.state.userData.email);
       // console.log(this.state.recordVideo.blob);
       // console.log(this.state.autoCustomerStudyId);
       // console.log(this.state.autoCustomerStudyOrderNumber);
       // console.log(this.state);
       // console.log(this.state.studyQuestions[this.state.currentQuestionIndex]['_id'].$oid);
       // console.log(this.state.studyQuestions[this.state.currentQuestionIndex].question);
       // console.log(this.state.studyQuestions[this.state.currentQuestionIndex].question_order_number);
       // console.log(this.state.studyQuestions)
      // var userId = (this.state.user.user_id) ? this.state.user.user_id.$oid : this.state.user._id.$oid;
      var studyVideoObject = {
        user_email: this.state.userData.email,
        video: this.state.recordVideo.blob,
        auto_customer_study_id: this.state.autoCustomerStudyId,
        auto_customer_study_order_number: this.state.autoCustomerStudyOrderNumber,
        question: this.state.studyQuestions[this.state.currentQuestionIndex].question,
        question_id: this.state.studyQuestions[this.state.currentQuestionIndex]['_id'].$oid,
        question_order_number: this.state.studyQuestions[this.state.currentQuestionIndex].question_order_number,
        // questionId: this.state.questionsList[this.state.currentQuestionIndex]._id.$oid,
        // questionPosition: this.state.questionsList[this.state.currentQuestionIndex].question_order_number,
        thumbnail: thumbnailBlob
        // isStudyCompleted: (this.state.completedQuestionCount === this.state.questionsList.length - 1) ? true : false
      }
      }
      // this.setState({isUploading: true});

      Api.uploadStudyVideo(studyVideoObject, (response) => {
        var completedQuestionCount = 0;
        console.log(response);
        this.setState({
          isUploading: false,
          isUploadSuccessful: (response.status === 'success' || response.status === 'Accepted') ? true : false,
          isSlowNetwork: false
        });
        if(response.status === 'success' || response.status === 'Accepted') {
         alert("success");
         console.log(this.state.currentQuestionIndex);
         this.state.currentQuestionIndex++;
          // this.releaseWebcam();
          this.resetCamera();
          // window.location.reload();
        }
      });
    }.bind(this));
  }






  render() {
    const componentStyles = {
        logo: {
          height: '70px',
          width: '312px',
          marginTop: '35px',
          paddingBottom: '23px',
          marginLeft: '439px'

        },
        rectangle: {
         height: '300px',
         width: '540px',
         backgroundColor: '#EFEFEF',
         marginLeft: '500px',
         marginRight: '300px',
         marginTop: '80px',  
         marginBottom: '300px'
        },
         getStartedButton: {
          backgroundColor: '#1FBAEE',
          color: 'white',
          borderRadius: '0px',
          paddingLeft: '40px',
          paddingRight: '40px',
          fontFamily: '"Open Sans"',
          fontSize: '17px',
          fontWeight: '600px',
          lineHeight: '20px',
          textAlign: 'center',
          height: '50px',
          marginTop: '100px',
          marginLeft: '500px',
          width: '300px'
        },
        userQuestion: {
         marginTop: '30px',
         color: '000000',
         fontFamily: "Open Sans",
         fontSize: '25px',
         fontWeight: 'normal',
         textAlign: 'left',
         maxHeight: '165px',
         // overflowY: 'auto',
         marginBottom: '20px',
         marginLeft: '0px',
         marginRight: '0px',
         marginTop: '0px',
         paddingRight: '10px',
         paddingLeft: '10px'
      }


    }
    
     //  const userStudyQuestion = this.state.studyQuestion.map((row, key) => {
     //    console.log(row.question);
     //    return (
     //     <div key={key}>
       
     //           <div>
     //               <p>{key+1}){row.question}</p>
     //           </div>
             
     //     </div>
     //   )
     // })
    



      const webcam = (
        <Webcam
        ref="webcam"
        src={this.state.src}
        previewSrc={this.state.recordVideo}
        webcamFailed={this.state.webcamFailed}
        isInitialized={this.state.isCameraInitialized}
        isRecording={this.state.isRecording}
        isUploading={this.state.isUploading}
        isRecordingPreview={this.state.isRecordingPreview}
        isAppleDevice={this.state.isAppleDevice}
        onInitializeCamera={() => this.onInitializeCamera()}
        onUploadAnswerButtonClicked={() => this.onUploadAnswerButtonClicked()}
        onCameraStreamActive={() => this.setState({isCameraStreamActive: true})}/>)


      var answerQuestionButton = '';
    if(!this.state.isRecording && !this.state.isRecordingPreview) {
      answerQuestionButton = (
        <div className="center-horizontal" style={componentStyles.callToActionButtonContainer} onClick={this.onCallToActionButtonClicked.bind(this)}>
          <div style={{textAlign: 'center', color: 'white'}}>
            <Popup
              trigger={<img src={recordButton}  style={{width: '64px'}}/>}
              
              content='Start Recording'
              position='top center'
              inverted
              />
          </div>
        </div>
      );
    } else if (!this.state.isRecording && this.state.isRecordingPreview) {
      answerQuestionButton = (
        <div className="center-horizontal" style={componentStyles.callToActionButtonContainer} onClick={this.onCallToActionButtonClicked.bind(this)}>
          <div style={{textAlign: 'center', color: 'white'}}>
            <Popup
              trigger={<img src={uploadButton}  style={{width: '64px'}}/>}
              content='Submit Opinion'
              position='top center'
              inverted
              />
          </div>
        </div>
      ); 
    } else if (this.state.isRecording) {
      answerQuestionButton = (
        <div className="center-horizontal" style={componentStyles.callToActionButtonContainer} onClick={this.onCallToActionButtonClicked.bind(this)}>
          <div style={{textAlign: 'center', color: 'white'}}>
            <Popup
              trigger={<img src={stopButton}  style={{width: '64px'}}/>}
              content='Stop Recording'
              position='top center'
              inverted
              />
          </div>
        </div>
      );
    }

     const redoButtonComponent = (
      <img id="redoButton" src={redoButton} className="center-vertical" style={{width: '36px', display: (this.state.isRecordingPreview) ? 'inherit' : 'none'}}/>
    );

    const userStudiesData =  user_study.map((row, key) => {
                     this.state.autoCustomerStudyId = row['_id'].$oid;
                     this.state.autoCustomerStudyOrderNumber = row.auto_customer_study_order_number
                     if (row['questions'] != null) {
                     this.state.studyQuestions = row['questions']
                       const userStudyQuestion = row['questions'].map((row, key) => {
                        console.log(this.state.studyQuestions[this.state.currentQuestionIndex].question);
                        this.state.currentQuestion = this.state.studyQuestions[this.state.currentQuestionIndex].question
                      })
                    }
    

    });


      

    const webcamControls = (
        <div>
          {(this.state.isCameraInitialized || this.state.videoAnswerFile !== null) && !this.state.isUploading &&
            <div style={{width: '100%', position: 'absolute', bottom: '15px', display: 'flex', justifyContent: 'center', marginTop: '20px'}}>
              <div>
                <div className="center-horizontal" style={Object.assign({}, componentStyles.callToActionButtonContainer, {backgroundColor: 'transparent', cursor: 'pointer'})} onClick={() => this.resetCamera()}>
                  <div className="center-vertical" style={{width: '100%', textAlign: 'center', color: 'white'}}>
                    <Popup
                      trigger={<img src={redoButton}  style={{width: '36px', display: (this.state.isRecordingPreview) ? 'inherit' : 'none'}}/>}
                      content='Redo Recording'
                      position='top center'
                      inverted
                      />
                  </div>
                </div>
              </div>
              <div>
                {this.state.isCameraInitialized && answerQuestionButton}
              </div>
              <div>
                <div className="center-horizontal" style={Object.assign({}, componentStyles.callToActionButtonContainer, {backgroundColor: 'transparent'})}>
                  {this.state.isCameraInitialized && this.state.isRecording &&
                    <span className="center-vertical" style={componentStyles.recordingTimerLabel}>
                      {
                        (this.state.recordingTimerHours ? (this.state.recordingTimerHours > 9 ? this.state.recordingTimerHours : "0" + this.state.recordingTimerHours) : "00") + ":" +
                        (this.state.recordingTimerMinutes ? (this.state.recordingTimerMinutes > 9 ? this.state.recordingTimerMinutes : "0" + this.state.recordingTimerMinutes) : "00") + ":" +
                        (this.state.recordingTimerSeconds > 9 ? this.state.recordingTimerSeconds : "0" + this.state.recordingTimerSeconds)
                      }
                    </span>
                  }
                </div>
              </div>
            </div>
          }
        </div>
      );



    
        return(
          <div>
          <img src={logo} className="logo-study center-vertical" alt="Youplus Logo" style={componentStyles.logo} onClick={() => this.props.history.push('/user/home/studies')}/>
        <div className="center-div">
        <div style={componentStyles.userQuestion}>
         {this.state.currentQuestion}
        </div>        
          <Button style={componentStyles.getStartedButton} primary fluid onClick={() => this.onInitializeCamera()}>Please Give Your Opinion</Button>
         <div>
          {webcam}
        </div>
        
        <div>
          {webcamControls}
        </div>
        </div>
        </div>
       

    );   
  }
}
export default UserStudyQuestions;
