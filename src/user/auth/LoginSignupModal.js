import React, { Component } from 'react';
import {withRouter} from "react-router-dom";
import { Modal } from 'semantic-ui-react';
// import UserSignup from './UserSignup';
// import UserSignin from './UserSignin';
// import UserUpdateProfile from './UserUpdateProfile';
// import UserForgotPassword from './UserForgotPassword';

class LoginSignupModal extends Component {
  constructor(props){
    super(props);
    this.state = {
      modalComponentType: this.props.initialComponent || 'signin'
    }
  }
  
  componentWillReceiveProps(nextProps) {
    // if(nextProps.initialComponent !== this.state.modalComponentType) {
    //   this.setState({modalComponentType: nextProps.initialComponent});
    // }
  }
  
  renderModalComponent() {
    switch(this.state.modalComponentType) {
      case 'signup': 
      // return (
      //   <UserSignup 
      //   isMobile={this.props.isMobile}
      //   studyObject={this.props.studyObject}
      //   showSignin={() => this.setState({modalComponentType: 'signin'})}
      //   showUpdateProfile={() => this.setState({modalComponentType: 'update'})}
      //   onClose={this.props.onClose}
      //   />
      // );
      case 'signin': 
      // return (
      //   <UserSignin
      //   isMobile={this.props.isMobile}
      //   studyObject={this.props.studyObject}
      //   showSignup={() => this.setState({modalComponentType: 'signup'})}
      //   showForgotPassword={() => this.setState({modalComponentType: 'forgotPassword'})}
      //   onClose={this.props.onClose}
      //   />
      // );
      case 'update': 
      // return (
      //   <UserUpdateProfile
      //   isMobile={this.props.isMobile}
      //   studyObject={this.props.studyObject}
      //   onClose={this.props.onClose}
      //   onUpdateProfile={() => {
      //     if(this.props.onUpdateProfile) {
      //       this.props.onUpdateProfile();
      //     }
      //   }}
      //   />
      // );
      case 'forgotPassword': 
      // return (
      //   <UserForgotPassword
      //   isMobile={this.props.isMobile}
      //   showSignin={() => this.setState({modalComponentType: 'signin'})}
      //   onClose={this.props.onClose}
      //   />
      // );
    }
  }
  
  render() {
    const componentStyles = {
      mobileModal: {
        position: 'fixed',
        top: '0px',
        background: 'white',
        width: '100%',
        height: '100%',
        overflowY: 'auto'
      }
    };
    
    if(this.props.isMobile) {
      if(this.props.isOpen) {
        return (
          <div style={componentStyles.mobileModal}>
          {this.renderModalComponent()}
          </div>
        );
      } else {
        return (
          <div></div>
        );
      }
    }
    
    return (
      <Modal closeIcon open={this.props.isOpen} size='tiny' onClose={this.props.onClose}>
      {this.renderModalComponent()}
      </Modal>
    );
  }
}

export default withRouter(LoginSignupModal);