import {
  API_BASE,
  IS_PROD,
  UPLOAD_STUDY_VIDEO_BASE_URL
} from './Constants';


function parseJSON(response) {
  return response.json();
}

function getUserStudies(email, auto_customer_studies_id,callback) {
  const formData = new FormData();
  console.log(email)
  console.log(auto_customer_studies_id)
  console.log(API_BASE);
  formData.append('email', email);
  formData.append('auto_customer_studies_id', auto_customer_studies_id);
  fetch(`${API_BASE}` , {
    method: 'post',
    body: formData
  }).then(parseJSON)
  .then(callback);
}


function uploadStudyVideo(studyVideoObject, callback) {
  alert("yappa");
  if(studyVideoObject.question_id === undefined){
     const formData = new FormData();
    formData.append('user_email', studyVideoObject.user_email);
    formData.append('video', studyVideoObject.video);
    formData.append('thumbnail', studyVideoObject.thumbnail);
    formData.append('auto_customer_study_id', studyVideoObject.auto_customer_study_id);
    formData.append('type', 'video/webm');
    formData.append('auto_customer_study_order_number', studyVideoObject.auto_customer_study_order_number);
    fetch(UPLOAD_STUDY_VIDEO_BASE_URL, {
        method: 'post',
         body: formData
      }).then(parseJSON)
      .then(callback);
    }else{
      const formData = new FormData();
      formData.append('user_email', studyVideoObject.user_email);
      formData.append('video', studyVideoObject.video);
      formData.append('thumbnail', studyVideoObject.thumbnail);
      formData.append('auto_customer_study_id', studyVideoObject.auto_customer_study_id);
      formData.append('question_id', studyVideoObject.question_id);
      formData.append('type', 'video/webm');
      formData.append('auto_customer_study_order_number', studyVideoObject.auto_customer_study_order_number);
      formData.append('question_order_number', studyVideoObject.question_order_number);
      fetch(UPLOAD_STUDY_VIDEO_BASE_URL, {
        method: 'post',
         body: formData
      }).then(parseJSON)
      .then(callback);
}
}

const Api = {
getUserStudies,
uploadStudyVideo
}



export default Api;
