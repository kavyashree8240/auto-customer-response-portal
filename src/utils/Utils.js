import Api from './Api.js';

// export function getUserObject() {
//   var userAuthItem = localStorage.getItem('userAuthItem');
//   if(userAuthItem !== null) {
//     var userAuthObject = JSON.parse(userAuthItem);
//     return userAuthObject;
//   }
//   return null;
// }

export function getUserObject() {
  console.log("am at get userobject");
  console.log("hhhhh")

}

export function getPartnerObject() {
  var partnerAuthItem = localStorage.getItem('partnerAuthItem');
  if(partnerAuthItem !== null) {
    var partnerAuthObject = JSON.parse(partnerAuthItem);
    return partnerAuthObject;
  }
  return null;
}

export function signOutUser(shouldConfirm) {
  var confirm;
  if(shouldConfirm) {
    confirm = window.confirm("Are you sure you want to logout?");
  } else {
    confirm = true;
  }
  if(confirm) {
    var userObject = getUserObject();
    if(userObject !== null) {
      var signoutObject = {
        email: userObject.email,
        authToken: userObject.auth_token
      }
      Api.signOutUser(signoutObject, (response) => {
        window.location = '/logout';
        console.log(response);
      });
    }
    localStorage.removeItem('userAuthItem');
  }
}

export function signOutPartner() {
  var partnerObject = getPartnerObject();
  if(partnerObject !== null) {
    var signoutObject = {
      email: partnerObject.email,
      authToken: partnerObject.auth_token
    }
    Api.signOutPartner(signoutObject, (response) => {
      localStorage.removeItem('partnerAuthItem');
      window.location = '/partner/signin';
      console.log(response);
    });
  }
}

export function validateEmail(email) {
  if(email === null || email === undefined || email.length === 0) {
    return false;
  }
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

export function validatePassword(password) {
  if(password === null || password === undefined || password.length < 8) {
    return false;
  }
  return true;
}

export function validatePhoneNumber(phoneNumber) {
  if(phoneNumber === null || phoneNumber === undefined || phoneNumber.length === 0) {
    return false;
  }
  var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
  return re.test(phoneNumber);
}

// prompt the user to grant video permissions if not already granted
export function captureUserMedia(callback) {
  var params = { audio: true, video: true };
  if(navigator.getUserMedia === undefined) {
    callback(null);
  } else {
    navigator.getUserMedia(params, (stream) => {
      callback(stream);
    }, (error) => {
      if(error) {
        console.log(error);
        callback(null);
        switch(error.name) {
          case 'PermissionDeniedError':
          alert('Camera access blocked. Please go into your site settings and allow access.');
          break;
          case 'TrackStartError':
          case 'NotReadableError':
          alert('Direct webcam access is not allowed. Please try with "Upload Answer" option below.');
          break;
        }
      }    
    });
  }
}

export function convertToCSV(objArray) {
  var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
  var str = '';
  
  for (var i = 0; i < array.length; i++) {
    var line = '';
    for (var index in array[i]) {
      if (line != '') line += ','
      
      line += array[i][index];
    }
    
    str += line + '\r\n';
  }
  
  return str;
}

export function exportCSVFile(headers, items, fileTitle) {
  if (headers) {
    items.unshift(headers);
  }
  
  // Convert Object to JSON
  var jsonObject = JSON.stringify(items);
  
  var csv = convertToCSV(jsonObject);
  
  var exportedFilenmae = fileTitle + '.csv' || 'export.csv';
  
  var blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
  if (navigator.msSaveBlob) { // IE 10+
    navigator.msSaveBlob(blob, exportedFilenmae);
  } else {
    var link = document.createElement("a");
    if (link.download !== undefined) { // feature detection
      // Browsers that support HTML5 download attribute
      var url = URL.createObjectURL(blob);
      link.setAttribute("href", url);
      link.setAttribute("download", exportedFilenmae);
      link.style.visibility = 'hidden';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }
}

export function isNumberKey(key) {
  var keys = [8, 9, 13, 16, 17, 18, 19, 20, 27, 46, 48, 49, 50,
    51, 52, 53, 54, 55, 56, 57, 91, 92, 93
  ];
  if (key && keys.indexOf(key) === -1)
  return false;
  else
  return true;
}

export function isHover(e) {
  return (e.parentElement.querySelector(':hover') === e);
}

export function isSafari() {
  return navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
  navigator.userAgent && !navigator.userAgent.match('CriOS');
}

export function isIOS() {
  return /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
}

export function isAppleDevice() {
  return isSafari() || isIOS();
}

// pass in the blob for a video file and get back a blob for the video's thumbnail
export function generateThumbnailBlobFromFileBlob(blob, callback) {
  var url = URL.createObjectURL(blob);
  var video = document.createElement('video');
  var timeupdate = function() {
    if (snapImage()) {
      video.removeEventListener('timeupdate', timeupdate);
      video.pause();
    }
  };
  video.addEventListener('loadeddata', function() {
    if (snapImage()) {
      video.removeEventListener('timeupdate', timeupdate);
    }
  });
  // draw the video onto the canvas and capture a frame to use as a thumbnail 
  var snapImage = function() {
    var canvas = document.createElement('canvas');
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
    canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
    canvas.toBlob(function(thumbnailBlob) {
      if(thumbnailBlob === null) {
        console.log("Error generating the video's thumbnail");
      }
      URL.revokeObjectURL(url);
      callback(thumbnailBlob);
    });
    return true;
  };
  
  video.addEventListener('timeupdate', timeupdate);
  video.preload = 'metadata';
  video.src = url;
  // Load video in Safari / IE11
  video.muted = true;
  video.playsInline = true;
  video.play();
}