import React, { Component } from 'react';
import { Button } from 'semantic-ui-react';
import playButton from './static/img/button_play.png';

class Webcam extends Component {
  constructor(props){
    super(props);
    this.state = {
      isMuted: true
    }
  }
  
  componentDidMount() {
    if(this.props.isRecordingPreview && !this.props.isAppleDevice) {
      this.setState({isMuted: false});
    }
  }
  
  toggleMute() {
    this.setState({isMuted: !this.state.isMuted});
  }
  
  togglePlay() {
    var video = document.getElementById('studyWebcamVideo');
    if(video.paused) {
      var playPromise = video.play();
      if (playPromise !== undefined) {
        playPromise.then(function() {
          this.setState({isPlaying: true});
        }.bind(this));
      }
    } else {
      video.pause();
      this.setState({isPlaying: false});
    }
  }
  
  render() {
    const componentStyles = {
      webcamContainer: {
        textAlign: 'center',
        position: 'relative'
      },
      webcamVideo: {
        width: '50%',
        height: '70vh',
        maxHeight: 'inherit',
        paddingBottom: '100px',
        display: 'inline-block',
        objectFit: 'cover'
      },
      disclaimerContainer: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        background: '#F4F7F9',
        marginRight: '100px'
      },
      disclaimerText: {
        color: '#9B9B9B',
        width: '100%'
      },
      muteButton: {
        position: 'absolute',
        right: '2px',
        top: '-50px',
        marginBottom: '20px',
        marginRight: '400px'
      }
    }
    const src = (this.props.isRecordingPreview) ? (window.URL || window.webkitURL).createObjectURL(this.props.previewSrc.blob) : this.props.src;
    
    return (
      <div style={componentStyles.webcamContainer}>
        {src === undefined &&
          
            <span className="center-div" style={componentStyles.disclaimerText}>
              {'We will access your camera after you click "Please Give Your Opinion"'}
            </span>
          
        }
        {!this.props.isUploading &&
          <video
            id="studyWebcamVideo"
            style={componentStyles.webcamVideo}
            loop
            autoPlay={(!this.props.isRecordingPreview || this.state.isPlaying) ? true : false}
            playsInline
            muted={this.state.isMuted}
            src={src} />
        }
        {this.props.isRecordingPreview &&
          <Button style={componentStyles.muteButton} icon={(this.state.isMuted) ? 'volume off' : 'volume up'} onClick={() => this.toggleMute()} />
        }
        {this.props.isRecordingPreview && !this.props.isPlaying &&
          <Button className="center-div" icon={(this.state.isPlaying) ? 'pause' : 'play'} onClick={() => this.togglePlay()} />
        }
      </div>
    )
  }
}

export default Webcam;